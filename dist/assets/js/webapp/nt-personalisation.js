/**
 * @author Nik Cross
 */
define("NtPersonalisation", ['jquery'], function ($) {
    "use strict";

    return new function () {
        var self = this;

        var loginResourceHandler = null;
        var personalisedResourceErrorHandler = null;
        var userProfileRequested = false;
        var savedPlaces = null;
        var lastClickedPlaceId = null;

        self.getPersonalisedResource = function (resourceUrl, resourceHandler, csrf, noLogin) {

            $.ajax({
                url: resourceUrl,
                beforeSend: function (request) {
                    if (csrf) {
                        request.setRequestHeader(csrf.header, csrf.token);
                    }
                    request.setRequestHeader('Accept', 'text/html');
                },
                type: 'GET',
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true,
                processData: false,
                success: function (data, textStatus, jqXHR) {
                    routePersonalisedResourceResponse(data, textStatus, jqXHR, resourceHandler, noLogin);
                },
                error: function (data) {
                    processPersonalisedResourceError(data);
                }
            });
        }

        self.postLoginForm = function (resourceUrl, csrf, data) {
            $.ajax({
                url: resourceUrl,
                type: 'POST',
                xhrFields: {
                    withCredentials: true
                },
                beforeSend: function (xhr) {
                    xhr.setRequestHeader(csrf.header, csrf.token);
                    xhr.setRequestHeader('Accept', 'text/html');
                },
                crossDomain: true,
                processData: false,
                data: data,
                success: function (data, textStatus, jqXHR) {
                    //Get account details from http header
                    var accountRef = jqXHR.getResponseHeader("myNTAccountRef");
                    if (accountRef !== null) {
                        globalDataLayer.myNTAccountType = jqXHR.getResponseHeader("myNTAccountType");
                        globalDataLayer.myNTAccountRef = jqXHR.getResponseHeader("myNTAccountRef");
                        globalDataLayer.myNTAccountSignInSuccessOverlay = "True";
                        //Inform analytics that user has logged in
                        $(document).trigger("logInTrigger", globalDataLayer);
                    }

                    routePersonalisedResourceResponse(data, textStatus, jqXHR, loginResourceHandler);
                },
                error: function (data) {
                    processPersonalisedResourceError(data);
                }

            });
        }

        self.handleSavePlace = function (elementForResponse, data) {
            elementForResponse.html(data);
            $('html, body').css({"overflow-y": ""});
            $("#modal-login").hide();
            $("#modal-login").removeClass("active");
        }

        self.showFavouriteDropDown = function (elementForResponse) {
            if (!elementForResponse.hasClass('f-open-dropdown')) {
                Foundation.libs.dropdown.open(elementForResponse, $('[data-toggle=' + elementForResponse.attr("id") + ']'));
            }
        }

        self.openFavouritesHelp = function () {
            $.get( "/personalisation-request/first-saved-place-modal.html", function(data) {
                $('body').append(data);
                $(".nt-main-navigation-container").removeClass("active").hide();
                $("#modal-favourites").addClass("active").show();
                $('html, body').css({"overflow-y": "hidden"});
            });

            return false;
        }

        self.closeFavouritesHelp = function () {
            $('html, body').css({"overflow-y": ""});
            $("#modal-favourites").hide();
            $("#modal-favourites").removeClass("active");
            return false;
        };

        var routePersonalisedResourceResponse = function (data, textStatus, jqXHR, resourceHandler, noLogin) {
            var loginFormType = jqXHR.getResponseHeader("NT-NTAuth-Login-Type");

            if (loginFormType == "FORM_ONLY") {
                //Don't display login if this var is true
                if (noLogin) return;
                $('#protected-resource-login').html(data);
                //Make login resource handler available for postLoginForm
                loginResourceHandler = resourceHandler;

                $(".nt-main-navigation-container").removeClass("active").hide();
                $(".nt-holiday-secondary-nav-container").removeClass("active").hide();
                $("#modal-login").addClass("active").show();
                $('html, body').css({"overflow-y": "hidden"});
            } else {
                if (!loginHandled()){
                    self.closeLoginModal();
                    $("body").trigger("login-success");
                }
                resourceHandler(data);
            }
        };

        var processPersonalisedResourceError = function (data) {
            if (personalisedResourceErrorHandler != null) {
                personalisedResourceErrorHandler(data);
            }
        }

        var loginHandled = function(){
            return $("#top-nav-profile, #hamburger-nav-profile").length > 0;
        }

        self.getUserProfile = function (force, noLogin) {
            if (!force && userProfileRequested) return;
            userProfileRequested = true;
            self.getPersonalisedResource(
                '/personalisation-request/user/profile-menu',
                function (data) {

                    var topNavProfile = $(data).filter('#top-nav-profile');
                    var hamburgerNavProfile = $(data).filter('#hamburger-nav-profile');

                    $('#sign-in-register-btn-TopNavigation').replaceWith(topNavProfile);
                    $('li.c-profile--responsive-menu').replaceWith(hamburgerNavProfile);

                    $(document).foundation('dropdown', 'reflow');
                    $("body").trigger("get-user-profile-success");
                },
                nt_csrf,
                noLogin);
        }

        //Required by login form
        self.closeLoginModal = function () {
            $('html, body').css({"overflow-y": ""});
            $("#modal-login").hide().removeClass("active");
            return false;
        };

        $("#close-button-protected-resource").on('click', function() {
            $("body").unbind("get-user-profile-success.show-profile-menu");
            self.closeLoginModal();
        });

        $('#sign-in-register-btn-TopNavigation').on('click', function () {
            $("body").on("get-user-profile-success.show-profile-menu", function() {
                Foundation.libs.dropdown.open($('#c-profile-menu'), $('[data-toggle=c-profile-menu]'));
            });
        });

        $(".sign-in-register-btn").on('click', function () {
            self.getUserProfile(true, false);
            return false;
        });

        $("body").on("login-success", function (){
            if (!loginHandled()){
                self.getUserProfile();
            }
        });

		self.handlePlaceButtonClick = function () {
			lastClickedPlaceId = $(this).data('placeId');
			var cookieDomain = nt_cookieDomain;
			var elementForResponse = $(this).next('ul.my-places-actions');

			if (typeof(nt_hasNtAuthSession) == "undefined") {
			  //User is not logged in and may be about to register
			  var expireDate = new Date();
			  //Expires after 3 days to exceed registration email expire date by one day
			  // just in case registration process takes a while
			  expireDate.setDate(expireDate.getDate() + 3);
			  document.cookie = 'savePlaceWaiting=' + lastClickedPlaceId +
				  ";path=/; expires=" + expireDate.toUTCString() +
				  "; domain=" + cookieDomain;
			}

			self.getPersonalisedResource(
			  '/personalisation-request/user/my-places/place/' + lastClickedPlaceId + '/save',
			  function (data) {
				  self.handleSavePlace(elementForResponse, data);
				  self.showFavouriteDropDown(elementForResponse);
			  },
			  nt_csrf
			);
	    };

		self.handleMyPlaceActionsClick = function () {
			var listAction = $(this).data("visit-status-action");
			nt.NtPersonalisation.getPersonalisedResource(
				'/personalisation-request/user/my-places/place/' + lastClickedPlaceId + '/save?listAction=' + listAction,
				function (data) {
					self.handleSavePlace($(this).parents('ul.my-places-actions'), data);
					if (listAction === "REMOVE_FROM_LIST") {
						self.nt_showFavourite(false);
					} else {
						if (self.getShowFavouritesHelp() != false) {
							self.openFavouritesHelp();
						}

						self.nt_showFavourite(true);
					}
				},
				nt_csrf
			);
		};

        $("body").on('click', ".save-place-btn", self.handlePlaceButtonClick);
		$("body").on('click', '.my-places-actions a[data-visit-status-action]', self.handleMyPlaceActionsClick);

        self.nt_showFavourite = function (isFavourite, placeButton) {
			var placeButton = placeButton ? placeButton : $(".save-place-btn[data-place-id=" + lastClickedPlaceId + "]");
			if (isFavourite) {
				savedPlaces.push(placeButton.data("place-id"));
			} else {
				var index = savedPlaces.indexOf(placeButton.data("place-id"));
				if (index > -1) {
    				savedPlaces.splice(index, 1);
				}
			}

			if (placeButton.hasClass('multi-save-place-btn')) {
				if (isFavourite) {
                	placeButton.html('<span class="icon saved" data-icon="&#xe919;">');
				} else {
                	placeButton.html('<span class="icon unsaved" data-icon="&#xe91a;">');
				}
			} else {
				if (isFavourite) {
					placeButton.html("<span class=\"icon\" data-icon=\"&#xe919;\"></span>Saved to My places");
				} else {
					placeButton.html("<span class=\"icon\" data-icon=\"&#xe91a;\"></span>Save to My places");
				}
			}
        }


        self.loadSavedPlaces = function(forceReload, callback) {
        	if (!savedPlaces || forceReload) {
        		$.ajax({
					url: '/personalisation-request/my-places/',
					success: function(data) {
						savedPlaces = data;
						callback();
					}
				});
			} else {
				callback();
			}
        }

		self.decorateFavouriteButtons = function(forceReload) {
			if ($('.save-place-btn').length) {
				self.loadSavedPlaces(forceReload, function() {
					$.each(savedPlaces, function(index, placeId) {
						self.decorateFavouriteButton(placeId);
					});
				});
			}
		}

		self.decorateFavouriteButton = function(placeId, forceReload) {
			self.loadSavedPlaces(forceReload, function() {
				if (savedPlaces && $.inArray(placeId, savedPlaces) !== -1) {
					self.nt_showFavourite(true, $(".save-place-btn[data-place-id=" + placeId + "]"));
				}
			});
		}

        $("body").on("login-success", function() {
       		self.decorateFavouriteButtons(true);
        });

        $("body").on('click', "#close-button-favourites", self.closeFavouritesHelp);

        $("body").on('click', '#favourites-dont-show-again', function () {
            self.setShowFavouritesHelp(false);
            self.closeFavouritesHelp();
        });

        //If there is a session id then request the user profile
        // but if not logged in, do not prompt to login
        if (typeof(nt_hasNtAuthSession) !== "undefined" && !loginHandled()) {
            self.getUserProfile(true, true);
        }

        self.getShowFavouritesHelp = function () {
            return document.cookie.match(/cookieShowFavouritesHelp=false/g) == null;
        };

        self.setShowFavouritesHelp = function (state) {
            document.cookie = 'cookieShowFavouritesHelp=' + state + '; path=/';
        }

        self.init = function() {
        	self.decorateFavouriteButtons();
        }

    };
});