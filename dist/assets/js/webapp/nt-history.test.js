'use strict';
var requirejs = require("requirejs");
require.main.require('develop/js/mocha-tests/common-mocha-test-header.js');
var should = require("should");

suite('Test NtHistory', function () {
    var NtHistory,
    	NtClientStorageMock;

    suiteSetup(function (done) {

        requirejs(['NtHistory', 'NtClientStorage'],
            function (mod, client) {
                NtHistory = mod;
				NtClientStorageMock = client;
                done();
            });
    });

	test('has expected functions', function () {
        NtTest.hasFunctions(
            NtHistory ,
            [
                {
                    "methodName": "writeHistoryToCookie",
                    "arguments": [
                        "cookieExpiry"
                    ]
                }
            ] ,
            {strict: true, exclude: []}
        );
	});

	suite('Test writeHistoryToCookie function', function() {
			var argsCapture;

		suiteSetup(function(done) {
			NtClientStorageMock.pushData = function() { argsCapture = {
				key: arguments[0],
				input: arguments[1],
				storageOption: arguments[2],
				rulesFunc: arguments[3],
				cookiePath: arguments[4],
				cookieExpiry: arguments[5]
			}}
			global.globalDataLayer = {
				taxonomy: true,
				propertyID: 'UniquePropertyId',
				cid: 'UniqueContentId'
			}
			done();
		});

		test('test PlaceHomePage adds history', function() {
			// Given
			global.globalDataLayer.contentType = 'PlaceHomePage';

			// When
			NtHistory.writeHistoryToCookie('cookieExpiry');

			// Then
			should(argsCapture.key).be.equal('pagesVisited');
			should(argsCapture.input).be.equal('UniquePropertyId');
			should(argsCapture.storageOption).be.equal(NtClientStorageMock.StorageOptionEnum.COOKIE);
			should(argsCapture.cookiePath).be.equal('/');
			should(argsCapture.cookieExpiry).be.equal('cookieExpiry');
			NtTest.hasFunction(argsCapture, 'rulesFunc', ['result']);
		});

		test('test ContentPage adds history', function() {
			// Given
			global.globalDataLayer.contentType = 'ContentPage';

			// When
			NtHistory.writeHistoryToCookie('cookieExpiry');

			// Then
			should(argsCapture.key).be.equal('pagesVisited');
			should(argsCapture.input).be.equal('UniqueContentId');
			should(argsCapture.storageOption).be.equal(NtClientStorageMock.StorageOptionEnum.COOKIE);
			should(argsCapture.cookiePath).be.equal('/');
			should(argsCapture.cookieExpiry).be.equal('cookieExpiry');
			NtTest.hasFunction(argsCapture, 'rulesFunc', ['result']);
		});


		suite('Test removeOldestWhenLengthOverLimit function', function() {
			var removeOldestWhenLengthOverLimit;

			suiteSetup(function(done) {
				NtHistory.writeHistoryToCookie('cookieExpiry');
				removeOldestWhenLengthOverLimit = argsCapture.rulesFunc;
				done();
			});

			test('test array at record size limit remains unchanged', function() {
				// Given
				var initial = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];

				// When
				var result = removeOldestWhenLengthOverLimit(initial);

				// Then
				should(result).have.size(20);
				should(result).be.eql([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]);
			});

			test('test oldest record is removed when the array is over limit.', function() {
				// Given
				var initial = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21];

				// When
				var result = removeOldestWhenLengthOverLimit(initial);

				// Then
				should(result).have.size(20);
				should(result).be.eql([2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21]);
			});

			test('test very large array has old history removed', function() {
				// Given
				var initial = [],
					oldLimit = 100;
				for (var i = 0; i <= oldLimit; i++) {
					initial.push(i);
				}

				// When
				var result = removeOldestWhenLengthOverLimit(initial);

				// Then
				should(result).have.size(20);
				should(result).be.eql([81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100]);
			});
		});
	});
});