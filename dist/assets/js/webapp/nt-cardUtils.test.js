'use strict';
var requirejs = require("requirejs");
require.main.require('develop/js/mocha-tests/common-mocha-test-header.js');

suite('Test ntCardUtils', function () {
    var ntCardUtils;

    suiteSetup(function (done) {
        ntCardUtils = NtTest.loadScript('develop/js/webapp/nt-cardUtils.js', 'ntCardUtils');
        done();
    });

    suite('ntCardUtils', function () {
        test('has expected functions', function () {
            NtTest.hasFunctions(
                ntCardUtils ,
                [
                    {
                        "methodName": "generateUrlWithCardCount",
                        "arguments": [
                            "cardsToShow"
                        ]
                    },
                    {
                        "methodName": "registerCardEventHandlers",
                        "arguments": [
                            "cardIdPrefix",
                            "cardCount",
                            "cardIncrement"
                        ]
                    }
                ] ,
                {strict: true, exclude: []}
            );
        });
    });
});