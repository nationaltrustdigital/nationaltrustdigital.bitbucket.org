/**
 * @author Samuel Blackwell
 */

"use strict";

var ntCardUtils = {};

ntCardUtils.generateUrlWithCardCount = function (cardsToShow) {
    var namedParams;
    var isCardCountExists = false;
    var path = window.location.pathname;

    if (window.location.search.length > 0) {
        namedParams = window.location.search.replace('?', '').split("&");
    }
    else {
        namedParams = [];
    }

    for (var i = 0; i < namedParams.length; i++) {
        if (namedParams[i].indexOf("cardCount") >= 0) {
            namedParams[i] = "cardCount=" + cardsToShow;
            isCardCountExists = true;
            break;
        }
    }

    if (!isCardCountExists) {
        namedParams[namedParams.length] = "cardCount=" + cardsToShow;
    }

    console.log(path, "path");
    console.log(namedParams, "namedParams");
    return window.location.pathname + "?" + (namedParams.join("&"));

};

ntCardUtils.registerCardEventHandlers = function (cardIdPrefix, cardCount, cardIncrement) {
    var bottomOfListCursor = cardCount + 1;
    var ntSeeMore = $("#nt-see-more");
    var ntScrollToCard = $('#nt-scroll-to-card');

    ntSeeMore.on('click', function (event) {
        event.preventDefault();
        for (var i = 0; i < cardIncrement; i++) {

            $(cardIdPrefix + (i + bottomOfListCursor)).removeClass('hidden');
            var imageElement = $(cardIdPrefix + (i + bottomOfListCursor) + "-img");
            imageElement.attr('src', imageElement.data('src'));

            if ($(cardIdPrefix + (i + bottomOfListCursor + 1)).length === 0) {
                ntSeeMore.parent().addClass("hidden");
                break;
            }
        }
        bottomOfListCursor += i;
        ntScrollToCard.click();
        ntScrollToCard.attr('href', cardIdPrefix + bottomOfListCursor);

        var cardsToShow = $('.nt-masonry-single-result:not(.hidden)').length;

        if (history.replaceState) {
            history.replaceState(null, "", ntCardUtils.generateUrlWithCardCount(cardsToShow));
        }

        $(this).trigger('initialise-carousel');
    });
};


