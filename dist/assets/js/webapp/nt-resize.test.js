'use strict';
var requirejs = require("requirejs");
require.main.require('develop/js/mocha-tests/common-mocha-test-header.js');
var should = require("should");

suite('Test nt_resize', function () {
    var nt_loadLibraries;

    suiteSetup(function (done) {
        global.Foundation = {};
        global.nt_loadLibraries = NtTest.loadScript('develop/js/webapp/nt-load-libraries.js', 'nt_loadLibraries');
        NtTest.loadScript('develop/js/webapp/nt-resize.js');
        done();
    });

    suite('nt_resize', function () {

        test('has expected functions', function () {
            NtTest.hasFunction($.fn, 'nt_resize', ['params']);
        });
    });
});