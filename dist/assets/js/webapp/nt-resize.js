nt_loadLibraries([{path: "jQuery"}, {path: "Foundation"}], function() {		
	(function($) {
		$.fn.nt_resize = function(params) {
			return this.each(function() {
				var f = buildEventHandler(params);
				$(this).on('resize.nt-resize', f);
			});
		};
		function buildEventHandler(params) {
			var resizeTrigger = function() {
				jQuery(window).trigger('nt-resize');
			};
			if (params.throttleDelay) {
				var resizeTrigger = Foundation.utils.throttle(resizeTrigger, params.throttleDelay);
			}
			
			if (params.endDelay) {
				var resizeEndTrigger = function() {
					jQuery(window).trigger('nt-resize-end');
				};				
				var resizeEndTrigger = Foundation.utils.debounce(resizeEndTrigger, params.endDelay);

				var f = function(event) {
					resizeTrigger();
					resizeEndTrigger();
				}				
			} else {
				var f = function(event) {
					resizeTrigger();
				}
			}
			return f;
		}
	}(jQuery));
});
