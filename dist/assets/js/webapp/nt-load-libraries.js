/*
 * Function to dynamically load an array of libraries, then wait for them all to load and invoke the passed function.
 * e.g. 
 * nt_loadLibraries([
 * 		{src: "/assets/js/webapp/search/infobox-1.1.13.js", path: "InfoBox"},
 * 		{path: "jQuery.fn.nt_resize"}],
 * 		function() {alert("Do stuff after the load");});
 * 
 * src:		 optional property for source of library - if present the library is loaded.
 * path:	 optional string path of object to check that library is loaded. This can contain nested dot separated objects.
 * 			 The above example "jQuery.fn.nt_resize" checks that jQuery plugin "nt_resize" has been registered as well as jQuery being loaded.
 *           The path, if present, is checked for prior to script load to avoid double loading.
 * function: optional callback to be performed after all libraries are loaded.
 * 
 * To just protect against double loading, pass a path but not a function.
 * To check for double loading and perform a callback action, pass a path and function.
 * You can still perform a callback action where some libraries do not specify a path - no path check is done for these so success is assumed.
 */
function nt_loadLibraries(libs, postLoadAction) {

	loadScripts(libs);
	if (postLoadAction) {
		whenLoaded(libs, postLoadAction);		
	}
	
	function loadScripts(libs) {
		for (var i=0; i<libs.length; i++) {
			var lib = libs[i];
			if (lib.src && (!lib.path || !pathExists(lib.path))) {
				loadScript(lib.src);
			}
		}
	}
	
	function whenLoaded(libs, postLoadAction) {

		var delay = 200; //retry delay in milliseconds

		for (var i=0;i<libs.length ;i++) {
			var lib = libs[i];
			if (lib.path && !pathExists(lib.path)) {
				setTimeout(function() {whenLoaded(libs, postLoadAction);}, delay);
				return;
			}		
		}

		postLoadAction();
	}
		
	function loadScript(src) {
		var script = document.createElement("script");
		script.setAttribute("src", src);
		script.setAttribute("type", "text/javascript");
		document.getElementsByTagName("head")[0].appendChild(script);
	}
  
  function pathExists(path) {
    try{
      if( typeof(eval(path))!=="undefined" ) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }
}