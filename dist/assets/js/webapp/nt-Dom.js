"use strict";

/**
 * Useful dom manipulation functions using js
 * Avoid JQuery additions to this library to preserve performance
 *
 * @author Sam blackwell
 */

define("NtDom", [], function () {

    var addElementWithId = function (ele, id) {
        var newDiv = document.createElement(ele);
        newDiv.id = id;
        document.body.appendChild(newDiv);
    };

    var addElementWithIdIfIdDoesNotExist = function (ele, id) {
        if (!document.getElementById(id)) {
            addElementWithId(ele, id);
        }
    };

    var checkForExistenceOfId = function (id) {
        //get element by id returns null if it cannot find an id
        return (document.getElementById(id) !== null);
    };

    return new function () {
        this.checkForExistenceOfId = checkForExistenceOfId;
        this.addElementWithIdIfIdDoesNotExist = addElementWithIdIfIdDoesNotExist;
    };
});