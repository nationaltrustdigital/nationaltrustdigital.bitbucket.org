/**
 * @author Nik Cross
 */
define(['jquery'],
    function ($) {
        "use strict";

        $.getParameterByName = function (name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            if (results === null) return;
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        };

        $.getURLParameters = function (url) {
            var urlPrefix = url;
            var param = [];

            if (url.indexOf("?") != -1) {
                var urlPrefix = url.substring(0, url.indexOf("?"));
                var params = url.substring(url.indexOf("?") + 1).split("&");
                var param = [];
                for (var i = 0; i < params.length; i++) {
                    var parts = params[i].split("=");
                    if (param[parts[0]]) {
                        if (typeof param[parts[0]] != "object") {
                            param[parts[0]] = [param[parts[0]]];
                        }
                        param[parts[0]].push(parts[1]);
                    } else {
                        param[parts[0]] = parts[1];
                    }
                }
            }
            return param;
        }

        $.setURLParameters = function (url, param) {
            var urlPrefix = url;
            if (url.indexOf("?") != -1) {
                var urlPrefix = url.substring(0, url.indexOf("?"));
            }
            var newUrl = "";
            for (var key in param) {
                if (newUrl.length === 0) {
                    newUrl += "?";
                } else {
                    newUrl += "&";
                }
                if (typeof param[key] === "object") {
                    for (var i = 0; i < param[key].length; i++) {
                        if (i > 0) newUrl += "&";
                        newUrl += key + "=" + param[key][i];
                    }
                } else {
                    newUrl += key + "=" + param[key];
                }
            }
            return urlPrefix + newUrl;
        }
    });
