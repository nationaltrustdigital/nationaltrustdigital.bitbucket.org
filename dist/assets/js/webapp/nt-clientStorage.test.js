'use strict';
var requirejs = require("requirejs");
require.main.require('develop/js/mocha-tests/common-mocha-test-header.js');
var should = require("should");

suite('Test NtClientStorage', function () {
    var NtClientStorage;

    suiteSetup(function (done) {

        requirejs(['NtClientStorage'],
            function (mod) {
                NtClientStorage = mod;
                done();
            });
    });

    suite('NtClientStorage', function () {

        test('has expected functions', function () {
            NtTest.hasFunctions(
                NtClientStorage ,
                [
                    {
                        "methodName": "pushData",
                        "arguments": [
                            "key",
                            "input",
                            "storageOption",
                            "rulesFunc",
                            "cookiePath",
                            "cookieExpiry"
                        ]
                    },
                    {
                        "methodName": "deleteData",
                        "arguments": [
                            "key",
                            "storageOption"
                        ]
                    },
                    {
                        "methodName": "getData",
                        "arguments": [
                            "key",
                            "storageOption"
                        ]
                    },
                    {
                        "methodName": "replaceData",
                        "arguments": [
                            "input",
                            "storageOption",
                            "cookiePath",
                            "cookieExpiry"
                        ]
                    }
                ] ,
                {strict: true, exclude: ["StorageOptionEnum"]}
            );
        });
    });
});