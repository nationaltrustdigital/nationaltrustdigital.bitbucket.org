'use strict';
var requirejs = require("requirejs");
require.main.require('develop/js/mocha-tests/common-mocha-test-header.js');
var should = require("should");

suite('Test NtLatLngCapture', function () {
    var NtLatLngCapture;

    suiteSetup(function (done) {
        requirejs("navigator");

        requirejs(['NtLatLngCapture'],
            function (mod) {
                NtLatLngCapture = mod;
                done();
            });
    });

    suite('NtLatLngCapture', function () {

        test('has expected functions', function () {
            NtTest.hasFunctions(
                NtLatLngCapture ,
                [
                    {
                        "methodName": "capture",
                        "arguments": [
                            "doFunc"
                        ]
                    }
                ] ,
                {strict: true, exclude: []}
            );
        });

        test('passes geolocation to supplied function', function () {
            //Given
            var latitude = 1;
            var longitude = 2;
            navigator.mock.setLocation(latitude, longitude);

            var result;
            var callBack = function (location) {
                result = location;
            }
            //When
            NtLatLngCapture.capture(callBack);

            //Then
            should(result.latitude).be.equal(1);
            should(result.longitude).be.equal(2);
        });


        test('uses found geolocation when capture called a second time', function () {
            //Given
            var latitude = 1;
            var longitude = 2;
            navigator.mock.setLocation(latitude, longitude);

            var result;
            var callBack = function (location) {
                result = location;
            }

            NtLatLngCapture.capture(callBack);
            navigator.mock.setLocation(2, 3);

            //When
            NtLatLngCapture.capture(callBack);

            //Then
            should(result.latitude).be.equal(1);
            should(result.longitude).be.equal(2);
        });
    });
});