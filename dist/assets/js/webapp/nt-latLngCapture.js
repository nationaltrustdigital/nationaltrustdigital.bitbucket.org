// "use strict";

/**
 * Library to capture a users location for personalised content, encapsulates the business rules for when
 * a request for location should be made
 *
 * @author Sam blackwell
 */

define("NtLatLngCapture", [], function () {

    if (typeof(currentPosition) == "undefined") currentPosition = null;

    //empty constructor to attach functions to prototype
    function NtLocationCapture() {

    }

    var checkForGeoLocationService = function () {
        var isAvailable = false;
        if ("geolocation" in navigator) {
            isAvailable = true;
        }
        return isAvailable;
    };

    var capture = function (doFunc) {

        if (currentPosition != null) {
            doFunc(currentPosition);
            return;
        }

        if (checkForGeoLocationService()) {
            navigator.geolocation.getCurrentPosition(
                function (position) {
                    var geolocation = {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude
                    };

                    currentPosition = geolocation;

                    doFunc(geolocation);
                },
                function (error) {
                },
                {
                    enableHighAccuracy: false
                }
            );
        }
    };

    return new function () {
        this.capture = capture;
    };

});