'use strict';
var requirejs = require("requirejs");
require.main.require('develop/js/mocha-tests/common-mocha-test-header.js');
var should = require("should");

suite('Test NtDom', function () {
    var ntDom;

    suiteSetup(function (done) {
        requirejs("window");

        requirejs(['NtDom'],
            function (mod) {
                ntDom = mod;
                done();
            });
    });

    suite('NtDom', function () {

        test('has expected functions', function () {
            NtTest.hasFunctions(
                ntDom ,
                [
                    {
                        "methodName": "checkForExistenceOfId",
                        "arguments": [
                            "id"
                        ]
                    },
                    {
                        "methodName": "addElementWithIdIfIdDoesNotExist",
                        "arguments": [
                            "ele",
                            "id"
                        ]
                    }
                ] ,
                {strict: false}
            );
        });

        test('checkForExistenceOfId returns false when id not in dom', function () {
            //Given
            var id = "testId";

            //When
            var result = ntDom.checkForExistenceOfId('testId');

            //Then
            should(result).be.false();
        });

        test('checkForExistenceOfId returns true when id in dom', function () {
            //Given
            var id = "testId";

            //When
            var result = ntDom.checkForExistenceOfId('A');

            //Then
            should(result).be.true();
        })
    });
});