'use strict';
var requirejs = require("requirejs");
require.main.require('develop/js/mocha-tests/common-mocha-test-header.js');
var should = require("should");

suite('Test NtPersonalisation', function () {
    var NtPersonalisation;

    suiteSetup(function (done) {
        requirejs("window");

        requirejs(['NtPersonalisation'],
            function (mod) {
                NtPersonalisation = mod;
                done();
            });
    });

    suite('NtPersonalisation', function () {

        test('has expected functions', function () {
            NtTest.hasFunctions(
                NtPersonalisation ,
                [
                    {
                        "methodName": "getPersonalisedResource",
                        "arguments": [
                            "resourceUrl",
                            "resourceHandler",
                            "csrf",
                            "noLogin"
                        ]
                    },
                    {
                        "methodName": "postLoginForm",
                        "arguments": [
                            "resourceUrl",
                            "csrf",
                            "data"
                        ]
                    },
                    {
                        "methodName": "getUserProfile",
                        "arguments": [
                            "force",
                            "noLogin"
                        ]
                    },
                    {
                        "methodName": "closeLoginModal",
                        "arguments": []
                    },
                    {
                        "methodName": "getShowFavouritesHelp",
                        "arguments": []
                    },
                    {
                        "methodName": "setShowFavouritesHelp",
                        "arguments": [
                            "state"
                        ]
                    },
                    {
                        "methodName": "handleSavePlace",
                        "arguments": [
                            "elementForResponse",
                            "data"
                        ]
                    },
                    {
                        "methodName": "showFavouriteDropDown",
                        "arguments": ["elementForResponse"]
                    },
                    {
                        "methodName": "openFavouritesHelp",
                        "arguments": []
                    },
                    {
                        "methodName": "closeFavouritesHelp",
                        "arguments": []
                    },
                    {
                        "methodName": "loadSavedPlaces",
                        "arguments": ["forceReload", "callback"]
                    },
                    {
                        "methodName": "decorateFavouriteButtons",
                        "arguments": ["forceReload"]
                    },
                    {
                        "methodName": "decorateFavouriteButton",
                        "arguments": ["placeId", "forceReload"]
                    },
                    {
                        "methodName": "nt_showFavourite",
                        "arguments": ["isFavourite", "placeButton"]
                    },
                    {
                        "methodName": "handleMyPlaceActionsClick",
                        "arguments": []
                    },
                    {
                        "methodName": "handlePlaceButtonClick",
                        "arguments": []
                    },
                    {
                        "methodName": "init",
                        "arguments": []
                    }
                ] ,
                {strict: true, exclude: []}
            );
        });

        test('shows favourites help on first visit', function () {
            //Given
            document.cookie = "cookieA=123;cookieB=345;";

            //When
            var result = NtPersonalisation.getShowFavouritesHelp();

            //Then
            should(result).be.equal(true);
        });



        test('does not show help on second visit', function () {
            //Given
            document.cookie = "cookieA=123;cookieShowFavouritesHelp=false;cookieB=345;";

            //When
            var result = NtPersonalisation.getShowFavouritesHelp();

            //Then
            should(result).be.equal(false);
        });

        test('loads login form', function () {
            //Given
            var args = getResourceArguments();

            NtPersonalisation.getPersonalisedResource(args.resourceUrl, args.resourceHandler, args.csrf, args.noLogin);
            var ajaxRequest = jquery.mock.ajaxRequest;

            var data = "some content";
            var textStatus = "textStatus";
            var jqXHR = {
                getResponseHeader: function (key) {
                    if (key === "NT-NTAuth-Login-Type") return "FORM_ONLY";
                }
            }

            var htmlData;
            jquery.mock.addInternal("html", function (newData) {
                htmlData = newData;
            });

            var removedClassName;
            var hideCalled = false;
            jquery.mock.addInternal("removeClass", function (newClassName) {
                removedClassName = newClassName;

                return {
                    hide: function () {
                        hideCalled = true;
                    }
                }
            });

            var cssAttribute;
            jquery.mock.addInternal("css", function (attribute) {
                cssAttribute = attribute;
            });

            var addedClassName;
            var showCalled = false;
            jquery.mock.addInternal("addClass", function (newClassName) {
                addedClassName = newClassName;
                return {
                    show: function () {
                        showCalled = true;
                    }
                }
            });

            //When
            ajaxRequest.success(data, textStatus, jqXHR);

            //Then
            should(htmlData).be.equal(data);
            should(removedClassName).be.equal("active");
            should(hideCalled).be.true()
            should(addedClassName).be.equal("active");
            should(showCalled).be.true();
            should(cssAttribute["overflow-y"]).be.equal("hidden");
        });

        test('does not load login form if noLogin set true', function () {
            //Given
            var args = getResourceArguments();
            args.noLogin = true;

            NtPersonalisation.getPersonalisedResource(args.resourceUrl, args.resourceahandler, args.csrf, args.noLogin);
            var ajaxRequest = jquery.mock.ajaxRequest;

            var data = "some content";
            var textStatus = "textStatus";
            var jqXHR = {
                getResponseHeader: function (key) {
                    if (key === "NT-NTAuth-Login-Type") return "FORM_ONLY";
                }
            }

            var htmlNotCalled = true;
            jquery.mock.addInternal("html", function (newData) {
                htmlNotCalled = false;
            });

            //When
            ajaxRequest.success(data, textStatus, jqXHR);

            //Then
            should(htmlNotCalled).be.true();
        });

        test('personalised resource request constructed', function() {
            //Given
            var args = getResourceArguments();

            //When
            NtPersonalisation.getPersonalisedResource(args.resourceUrl, args.resourceaHandler, args.csrf, args.noLogin);
            var ajaxRequest = jquery.mock.ajaxRequest;

            //Then
            should(ajaxRequest.url).be.equal(args.resourceUrl);
            should(ajaxRequest.type).be.equal("GET");
            should(ajaxRequest.xhrFields.withCredentials).be.true();
            should(ajaxRequest.crossDomain).be.true();
            should(ajaxRequest.processData).be.false();
        });

        test('beforeSend sets requestHeader',function() {
            //Given
            var args = getResourceArguments();
            NtPersonalisation.getPersonalisedResource(args.resourceUrl, args.resourceHandler, args.csrf, args.noLogin);
            var ajaxRequest = jquery.mock.ajaxRequest;

            //When
            ajaxRequest.beforeSend(args.request);

            //Then
            should(args.requestHeader[args.csrf.header]).be.equal(args.csrf.token);
            should(args.requestHeader["Accept"]).be.equal("text/html");
        });

        test('loads resource', function () {
            //Given
            var args = getResourceArguments();
            NtPersonalisation.getPersonalisedResource(args.resourceUrl, args.resourceHandler, args.csrf, args.noLogin);
            var ajaxRequest = jquery.mock.ajaxRequest;

            var data = "some content";
            var textStatus = "textStatus";
            var jqXHR = {
                getResponseHeader: function (key) {
                    if (key === "NT-NTAuth-Login-Type") return "NOT FORM_ONLY";
                }
            }

            var triggerNames = [];
            $("anything").trigger = function (name, data) {
                triggerNames.push(name);
            }

            var removedClassName = null;
            jquery.mock.addInternal("removeClass", function (newClassName) {
                removedClassName = newClassName;
            });

            var hideCalled = false;
            jquery.mock.addInternal("hide", function () {
                hideCalled = true;
                return $(this);
            });

            var cssAttribute = null;
            jquery.mock.addInternal("css", function (attribute) {
                cssAttribute = attribute;
            });

            //When
            ajaxRequest.success(data, textStatus, jqXHR);

            //Then
            should(args.resourceData).be.equal(data);
            should(removedClassName).be.equal("active");
            should(hideCalled).be.true();

            should(cssAttribute["overflow-y"]).be.equal("");
            should(triggerNames).containEql("login-success");
        });

        test('sets globalDataLayer on login', function () {
            //Given
            global.globalDataLayer = {};
            var resourceUrl = "resourceUrl";
            var csrf = {header: "csrfHeader", token: "csrfToken"};
            var data = "html data";
            var textStatus = "success";
            var ajaxData = {};
            var jqXHR = {
                getResponseHeader: function (name) {
                    if (name === "myNTAccountRef") {
                        return "Ref:1234";
                    }
                    if (name === "myNTAccountType") {
                        return "Web Account";
                    }
                }
            };
            var triggerNames = [];
            var triggerDatas = [];

            $.ajax = function (theAjaxData) {
                ajaxData = theAjaxData;
            }
            $("anything").trigger = function (name, data) {
                triggerNames.push(name);
                triggerDatas.push(data);
            }

            //When
            NtPersonalisation.postLoginForm(resourceUrl, csrf, data);
            ajaxData.success(data, textStatus, jqXHR);

            //Then
            should(globalDataLayer.myNTAccountRef).be.equal("Ref:1234");
            should(globalDataLayer.myNTAccountType).be.equal("Web Account");
            should(globalDataLayer.myNTAccountSignInSuccessOverlay).be.equal("True");

            should(triggerNames).containEql("logInTrigger");
            should(triggerDatas).containEql(globalDataLayer);

            should(triggerNames).containEql("login-success");
        });
    });

    function getResourceArguments() {
        var args = {
            resourceUrl: "resourceUrl",
            resourceData: {},
            resourceHandler: function (resource) {},
            csrf: {header: "csrfHeader", token: "csrfToken"},
            noLogin: false,
            requestHeader: { csrfHeader: ""},
            request: {
                setRequestHeader: function (key, value) {
                    requestHeader[key] = value;
                }
            }
        };


        args.resourceHandler = function(resource) {
            args.resourceData = resource;
        };

        args.request.setRequestHeader = function (key, value) {
            args.requestHeader[key] = value;
        };

        return args;
    }
});