"use strict";


/**
 * @author Samuel Blackwell
 *
 * A small library to easily store data on the client side using the best available storage strategy
 * storage strategies can be disabled by removing the check for them in ntBrowserStorageLibrary.utils.configureStorageStrategy
 */
define("NtClientStorage", [], function () {
    var ntBrowserStorageLibrary = {};
    var ntStorageOptionEnum = {};


    var storageMode, storageSet;
    /**
     *
     * @type {{LOCAL_STORAGE: number, COOKIE: number, SESSION_STORAGE: number}}
     *
     * Supported client storage options that this library can use
     */
    var StorageOptionEnum = function () {
        var StorageOptionEnums = {
            LOCAL_STORAGE: 1,
            COOKIE: 2,
            SESSION_STORAGE: 3
        };
        ntStorageOptionEnum = (Object.freeze) ? Object.freeze(StorageOptionEnums) : StorageOptionEnums;
        return ntStorageOptionEnum;
    }();



    /**
     * Decides based on browser capabilities what client storage strategy to use, falls back to cookie
     *
     */
    var configureStorageStrategy = function (storageOption) {

        if (typeof storageOption !== "undefined") {
            storageMode = storageOption;
            return;
        }

        if (typeof(Storage) !== "undefined") {
            storageMode = StorageOptionEnum.LOCAL_STORAGE;
        } else {
            storageMode = StorageOptionEnum.COOKIE;
        }
        storageSet = true;

    };

    /**
     * Ensures there is a client storage strategy for this browser
     */
    var ensureStorageStrategy = function (storageOption) {
        if (!storageSet) {
            configureStorageStrategy(storageOption);
        }

    };

    /**
     *
     * @param input entity to be checked
     * @returns {boolean} true if the entity is both a valid and populated array
     */
    var isValidArrayAndPopulated = function (input) {
        var isValid = false;
        if ((typeof input !== "undefined")) {
            if (input !== null && input.constructor === Array && input.length > 0) {
                isValid = true;
            }
        }
        return isValid;
    };

    /**
     *
     * @param input the entity that will be turned into an array
     * @returns {Array} an array with position 0 being the entity
     */
    var produceArray = function (input) {

        var arrayOfKeyValues = [];
        //if it is not an array it is a single result so wrap it
        if (!isValidArrayAndPopulated(input)) {
            arrayOfKeyValues.push(input);
        } else {
            arrayOfKeyValues = input;
        }
        return arrayOfKeyValues;
    };

    /**
     *
     * @param result the client string which is expected to be an array if it is not it is converted to one
     * @returns {Array} an array, either a stringified array that has been parsed or an entity that has been pushed into an array
     */
    var getClientValueAsArray = function (result) {
        if (result) {
            result = JSON.parse(decodeURIComponent(result));
            if (!isNaN(result)) {
                result = String(result);
            }
        }
        if (!isValidArrayAndPopulated(result)) {
            result = produceArray(result);
        }
        return result;
    };

    /**
     *
     * @param arr an array
     * @param arr2 another array
     * @returns the result of merging arr and arr2
     */
    var mergeArrays = function (arr, arr2) {
        arr = cleanArray(arr);
        arr2 = cleanArray(arr2);
        return arr.concat(arr2);
    };

    /**
     *
     * @param arr the array to clean
     * @returns {Array} an array of truthy values
     */
    var cleanArray = function (arr) {
        var result = [];
        for (var i = 0; i < arr.length; i++) {
            if (arr[i]) {
                result.push(arr[i]);
            }
        }
        return result;
    };

    /**
     *
     * @param cookieName the name of the cookies you wish returned
     * @returns {*} the value of the cookie
     * @private Do not use this function directly outside of the this library
     *
     * Implementation of retrieving data using the cookie client storage strategy
     */
    var getCookie = function (cookieName) {
        var name = cookieName + "=";
        var cookieArray = document.cookie.split(';');
        for (var i = 0; i < cookieArray.length; i++) {
            var cookie = cookieArray[i];
            while (cookie.charAt(0) == ' ') {
                cookie = cookie.substring(1);
            }
            if (cookie.indexOf(name) == 0) {
                return cookie.substring(name.length, cookie.length);
            }
        }
        return "";
    };

    /**
     *
     * @param key the key to search for in local storage
     * @private Do not use this function directly outside of this library
     *
     * Implementation of retrieving data using the local storage client storage strategy
     */
    var getFromLocalStorage = function (key) {
        return localStorage.getItem(key);
    };

    /**
     *
     * @param key the key to search for in session storage
     * @private Do not use this function directly outside of this library
     *
     * Implementation of retrieving data using the session storage client storage strategy
     */
    var getFromSessionStorage = function (key) {
        return sessionStorage.getItem(key);
    };

    /**
     *
     * @param arrayOfCookies an array of key value pairs, see replaceData for expected structure
     * @param cookiePath (optional) the path for where the cookie is stored
     * @param cookieExpiry (optional) the cookies expiry date
     * @private Do not use this function directly outside of this library
     *
     * Implementation of replacing data in a cookie for the cookie client storage strategy
     */
    var replaceCookie = function (arrayOfCookies, cookiePath, cookieExpiry) {
        for (var i = 0; i < arrayOfCookies.length; i++) {
            if (typeof arrayOfCookies[i].key !== "undefined" && typeof arrayOfCookies[i].value !== "undefined") {
                var cookieBuilder = arrayOfCookies[i].key + "=" + arrayOfCookies[i].value;

                if (typeof cookiePath !== "undefined" && cookiePath !== null) {
                    cookieBuilder = cookieBuilder + ";path=" + cookiePath;
                }
                if (typeof cookieExpiry !== "undefined" && cookieExpiry !== null) {
                    cookieBuilder = cookieBuilder + ";expires=" + cookieExpiry.toUTCString();
                }
                document.cookie = cookieBuilder;
            }
        }
    };

    /**
     *
     * @param arrayOfKeyValuePairs an array of key value pairs, see replaceData for expected structure
     * @private Do not use this function directly outside of this library
     *
     * Implementation of retrieving data using the local storage client storage strategy
     */
    var replaceLocalStorage = function (arrayOfKeyValuePairs) {

        for (var i = 0; i < arrayOfKeyValuePairs.length; i++) {
            var keyValElement = arrayOfKeyValuePairs[i];
            localStorage.setItem(keyValElement.key, keyValElement.value);
        }

    };

    /**
     *
     * @param arrayOfKeyValuePairs an array of key value pairs, see replaceData for expected structure
     * @private Do not use this function directly outside of this library
     *
     * Implementation of retrieving data using the session storage client storage strategy
     */
    var replaceSessionStorage = function (arrayOfKeyValuePairs) {
        for (var i = 0; i < arrayOfKeyValuePairs.length; i++) {
            var keyValElement = arrayOfKeyValuePairs[i];
            sessionStorage.setItem(keyValElement.key, keyValElement.value);
        }
    };

    /**
     *
     * @param key The key of the value to be deleted
     * @private Do not use this function directly outside of this library
     *
     * Implementation of deleting data from local storage
     */
    var deleteFromLocalStorage = function (key) {
        localStorage.removeItem(key);
    };

    /**
     *
     * @param key The key of the value to be deleted
     * @private Do not use this function directly outside of this library
     *
     * Implementation of deleting data from session storage
     */
    var deleteFromSessionStorage = function (key) {
        sessionStorage.removeItem(key)
    };

    /**
     *
     * @param key The key of the value to be deleted
     * @private Do not use this function directly outside of this library
     *
     * Implementation of deleting data from a cookie
     */
    var deleteCookie = function (key) {
        document.cookie = key + "=;expires=Thu, 01 Jan 1970 00:00:01 GMT";
    };

    /**
     *
     * @param getFunc the function used to retrieve the initial array
     * @param replaceFunc the function used to replace the array with a new one
     * @param rulesFunc (optional) a function that must have exactly one argument, this function will be applied to the result of
     * the merging of the result of getFunc and the inputArray
     * @param key the key to use to retrieve the initial array
     * @param inputArray the array to merge to the intiial array
     * @param cookiePath if we are using cookies details where to store the cookie
     * @param cookieExpiry if we are using cookies details when to expire the cookie
     */
    var pushTo = function (getFunc, replaceFunc, rulesFunc, key, input, cookiePath, cookieExpiry) {
        var result = getFunc(key);
        result = getClientValueAsArray(result);
        result.push(input);
        if (typeof rulesFunc == "function") {
            result = rulesFunc(result);
        }
        result = cleanArray(result);
        replaceFunc([{key: key, value: encodeURIComponent(JSON.stringify(result))}], cookiePath, cookieExpiry);
    };


    /**
     *
     * @param input an input that can either be an object of {key: "<key>" , value: "<value>"} or an array of this object
     * @param storageOption a StorageOptionEnum, if provided the library will use the provided storage mechanism and will not attempt
     * to override in cases where the browser does not support the provided storage strategy
     * @param cookiePath when StorageOptionEnum.COOKIE is the chosen strategy set this as the path the cookie will be stored to
     * @param cookieExpiry when StorageOptionEnum.COOKIE is the chosen strategy set this as the expiry for the cookie
     */
    ntBrowserStorageLibrary.replaceData = function (input, storageOption, cookiePath, cookieExpiry) {
        ensureStorageStrategy(storageOption);
        var replaceDataFunction = function () {
            return "no function found capable of replacing data"
        };
        switch (storageMode) {
            case StorageOptionEnum.LOCAL_STORAGE:
                replaceDataFunction = replaceLocalStorage;
                break;
            case StorageOptionEnum.SESSION_STORAGE:
                replaceDataFunction = replaceSessionStorage;
                break;
            case StorageOptionEnum.COOKIE:
                replaceDataFunction = replaceCookie;
                break;
        }
        return replaceDataFunction(produceArray(input), cookiePath, cookieExpiry);
    };


    /**
     *
     * Gets data from the chosen local storage strategy
     *
     * @param key key of the value to be returned
     *
     * @param storageOption a StorageOptionEnum, if provided the library will use the provided storage mechanism and will not attempt
     * to override in cases where the browser does not support the provided storage strategy
     */
    ntBrowserStorageLibrary.getData = function (key, storageOption) {
        if (typeof key === "undefined") {
            return;
        }
        ensureStorageStrategy(storageOption);

        var getDataFunction = function () {
            return "no function found capable of getting data"
        };

        switch (storageMode) {
            case StorageOptionEnum.LOCAL_STORAGE:
                getDataFunction = getFromLocalStorage;
                break;
            case StorageOptionEnum.SESSION_STORAGE:
                getDataFunction = getFromSessionStorage;
                break;
            case StorageOptionEnum.COOKIE:
                getDataFunction = getCookie;
                break;
        }


        return getDataFunction(key);
    };

    /**
     *
     * @param key The key for the value that is to be removed
     * @param storageOption a StorageOptionEnum, if provided the library will use the provided storage mechanism and will not attempt
     * to override in cases where the browser does not support the provided storage strategy
     */
    ntBrowserStorageLibrary.deleteData = function (key, storageOption) {
        if (typeof key === "undefined") {
            return;
        }
        ensureStorageStrategy(storageOption);

        var deleteDataFunction = function () {
            return "no function found capable of deleting data"
        };
        switch (storageMode) {
            case StorageOptionEnum.LOCAL_STORAGE:
                deleteDataFunction = deleteFromLocalStorage;
                break;
            case StorageOptionEnum.SESSION_STORAGE:
                deleteDataFunction = deleteFromSessionStorage;
                break;
            case StorageOptionEnum.COOKIE:
                deleteDataFunction = deleteCookie;
                break;
        }
        return deleteDataFunction(key);
    };

    /**
     *
     * @param key the key which to use to retrieve the initial array
     * @param input the input to push onto the initial array
     * @param storageOption which client storage option to both read and write to
     * @param rulesFunc a function that can apply rules/transformations to the array after the input has been pushed onto the initial array
     * @param cookiePath if using a cookie as the storage option specifies the path the cookie is to be stored
     * @param cookieExpiry if using a cookie as the storage option specifies the cookies expiry
     * @returns {string} if this function errors, will return a message detailing what went wrong
     */
    ntBrowserStorageLibrary.pushData = function (key, input, storageOption, rulesFunc, cookiePath, cookieExpiry) {
        debugger;
        if (typeof key === "undefined") {
            throw "no key provided to get array to be pushed onto";
        }
        ensureStorageStrategy(storageOption);
        switch (storageOption) {
            case StorageOptionEnum.LOCAL_STORAGE:
                pushTo(getFromLocalStorage, replaceLocalStorage, rulesFunc, key, input);
                break;
            case StorageOptionEnum.SESSION_STORAGE:
                pushTo(getFromSessionStorage, replaceSessionStorage, rulesFunc, key, input);
                break;
            case StorageOptionEnum.COOKIE:
                pushTo(getCookie, replaceCookie, rulesFunc, key, input, cookiePath, cookieExpiry);
                break;
            default:
                throw "no function found capable of pushing data"
        }

    };

    return new function(){
      this.pushData = ntBrowserStorageLibrary.pushData;
        this.deleteData = ntBrowserStorageLibrary.deleteData;
        this.getData = ntBrowserStorageLibrary.getData;
        this.replaceData = ntBrowserStorageLibrary.replaceData;
        this.StorageOptionEnum = StorageOptionEnum;
    };

});

