/**
 * This module is responsible for binding functionality to the search form
 */
 var nt_websiteFilterSearch;
(function (nt_websitePaginationSearch, $) {
 	 nt_websiteFilterSearch = new function() {
        var self = this;
        var lozengeTemplate = $(
            '<a href="#" class="button small round nt-button active nt-search-filters-tag nt-palette-hover nt-palette-neutral" style="cursor:pointer;">' +
                '<span class="icon" data-icon="&#58905;"></span>' +
                '<span class="nt-lozenge-label"></span>' +
                '<span class="icon" data-icon="&#10005;"></span>' +
            '</a>'
        );

        self.init = function() {
            rebuildLozenges();

            $('#search-form').unbind('submit').on('submit', function(evt) {
                evt.preventDefault();
                evt.stopImmediatePropagation();

                $('.nt-js-close-modal').click();
                nt_websitePaginationSearch.filterChanged();
                rebuildLozenges();
            });
        }

        var rebuildLozenges = function() {
            var lozengeRow = $('.nt-applied-filter-tags div')
            lozengeRow.empty();
            $('#search-form :checkbox:checked[id*=nt-filter-group]').each(function() {
                var hiddenCheckbox = $(this);
                var lozenge = lozengeTemplate.clone();
                var reference = hiddenCheckbox.val();
                var label = getLabelInModalContent(reference);
                lozenge.attr('data-reference', reference);
                lozenge.find('span.nt-lozenge-label').text(label.text().trim());
                lozengeRow.append(lozenge);

                lozenge.on('click', function() {
                    var reference = $(this).data('reference');
                    uncheckFilter(reference)
                    nt_websitePaginationSearch.filterChanged();
                    rebuildLozenges();
                });

            });
        }

        var getLabelInModalContent = function(reference) {
            return $('label[for=nt-filter-group-' + reference + ']');
        }

        var uncheckFilter = function(reference) {
            $('#nt-filter-group-' + reference).prop('checked', false);
            $('#nt-filter-group-' + reference + '-actual').prop('checked', false);
        }

    };
}(nt_websitePaginationSearch, jQuery));

