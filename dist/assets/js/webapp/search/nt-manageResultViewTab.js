/**
 * Responsible for managing what happens when a user clicks on the tab that allows them to view data in either map or table form
 *
 * @author Sam Blackwell
 */

var ntManageResultViewTab = {};

nt_loadLibraries([{path: "$"}, {path: "NtMap"}], function () {


    (function () {

        var MAP_MODE = 'map';

        var mapContainerId = 'map-canvas';

        /**
         * Initialises the map and required layers
         * @param mapContainerId the id of the map container
         * @param focussedPlaces an array of place ids which are used to create a maps bounding box, where all places
         * will fall within the boounding box
         * @param showAllPlaces sets whether or not to show all places
         * @param myLocationMarkerTitle a title to be set if user is searching by their location
         */
        var launchMap = function (mapContainerId, focussedPlaces, myLocationMarkerTitle) {
            $.ajax({
                type: 'GET',
                url: '/search/data/mapCardTemplate',
                success: function (data) {
                    NtMap.cardTemplate = data;

                },
                error: function (request, textStatus, errorThrown) {
                    console.log("error: " + errorThrown);
                }
            });

            var mapConfig = {
                focus: {
                    location: NtMap.getGeolocationURLParameters(),
                    markers: focussedPlaces,
                    title: myLocationMarkerTitle,
                    fitMapToPins: false
                },
                markerConfig: {
                    maxZoom: 8
                },
                isUpdatableMap: false
            };

            mapConfig = NtMap.getConfigBuilder(mapConfig).withRecentreControl().build();

            var map = NtMap.launchCustomMap(mapContainerId, mapConfig, NtMap.getAllPlacePins, NtMap.renderPlaceCard);
            map.resize();
        };

        /**
         * @param mapCanvas jquery object of the map canvas
         * Set the maps canvas height
         */
        var setMapCanvasHeight = function (mapCanvas) {
            // set height of containing box ahead of map load
            mapCanvas.height(NtMap.calculateHeight());
        };

        /**
         * @param viewSearchFormFieldId id of viewSearchFormFieldId
         * checks if the tab is in map mode
         * @returns {boolean} true if tab is in map mode, false otherwise
         */
        var isViewModeMap = function (viewSearchFormFieldId) {
            return $(viewSearchFormFieldId).val() === MAP_MODE;
        };

        /**
         * @param searchQuery search query input jquery object
         * checks if a search using 'My Location' is being performed
         * @returns my location text if true otherwise returns null
         */
        var isMyLocationSearch = function (searchQuery) {
            if (searchQuery.val().indexOf("My location") === 0) {
                return searchQuery.val();
            }
            else {
                return null;
            }

        };

        /**
         * Initialises the widget responsible for allowing a user to switch between list and map views
         *
         * @param focussedPlaces an array of place ids which are used to create a maps bounding box, where all places
         * will fall within the boounding box
         */
        ntManageResultViewTab.initMapListTabWidget = function (focussedPlaces) {
            var LIST_MODE = 'list';

            var viewSearchFormField = $('#search-form-view'),
                body = $('body');

            var listViewTabId = '#tabsearch-results-list-view',
                mapViewTabId = '#tabsearch-results-map-view';

            var addNewViewStateToHistory = function (viewState) {
                var VIEW_PARAM_REGEX = /(view=)[^\&]+/;
                var url = window.location.href;
                url = url.replace(VIEW_PARAM_REGEX, '$1' + viewState);
                history.replaceState(null, null, url);
            };

            // update search form with selected map/list view
            body.on('click', listViewTabId, function () {
                viewSearchFormField.val(LIST_MODE);
                addNewViewStateToHistory(LIST_MODE);
                $(this).trigger('initialise-carousel');
            });
            body.on('click', mapViewTabId, function () {
                viewSearchFormField.val(MAP_MODE);
                var searchQuery = $("#search-query");
                var myLocationMarkerTitle = isMyLocationSearch(searchQuery);
                launchMap(mapContainerId, focussedPlaces, myLocationMarkerTitle);
                addNewViewStateToHistory(MAP_MODE);
            });
        };


        /**
         * Initialises the map view when a user lands on map view
         *
         * @param focussedPlaces an array of place ids which are used to create a maps bounding box, where all places
         * will fall within the boounding box
         */
        ntManageResultViewTab.initMapView = function (focussedPlaces) {
            var mapCanvas = $('#map-canvas'),
                searchQuery = $("#search-query");

            var viewSearchFormFieldId = '#search-form-view';

            var myLocationMarkerTitle = isMyLocationSearch(searchQuery);

            setMapCanvasHeight(mapCanvas);
            // init map on page load when in map mode
            if (isViewModeMap(viewSearchFormFieldId)) {
                launchMap(mapContainerId, focussedPlaces, myLocationMarkerTitle);
            }
        }
    })();
});