'use strict';
var requirejs = require("requirejs");
require.main.require('develop/js/mocha-tests/common-mocha-test-header.js');

var should = require("should");

suite('Test NtMap', function () {
    var NtMap;

    suiteSetup(function (done) {
        requirejs("window");
        requirejs("jquery");
        requirejs("location");
        requirejs("google");
        requirejs("MarkerClusterer");
        requirejs("InfoBox");

        global.nt_loadLibraries = NtTest.loadScript('develop/js/webapp/nt-load-libraries.js', 'nt_loadLibraries');

        NtMap = NtTest.loadScript('develop/js/webapp/search/nt-map.js', 'NtMap');
        NtTest.loadScript('develop/js/webapp/search/nt-map-test-data.js');
        done();
    });

    var getNtMapInstance = function() {
        //Given

        $.getJSON = function () {
        };
        var canvasId = "canvas";
        var withAllPlaces = true;
        var myLocationMarkerTitle = "My Location";
        var currentFocus = 123;

        //When

        return NtMap.launchMap(canvasId, currentFocus, withAllPlaces, myLocationMarkerTitle);
    }

    suite('Nt Map', function () {
        test('has expected functions', function () {
            NtTest.hasFunctions(
                NtMap ,
                [
                    {
                        "methodName": "calculateHeight",
                        "arguments": []
                    },
                    {
                        "methodName": "getGeolocationURLParameters",
                        "arguments": []
                    },
                    {
                        "methodName": "renderPlaceCard",
                        "arguments": [
                            "place",
                            "options"
                        ]
                    },
                    {
                        "methodName": "getAllPlacePins",
                        "arguments": [
                            "callback",
                            "urlRoot"
                        ]
                    },
                    {
                        "methodName": "launchMap",
                        "arguments": [
                            "canvasId",
                            "currentFocus",
                            "withAllPlaces",
                            "myLocationMarkerTitle"
                        ]
                    },
                    {
                        "methodName": "launchCustomMap",
                        "arguments": [
                            "canvasId",
                            "config",
                            "getAllPinsCallback",
                            "renderCardCallback",
                            "root"
                        ]
                    },
                    {
                        "methodName": "getConfigBuilder",
                        "arguments": [
                            "config"
                        ]
                    },
                    {
                        "methodName": "clearInstance",
                        "arguments": []
                    }
                ] ,
                {strict: true, exclude: ["cardTemplate"]}
            );

        });

        test('should calculate correct height', function () {
            //Given
            window.screen = {height: 300};

            //When
            var height = NtMap.calculateHeight();

            //Then
            should(height).be.equal(180);
        });

        test('should constrain height to maximum', function () {
            //Given
            window.screen = {height: 1000};

            //When
            var height = NtMap.calculateHeight();

            //Then
            should(height).be.equal(640);
        });

        test('should get location url parameters', function () {
            //Given
            location.search = "?lat=12.34&lon=43.21";

            //When
            var geolocation = NtMap.getGeolocationURLParameters();

            //Then
            should(geolocation.latitude).be.equal(12.34);
            should(geolocation.longitude).be.equal(43.21);
        });

        test('should return undefined when no location url parameters supplied', function () {
            //Given
            location.search = "?";

            //When
            var geolocation = NtMap.getGeolocationURLParameters();

            //Then
            should(typeof(geolocation)).be.equal("undefined");
        });

        test('should render place card', function () {
            //Given
            $.map = function () {
            };
            $.each = function () {
            };
            jquery.mock.addInternal("data", function () {

            });

            NtMap.cardTemplate = NT_MAP_TEMPLATE;

            var place = {
                id: "123",
                title: "title",
                subTitle: "subTitle",
                websiteUrl: "websiteUrl",
                imageDescription: "imageDescription",
                imageUrl: "imageUrl",
                activityTagsAsCsv: "Walking, Running"

            };
            var options = {
                smallScreen: false,
                displayWhatsOn: false
            };

            //When
            var result = NtMap.renderPlaceCard(place, options);

            //Then
            should(result.html).containEql(place.title);
            should(result.html).containEql(place.subTitle);
            should(result.html).containEql(place.imageDescription);
            should(result.html).containEql(place.imageUrl);

            should(result.cardOptions.maxWidth).be.equal(400);
        });

        test('an instance of NtMap has the expected functions', function () {
            //Given

            var ntMap = getNtMapInstance();

            NtTest.hasFunctions(
                ntMap ,
                [
                    {
                        "methodName": "isMapReady",
                        "arguments": []
                    },
                    {
                        "methodName": "init",
                        "arguments": [
                            "places",
                            "mapCanvas",
                            "config",
                            "renderCardCallback"
                        ]
                    },
                    {
                        "methodName": "updatePins",
                        "arguments": [
                            "pins"
                        ]
                    },
                    {
                        "methodName": "getUrlRoot",
                        "arguments": []
                    },
                    {
                        "methodName": "setIcon",
                        "arguments": [
                            "placeId",
                            "icon"
                        ]
                    },
                    {
                        "methodName": "removeIcon",
                        "arguments": [
                            "placeId"
                        ]
                    },
                    {
                        "methodName": "triggerMapUpdatedEvent",
                        "arguments": []
                    },
                    {
                        "methodName": "triggerMapPannedEvent",
                        "arguments": []
                    },
                    {
                        "methodName": "triggerInfoWindowDisplayed",
                        "arguments": []
                    },
                    {
                        "methodName": "updateRenderCardFunction",
                        "arguments": [
                            "renderCardCallback"
                        ]
                    },
                    {
                        "methodName": "resize",
                        "arguments": []
                    },
                    {
                        "methodName": "setSmallScreen",
                        "arguments": [
                            "state"
                        ]
                    },
                    {
                        "methodName": "setDraggable",
                        "arguments": [
                            "state"
                        ]
                    },
                    {
                        "methodName": "setScrollWheel",
                        "arguments": [
                            "state"
                        ]
                    },
                    {
                        "methodName": "setDisplayWhatsOn",
                        "arguments": [
                            "state"
                        ]
                    },
                    {
                        "methodName": "setActivitySearch",
                        "arguments": [
                            "state"
                        ]
                    },
                    {
                        "methodName": "setFilteredSearch",
                        "arguments": [
                            "state"
                        ]
                    },
                    {
                        "methodName": "centerOnPlace",
                        "arguments": [
                            "placeId"
                        ]
                    },
                    {
                        "methodName": "displayResults",
                        "arguments": [
                            "placeIds",
                            "mapCenter",
                            "applyBounds"
                        ]
                    },
                    {
                        "methodName": "placeMarker",
                        "arguments": [
                            "position",
                            "title",
                            "centerOnPosition"
                        ]
                    },
                    {
                        "methodName": "getLastMarker",
                        "arguments": []
                    },
                    {
                        "methodName": "getMap",
                        "arguments": []
                    },
                    {
                        "methodName": "allPlaces",
                        "arguments": []
                    },
                    {
                        "methodName": "focusedPlaces",
                        "arguments": []
                    }
                ] ,
                {strict: true, exclude: []}
            );
        });

        test('an instance of custom NtMap is constructed', function () {
            //Given

            location.search = "?lat=1&lon=2";
            $.getJSON = function () {
            }
            var canvasId = "canvas";
            var config = {
                focus: {
                    location: {latitude: 12.3, longitude: 34.5},
                    markers: 123,
                    title: 'location'
                },
                markerConfig: {
                    maxZoom: 21
                }
            };

            var places = {
                "123": {
                    location: {latitude: 1, longitude: 2}
                }
            };

            var getAllPinsCallback = function (callback, urlRoot) {
                callback(places);
            };
            var renderCardCallback = function (place, options) {
                return {
                    cardOptions: {}
                }
            };
            var root = "root123";

            NtMap.clearInstance();

            //When

            var ntMap = NtMap.launchCustomMap(canvasId, config, getAllPinsCallback, renderCardCallback, root);

            //Then

            should(ntMap.getUrlRoot()).be.equal(root);
        });

        test('map view extends to fit pins', function () {
            //Given

            location.search = "?lat=1&lon=2";
            $.getJSON = function () {
            }
            var canvasId = "canvas";
            var config = {
                focus: {
                    location: {latitude: 12.3, longitude: 34.5},
                    markers: 123,
                    title: 'location'
                },
                markerConfig: {
                    maxZoom: 21
                }
            };

            var places = {
                "123": {
                    location: {latitude: 1, longitude: 2}, id: "123"
                },
                "124": {
                    location: {latitude: 1, longitude: 3}, id: "124"
                },
                "125": {
                    location: {latitude: 1, longitude: 4}, id: "125"
                },
                "126": {
                    location: {latitude: 1, longitude: 5}, id: "126"
                },
                "127": {
                    location: {latitude: 1, longitude: 6}, id: "127"
                }
            };

            var getAllPinsCallback = function (callback, urlRoot) {
                callback(places);
            };
            var renderCardCallback = function (place, options) {
                return {
                    cardOptions: {}
                }
            };
            var root = "root123";


            //Mock resize function
            $.fn.nt_resize = function() {};

            NtMap.clearInstance();

            //When

            var ntMap = NtMap.launchCustomMap(canvasId, config, getAllPinsCallback, renderCardCallback, root);
            ntMap.displayResults([123]);

            //Then

            should(google.maps.mock.extendedTo.length).be.equal(2);

            //When

            ntMap.displayResults([123, 124, 125]);
            //Then

            should(google.maps.mock.extendedTo.length).be.equal(6);
            should(google.maps.mock.fitBoundsCalled).be.equal(true);
        })

    });
});