NtDateSearch = new function() {
  var self = this;

  function DDMMYYYYToDate(dateString) {
    if(dateString.length!==10) return;
    try{
      var year = parseInt( dateString.substring(6,10) ,10 );
      var month = parseInt( dateString.substring(3,5),10 );

      var day = parseInt( dateString.substring(0,2),10 );
      var date = new Date( year, month-1, day );
      return date;
    } catch(e) {
      return;
    }
  }

  var nowDate=new Date();
  nowDate.setHours(0);
  nowDate.setMinutes(0);
  nowDate.setSeconds(0);
  nowDate.setMilliseconds(0);
  var fromDate=null;
  var toDate=null;
  var outerDate=new Date();
  outerDate.setDate( outerDate.setFullYear( nowDate.getFullYear()+2 ) );

  $("#fromDatePickerContainer").on('click', function() { $('#fromDatePicker').focus(); return false; });
  $("#toDatePickerContainer").on('click', function() { $('#toDatePicker').focus(); return false; });

  var fromDateObj = $('#fromDatePicker').fdatepicker({
    onRender: function (date) {
      if(date.valueOf() < nowDate.valueOf() || date.valueOf() > outerDate.valueOf()) {
        return 'disabled';
      } else {
        return '';
      }
    }
  }).on('changeDate', function (ev) {
    console.log("changed to "+ev.date);
    fromDateObj.hide();
    fromDateObj.setDate(ev.date);
    $("#validationMessage").text("");
  }).data('datepicker');

  if(!fromDateObj) fromDateObj={};

  fromDateObj.setDate = function(date) {
    $('#fromDatePicker').fdatepicker('update',date);
    fromDate = date;
    if (date.valueOf() > toDateObj.date.valueOf()) {
      var newDate = new Date(date);
      newDate.setDate(newDate.getDate() + 1);
      toDateObj.update(newDate);
      fromDate = newDate;
    }
  };

  var toDateObj = $('#toDatePicker').fdatepicker({
    onRender: function (date) {
      if(date.valueOf() <= fromDateObj.date.valueOf() || date.valueOf() > outerDate.valueOf()) {
        return 'disabled';
      } else {
        return '';
      }
    }
  }).on('changeDate', function (ev) {
    toDateObj.hide();
    toDateObj.setDate(ev.date);
    $("#validationMessage").text("");
  }).data('datepicker');

  if(!toDateObj) toDateObj={};

  toDateObj.setDate = function(date) {
    $('#toDatePicker').fdatepicker('update',date);
    toDate = date;
  };

  $('body').on("blur", '#fromDatePicker',
               function() {
                 var newDate = DDMMYYYYToDate($('#fromDatePicker').val());
                 if(!newDate) {
                   $("#validationMessage").text("The date must be in the format dd/mm/yyyy");
                   fromDateObj.setDate(fromDate);
                 } else if( newDate.valueOf()<nowDate.valueOf() ) {
                   $("#validationMessage").text("Dates must be in the future");
                   fromDateObj.setDate(fromDate);
                 } else if( newDate.valueOf()>toDate.valueOf() ) {
                   $("#validationMessage").text("The from date must be before the to date");
                   fromDateObj.setDate(fromDate);
                 } else if(newDate.valueOf()>outerDate.valueOf()) {
                   $("#datePickerValidationMessage").text("The date must be less than 2 years in the future");
                   fromDateObj.setDate(fromDate);
                 } else {
                   $("#validationMessage").text("");
                 }
               }
              );

  $('#toDatePicker').on("blur",
                        function() {
                          var newDate = DDMMYYYYToDate($('#toDatePicker').val());
                          if(!newDate) {
                            $("#validationMessage").text("The date must be in the format dd/mm/yyyy");
                            toDateObj.setDate(toDate);
                          } else if( newDate.valueOf()<fromDate.valueOf() ) {
                            $("#validationMessage").text("The to date must be after the from date");
                            toDateObj.setDate(toDate);
                          } else if(newDate.valueOf()>outerDate.valueOf()) {
                            $("#validationMessage").text("The date must be less than 2 years in the future");
                            toDateObj.setDate(toDate);
                          } else {
                            $("#validationMessage").text("");
                          }
                        }
                       );

  self.setDates = function(newFromDate,newToDate,newNowDate) {
    nowDate = new Date();
    nowDate.setHours(0);
    nowDate.setMinutes(0);
    nowDate.setSeconds(0);
    nowDate.setMilliseconds(0);
    if(newNowDate) {
      nowDate = newNowDate;
    }
    fromDate = nowDate;
    if(newFromDate) {
      fromDate = newFromDate;
    }

    if(newToDate) {
      toDate = newToDate;
    } else {
      toDate = new Date(fromDate);
      toDate.setMonth( fromDate.getMonth()+1 );
    }

    outerDate = new Date();
    outerDate.setFullYear( nowDate.getFullYear()+2 );

    fromDateObj.setDate(fromDate);
    toDateObj.setDate(toDate);
  };

  var fromDateParameter = $.getParameterByName("fromDate");
  if(fromDateParameter) {
    fromDate = DDMMYYYYToDate(fromDateParameter);
  }

  var toDateParameter = $.getParameterByName("toDate");
  if(toDateParameter) {
    toDate = DDMMYYYYToDate(toDateParameter);
  }

  self.setDates(fromDate,toDate);
};