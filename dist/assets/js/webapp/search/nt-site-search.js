'use strict';

var nt_siteSearch;

(function (window) {

    nt_siteSearch = new function() {
        var self = this;
        var cardTemplate;

        //From config
        var searchHost;
        var googleSearchKey;
        var customSearchEngineId;
        var ntImagesHost;
        var responsiveImageServer;

        var resultsPerPage = 9;
        var currentQuery = "";
        var nextIndex = 1;
        var replaceHistoryOnNextSearch = false;

        self.init = function(query,config) {
            searchHost = config.searchHost;
            googleSearchKey = config.googleSearchKey;
            customSearchEngineId = config.customSearchEngineId;
            ntImagesHost = config.ntImagesHost;
            responsiveImageServer = config.responsiveImageServer;

            if(!query) query = "";
            if(query.length!=0) {
                if(window.history.state &&
                    window.history.state.query &&
                    window.history.state.query == query
                ) {
                    nextIndex = window.history.state.nextIndex;
                    returnToSearchFromHistory( window.history );
                    currentQuery = query;
                    getCardTemplate('');
                    return;
                } else {
                    replaceHistoryOnNextSearch = true;
                }
            }
            getCardTemplate(query);
        };

        var getCardTemplate = function(query) {
            $.ajax({
                type: 'GET',
                url: '/search/data/cardTemplate',
                success: function (data) {
                    cardTemplate = data;
                    self.search(query);
                },
                error: function (request, textStatus, errorThrown) {
                    alert("error: " + errorThrown);
                }
            });
        };

        self.search = function(query) {
            $("#search-query")[0].value = query;

            if(!cardTemplate || !query || query.length==0) {
                return;
            }

            if(query!=currentQuery) {
                currentQuery = query;
                nextIndex = 1;
                $("#nt-see-more").hide();
                $("#search-information").hide();
            }

            $("#nt-see-more").unbind("click");
            $("#nt-see-more").on("click",
                function(searchSubmitButton) {
                    nt_siteSearch.search( $("#search-query")[0].value );
                    searchSubmitButton.preventDefault();
                });

            $.ajax({
                type: 'GET',
                url: searchHost + '/v1?q='+query+'&key=' + googleSearchKey + '&cx=' + customSearchEngineId + '&num=' + resultsPerPage + '&start=' + nextIndex,
                success: function (response) {
                    displaySearchResults(query,response);
                },
                error: function (request, textStatus, errorThrown) {
                    console.error("error: " + errorThrown);
                }
            });
        };

        var displaySearchResults = function(query,response) {
            var resultsHtml = "";
            var searchInformation = "Showing results for "+query;

            if(response.items) {
                if(response.queries.request[0].startIndex == 1 && response.promotions) {
                    for (var i = 0; i < response.promotions.length; i++) {
                        var promotion = response.promotions[i];
                        var title = promotion.title;
                        var metadata = "";
                        var description = "";
                        if(promotion.bodyLines.length>0) {
                            description = promotion.bodyLines[0].title;
                        }
                        var thumbnailUrl = '/assets/img/transparent.gif';
                        if(promotion.image) {
                            var imgSrc = promotion.image.source;
                            if(imgSrc.indexOf("url=")!=-1) {
                                imgSrc = imgSrc.substring(imgSrc.indexOf("url=")+4,imgSrc.indexOf("&"));
                            }
                            if(imgSrc.indexOf(ntImagesHost)==0) {
                                imgSrc = 'https://' + responsiveImageServer + imgSrc.substring(ntImagesHost.length);
                            }
                            thumbnailUrl = imgSrc;
                        }
                        var link = promotion.link;
                        var assetType = '<div class="nt-line-device"></div>';

                        var card = replaceTemplatePlaceholders(cardTemplate, link, thumbnailUrl, metadata, description, title, assetType);
                        resultsHtml += card;
                    }
                }
                for (var i = 0; i < response.items.length; i++) {
                    var item = response.items[i];

                    var title = item.title;
                    if (title.indexOf(" | National") != -1) {
                        title = title.substring(0, title.indexOf(" | National"));
                    }

                    var hasMetadata = item.pagemap &&
                        item.pagemap.metatags &&
                        item.pagemap.metatags[0];

                    var metadata = "";
                    if (hasMetadata && item.pagemap.metatags[0]['nt:contentTypeName']) {
                        metadata = item.pagemap.metatags[0]['nt:contentTypeName'];
                    }

                    var description = item.snippet;
                    var thumbnailUrl = '/assets/img/transparent.gif';
                    if (item.pagemap && item.pagemap.cse_image && item.pagemap.cse_image.length > 0) {
                        var imgSrc = item.pagemap.cse_image[0].src;
                        if(imgSrc.indexOf(ntImagesHost)==0) {
                            imgSrc = 'https://' + responsiveImageServer + imgSrc.substring(ntImagesHost.length);
                        }
                        thumbnailUrl = imgSrc;
                    }
                    var link = item.link;

                    var assetType = '<div class="nt-line-device"></div>';
                    if (hasMetadata && item.pagemap.metatags[0]['nt:assettype']) {
                        assetType = '<div class="nt-masonry-single-result-category">' + item.pagemap.metatags[0]['nt:assettype'] + '</div>'
                    }

                    var card = replaceTemplatePlaceholders(cardTemplate, link, thumbnailUrl, metadata, description, title, assetType);
                    resultsHtml += card;
                }

                if(response.spelling) {
                    searchInformation = "Showing results for " + query + ". Did you mean <a href='#' onClick='nt_siteSearch.search(\"" + response.spelling.correctedQuery + "\");return false;'>" + response.spelling.correctedQuery + "</a>";
                }

                if (response.queries.request[0].startIndex == 1) {
                    $("#search-result-cards").html(resultsHtml);
                    recordSearchInHistory(query, searchInformation, $("#search-result-cards").html(), replaceHistoryOnNextSearch);
                    replaceHistoryOnNextSearch = false;
                } else {
                    $("#search-result-cards").append(resultsHtml);
                    recordSearchInHistory(query, searchInformation, $("#search-result-cards").html(), true);
                }

            } else if (response.queries.request[0].startIndex == 1) {
                var resultsHtml = "";
                searchInformation = "<span id='search-has-no-results'>Sorry, no results found for " + query + "</span>";
                $("#search-result-cards").html(resultsHtml);
                recordSearchInHistory(query, searchInformation, $("#search-result-cards").html(), false);
            }


            $("#search-information").html(searchInformation);
            $("#search-information").show();

            if(response.queries.nextPage && response.queries.nextPage.length>0 && response.queries.nextPage[0].startIndex+9 < 100) {
                nextIndex = response.queries.nextPage[0].startIndex;
                $("#nt-see-more").show();
            } else {
                $("#nt-see-more").hide();
            }
        };

        var replaceTemplatePlaceholders = function(cardTemplate, link, thumbnailUrl, metadata, description, title, assetType) {
            return cardTemplate.replace(/{{resourceUrl}}/g, link)
                .replace(/{{thumbnailUrl}}/g, thumbnailUrl)
                .replace(/{{metadata}}/g, metadata)
                .replace(/{{description}}/g, description)
                .replace(/{{title}}/g, title)
                .replace(/{{assetTypeDiv}}/g, assetType);
        }

        var recordSearchInHistory = function(query,searchInformation,content,replace) {
            var search = window.location.search;

            var startIndex = search.indexOf("query=");
            if(startIndex==-1) {
                search="?query=&"+search.substring(1);
                startIndex = 7;
                endIndex = 8;
            } else {
                startIndex+=6;
            }
            var endIndex = search.substring(startIndex).indexOf("&")+startIndex;
            var newSearch = search.substring(0,startIndex) + query + search.substring(endIndex);

            if(replace) {
                window.history.replaceState({query: query, searchInformation: searchInformation, content: content, nextIndex: nextIndex}, null, "/search" + newSearch);
            } else {
                window.history.pushState({query: query, searchInformation: searchInformation, content: content, nextIndex: nextIndex}, null, "/search" + newSearch);
            }
        };

        var returnToSearchFromHistory = function(event) {
            $("#search-query")[0].value = event.state.query;
            $("#search-result-cards").html( event.state.content );
            $("#search-information").html( event.state.searchInformation );
        };
        window.addEventListener('popstate',returnToSearchFromHistory );
    };

}(window));