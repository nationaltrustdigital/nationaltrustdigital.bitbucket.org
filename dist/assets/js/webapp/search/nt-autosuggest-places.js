/*
 * This plugin controls places autosuggest functionality
 */
(function ($) {
    $.fn.nt_autosuggestPlaces = function (params) {
        return this.each(function () {
            autosuggest(this, params);
        });
    };

    $.fn.nt_autosuggestPlaces.exactMatchUrl = "";
    $.fn.nt_autosuggestPlaces.exactMatchName = "";

    function autosuggest(control, params) {
        $.getJSON(params.url, function (places) {

            var placesIndex = createPlacesIndex(places);

            $(control).typeahead({
                hint: true,
                highlight: true,
                minLength: 2
            }, {
                name: 'places',
                source: matcher(places),
                limit: 5
            })
                .on("typeahead:selected", onSelected)

            function onSelected(obj, value) {
                if ($("#search-form-type").val() === "place") {

                    var placeFrom = {
                        key: "fromSearchBox", value: JSON.stringify({
                            placeName: placesIndex[value].name,
                            cameFrom: window.location.pathname
                        })
                    };

                    localStorage.setItem(placeFrom.key, placeFrom.value);
                    window.location.href = placesIndex[value].websiteUrl;

                } else {
                    $("#search-query").val(value);
                    $("#search-form").submit();
                }
            }

            function createPlacesIndex(places) {
                var placesIndex = {};
                for (var index = 0, len = places.length; index < len; index++) {
                    placesIndex[places[index].name] = places[index];
                }
                return placesIndex;
            }
        });

        var matcher = function (places) {

            return function (query, callback) {
                /*
                 * ref wsr-41 - strip all non alphanumerics except '&,:- from the search term and match at the start of each word
                 * allow all unicode characters (for Welsh diacriticals) and convert multiple spaces to single space
                 */
                var cleaned_query = XRegExp.replace(query, XRegExp("/[^\\w\\s\\p{L}\'&,:-]/g"), "");
                cleaned_query = $.trim(XRegExp.replace(cleaned_query, XRegExp("\\s+"), " "));

                var startOfWordPattern = XRegExp("\\b" + cleaned_query, "i");
                var matches = [];
                var matchedPlaces = [];

                //First word matches take precedence so add them to lists first
                $.each(places, function (index, place) {
                    if (place.name.toLowerCase().indexOf(cleaned_query.toLowerCase()) === 0) {
                        matches.push(place.name);
                        matchedPlaces.push(place);
                    }
                });
                //Sort matches
                matches.sort();
                matchedPlaces.sort(function (a, b) {
                    return a.name > b.name;
                });

                //Close matches
                //Keep close matches in separate array so they can be sorted
                var closeMatches = [];
                var closeMatchedPlaces = [];
                $.each(places, function (index, place) {
                    //Include start of word matches but exclude start of first word matches as already added above
                    if (startOfWordPattern.test(place.name) && place.name.toLowerCase().indexOf(cleaned_query.toLowerCase()) !== 0) {
                        closeMatches.push(place.name);
                        closeMatchedPlaces.push(place);
                    }
                });
                //Sort close matches
                closeMatches.sort();
                closeMatchedPlaces.sort(function (a, b) {
                    return a.name > b.name;
                });

                //Combine close matches with matches
                matches = matches.concat(closeMatches);
                matchedPlaces = matchedPlaces.concat(closeMatchedPlaces);

                if (matchedPlaces.length === 1 && matchedPlaces[0].name.toLowerCase() === cleaned_query.toLowerCase()) {
                    $.fn.nt_autosuggestPlaces.exactMatchUrl = matchedPlaces[0].websiteUrl;
                    $.fn.nt_autosuggestPlaces.exactMatchName = matchedPlaces[0].name;
                } else {
                    $.fn.nt_autosuggestPlaces.exactMatchUrl = "";
                    $.fn.nt_autosuggestPlaces.exactMatchName = "";
                }
                callback(matches);
            };
        };
    }
}(jQuery));