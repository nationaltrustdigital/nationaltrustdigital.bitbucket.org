NT_MAP_TEMPLATE =
    "<a href=\"{{placePageUrl}}\" style=\"display:block;border-bottom-style:none;\" class=\"no-icon\" target=\"linkTarget\">" +
    "    <div class=\"card nt-masonry-single-result\" id=\"place-card\">" +
    "        <div class=\"card-inner nt-masonry-single-result-inner\">" +
    "            <div style=\"display: {{optionsNotSmallScreen}}\" class=\"image nt-masonry-single-result-image nt-image-wrap nt-image-wrap-16x9\">" +
    "                <img src=\"{{imageUrl}}\" alt=\"{{imageDescription}}\">" +
    "            </div>" +
    "            <div class=\"content nt-masonry-single-result-inner map-card-inner\">" +
    "                <h3 class=\"card-title nt-link-chevron\">" +
    "                    <a href=\"{{placePageUrl}}\" target=\"linkTarget\">" +
    "                        {{title}}</a>" +
    "                </h3>" +
    "                <div class=\"nt-masonry-single-result-category\">" +
    "                    {{subTitle}}</div>" +
    "                <p>" +
    "                    {{description}}</p>" +
    "                <p style=\"display: {{hasOpeningTimeStatus}}\" class=\"place-card-opening-time-status\">" +
    "          <span>" +
    "            <b>" +
    "              {{openingTimeStatus}}</b>" +
    "          </span>" +
    "                    <br />" +
    "                    <a style=\"border-bottom-color: #2c2c2c;\" href=\"{{openingTimeUrl}}\" class=\"no-icon\">" +
    "                        View all opening times&nbsp;<span class=\"icon\" data-icon=\"&#xe600;\" target=\"linkTarget\">" +
    "            </span>" +
    "                    </a>" +
    "                </p>" +
    "                <a style=\"display: {{displayWhatsOnAndSelected}}\" href=\"{{eventsPageUrl}}\" class=\"no-icon\" target=\"linkTarget\">" +
    "                    <div class=\"nt-search-whats-on-result-event-link nt-palette-neutral nt-palette-hover nt-js-link nt-link-chevron\">" +
    "                        View all events&nbsp;</div>" +
    "                </a>" +
    "                <div style=\"display: {{activitySearchAndSelected}}\" class=\"nt-search-whats-on-result-event-link nt-palette-neutral nt-palette-hover\" style=\"padding: 1em\">" +
    "          <span display: {{hasActivityTags}}>" +
    "            <b>" +
    "              Activities:</b>" +
    "            <p class=\"tag-list\" style=\"padding-left: 0px;margin-top: 1em;margin-bottom: 1em\">" +
    "              <i>" +
    "                {{activityTags}}</i>" +
    "            </p>" +
    "          </span>" +
    "                    <a style=\"border-bottom: 1px solid #2c2c2c;\" href=\"{{activitiesPageUrl}}\" class=\"no-icon\" target=\"linkTarget\">" +
    "                        View all activities&nbsp;<span class=\"icon\" data-icon=\"&#xe600;\">" +
    "            </span>" +
    "                    </a>" +
    "                </div>" +
    "            </div>" +
    "        </div>" +
    "    </div>" +
    "</a>";