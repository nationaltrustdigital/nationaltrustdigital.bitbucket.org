'use strict';
var requirejs = require("requirejs");
require.main.require('develop/js/mocha-tests/common-mocha-test-header.js');
var should = require("should");

suite('Test nt_siteSearch', function () {
    var nt_siteSearch;

    suiteSetup(function (done) {
        requirejs("window");
        nt_siteSearch = NtTest.loadScript('develop/js/webapp/search/nt-site-search.js', 'nt_siteSearch');
        done();
    });

    suite('nt_siteSearch', function () {
        test('has expected functions', function () {

            NtTest.generateFunctionsTest( nt_siteSearch, "nt_siteSearch" );
        });
    });
});