/**
 * This module is responsible for binding functionality to the search form
 */
var nt_websitePaginationSearch;
(function ($) {
	nt_websitePaginationSearch = new function() {
        var self = this;
        var active = false;
        var searchEndpoint = '/search-results';
        var page = {
            pageIndex: 0,
            pageSize: 18
        }

        self.init = function(options) {
        	page.pageIndex = options.pageIndex;
        	page.pageSize = options.pageSize;
            self.bindSeeMoreButton();
        }

        self.filterChanged = function() {
            page.pageIndex = 0;
            page.pageSize = 18;
            self.search();
        }

        self.search = function() {
            active = true;
            var query = $("#search-query").blur().val();
            var requestResults = function() {
                var pageParam = $.param(page) ? "&" + $.param(page) : '';
                var filterParam = $("#search-form :input").filter(function(){ return $(this).val() }).serialize();
                var params = '?' + filterParam + pageParam;

                $.ajax({
                    type: 'GET',
                    url: searchEndpoint + params,
                    success: function (response) {
                        displaySearchResults(response);
                        updateGlobalDataLayer(query);

                        if ($("#redirect-indicator").size()) {
                            window.location = $("#redirect-indicator").data("redirect");
                        } else {
                        	replaceHistory(params);
                        }

                        active = false;
                    },
                    error: function (request, textStatus, errorThrown) {
                        console.error("error: " + errorThrown);
                    }
                });
            }


        	if (typeof nt_searchBox === "undefined" || nt_searchBox.goToPlaceIfExactMatch() === false) {
			    if (NtGeocode && query.indexOf("My location") !== 0) {
					NtGeocode.geocode(query, handleGeocodeResult(requestResults));
				} else {
					requestResults()
				}
			}
        };

        self.isActive = function() {
            return active;
        }

        self.bindSeeMoreButton = function() {
            $("#nt-see-more").unbind("click");
            $("#nt-see-more").on("click", function(searchSubmitButton) {
                page.pageIndex += page.pageSize;
                self.search();
                $(this).blur()
                searchSubmitButton.preventDefault();
            });
        }

        var displaySearchResults = function(response) {
            var resultsHtml = response;

            if (page.pageIndex == 0) {
                $("#search-result-cards").html(resultsHtml);
            } else {
                $("#search-result-cards").append(resultsHtml);
            }

            var displayedResults = $("#search-result-cards .nt-masonry-single-result").size();
            var hasResults = displayedResults !== 0
            var filterApplied = $('.nt-search-filters-tag').size() !== 0;
            var isLastPage = $("#last-page-indicator").size() !== 0;

            $("#nt-see-more").toggle(!isLastPage);
            $(".nt-show-with-results").toggle(hasResults);
            $(".nt-hide-with-results").toggle(!hasResults);
            $(".nt-hide-with-unfiltered-empty-results").toggle(hasResults || filterApplied);

            $('body').trigger('initialise-carousel');

            updateSearchTypeHrefs();

            $("#search-result-cards .nt-masonry-single-result").eq(page.pageIndex + 1).focus();
        };

        var replaceHistory = function(params) {
            window.history.replaceState({}, null, '/search' + params);
        }

        var handleGeocodeResult = function (callback) {
            return function(query, latitude, longitude) {
                // set hidden form fields
                if (latitude && longitude) {
                    $("#search-form-lat").val(latitude);
                    $("#search-form-lon").val(longitude);
                } else {
                    $("#search-form-lat").val("");
                    $("#search-form-lon").val("");
                }
                callback();
            };
        };

        var updateSearchTypeHrefs = function() {
            var params = {};
            var setParam = function(selector, prop) {
                if ($(selector).val()) {
                    params[prop] = $(selector).val();
                }
            }
            var updateHref = function(selector, type) {
                if ($("#search-form-type").val() !== type) {
                    $(selector).attr("href", "/search?type=" + type + (params ? "&" : "") + params);
                }
            }
            setParam("#search-query", "query");
            setParam("#search-form-lat", "lat");
            setParam("#search-form-lon", "lon");
            params = $.param(params);
            updateHref("#search-place", "place");
            updateHref("#search-event", "event");
            updateHref("#search-activity", "activity");
        };

        var updateGlobalDataLayer = function(query) {
            globalDataLayer.searchTerm = query;
            var appliedFilters = $("#search-form [name*=Filter]:checked").map(function() { return $(this).val() }).sort().get().join(';');
            if (appliedFilters) {
                globalDataLayer.filterCategories = appliedFilters
            }

            var displayedResults = $("#search-result-cards .nt-masonry-single-result").size();
			globalDataLayer.searchSuccess = displayedResults > 0 ? "True" : "False";
        }
    };

}(jQuery));

