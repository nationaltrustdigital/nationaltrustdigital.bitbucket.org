/**
 * This module is responsible for binding functionality to the search form
 */
 var nt_searchBox;
(function (NtGeocode, $) {
	nt_searchBox = new function() {
		var self = this;
		//configure autosuggest
		$('#search-query').nt_autosuggestPlaces({
			url: [['/autosuggest']]
		});

		// if we have an exact match, go to that URL
		self.goToPlaceIfExactMatch = function () {
			var searchType = $("#search-form-type").val();
			if (searchType === "place" && $.fn.nt_autosuggestPlaces && $.fn.nt_autosuggestPlaces.exactMatchUrl && $.fn.nt_autosuggestPlaces.exactMatchUrl !== "") {

				var placeFrom = {
					key: "fromSearchBox", value: JSON.stringify({
						placeName: $.fn.nt_autosuggestPlaces.exactMatchName,
						cameFrom: window.location.pathname
					})
				};

				localStorage.setItem(placeFrom.key, placeFrom.value);
				window.location.href = $.fn.nt_autosuggestPlaces.exactMatchUrl;
				return true;
			}
			return false;
		};

		//process a result from geocode lookup to extract lat and lon
		var handleGeocodeResult = function (query, latitude, longitude) {

			// set hidden form fields
			if (latitude && longitude) {
				$("#search-form-lat").val(latitude);
				$("#search-form-lon").val(longitude);
			} else {
				$("#search-form-lat").remove();
				$("#search-form-lon").remove();
			}

			// submit the form
			var searchForm = $("#search-form");
			searchForm.unbind("submit");
			searchForm.submit();
		};

		$('#search-query').focus(
			function () {
				if (navigator.geolocation) {
					$('#nt-location-search').addClass("nt-show-geolocation");
				}
			}
		);

		$('#search-query').blur(
			function () {
				if (navigator.geolocation) {
					setTimeout(function () {
						$('#nt-location-search').removeClass("nt-show-geolocation");
					}, 1000);
				}
			}
		);

		$("#search-getUsersLocation").click(function (event) {
			NtGeocode.getUsersLocation(
				function (geolocation) {
					$("#search-form-lat").val(geolocation.latitude);
					$("#search-form-lon").val(geolocation.longitude);
					$("#search-query").typeahead('val', "My location" + geolocation.name);
					$("#search-form").submit();
				}, // ok
				function (message) {
					$("#locationMessage").text("Please enable Location Services in your device settings or browser");
					$("#locationMessage").show();
				},// blocked
				function (message, code) {
					$("#locationMessage").text("We can't find your location at the moment. Try searching for a place instead.");
					$("#locationMessage").hide();
				} // error
			);
			return false;
		});

		// initial binding of the search form submission to capture the search so we can perform a geocode lookup first
		$("#search-form").submit(function (event) {

			if (typeof(Placeholders) !== "undefined") {
				Placeholders.disable();
			}

			var query = $("#search-query").val();

			//We might be able to shortcut this if the autocomplete gave us a url
			if (self.goToPlaceIfExactMatch() === false) {
				if (query.indexOf("My location") !== 0) {
					//We are not using the users location
					//We have not got an exact match, go a geocode lookup
					NtGeocode.geocode(query, handleGeocodeResult);
					event.preventDefault();
				}
				//We are using the users location. Submit the form
			} else {
				event.preventDefault();
			}
		});

		if ($.getParameterByName("geolocate")) {
			$("#search-form").submit();
		}
	};

}(NtGeocode, jQuery));

