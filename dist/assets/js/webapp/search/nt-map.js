'use strict';

var NtMap, ntMap;

(function (window) {

    NtMap = function (urlRoot) {
        if (!urlRoot) urlRoot = "";

        var CLUSTER_PIN_ICON = "/assets/img/44-location-searched-purple.png";
        var DEFAULT_PIN_ICON = "/assets/img/oakleaf.png";
        var SELECTED_PIN_ICON = "/assets/img/oakleaf-selected.png";
        var MAP_LOCATION_ICON = "/assets/img/location-searched.png";
        var CLOSE_ICON_URL = "/assets/img/nt-close-card-bk.png";
        var MIN_LAT_RADIUS = 0.01449; // approx. 1mi
        var MIN_LON_RADIUS = 0.024957258234454; // approx. 1mi
        var DEFAULT_MARKER_CONFIG = {maxZoom: 8};

        var canvas;
        var lastInfoWindow = null;
        var map = null;
        var smallScreen = false;
        var allPlaces = {};
        var self = this;
        var mapReadyTimeout;
        var bounceTimeout;
        var displayWhatsOn = false;
        var activitySearch = false;
        var filteredSearch = false;
        var markerClusterer;
        var lastMarker;
        var mapReady = false;
        var resizeScheduled = false;
        var mapConfig;
        var renderCard;
        var markers = [];

        // gets the map focus configuration provided in the main configuration object to the map
        var getMapFocus = function () {
            return mapConfig.focus || {};
        };

        // gets the map marker configuration, used to set clustering options
        var getMarkerConfig = function () {
            if (typeof(mapConfig.markerConfig) === 'undefined') {
                return DEFAULT_MARKER_CONFIG;
            }
            return mapConfig.markerConfig;
        };

        self.isMapReady = function () {
            return mapReady;
        }

        //initialize the map once all required libraries have been loaded
        self.init = function (places, mapCanvas, config, renderCardCallback) {

            if (mapReady) {
                initMap(places);
            } else {
                //Set in initMap
                //mapReady = true;

                canvas = mapCanvas;
                mapConfig = config;
                renderCard = renderCardCallback;
                nt_loadLibraries([
                    {path: "jQuery"},
                    {src: urlRoot + "/assets/js/webapp/nt-resize.js", path: "$.fn.nt_resize"},
                    {src: "https://maps.googleapis.com/maps/api/js?v=3\u0026key=AIzaSyDKIzOcpR-5CcJAWJjkEJWNz-_8W7lKsmY", path: "google"}
                ], function () {
                    nt_loadLibraries([{src: urlRoot + "/assets/js/webapp/search/markerclusterer-1.0.js", path: "MarkerClusterer"},
                            {src: urlRoot + "/assets/js/webapp/search/infobox-1.1.13.js", path: "InfoBox"}
                        ],
                        function () {
                            initMap(places);
                        }
                    );
                });
            }
        };

        self.updatePins = function (pins) {
            updateMarkers(pins);
        }

        self.getUrlRoot = function () {
            return urlRoot;
        }

        self.setIcon = function (placeId, icon) {
            allPlaces[placeId].icon = icon;
        }

        self.removeIcon = function (placeId) {
            delete allPlaces[placeId].icon;
        }

        self.triggerMapUpdatedEvent = function () {
            google.maps.event.trigger(map, 'map-updated');
        }

        self.triggerMapPannedEvent = function () {
            google.maps.event.trigger(map, 'map-panned');
        }

        self.triggerInfoWindowDisplayed = function () {
            google.maps.event.trigger(map, 'card-displayed');
        }

        self.updateRenderCardFunction = function (renderCardCallback) {
            renderCard = renderCardCallback;
        }

        var getPlaceIcon = function (placeId, defaultIcon) {
            if (allPlaces[placeId].icon) return allPlaces[placeId].icon;
            else return defaultIcon;
        }

        var clearAllMarkers = function () {
            markers.forEach(function (marker) {
                marker.setMap(null);
            });
            if (typeof markerClusterer !== 'undefined' && markerClusterer !== null) {
                markerClusterer.clearMarkers();
            }
            markers = [];
            allPlaces = {};
        }

        var getPinIconPath = function (place) {
            var iconPath;

            if (typeof mapConfig.pins !== 'undefined') {
                iconPath = mapConfig.pins[place.type];
            }

            if (typeof iconPath === 'undefined' || iconPath === null) {
                iconPath = DEFAULT_PIN_ICON;
            }

            return urlRoot + iconPath;
        }

        var getSelectedPinIconPath = function (place) {
            var iconPath;

            if (typeof mapConfig.pins !== 'undefined') {
                iconPath = mapConfig.pins['SELECTED_PIN_ICON'];
            }

            if (typeof iconPath === 'undefined' || iconPath === null) {
                iconPath = SELECTED_PIN_ICON;
            }

            return urlRoot + iconPath;
        }

        var isSelected = function (place) {
            var mapFocus = getMapFocus();

            if (typeof mapFocus.markers === 'object' && typeof mapFocus.markers.includes === 'function') {
                return mapFocus.markers.includes(place.id);
            } else if (typeof mapFocus.markers === 'string') {
                return place.id === mapFocus.markers;
            }
            return false
        }

        var updateMarkers = function (places) {
            var place, placeId;
            var markerConfig = getMarkerConfig();
            var optimizedMarkers = (markerConfig.optimized !== 'undefined' && markerConfig.optimized);

            clearAllMarkers();

            for (placeId in places) {
                place = places[placeId];
                if (typeof(place.location) !== "undefined" && place.location !== null && typeof(place.location.latitude) !== "undefined") {
                    allPlaces[placeId] = places[placeId];
                }
            }

            //Create a pin for each place and add an info window
            //Associate the pin marker with the place object
            for (placeId in allPlaces) {
                place = allPlaces[placeId];

                place.marker = new google.maps.Marker({
                    optimized: optimizedMarkers,
                    map: map,
                    icon: {
                        url: isSelected(place) ? getSelectedPinIconPath(placeId, getPinIconPath(place)) : getPlaceIcon(placeId, getPinIconPath(place)),
                        size: new google.maps.Size(37, 51)
                    },
                    title: place.title,
                    position: {lat: place.location.latitude, lng: place.location.longitude}
                });

                if (place.isFrontMarker) {
                    place.marker.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
                }

                // displayResults method handles markers where all are generated when map is initialised and event handler only used to unhide the object;
                // For dynamic maps, where clicking on the marker actually gets the content as well as display, the event handlers have to be added again each time the markers are affected.
                if (typeof markerConfig.isDynamicContent !== 'undefined' && markerConfig.isDynamicContent === true) {
                    setupDynamicInfoWindowListener(place);
                }

                markers.push(place.marker);
            }

            //Apply marker clustering
            clusterMarkers(markers);
        }

        var setMapBoundsToMarkers = function () {
            var place, placeId,
                bounds = new google.maps.LatLngBounds();

            for (placeId in allPlaces) {
                place = allPlaces[placeId];
                //Extend bounds to include this pin with minimum bounding box
                bounds.extend(new google.maps.LatLng(place.location.latitude - MIN_LAT_RADIUS, place.location.longitude - MIN_LON_RADIUS));
                bounds.extend(new google.maps.LatLng(place.location.latitude + MIN_LAT_RADIUS, place.location.longitude + MIN_LON_RADIUS));
            }

            //Apply bounds to map
            map.setCenter(bounds.getCenter());
            map.fitBounds(bounds);
        }

        var registerListeners = function (map) {
            map.addListener("idle", function () {
                self.triggerMapUpdatedEvent();
            });
            map.addListener("center_changed", function () {
                self.triggerMapPannedEvent();
            });
            if (typeof mapConfig.registerListeners === 'function') {
                mapConfig.registerListeners(map);
            }
        }

        var getCustomControls = function () {
            return mapConfig.controls || {};
        }

        var createCustomControls = function (map) {
            var controls = getCustomControls();

            for (var controlName in controls) {
                var controlConfig = controls[controlName];
                var controlDiv = document.createElement('div');
                var control = new controlConfig.control(controlDiv, self);

                controlDiv.index = 1;
                map.controls[google.maps.ControlPosition[controlConfig.position]].push(controlDiv);
            }
        }

        var initMap = function (places) {
            var mapFocus = getMapFocus();
            if (window.matchMedia) {
                if (window.matchMedia("screen and (max-device-height: 599px)").matches) {
                    smallScreen = true;
                }
            }

            if (typeof(nt_eventResults) !== 'undefined' && nt_eventResults === true) {
                self.setDisplayWhatsOn(true);
            } else {
                self.setDisplayWhatsOn(false);
            }
            if (typeof(nt_activityResults) !== 'undefined' && nt_activityResults === true) {
                self.setActivitySearch(true);
            } else {
                self.setActivitySearch(false);
            }
            if (typeof(nt_isFilteredSearch) !== 'undefined' && nt_isFilteredSearch === true) {
                self.setFilteredSearch(true);
            } else {
                self.setFilteredSearch(false);
            }

            //Create the google map
            map = new google.maps.Map(
                canvas,
                {
                    center: {lat: 53.93, lng: -2.33},
                    zoom: 6,
                    fullscreenControl: true,
                    fullscreenControlOptions: {
                        position: google.maps.ControlPosition.LEFT_BOTTOM
                    }
                }
            );

            map.addListener("click", function (location) {
                if (lastInfoWindow !== null) {
                    lastInfoWindow.close();
                }
            });

            createCustomControls(map);

            registerListeners(map);

            updateMarkers(places);

            if (mapFocus.fitMapToPins) {
                setMapBoundsToMarkers();
            }

            initUserInteraction();
            self.resize();

            mapReady = true;

            if (typeof(mapFocus.title) !== "undefined" && mapFocus.title !== null) {
                // TODO - maybe getGeolocationURLParameters() can be replaced by mapFocus.location
                self.placeMarker(NtMap.getGeolocationURLParameters(), mapFocus.title, true);
            }
        };

        var initUserInteraction = function () {

            $(window).resize(function () {
                if (!resizeScheduled) {
                    resizeScheduled = true;
                    setTimeout(function () {
                        resizeScheduled = false;
                        self.resize();
                    }, 500);
                }
            });

            self.setScrollWheel(false);

            google.maps.event.addListener(self.getMap(), 'mousedown', function () {
                self.setScrollWheel(true);
            });

            $('body').on('mousedown', function (event) {
                var clickedInsideMap = $(event.target).parents("#" + canvas.id).length > 0;

                if (!clickedInsideMap) {
                    self.setScrollWheel(false);
                }
            });

            $(window).scroll(function () {
                self.setScrollWheel(false);
            });
        };

        //Create an info window for a place with content
        var setupDynamicInfoWindowListener = function (place) {

            var displayInfoWindow = function (dynamicContent) {

                if (lastInfoWindow !== null) {
                    lastInfoWindow.close();
                }

                var cardMaxWidth = dynamicContent.cardOptions.maxWidth;

                var infoOptions = {
                    position: new google.maps.LatLng(place.location.latitude, place.location.longitude),
                    pixelOffset: new google.maps.Size(-(cardMaxWidth / 2), -60),
                    infoBoxClearance: new google.maps.Size(1, 1),
                    isHidden: false,
                    pane: "floatPane",
                    alignBottom: true,
                    disableAutoPan: false,
                    enableEventPropagation: false,
                    closeBoxMargin: "2px 2px 2px 2px",
                    closeBoxURL: urlRoot + CLOSE_ICON_URL,
                    content: dynamicContent.html,
                    boxStyle: {
                        width: "100%",
                        maxWidth: "" + cardMaxWidth + "px",
                        backgroundColor: "white"
                    }
                };

                var infoWindow = new InfoBox(
                    infoOptions
                );

                infoWindow.open(map, place.marker);
                infoWindow.addListener("domready", function () {
                    self.triggerInfoWindowDisplayed();
                	$("#map-canvas").trigger("info-box-opened");
                });
                place.marker.setAnimation(google.maps.Animation.BOUNCE);
                bounceTimeout = setTimeout(function () {
                    place.marker.setAnimation(null);
                }, 2800);
                lastInfoWindow = infoWindow;
                map.panTo(place.marker.getPosition());
            };

            var successCallback = function (dynamicContent) {
                displayInfoWindow(dynamicContent);
            };

            var failCallback = function (dynamicContent) {
                displayInfoWindow(dynamicContent);
            };

            // Set infoWindow to open on clicking on place
            place.marker.addListener('click',
                function () {
                    renderCard(place, {
                        successCallback: successCallback,
                        failCallback: failCallback,
                        placeSearchMapAnalyticsCode: 'PlaceMapClick'
                    });
                });
        };

        //Create an info window for a place with content
        var setInfoWindow = function (place, content) {
            var cardMaxWidth = content.cardOptions.maxWidth;
            var infoOptions = {
                position: new google.maps.LatLng(place.location.latitude, place.location.longitude),
                pixelOffset: new google.maps.Size(-(cardMaxWidth / 2), -60),
                infoBoxClearance: new google.maps.Size(1, 1),
                isHidden: false,
                pane: "floatPane",
                alignBottom: true,
                disableAutoPan: false,
                enableEventPropagation: false,
                closeBoxMargin: "2px 2px 2px 2px",
                closeBoxURL: urlRoot + CLOSE_ICON_URL,
                content: content.html,
                boxStyle: {
                    width: "100%",
                    maxWidth: "" + cardMaxWidth + "px",
                    backgroundColor: "white"
                }
            };
            var infoWindow = new InfoBox(
                infoOptions
            );
            // Set infoWindow to open on clicking on place
            place.marker.addListener('click',
                function () {
                    if (lastInfoWindow !== null) {
                        lastInfoWindow.close();
                    }

                    infoWindow.open(map, place.marker);
                    place.marker.setAnimation(google.maps.Animation.BOUNCE);
                    bounceTimeout = setTimeout(function () {
                        place.marker.setAnimation(null);
                    }, 2800);
                    lastInfoWindow = infoWindow;
                    map.panTo(place.marker.getPosition());
                    infoWindow.addListener("domready", function () {
						$("#map-canvas").trigger("info-box-opened");
					});
                }
            );

        };


        //Apply marker clustering
        var clusterMarkers = function (markers) {
            var markerConfig = getMarkerConfig();
            markerClusterer = new MarkerClusterer(map, markers,
                {
                    gridSize: 50,
                    maxZoom: markerConfig.maxZoom,
                    styles: [{
                        url: urlRoot + CLUSTER_PIN_ICON,
                        height: 32,
                        width: 32,
                        textColor: "white"
                    }]
                }
            );
        };

        self.resize = function () {
            if (!mapReady) {
                mapReadyTimeout = setTimeout(function () {
                    self.resize();
                }, 250);
                return;
            }

            var mapFocus = getMapFocus();

            var height = NtMap.calculateHeight();
            canvas.style.height = height + "px";
            google.maps.event.trigger(map, "resize");

            if (typeof mapFocus !== 'undefined' && mapFocus.markers && mapFocus.markers !== null) {
                if (typeof mapFocus.markers === "object") {
                    if (mapFocus.markers.length && mapFocus.markers.length > 0) {
                        self.displayResults(mapFocus.markers, mapFocus.location);
                    }
                } else {
                    self.centerOnPlace(mapFocus.markers);
                }
            }
        };

        self.setSmallScreen = function (state) {
            smallScreen = state;
        };

        self.setDraggable = function (state) {
            if (!mapReady) {
                mapReadyTimeout = setTimeout(function () {
                    self.setDraggable(state);
                }, 250);
                return;
            }
            map.setOptions({draggable: state});
        };

        self.setScrollWheel = function (state) {
            if (!mapReady) {
                mapReadyTimeout = setTimeout(function () {
                    self.setScrollWheel(state);
                }, 250);
                return;
            }
            map.setOptions({scrollwheel: state});
        };

        self.setDisplayWhatsOn = function (state) {
            displayWhatsOn = state;
        };

        self.setActivitySearch = function (state) {
            activitySearch = state;
        };

        self.setFilteredSearch = function (state) {
            filteredSearch = state;
        };

        self.centerOnPlace = function (placeId) {
            if (!mapReady) {
                mapReadyTimeout = setTimeout(function () {
                    self.centerOnPlace(placeId);
                }, 250);
                return;
            }

            var place = allPlaces[placeId];
            self.displayResults([placeId], place.location);
        };

        //Display a list of place ids on the map and set map to show the set of places
        self.displayResults = function (placeIds, mapCenter, applyBounds) {
            if (!mapReady) {
                mapReadyTimeout = setTimeout(function () {
                    self.displayResults(placeIds, mapCenter);
                }, 250);
                return;
            }

            var place;
            var placeId;
            var bounds = new google.maps.LatLngBounds();
            var filterDisplayedPlaces = displayWhatsOn || activitySearch || filteredSearch;

            //Set all places to use the default pin
            for (placeId in allPlaces) {
                place = allPlaces[placeId];
                place.marker.setIcon(getPlaceIcon(placeId, getPinIconPath(place)));
                place.selected = false;

                if (filterDisplayedPlaces) {
                    place.marker.setVisible(false);
                }
            }
            if (filterDisplayedPlaces) {
                markerClusterer.clearMarkers();
            }

            //Set selected places to use the selected pin
            for (var i = 0; i < placeIds.length; i++) {
                placeId = placeIds[i];
                place = allPlaces[placeId];
                if (typeof(place) !== "undefined") {

                    if ($('#places-nearby-map').length) {
                        place.marker.setIcon(getPlaceIcon(placeId, getSelectedPinIconPath(place)));
                    }
                    if (filterDisplayedPlaces) {
                        place.marker.setVisible(true);
                        markerClusterer.addMarker(place.marker);
                    }
                    place.selected = true;
                    //Extend bounds to include this pin with minimum bounding box
                    bounds.extend(new google.maps.LatLng(place.location.latitude - MIN_LAT_RADIUS, place.location.longitude - MIN_LON_RADIUS));
                    bounds.extend(new google.maps.LatLng(place.location.latitude + MIN_LAT_RADIUS, place.location.longitude + MIN_LON_RADIUS));
                }
            }

            //Set all place infoWindow content
            for (placeId in allPlaces) {
                place = allPlaces[placeId];
                if (typeof mapConfig.markerConfig.isDynamicContent === 'undefined' || mapConfig.markerConfig.isDynamicContent === false) {
                    setInfoWindow(place, renderCard(place, {
                        smallScreen: smallScreen,
                        displayWhatsOn: displayWhatsOn,
                        activitySearch: activitySearch,
                        eventSearchMapAnalyticsCode: 'EventMapClick',
                        placeSearchMapAnalyticsCode: 'PlaceMapClick',
                        activitySearchMapAnalyticsCode: 'ActivityMapClick'
                    }));
                }
            }

            if (typeof(mapCenter) !== "undefined" && mapCenter !== null) {
                //Center map
                map.setCenter(new google.maps.LatLng(mapCenter.latitude, mapCenter.longitude));
                map.setZoom(10);
            } else if (placeIds.length == 0) {
                map.setCenter(new google.maps.LatLng(54, -3));
                map.setZoom(6);
            } else if (typeof applyBounds == "undefined" || applyBounds == true) {
                //Apply bounds to map
                map.fitBounds(bounds);
            }
        };

        self.placeMarker = function (position, title, centerOnPosition) {
            if (!mapReady) {
                mapReadyTimeout = setTimeout(function () {
                    self.placeMarker(position, title, centerOnPosition);
                }, 50);
                return;
            }

            lastMarker = new google.maps.Marker({
                position: {lat: position.latitude, lng: position.longitude},
                map: map,
                title: title,
                icon: MAP_LOCATION_ICON
            });

            if (centerOnPosition) {
                map.setCenter(new google.maps.LatLng(position.latitude, position.longitude));
            }
        };

        self.getLastMarker = function () {
            return lastMarker;
        };

        self.getMap = function () {
            return map;
        };

        self.allPlaces = function () {
            return allPlaces;
        };

        self.focusedPlaces = function () {
            return getMapFocus();
        }
    };

    NtMap.calculateHeight = function () {
        var HEADER_HEIGHT = 120,
            MAX_HEIGHT = 640,
            heightMinusHeader = window.screen.height - HEADER_HEIGHT;

        return Math.min(heightMinusHeader, MAX_HEIGHT);
    };

    NtMap.getGeolocationURLParameters = function () {
        var getParameterByName = function (name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            if (results === null) return;
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }

        var lat = getParameterByName("lat");
        var lon = getParameterByName("lon");
        if (typeof(lat) === "undefined" || typeof(lon) === "undefined" || lat === "" || lon === ""
        ) return;
        return {latitude: parseFloat(lat), longitude: parseFloat(lon)};
    };

    NtMap.cardTemplate = "";

    // renders a place card on the map.
    // options.openInNewTab: if true, links will open a new tab, otherwise links open in the current window
    NtMap.renderPlaceCard = function (place, options) {
        var getPlacePageUrl = function (place) {
            return place.websiteUrl;
        };

        var trimTrailingSlash = function (url) {
            return (url[url.length - 1] === '/') ? url.substr(0, url.length - 1) : url;
        };

        var getEventsPageUrl = function (place) {
            return trimTrailingSlash(place.websiteUrl) + '/whats-on';
        };

        var getActivitiesPageUrl = function (place) {
            return trimTrailingSlash(place.websiteUrl) + '/activities';
        };
        // converts a javascript map into array of individual query string params URL encoded
        // e.g. The following input:
        //
        // {'key1': 'abc', 'key2': 'def', 'key3': 'g h i'}
        //
        // returns
        //
        // ["key1=abc", "key2=def", "key3=g%20h%20i"]
        var mapToQueryStringParams = function (queryStringMap) {
            return $.map(queryStringMap, function (value, key) {
                return encodeURIComponent(key) + '=' + encodeURIComponent(value);
            });
        };

        // append query string parameters into a single query string
        // e.g.
        // Input
        //
        // ["key1=abc", "key2=def", "key3=g%20h%20i"]
        //
        // Output:
        //
        // &key1=abc&key2=def&key3=g%20h%20i
        var concatenateQueryStringParams = function (queryStringParams) {
            var queryString = '';
            $.each(queryStringParams, function (i, value) {
                queryString += '&' + value;
            });
            return queryString;
        };

        // given a url, and an object map of query string parameters and corresponding values
        // returns a new url that has the query string parameters appended
        // e.g.
        // url: http://www.nationaltrust.org.uk/places/killerton
        // queryStringMap: {'EventMapClick', 'Killerton'}
        //
        // returns: http://www.nationaltrust.org.uk/places/killerton?EventMapClick=Killerton
        var augmentUrlWithAdditionalQueryStringParams = function (url, queryStringMap) {
            // anchor tag converts the URL into a Location object
            var a = document.createElement('a'),
                queryString = concatenateQueryStringParams(mapToQueryStringParams(queryStringMap));
            a.href = url;

            // append query string params
            if (queryString !== '') {
                if (a.search === '') {
                    a.search = queryString.substring(1);
                } else {
                    a.search = queryString;
                }
            }

            return a.toString();
        };

        // Add the appropriate place/event search analytic code to the URL's query string
        var getAugmentedUrlWithAnalyticsCode = function (url, place) {
            // the analytic code depends upon the type of map displayed on the page
            var queryStringMap = {},
                analyticsCode = options.placeSearchMapAnalyticsCode;

            if (options.displayWhatsOn) {
                analyticsCode = options.eventSearchMapAnalyticsCode;
            }
            if (options.activitySearch) {
                analyticsCode = options.activitySearchMapAnalyticsCode;
            }

            queryStringMap[analyticsCode] = place.id;

            return augmentUrlWithAdditionalQueryStringParams(url, queryStringMap);
        };

        //Converts a boolean into show or hide class
        var toDisplay = function (state) {
            if (state === true) {
                return "";
            } else {
                return "hidden";
            }
        };

		var id = place.id;
        var title = place.title;
        var subTitle = place.subTitle;
        var description = place.description;
        var openingTimeStatus = place.openingTimeStatus;
        var openingTimeUrl = '/place-pages/' + place.id + '/pages/opening-times-calendar';
        var imageUrl = place.imageUrl;
        var imageDescription = place.imageDescription;
        var activityTags = place.activityTagsAsCsv;
        var placePageUrl = getAugmentedUrlWithAnalyticsCode(getPlacePageUrl(place), place);
        var eventsPageUrl = getAugmentedUrlWithAnalyticsCode(getEventsPageUrl(place), place);
        var activitiesPageUrl = getAugmentedUrlWithAnalyticsCode(getActivitiesPageUrl(place), place);
        var linkTarget = options.openInNewTab ? '_blank' : '';

        var htmlString = NtMap.cardTemplate.replace(/{{placeId}}/g, id).replace(/{{title}}/g, title).replace(/{{subTitle}}/g, subTitle).replace(/{{description}}/g, description);
		htmlString = htmlString.replace(/{{openingTimeStatus}}/g, openingTimeStatus).replace(/{{openingTimeUrl}}/g, openingTimeUrl);
        htmlString = htmlString.replace(/{{imageUrl}}/g, imageUrl).replace(/{{imageDescription}}/g, imageDescription);
        htmlString = htmlString.replace(/{{activityTags}}/g, activityTags).replace(/{{placePageUrl}}/g, placePageUrl).replace(/{{eventsPageUrl}}/g, eventsPageUrl);
        htmlString = htmlString.replace(/{{displayWhatsOnAndSelected}}/g, toDisplay(options.displayWhatsOn && place.selected));
        htmlString = htmlString.replace(/{{activitySearchAndSelected}}/g, toDisplay(options.activitySearch && place.selected));
        htmlString = htmlString.replace(/{{hasOpeningTimeStatus}}/g, toDisplay(typeof openingTimeStatus !== "undefined" && openingTimeStatus !== null));
        htmlString = htmlString.replace(/{{hasActivityTags}}/g, toDisplay(typeof(activityTags) !== "undefined" && activityTags !== null));
        htmlString = htmlString.replace(/{{activitiesPageUrl}}/g, activitiesPageUrl).replace(/{{linkTarget}}/g, linkTarget);
        htmlString = htmlString.replace(/{{optionsSmallScreen}}/g, toDisplay(!options.smallScreen));

        return {
            html: htmlString,
            cardOptions: {
                maxWidth: 400
            }
        };
    };

    NtMap.getAllPlacePins = function (callback, urlRoot) {
        if (!urlRoot) urlRoot = "";
        $.ajax({
            dataType: "json",
            crossDomain: true,
            url: urlRoot + "/search/data/all-places",
            data: "",
            success: function (response) {
                callback(response);
            }
        });
    };

    /**
     * National Trust Place specific map
     * @param canvasId id of the map canvas container
     * @param currentFocus single marker id, or an array of marker ids
     * @param withAllPlaces boolean representing whether all national trust places should be displayed
     * @param myLocationMarkerTitle optional title describing the user's current location
     * @deprecated use NtMap.launchCustomMap
     */
    NtMap.launchMap = function (canvasId, currentFocus, withAllPlaces, myLocationMarkerTitle) {

        var config = {
            focus: {
                location: NtMap.getGeolocationURLParameters(),
                markers: currentFocus,
                title: myLocationMarkerTitle,
                fitMapToPins: false
            },
            markerConfig: {
                maxZoom: 8
            },
            isUpdatableMap: false
        };

        var getAllPlacePinsCallback = function (callback, urlRoot) {
            if (withAllPlaces) {
                NtMap.getAllPlacePins(callback, urlRoot);
            } else {
                callback([]);
            }
        }


        return NtMap.launchCustomMap(canvasId, config, getAllPlacePinsCallback, NtMap.renderPlaceCard);
    };

    /**
     * More flexible alternative to NtMap.launchMap that allows custom cards and data for different marker types. For example, holiday cottages.
     *
     * @param canvasId id of the map canvas container
     * @param config is an object containing a focus object: marker (single id or array of ids), or a location (lat/lon), and an optional title.
     *        and a markerConfig object containing configuration parameters for the markers.
     *        e.g. { focus : { location: {latitude: 51.0, longitude: 1.2}, markers: ['1', '2'], title: 'My location, Swindon' }, markerConfig : { maxZoom : 21 }}
     * @param getAllPinsCallback is a function that will call to a callback function with an array of map marker data, e.g. National Trust places.
     * @param renderCardCallback is a function that understands how to render the html for a single card for a map marker. It is supplied the marker object
     * @returns an initialised map, either the pre-initialised map, or a new one
     */
    NtMap.launchCustomMap = function (canvasId, config, getAllPinsCallback, renderCardCallback, root) {
        if (ntMap) {
            if (config.isUpdatableMap) {
                ntMap.updateRenderCardFunction(renderCardCallback);
                getAllPinsCallback(function (pins) {
                    ntMap.updatePins(pins);
                    // fire the event used to identify that the map has finished updating
                    ntMap.triggerMapUpdatedEvent();
                });
            }
            return ntMap;
        }

        ntMap = new NtMap(root);

        var mapCanvas = document.getElementById(canvasId);

        getAllPinsCallback(function (data) {
                ntMap.init(data, mapCanvas, config, renderCardCallback);
            },
            ntMap.getUrlRoot());

        return ntMap;
    };

    NtMap.getConfigBuilder = function (config) {
        var Builder = function (config) {
            var self = this;
            if (!config) config = {};

            self.withAllPlacesControl = function () {
                var controls = getControls();

                controls.togglePlacesMapControl = {
                    control: toggleNtPlacesControl,
                    position: 'BOTTOM_CENTER'
                };
                return self;
            }

            self.withRecentreControl = function () {
                var controls = getControls();

                controls.recentreMapControl = {
                    control: reCentreMapControl,
                    position: 'TOP_LEFT'
                };
                return self;
            }

            var getControls = function () {
                if (!config.controls) {
                    config.controls = {};
                }
                return config.controls;
            }

            var reCentreMapControl = function (controlDiv, ntMap) {
                var controlUI = $('<div class="nt-map-control nt-button-map-control nt-map-control-recentre" title="Click to recentre the map"><div class="nt-map-control-text">Recentre</div></div>');

                $(controlDiv).append(controlUI);

                // Setup the click event listener.
                $(controlUI).on('click', function () {
                    ntMap.resize();
                });
            };

            var toggleNtPlacesControl = function (controlDiv, ntMap) {
                var controlUI = $('<div class="nt-map-control nt-map-control-toggle-places" title="Show and hide National Trust places on the map">' +
                    '<input id="nt-place-toggle" class="nt-place-toggle" type="checkbox"/>' +
                    '<label class="nt-map-control-text" for="nt-place-toggle">Show National Trust places</label>' +
                    '</div>');

                $(controlDiv).append(controlUI);

                var allPlaces = {};
                NtMap.getAllPlacePins(function (response) {
                    allPlaces = response;
                });
                var focusedPlaces = ntMap.focusedPlaces().markers;

                // Setup the click event listener.
                $('body').on('change', '.nt-place-toggle', function () {
                    if (this.checked) {
                        ntMap.updatePins(allPlaces);
                    } else {
                        ntMap.displayResults(focusedPlaces);
                    }
                });
            };

            self.build = function () {
                return config;
            }
        };
        return new Builder(config);
    };

    /**
     * Required for tests
     **/
    NtMap.clearInstance = function () {
        ntMap = null;
    }
}(window));