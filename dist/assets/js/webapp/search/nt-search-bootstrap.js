/**
 * This module will progromatically load libraries required for search, but only if they do not exist already.
 * 
 * Some libraries are dependant on others being present before they can load (e.g. google).
 * This is handled using the post load callback feature of nt_loadLibraries
 */
var NtSearchBootstrap = {initialised: false};

(function () {
  var bindEvent = function (element, type, handler) {
    if (element.addEventListener) {
      element.addEventListener(type, handler, false);
    } else {
      element.attachEvent('on' + type, handler);
    }
  };

  var markModuleAsInitialised = function () {
	  NtSearchBootstrap.initialised = true;
  };

  var libsLoading = false;

  var loadLibs = function () {
	  if (libsLoading){
		  return;
	  } else {
		  libsLoading = true;
	  }

	  nt_loadLibraries([
			  {path: "jQuery"},
		  		{src: "https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyDKIzOcpR-5CcJAWJjkEJWNz-_8W7lKsmY", path: "google"}
		  ], function () {
			  nt_loadLibraries([
				  {src: "/assets/js/lib/typeahead.jquery.min.js", path: "$.fn.typeahead"},
				  {src: "/assets/js/lib/xregexp-all-min.js", path: "XRegExp"},
				  {src: "/assets/js/webapp/search/nt-autosuggest-places.js", path: "$.fn.nt_autosuggestPlaces"},
				  {src: "/assets/js/webapp/search/nt-geocode-module.min.js", path: "NtGeocode"}
			  ],
				  function () {
					  nt_loadLibraries([{src: "/assets/js/webapp/search/nt-search-box.min.js"}], markModuleAsInitialised);
				  }
			  );
		  }
	  );
  };
  bindEvent(document, 'readystatechange', loadLibs);
}());
