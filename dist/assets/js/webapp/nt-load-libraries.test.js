'use strict';
var requirejs = require("requirejs");
require.main.require('develop/js/mocha-tests/common-mocha-test-header.js');

global.window = {};

suite('Test nt_loadLibraries', function () {
    var nt_loadLibraries;

    suiteSetup(function (done) {

        requirejs("window");
        nt_loadLibraries = NtTest.loadScript('develop/js/webapp/nt-load-libraries.js', 'nt_loadLibraries');
        done();
    });

    suite('nt_loadLibraries', function () {

        test('has expected functions', function () {
            NtTest.hasFunctions(
                nt_loadLibraries ,
                [] ,
                {strict: true, exclude: []}
            );
        });

        test('loads a library that is not loaded', function () {
            var called = false;
            var onLoadFunction = function () {
                called = true;
            };

            nt_loadLibraries([{src: "mySource", path: "myPath"}], onLoadFunction);
            global.myPath = true;

            setTimeout(function () {
                if (window.mock.hasBeenCalled("createElement") === false) {
                    throw new Error("mySource was not loaded");
                }
                if (called === false) {
                    throw new Error("on load function was not called");
                }
            }, 1000);
        });

        test('does not load a library that is already loaded', function () {
            var called = false;
            var onLoadFunction = function () {
                called = true;
            };

            global.myPath = true;

            nt_loadLibraries([{src: "mySource", path: "myPath"}], onLoadFunction);

            if (window.mock.hasBeenCalled("createElement") === true) {
                throw new Error("mySource was loaded");
            }
            if (called === false) {
                throw new Error("on load function was not called");
            }
        });
    });
})
;