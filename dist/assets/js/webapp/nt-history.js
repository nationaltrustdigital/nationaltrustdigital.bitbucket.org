"use strict";

/**
 * Library to track a users page history through the site by capturing cid or place ids
 *
 * @author Sam blackwell
 */

define("NtHistory",["NtClientStorage"], function (NtClientStorage) {

    //empty constructor to attach functions to prototype
    function NtHistory() {
    }

    //the cookie name for which the stringified array of cids will be stored
    var key = "pagesVisited";

    //the limit of the page history we are tracking.
    var historyLimit = 20;

    /**
     *
     * @param result the array to pass through to have oldest contents removed
     * @returns an array with a defined number of entries with the oldest records removed
     */
    var removeOldestWhenLengthOverLimit = function (result) {
        if (result.length > historyLimit) {
            result = result.slice(result.length - historyLimit, result.length);
        }
        return result;
    };

    /**
     * Checks for existance of the globalDataLayer
     * @returns {boolean} true if the globalDataLayer exists
     */
    var shouldCapturePageHistory = function () {
        var isValid = false;
        if (typeof globalDataLayer !== "undefined" && globalDataLayer !== null && (globalDataLayer.taxonomy)) {
                isValid = true;
            }
        return isValid;
    };

    /**
     * function to add pages cid/placeId to the history array
     */
    var writeHistoryToCookie = function (cookieExpiry) {
        if (shouldCapturePageHistory()) {

            if (globalDataLayer.contentType && globalDataLayer.contentType === "PlaceHomePage") {
                NtClientStorage.pushData(key, globalDataLayer.propertyID, NtClientStorage.StorageOptionEnum.COOKIE, removeOldestWhenLengthOverLimit, "/", cookieExpiry);
            }
            else {
                NtClientStorage.pushData(key, globalDataLayer.cid, NtClientStorage.StorageOptionEnum.COOKIE, removeOldestWhenLengthOverLimit, "/", cookieExpiry);
            }
        }
    };

    return new function () {
        this.writeHistoryToCookie = writeHistoryToCookie;
    };

});