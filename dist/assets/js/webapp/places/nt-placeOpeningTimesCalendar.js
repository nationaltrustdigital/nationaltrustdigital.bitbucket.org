(function ($) {
	"use strict";

	var clearAllSelections = function () {
		// hide opening times for any displayed day
		$('main tr.nt-opening-times-calendar-day-details').hide();
		// deselect any selected days
		$("main td.nt-calendar-modal-day-cell").removeClass("calendar-modal-active calendar-modal-expanded");
	};

	var showOpeningTimes = function (day) {
		$('main tr.nt-opening-times-calendar-day-details[data-nt-day="' + day + '"]').show();
	};

	var highlightDay = function (day) {
		$('main td.nt-calendar-modal-day-cell[data-nt-day="' + day + '"]').addClass('calendar-modal-active calendar-modal-expanded');
		showOpeningTimes(day);
	};

	var selectDefaultDay = function () {
		var today = $('.calendar-modal-days-today').data('nt-day');
		clearAllSelections();
		highlightDay(today);
	};

	var buildEventHandler = function (params) {
		/*
		 * The function arguments (params) are cached in this closure.
		 * params is the optional parameters object passed into the plugin as per plugin standards.
		 * All events using this function use the same closure, and therefore the same params.
		 */
		return function (event) {
			var target = $(event.target);
			var expandedCellReclicked;
			/*
			 * if the click target was the anchor tag's inner span (if the user clicked directly on the day of month in the cell corner),
			 * make the anchor tag the click target. The anchor has class nt-calendar-modal-selectable.
			 * If this is a non selectable cell with no anchor tag, we will either end up on an inner span or the <td>,
			 * in which case the if test below will weed it out as it won't have class nt-calendar-modal-selectable.
			 */
			target = target.hasClass("nt-calendar-modal-selectable") ? target : target.parent();

			if (target.hasClass("nt-calendar-modal-selectable")) {
				expandedCellReclicked = target.parent().hasClass("calendar-modal-active");

				if (!expandedCellReclicked) {
					clearAllSelections();
					highlightDay(target.data('nt-day'));
				}
			}
		};
	};

	$.fn.nt_placeOpeningTimesCalendar = function (params) {

		//Open today's opening times on page load
		selectDefaultDay();

		$('.month-nav').off().on('click', function (event) {
			var displayedMonth = $(this).parents('.month-cal');
			var monthToShow = $($(this).attr('href'));

			displayedMonth.addClass('hidden');
			monthToShow.removeClass('hidden');
			event.preventDefault();
		});

		return this.each(function () {
			var f = buildEventHandler(params);
			$(this).click(f);
		});
	};

}(jQuery));

$('#nt-opening-times-calendar').nt_placeOpeningTimesCalendar({});
