/*
 * This plugin controls 'see more' functionality
 * It takes the following object literal parameters:-
 * 
 * url                 - url of end point to get next fragment
 * lastLoaded:           Note that a .last() is done internally in case there are multiple elements in the result,
 *                       so you don't need to do an (inefficient) :last on the selector
 *     .selector       - selector for element holding 'last fragment loaded' reference
 *     .attribute      - attribute containing the 'last fragment loaded' reference (which might be e.g. year-month)
 *     .pathVariable   - path variable placeholder name in url for 'last fragment loaded' reference
 *                       Note that this should be enclosed with {} in the url.
 *                       If both pathVariable and request Param are specified, pathVariable takes precedence and requestParam is ignored.
 *     .requestParam   - url parameter name for 'last fragment loaded' reference
 * containerSelector   - container selector - fragment will be inserted after last child in this container
 * noMoreDataSelector  - selector for element(s) tagged with 'no more data after this' (used to disable see more button)
 */
(function($) {
	$.fn.nt_seeMore = function(params) {
		return this.each(function() {
			var f = buildEventHandler(params);
			$(this).click(f);
		});
	};

	function buildEventHandler(params) {
		/*
		 * The function arguments (params) are cached in this closure. params is
		 * the parameters object passed into the plugin. All events using this
		 * function use the same closure, and therefore the same params.
		 */
		var f = function(event) {
			var target = $(event.target);

			var urlParams = {};
			var url = params.url;
			 // get the last element in case multiple elements are in the result.
			encodedAttributeValue = encodeURIComponent($(params.lastLoaded.selector).last().attr(params.lastLoaded.attribute));			
			if (params.lastLoaded.pathVariable) {
				url = url.replace("{" + params.lastLoaded.pathVariable + "}", encodedAttributeValue);
			} else {				
				urlParams[params.lastLoaded.requestParam] = encodedAttributeValue;
			}
			// get next fragment and insert into specified container
			getAndInsertFragment(target, params, url, urlParams)

			event.preventDefault();
		};
		return f;
	}
	
	function getAndInsertFragment(target, params, url,  urlParams) {
		$.get(url, urlParams, function(fragment) {
			$(params.containerSelector).append($(fragment));
			updateButtonState(target, params);
		});
	}

	function updateButtonState(target, params) {
		if ($(params.noMoreDataSelector).length) {
			$(target).hide();
		}
	}

}(jQuery));
