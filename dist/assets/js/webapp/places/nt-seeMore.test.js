'use strict';
var requirejs = require("requirejs");
require.main.require('develop/js/mocha-tests/common-mocha-test-header.js');
var should = require("should");

suite('Test nt_seeMore', function () {

    suiteSetup(function (done) {
        NtTest.loadScript('develop/js/webapp/places/nt-seeMore.js');
        done();
    });

    suite('nt_seeMore', function () {

        test('has expected functions', function () {
            NtTest.hasFunction($.fn, 'nt_seeMore', ['params']);
        });
    });
});