'use strict';
var requirejs = require("requirejs");
require.main.require('develop/js/mocha-tests/common-mocha-test-header.js');
var should = require("should");

suite('Test nt_placeOpeningTimesCalendar', function () {

    suiteSetup(function (done) {
        jquery.mock.addInternal("nt_placeOpeningTimesCalendar", function () {
        });
        NtTest.loadScript('develop/js/webapp/places/nt-placeOpeningTimesCalendar.js');
        done();
    });

    suite('nt_placeOpeningTimesCalendar', function () {

        test('has expected functions', function () {
            NtTest.hasFunction($.fn, 'nt_placeOpeningTimesCalendar', ['params']);
        });
    });
});