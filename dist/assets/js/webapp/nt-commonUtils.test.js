'use strict';
var requirejs = require("requirejs");
require.main.require('develop/js/mocha-tests/common-mocha-test-header.js');
var should = require("should");

suite('Test ntCommonUtils', function () {
    var nt_loadLibraries;

    suiteSetup(function (done) {
        requirejs("jquery");
        requirejs(['ntCommonUtils'],
            function (mod) {
                done();
            });
    });

    function parametersCount(param) {
        var count = 0;
        for (var key in param) {
            if (typeof param[key] === "object") {
                count += param[key].length;
            } else {
                count++;
            }
        }
        return count;
    }

    suite('ntCommonUtils', function () {

        test('has expected functions', function () {

            NtTest.hasFunctions($, [
                {methodName: 'getURLParameters', arguments: ['url']},
                {methodName: 'setURLParameters', arguments: ['url', 'param']},
                {methodName: 'getParameterByName', arguments: ['name']}
                ]
            );
        });

        test('parameters returned from url', function () {
            //Given
            var url = "http://www.nationaltrust.org.uk/search?query=1&lat=2&lng=3";

            //When
            var param = $.getURLParameters(url);

            //Then
            should(param.query).be.equal('1');
            should(param.lat).be.equal('2');
            should(param.lng).be.equal('3');

        });

        test('no parameters returned from url without parameters', function () {
            //Given
            var url = "http://www.nationaltrust.org.uk/search";

            //When
            var param = $.getURLParameters(url);

            //Then
            should(parametersCount(param)).be.equal(0);

        });

        test('existing parameter changed url', function () {
            //Given
            var url = "http://www.nationaltrust.org.uk/search?one=A&two=B&three=C&four=D";
            var param = $.getURLParameters(url);
            should(parametersCount(param)).be.equal(4);

            //When
            param.three = "Three";
            var newUrl = $.setURLParameters(url, param);

            //Then
            should(newUrl).be.equal("http://www.nationaltrust.org.uk/search?one=A&two=B&three=Three&four=D");
        });

        test('new parameter added to url', function () {
            //Given
            var url = "http://www.nationaltrust.org.uk/search?one=A&two=B&three=C&four=D";
            var param = $.getURLParameters(url);
            should(parametersCount(param)).be.equal(4);

            //When
            param.five = "Five";
            var newUrl = $.setURLParameters(url, param);

            //Then
            should(newUrl).be.equal("http://www.nationaltrust.org.uk/search?one=A&two=B&three=C&four=D&five=Five");
        });

        test('new parameter added to url without parameters', function () {
            //Given
            var url = "http://www.nationaltrust.org.uk/search";
            var param = $.getURLParameters(url);
            should(parametersCount(param)).be.equal(0);

            //When
            param.five = "Five";
            var newUrl = $.setURLParameters(url, param);

            //Then
            should(newUrl).be.equal("http://www.nationaltrust.org.uk/search?five=Five");
        });

        test('duplicated parameters maintained from url', function () {
            //Given
            var url = "http://www.nationaltrust.org.uk/search?category=A&category=B&category=C&other=D";

            //When
            var param = $.getURLParameters(url);

            //Then
            should(parametersCount(param)).be.equal(4);

            //When
            var newUrl = $.setURLParameters(url, param);

            //Then
            should(newUrl).be.equal(url);
        });
    });
});