(function () {/**
 * @license almond 0.3.2 Copyright jQuery Foundation and other contributors.
 * Released under MIT license, http://github.com/requirejs/almond/LICENSE
 */
//Going sloppy to avoid 'use strict' string cost, but strict practices should
//be followed.
/*global setTimeout: false */

var requirejs, require, define;
(function (undef) {
    var main, req, makeMap, handlers,
        defined = {},
        waiting = {},
        config = {},
        defining = {},
        hasOwn = Object.prototype.hasOwnProperty,
        aps = [].slice,
        jsSuffixRegExp = /\.js$/;

    function hasProp(obj, prop) {
        return hasOwn.call(obj, prop);
    }

    /**
     * Given a relative module name, like ./something, normalize it to
     * a real name that can be mapped to a path.
     * @param {String} name the relative name
     * @param {String} baseName a real name that the name arg is relative
     * to.
     * @returns {String} normalized name
     */
    function normalize(name, baseName) {
        var nameParts, nameSegment, mapValue, foundMap, lastIndex,
            foundI, foundStarMap, starI, i, j, part, normalizedBaseParts,
            baseParts = baseName && baseName.split("/"),
            map = config.map,
            starMap = (map && map['*']) || {};

        //Adjust any relative paths.
        if (name) {
            name = name.split('/');
            lastIndex = name.length - 1;

            // If wanting node ID compatibility, strip .js from end
            // of IDs. Have to do this here, and not in nameToUrl
            // because node allows either .js or non .js to map
            // to same file.
            if (config.nodeIdCompat && jsSuffixRegExp.test(name[lastIndex])) {
                name[lastIndex] = name[lastIndex].replace(jsSuffixRegExp, '');
            }

            // Starts with a '.' so need the baseName
            if (name[0].charAt(0) === '.' && baseParts) {
                //Convert baseName to array, and lop off the last part,
                //so that . matches that 'directory' and not name of the baseName's
                //module. For instance, baseName of 'one/two/three', maps to
                //'one/two/three.js', but we want the directory, 'one/two' for
                //this normalization.
                normalizedBaseParts = baseParts.slice(0, baseParts.length - 1);
                name = normalizedBaseParts.concat(name);
            }

            //start trimDots
            for (i = 0; i < name.length; i++) {
                part = name[i];
                if (part === '.') {
                    name.splice(i, 1);
                    i -= 1;
                } else if (part === '..') {
                    // If at the start, or previous value is still ..,
                    // keep them so that when converted to a path it may
                    // still work when converted to a path, even though
                    // as an ID it is less than ideal. In larger point
                    // releases, may be better to just kick out an error.
                    if (i === 0 || (i === 1 && name[2] === '..') || name[i - 1] === '..') {
                        continue;
                    } else if (i > 0) {
                        name.splice(i - 1, 2);
                        i -= 2;
                    }
                }
            }
            //end trimDots

            name = name.join('/');
        }

        //Apply map config if available.
        if ((baseParts || starMap) && map) {
            nameParts = name.split('/');

            for (i = nameParts.length; i > 0; i -= 1) {
                nameSegment = nameParts.slice(0, i).join("/");

                if (baseParts) {
                    //Find the longest baseName segment match in the config.
                    //So, do joins on the biggest to smallest lengths of baseParts.
                    for (j = baseParts.length; j > 0; j -= 1) {
                        mapValue = map[baseParts.slice(0, j).join('/')];

                        //baseName segment has  config, find if it has one for
                        //this name.
                        if (mapValue) {
                            mapValue = mapValue[nameSegment];
                            if (mapValue) {
                                //Match, update name to the new value.
                                foundMap = mapValue;
                                foundI = i;
                                break;
                            }
                        }
                    }
                }

                if (foundMap) {
                    break;
                }

                //Check for a star map match, but just hold on to it,
                //if there is a shorter segment match later in a matching
                //config, then favor over this star map.
                if (!foundStarMap && starMap && starMap[nameSegment]) {
                    foundStarMap = starMap[nameSegment];
                    starI = i;
                }
            }

            if (!foundMap && foundStarMap) {
                foundMap = foundStarMap;
                foundI = starI;
            }

            if (foundMap) {
                nameParts.splice(0, foundI, foundMap);
                name = nameParts.join('/');
            }
        }

        return name;
    }

    function makeRequire(relName, forceSync) {
        return function () {
            //A version of a require function that passes a moduleName
            //value for items that may need to
            //look up paths relative to the moduleName
            var args = aps.call(arguments, 0);

            //If first arg is not require('string'), and there is only
            //one arg, it is the array form without a callback. Insert
            //a null so that the following concat is correct.
            if (typeof args[0] !== 'string' && args.length === 1) {
                args.push(null);
            }
            return req.apply(undef, args.concat([relName, forceSync]));
        };
    }

    function makeNormalize(relName) {
        return function (name) {
            return normalize(name, relName);
        };
    }

    function makeLoad(depName) {
        return function (value) {
            defined[depName] = value;
        };
    }

    function callDep(name) {
        if (hasProp(waiting, name)) {
            var args = waiting[name];
            delete waiting[name];
            defining[name] = true;
            main.apply(undef, args);
        }

        if (!hasProp(defined, name) && !hasProp(defining, name)) {
            throw new Error('No ' + name);
        }
        return defined[name];
    }

    //Turns a plugin!resource to [plugin, resource]
    //with the plugin being undefined if the name
    //did not have a plugin prefix.
    function splitPrefix(name) {
        var prefix,
            index = name ? name.indexOf('!') : -1;
        if (index > -1) {
            prefix = name.substring(0, index);
            name = name.substring(index + 1, name.length);
        }
        return [prefix, name];
    }

    /**
     * Makes a name map, normalizing the name, and using a plugin
     * for normalization if necessary. Grabs a ref to plugin
     * too, as an optimization.
     */
    makeMap = function (name, relName) {
        var plugin,
            parts = splitPrefix(name),
            prefix = parts[0];

        name = parts[1];

        if (prefix) {
            prefix = normalize(prefix, relName);
            plugin = callDep(prefix);
        }

        //Normalize according
        if (prefix) {
            if (plugin && plugin.normalize) {
                name = plugin.normalize(name, makeNormalize(relName));
            } else {
                name = normalize(name, relName);
            }
        } else {
            name = normalize(name, relName);
            parts = splitPrefix(name);
            prefix = parts[0];
            name = parts[1];
            if (prefix) {
                plugin = callDep(prefix);
            }
        }

        //Using ridiculous property names for space reasons
        return {
            f: prefix ? prefix + '!' + name : name, //fullName
            n: name,
            pr: prefix,
            p: plugin
        };
    };

    function makeConfig(name) {
        return function () {
            return (config && config.config && config.config[name]) || {};
        };
    }

    handlers = {
        require: function (name) {
            return makeRequire(name);
        },
        exports: function (name) {
            var e = defined[name];
            if (typeof e !== 'undefined') {
                return e;
            } else {
                return (defined[name] = {});
            }
        },
        module: function (name) {
            return {
                id: name,
                uri: '',
                exports: defined[name],
                config: makeConfig(name)
            };
        }
    };

    main = function (name, deps, callback, relName) {
        var cjsModule, depName, ret, map, i,
            args = [],
            callbackType = typeof callback,
            usingExports;

        //Use name if no relName
        relName = relName || name;

        //Call the callback to define the module, if necessary.
        if (callbackType === 'undefined' || callbackType === 'function') {
            //Pull out the defined dependencies and pass the ordered
            //values to the callback.
            //Default to [require, exports, module] if no deps
            deps = !deps.length && callback.length ? ['require', 'exports', 'module'] : deps;
            for (i = 0; i < deps.length; i += 1) {
                map = makeMap(deps[i], relName);
                depName = map.f;

                //Fast path CommonJS standard dependencies.
                if (depName === "require") {
                    args[i] = handlers.require(name);
                } else if (depName === "exports") {
                    //CommonJS module spec 1.1
                    args[i] = handlers.exports(name);
                    usingExports = true;
                } else if (depName === "module") {
                    //CommonJS module spec 1.1
                    cjsModule = args[i] = handlers.module(name);
                } else if (hasProp(defined, depName) ||
                           hasProp(waiting, depName) ||
                           hasProp(defining, depName)) {
                    args[i] = callDep(depName);
                } else if (map.p) {
                    map.p.load(map.n, makeRequire(relName, true), makeLoad(depName), {});
                    args[i] = defined[depName];
                } else {
                    throw new Error(name + ' missing ' + depName);
                }
            }

            ret = callback ? callback.apply(defined[name], args) : undefined;

            if (name) {
                //If setting exports via "module" is in play,
                //favor that over return value and exports. After that,
                //favor a non-undefined return value over exports use.
                if (cjsModule && cjsModule.exports !== undef &&
                        cjsModule.exports !== defined[name]) {
                    defined[name] = cjsModule.exports;
                } else if (ret !== undef || !usingExports) {
                    //Use the return value from the function.
                    defined[name] = ret;
                }
            }
        } else if (name) {
            //May just be an object definition for the module. Only
            //worry about defining if have a module name.
            defined[name] = callback;
        }
    };

    requirejs = require = req = function (deps, callback, relName, forceSync, alt) {
        if (typeof deps === "string") {
            if (handlers[deps]) {
                //callback in this case is really relName
                return handlers[deps](callback);
            }
            //Just return the module wanted. In this scenario, the
            //deps arg is the module name, and second arg (if passed)
            //is just the relName.
            //Normalize module name, if it contains . or ..
            return callDep(makeMap(deps, callback).f);
        } else if (!deps.splice) {
            //deps is a config object, not an array.
            config = deps;
            if (config.deps) {
                req(config.deps, config.callback);
            }
            if (!callback) {
                return;
            }

            if (callback.splice) {
                //callback is an array, which means it is a dependency list.
                //Adjust args if there are dependencies
                deps = callback;
                callback = relName;
                relName = null;
            } else {
                deps = undef;
            }
        }

        //Support require(['a'])
        callback = callback || function () {};

        //If relName is a function, it is an errback handler,
        //so remove it.
        if (typeof relName === 'function') {
            relName = forceSync;
            forceSync = alt;
        }

        //Simulate async callback;
        if (forceSync) {
            main(undef, deps, callback, relName);
        } else {
            //Using a non-zero value because of concern for what old browsers
            //do, and latest browsers "upgrade" to 4 if lower value is used:
            //http://www.whatwg.org/specs/web-apps/current-work/multipage/timers.html#dom-windowtimers-settimeout:
            //If want a value immediately, use require('id') instead -- something
            //that works in almond on the global level, but not guaranteed and
            //unlikely to work in other AMD implementations.
            setTimeout(function () {
                main(undef, deps, callback, relName);
            }, 4);
        }

        return req;
    };

    /**
     * Just drops the config on the floor, but returns req in case
     * the config return value is used.
     */
    req.config = function (cfg) {
        return req(cfg);
    };

    /**
     * Expose module registry for debugging and tooling
     */
    requirejs._defined = defined;

    define = function (name, deps, callback) {
        if (typeof name !== 'string') {
            throw new Error('See almond README: incorrect module build, no module name');
        }

        //This module may not have dependencies
        if (!deps.splice) {
            //deps is not an array, so probably means
            //an object literal or factory function for
            //the value. Adjust args.
            callback = deps;
            deps = [];
        }

        if (!hasProp(defined, name) && !hasProp(waiting, name)) {
            waiting[name] = [name, deps, callback];
        }
    };

    define.amd = {
        jQuery: true
    };
}());

define("bower_components/almond/almond", function(){});

/*
 * Foundation Responsive Library
 * http://foundation.zurb.com
 * Copyright 2015, ZURB
 * Free to use under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
*/

(function ($, window, document, undefined) {
  'use strict';

  var header_helpers = function (class_array) {
    var head = $('head');
    head.prepend($.map(class_array, function (class_name) {
      if (head.has('.' + class_name).length === 0) {
        return '<meta class="' + class_name + '" />';
      }
    }));
  };

  header_helpers([
    'foundation-mq-small',
    'foundation-mq-small-only',
    'foundation-mq-medium',
    'foundation-mq-medium-only',
    'foundation-mq-large',
    'foundation-mq-large-only',
    'foundation-mq-xlarge',
    'foundation-mq-xlarge-only',
    'foundation-mq-xxlarge',
    'foundation-data-attribute-namespace']);

  // Enable FastClick if present

  $(function () {
    if (typeof FastClick !== 'undefined') {
      // Don't attach to body if undefined
      if (typeof document.body !== 'undefined') {
        FastClick.attach(document.body);
      }
    }
  });

  // private Fast Selector wrapper,
  // returns jQuery object. Only use where
  // getElementById is not available.
  var S = function (selector, context) {
    if (typeof selector === 'string') {
      if (context) {
        var cont;
        if (context.jquery) {
          cont = context[0];
          if (!cont) {
            return context;
          }
        } else {
          cont = context;
        }
        return $(cont.querySelectorAll(selector));
      }

      return $(document.querySelectorAll(selector));
    }

    return $(selector, context);
  };

  // Namespace functions.

  var attr_name = function (init) {
    var arr = [];
    if (!init) {
      arr.push('data');
    }
    if (this.namespace.length > 0) {
      arr.push(this.namespace);
    }
    arr.push(this.name);

    return arr.join('-');
  };

  var add_namespace = function (str) {
    var parts = str.split('-'),
        i = parts.length,
        arr = [];

    while (i--) {
      if (i !== 0) {
        arr.push(parts[i]);
      } else {
        if (this.namespace.length > 0) {
          arr.push(this.namespace, parts[i]);
        } else {
          arr.push(parts[i]);
        }
      }
    }

    return arr.reverse().join('-');
  };

  // Event binding and data-options updating.

  var bindings = function (method, options) {
    var self = this,
        bind = function(){
          var $this = S(this),
              should_bind_events = !$this.data(self.attr_name(true) + '-init');
          $this.data(self.attr_name(true) + '-init', $.extend({}, self.settings, (options || method), self.data_options($this)));

          if (should_bind_events) {
            self.events(this);
          }
        };

    if (S(this.scope).is('[' + this.attr_name() +']')) {
      bind.call(this.scope);
    } else {
      S('[' + this.attr_name() +']', this.scope).each(bind);
    }
    // # Patch to fix #5043 to move this *after* the if/else clause in order for Backbone and similar frameworks to have improved control over event binding and data-options updating.
    if (typeof method === 'string') {
      return this[method].call(this, options);
    }

  };

  var single_image_loaded = function (image, callback) {
    function loaded () {
      callback(image[0]);
    }

    function bindLoad () {
      this.one('load', loaded);

      if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
        var src = this.attr( 'src' ),
            param = src.match( /\?/ ) ? '&' : '?';

        param += 'random=' + (new Date()).getTime();
        this.attr('src', src + param);
      }
    }

    if (!image.attr('src')) {
      loaded();
      return;
    }

    if (image[0].complete || image[0].readyState === 4) {
      loaded();
    } else {
      bindLoad.call(image);
    }
  };

  /*! matchMedia() polyfill - Test a CSS media type/query in JS. Authors & copyright (c) 2012: Scott Jehl, Paul Irish, Nicholas Zakas, David Knight. Dual MIT/BSD license */

  window.matchMedia || (window.matchMedia = function() {
      "use strict";

      // For browsers that support matchMedium api such as IE 9 and webkit
      var styleMedia = (window.styleMedia || window.media);

      // For those that don't support matchMedium
      if (!styleMedia) {
          var style       = document.createElement('style'),
              script      = document.getElementsByTagName('script')[0],
              info        = null;

          style.type  = 'text/css';
          style.id    = 'matchmediajs-test';

          script.parentNode.insertBefore(style, script);

          // 'style.currentStyle' is used by IE <= 8 and 'window.getComputedStyle' for all other browsers
          info = ('getComputedStyle' in window) && window.getComputedStyle(style, null) || style.currentStyle;

          styleMedia = {
              matchMedium: function(media) {
                  var text = '@media ' + media + '{ #matchmediajs-test { width: 1px; } }';

                  // 'style.styleSheet' is used by IE <= 8 and 'style.textContent' for all other browsers
                  if (style.styleSheet) {
                      style.styleSheet.cssText = text;
                  } else {
                      style.textContent = text;
                  }

                  // Test if media query is true or false
                  return info.width === '1px';
              }
          };
      }

      return function(media) {
          return {
              matches: styleMedia.matchMedium(media || 'all'),
              media: media || 'all'
          };
      };
  }());

  /*
   * jquery.requestAnimationFrame
   * https://github.com/gnarf37/jquery-requestAnimationFrame
   * Requires jQuery 1.8+
   *
   * Copyright (c) 2012 Corey Frang
   * Licensed under the MIT license.
   */

  (function(jQuery) {


  // requestAnimationFrame polyfill adapted from Erik Möller
  // fixes from Paul Irish and Tino Zijdel
  // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
  // http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating

  var animating,
      lastTime = 0,
      vendors = ['webkit', 'moz'],
      requestAnimationFrame = window.requestAnimationFrame,
      cancelAnimationFrame = window.cancelAnimationFrame,
      jqueryFxAvailable = 'undefined' !== typeof jQuery.fx;

  for (; lastTime < vendors.length && !requestAnimationFrame; lastTime++) {
    requestAnimationFrame = window[ vendors[lastTime] + 'RequestAnimationFrame' ];
    cancelAnimationFrame = cancelAnimationFrame ||
      window[ vendors[lastTime] + 'CancelAnimationFrame' ] ||
      window[ vendors[lastTime] + 'CancelRequestAnimationFrame' ];
  }

  function raf() {
    if (animating) {
      requestAnimationFrame(raf);

      if (jqueryFxAvailable) {
        jQuery.fx.tick();
      }
    }
  }

  if (requestAnimationFrame) {
    // use rAF
    window.requestAnimationFrame = requestAnimationFrame;
    window.cancelAnimationFrame = cancelAnimationFrame;

    if (jqueryFxAvailable) {
      jQuery.fx.timer = function (timer) {
        if (timer() && jQuery.timers.push(timer) && !animating) {
          animating = true;
          raf();
        }
      };

      jQuery.fx.stop = function () {
        animating = false;
      };
    }
  } else {
    // polyfill
    window.requestAnimationFrame = function (callback) {
      var currTime = new Date().getTime(),
        timeToCall = Math.max(0, 16 - (currTime - lastTime)),
        id = window.setTimeout(function () {
          callback(currTime + timeToCall);
        }, timeToCall);
      lastTime = currTime + timeToCall;
      return id;
    };

    window.cancelAnimationFrame = function (id) {
      clearTimeout(id);
    };

  }

  }( $ ));

  function removeQuotes (string) {
    if (typeof string === 'string' || string instanceof String) {
      string = string.replace(/^['\\/"]+|(;\s?})+|['\\/"]+$/g, '');
    }

    return string;
  }

  function MediaQuery(selector) {
    this.selector = selector;
    this.query = '';
  }

  MediaQuery.prototype.toString = function () {
    return this.query || (this.query = S(this.selector).css('font-family').replace(/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g, ''));
  };

  window.Foundation = {
    name : 'Foundation',

    version : '5.5.3',

    media_queries : {
      'small'       : new MediaQuery('.foundation-mq-small'),
      'small-only'  : new MediaQuery('.foundation-mq-small-only'),
      'medium'      : new MediaQuery('.foundation-mq-medium'),
      'medium-only' : new MediaQuery('.foundation-mq-medium-only'),
      'large'       : new MediaQuery('.foundation-mq-large'),
      'large-only'  : new MediaQuery('.foundation-mq-large-only'),
      'xlarge'      : new MediaQuery('.foundation-mq-xlarge'),
      'xlarge-only' : new MediaQuery('.foundation-mq-xlarge-only'),
      'xxlarge'     : new MediaQuery('.foundation-mq-xxlarge')
    },

    stylesheet : $('<style></style>').appendTo('head')[0].sheet,

    global : {
      namespace : undefined
    },

    init : function (scope, libraries, method, options, response) {
      var args = [scope, method, options, response],
          responses = [];

      // check RTL
      this.rtl = /rtl/i.test(S('html').attr('dir'));

      // set foundation global scope
      this.scope = scope || this.scope;

      this.set_namespace();

      if (libraries && typeof libraries === 'string' && !/reflow/i.test(libraries)) {
        if (this.libs.hasOwnProperty(libraries)) {
          responses.push(this.init_lib(libraries, args));
        }
      } else {
        for (var lib in this.libs) {
          responses.push(this.init_lib(lib, libraries));
        }
      }

      S(window).load(function () {
        S(window)
          .trigger('resize.fndtn.clearing')
          .trigger('resize.fndtn.dropdown')
          .trigger('resize.fndtn.equalizer')
          .trigger('resize.fndtn.interchange')
          .trigger('resize.fndtn.joyride')
          .trigger('resize.fndtn.magellan')
          .trigger('resize.fndtn.topbar')
          .trigger('resize.fndtn.slider');
      });

      return scope;
    },

    init_lib : function (lib, args) {
      if (this.libs.hasOwnProperty(lib)) {
        this.patch(this.libs[lib]);

        if (args && args.hasOwnProperty(lib)) {
            if (typeof this.libs[lib].settings !== 'undefined') {
              $.extend(true, this.libs[lib].settings, args[lib]);
            } else if (typeof this.libs[lib].defaults !== 'undefined') {
              $.extend(true, this.libs[lib].defaults, args[lib]);
            }
          return this.libs[lib].init.apply(this.libs[lib], [this.scope, args[lib]]);
        }

        args = args instanceof Array ? args : new Array(args);
        return this.libs[lib].init.apply(this.libs[lib], args);
      }

      return function () {};
    },

    patch : function (lib) {
      lib.scope = this.scope;
      lib.namespace = this.global.namespace;
      lib.rtl = this.rtl;
      lib['data_options'] = this.utils.data_options;
      lib['attr_name'] = attr_name;
      lib['add_namespace'] = add_namespace;
      lib['bindings'] = bindings;
      lib['S'] = this.utils.S;
    },

    inherit : function (scope, methods) {
      var methods_arr = methods.split(' '),
          i = methods_arr.length;

      while (i--) {
        if (this.utils.hasOwnProperty(methods_arr[i])) {
          scope[methods_arr[i]] = this.utils[methods_arr[i]];
        }
      }
    },

    set_namespace : function () {

      // Description:
      //    Don't bother reading the namespace out of the meta tag
      //    if the namespace has been set globally in javascript
      //
      // Example:
      //    Foundation.global.namespace = 'my-namespace';
      // or make it an empty string:
      //    Foundation.global.namespace = '';
      //
      //

      // If the namespace has not been set (is undefined), try to read it out of the meta element.
      // Otherwise use the globally defined namespace, even if it's empty ('')
      var namespace = ( this.global.namespace === undefined ) ? $('.foundation-data-attribute-namespace').css('font-family') : this.global.namespace;

      // Finally, if the namsepace is either undefined or false, set it to an empty string.
      // Otherwise use the namespace value.
      this.global.namespace = ( namespace === undefined || /false/i.test(namespace) ) ? '' : namespace;
    },

    libs : {},

    // methods that can be inherited in libraries
    utils : {

      // Description:
      //    Fast Selector wrapper returns jQuery object. Only use where getElementById
      //    is not available.
      //
      // Arguments:
      //    Selector (String): CSS selector describing the element(s) to be
      //    returned as a jQuery object.
      //
      //    Scope (String): CSS selector describing the area to be searched. Default
      //    is document.
      //
      // Returns:
      //    Element (jQuery Object): jQuery object containing elements matching the
      //    selector within the scope.
      S : S,

      // Description:
      //    Executes a function a max of once every n milliseconds
      //
      // Arguments:
      //    Func (Function): Function to be throttled.
      //
      //    Delay (Integer): Function execution threshold in milliseconds.
      //
      // Returns:
      //    Lazy_function (Function): Function with throttling applied.
      throttle : function (func, delay) {
        var timer = null;

        return function () {
          var context = this, args = arguments;

          if (timer == null) {
            timer = setTimeout(function () {
              func.apply(context, args);
              timer = null;
            }, delay);
          }
        };
      },

      // Description:
      //    Executes a function when it stops being invoked for n seconds
      //    Modified version of _.debounce() http://underscorejs.org
      //
      // Arguments:
      //    Func (Function): Function to be debounced.
      //
      //    Delay (Integer): Function execution threshold in milliseconds.
      //
      //    Immediate (Bool): Whether the function should be called at the beginning
      //    of the delay instead of the end. Default is false.
      //
      // Returns:
      //    Lazy_function (Function): Function with debouncing applied.
      debounce : function (func, delay, immediate) {
        var timeout, result;
        return function () {
          var context = this, args = arguments;
          var later = function () {
            timeout = null;
            if (!immediate) {
              result = func.apply(context, args);
            }
          };
          var callNow = immediate && !timeout;
          clearTimeout(timeout);
          timeout = setTimeout(later, delay);
          if (callNow) {
            result = func.apply(context, args);
          }
          return result;
        };
      },

      // Description:
      //    Parses data-options attribute
      //
      // Arguments:
      //    El (jQuery Object): Element to be parsed.
      //
      // Returns:
      //    Options (Javascript Object): Contents of the element's data-options
      //    attribute.
      data_options : function (el, data_attr_name) {
        data_attr_name = data_attr_name || 'options';
        var opts = {}, ii, p, opts_arr,
            data_options = function (el) {
              var namespace = Foundation.global.namespace;

              if (namespace.length > 0) {
                return el.data(namespace + '-' + data_attr_name);
              }

              return el.data(data_attr_name);
            };

        var cached_options = data_options(el);

        if (typeof cached_options === 'object') {
          return cached_options;
        }

        opts_arr = (cached_options || ':').split(';');
        ii = opts_arr.length;

        function isNumber (o) {
          return !isNaN (o - 0) && o !== null && o !== '' && o !== false && o !== true;
        }

        function trim (str) {
          if (typeof str === 'string') {
            return $.trim(str);
          }
          return str;
        }

        while (ii--) {
          p = opts_arr[ii].split(':');
          p = [p[0], p.slice(1).join(':')];

          if (/true/i.test(p[1])) {
            p[1] = true;
          }
          if (/false/i.test(p[1])) {
            p[1] = false;
          }
          if (isNumber(p[1])) {
            if (p[1].indexOf('.') === -1) {
              p[1] = parseInt(p[1], 10);
            } else {
              p[1] = parseFloat(p[1]);
            }
          }

          if (p.length === 2 && p[0].length > 0) {
            opts[trim(p[0])] = trim(p[1]);
          }
        }

        return opts;
      },

      // Description:
      //    Adds JS-recognizable media queries
      //
      // Arguments:
      //    Media (String): Key string for the media query to be stored as in
      //    Foundation.media_queries
      //
      //    Class (String): Class name for the generated <meta> tag
      register_media : function (media, media_class) {
        if (Foundation.media_queries[media] === undefined) {
          $('head').append('<meta class="' + media_class + '"/>');
          Foundation.media_queries[media] = removeQuotes($('.' + media_class).css('font-family'));
        }
      },

      // Description:
      //    Add custom CSS within a JS-defined media query
      //
      // Arguments:
      //    Rule (String): CSS rule to be appended to the document.
      //
      //    Media (String): Optional media query string for the CSS rule to be
      //    nested under.
      add_custom_rule : function (rule, media) {
        if (media === undefined && Foundation.stylesheet) {
          Foundation.stylesheet.insertRule(rule, Foundation.stylesheet.cssRules.length);
        } else {
          var query = Foundation.media_queries[media];

          if (query !== undefined) {
            Foundation.stylesheet.insertRule('@media ' +
              Foundation.media_queries[media] + '{ ' + rule + ' }', Foundation.stylesheet.cssRules.length);
          }
        }
      },

      // Description:
      //    Performs a callback function when an image is fully loaded
      //
      // Arguments:
      //    Image (jQuery Object): Image(s) to check if loaded.
      //
      //    Callback (Function): Function to execute when image is fully loaded.
      image_loaded : function (images, callback) {
        var self = this,
            unloaded = images.length;

        function pictures_has_height(images) {
          var pictures_number = images.length;

          for (var i = pictures_number - 1; i >= 0; i--) {
            if(images.attr('height') === undefined) {
              return false;
            };
          };

          return true;
        }

        if (unloaded === 0 || pictures_has_height(images)) {
          callback(images);
        }

        images.each(function () {
          single_image_loaded(self.S(this), function () {
            unloaded -= 1;
            if (unloaded === 0) {
              callback(images);
            }
          });
        });
      },

      // Description:
      //    Returns a random, alphanumeric string
      //
      // Arguments:
      //    Length (Integer): Length of string to be generated. Defaults to random
      //    integer.
      //
      // Returns:
      //    Rand (String): Pseudo-random, alphanumeric string.
      random_str : function () {
        if (!this.fidx) {
          this.fidx = 0;
        }
        this.prefix = this.prefix || [(this.name || 'F'), (+new Date).toString(36)].join('-');

        return this.prefix + (this.fidx++).toString(36);
      },

      // Description:
      //    Helper for window.matchMedia
      //
      // Arguments:
      //    mq (String): Media query
      //
      // Returns:
      //    (Boolean): Whether the media query passes or not
      match : function (mq) {
        return window.matchMedia(mq).matches;
      },

      // Description:
      //    Helpers for checking Foundation default media queries with JS
      //
      // Returns:
      //    (Boolean): Whether the media query passes or not

      is_small_up : function () {
        return this.match(Foundation.media_queries.small);
      },

      is_medium_up : function () {
        return this.match(Foundation.media_queries.medium);
      },

      is_large_up : function () {
        return this.match(Foundation.media_queries.large);
      },

      is_xlarge_up : function () {
        return this.match(Foundation.media_queries.xlarge);
      },

      is_xxlarge_up : function () {
        return this.match(Foundation.media_queries.xxlarge);
      },

      is_small_only : function () {
        return !this.is_medium_up() && !this.is_large_up() && !this.is_xlarge_up() && !this.is_xxlarge_up();
      },

      is_medium_only : function () {
        return this.is_medium_up() && !this.is_large_up() && !this.is_xlarge_up() && !this.is_xxlarge_up();
      },

      is_large_only : function () {
        return this.is_medium_up() && this.is_large_up() && !this.is_xlarge_up() && !this.is_xxlarge_up();
      },

      is_xlarge_only : function () {
        return this.is_medium_up() && this.is_large_up() && this.is_xlarge_up() && !this.is_xxlarge_up();
      },

      is_xxlarge_only : function () {
        return this.is_medium_up() && this.is_large_up() && this.is_xlarge_up() && this.is_xxlarge_up();
      }
    }
  };

  $.fn.foundation = function () {
    var args = Array.prototype.slice.call(arguments, 0);

    return this.each(function () {
      Foundation.init.apply(Foundation, [this].concat(args));
      return this;
    });
  };

}(jQuery, window, window.document));

;(function ($, window, document, undefined) {
  'use strict';

  Foundation.libs.abide = {
    name : 'abide',

    version : '5.5.3',

    settings : {
      live_validate : true, // validate the form as you go
      validate_on_blur : true, // validate whenever you focus/blur on an input field
      // validate_on: 'tab', // tab (when user tabs between fields), change (input changes), manual (call custom events)

      focus_on_invalid : true, // automatically bring the focus to an invalid input field
      error_labels : true, // labels with a for="inputId" will receive an `error` class
      error_class : 'error', // labels with a for="inputId" will receive an `error` class
      // the amount of time Abide will take before it validates the form (in ms).
      // smaller time will result in faster validation
      timeout : 1000,
      patterns : {
        alpha : /^[a-zA-Z]+$/,
        alpha_numeric : /^[a-zA-Z0-9]+$/,
        integer : /^[-+]?\d+$/,
        number : /^[-+]?\d*(?:[\.\,]\d+)?$/,

        // amex, visa, diners
        card : /^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/,
        cvv : /^([0-9]){3,4}$/,

        // http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#valid-e-mail-address
        email : /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/,

        // http://blogs.lse.ac.uk/lti/2008/04/23/a-regular-expression-to-match-any-url/
        url: /^(https?|ftp|file|ssh):\/\/([-;:&=\+\$,\w]+@{1})?([-A-Za-z0-9\.]+)+:?(\d+)?((\/[-\+~%\/\.\w]+)?\??([-\+=&;%@\.\w]+)?#?([\w]+)?)?/,
        // abc.de
        domain : /^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,8}$/,

        datetime : /^([0-2][0-9]{3})\-([0-1][0-9])\-([0-3][0-9])T([0-5][0-9])\:([0-5][0-9])\:([0-5][0-9])(Z|([\-\+]([0-1][0-9])\:00))$/,
        // YYYY-MM-DD
        date : /(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))$/,
        // HH:MM:SS
        time : /^(0[0-9]|1[0-9]|2[0-3])(:[0-5][0-9]){2}$/,
        dateISO : /^\d{4}[\/\-]\d{1,2}[\/\-]\d{1,2}$/,
        // MM/DD/YYYY
        month_day_year : /^(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.]\d{4}$/,
        // DD/MM/YYYY
        day_month_year : /^(0[1-9]|[12][0-9]|3[01])[- \/.](0[1-9]|1[012])[- \/.]\d{4}$/,

        // #FFF or #FFFFFF
        color : /^#?([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/
      },
      validators : {
        equalTo : function (el, required, parent) {
          var from  = document.getElementById(el.getAttribute(this.add_namespace('data-equalto'))).value,
              to    = el.value,
              valid = (from === to);

          return valid;
        }
      }
    },

    timer : null,

    init : function (scope, method, options) {
      this.bindings(method, options);
    },

    events : function (scope) {
      var self = this,
          form = self.S(scope).attr('novalidate', 'novalidate'),
          settings = form.data(this.attr_name(true) + '-init') || {};

      this.invalid_attr = this.add_namespace('data-invalid');

      function validate(originalSelf, e) {
        clearTimeout(self.timer);
        self.timer = setTimeout(function () {
          self.validate([originalSelf], e);
        }.bind(originalSelf), settings.timeout);
      }

      form
        .off('.abide')
        .on('submit.fndtn.abide', function (e) {
          var is_ajax = /ajax/i.test(self.S(this).attr(self.attr_name()));
          return self.validate(self.S(this).find('input, textarea, select').not(":hidden, [data-abide-ignore]").get(), e, is_ajax);
        })
        .on('validate.fndtn.abide', function (e) {
          if (settings.validate_on === 'manual') {
            self.validate([e.target], e);
          }
        })
        .on('reset', function (e) {
          return self.reset($(this), e);
        })
        .find('input, textarea, select').not(":hidden, [data-abide-ignore]")
          .off('.abide')
          .on('blur.fndtn.abide change.fndtn.abide', function (e) {
              var id = this.getAttribute('id'),
                  eqTo = form.find('[data-equalto="'+ id +'"]');
            // old settings fallback
            // will be deprecated with F6 release
            if (settings.validate_on_blur && settings.validate_on_blur === true) {
              validate(this, e);
            }
            // checks if there is an equalTo equivalent related by id
            if(typeof eqTo.get(0) !== "undefined" && eqTo.val().length){
              validate(eqTo.get(0),e);
            }
            // new settings combining validate options into one setting
            if (settings.validate_on === 'change') {
              validate(this, e);
            }
          })
          .on('keydown.fndtn.abide', function (e) {
            var id = this.getAttribute('id'),
                eqTo = form.find('[data-equalto="'+ id +'"]');
            // old settings fallback
            // will be deprecated with F6 release
            if (settings.live_validate && settings.live_validate === true && e.which != 9) {
              validate(this, e);
            }
            // checks if there is an equalTo equivalent related by id
            if(typeof eqTo.get(0) !== "undefined" && eqTo.val().length){
              validate(eqTo.get(0),e);
            }
            // new settings combining validate options into one setting
            if (settings.validate_on === 'tab' && e.which === 9) {
              validate(this, e);
            }
            else if (settings.validate_on === 'change') {
              validate(this, e);
            }
          })
          .on('focus', function (e) {
            if (navigator.userAgent.match(/iPad|iPhone|Android|BlackBerry|Windows Phone|webOS/i)) {
              $('html, body').animate({
                  scrollTop: $(e.target).offset().top
              }, 100);
            }
          });
    },

    reset : function (form, e) {
      var self = this;
      form.removeAttr(self.invalid_attr);

      $('[' + self.invalid_attr + ']', form).removeAttr(self.invalid_attr);
      $('.' + self.settings.error_class, form).not('small').removeClass(self.settings.error_class);
      $(':input', form).not(':button, :submit, :reset, :hidden, [data-abide-ignore]').val('').removeAttr(self.invalid_attr);
    },

    validate : function (els, e, is_ajax) {
      var validations = this.parse_patterns(els),
          validation_count = validations.length,
          form = this.S(els[0]).closest('form'),
          submit_event = /submit/.test(e.type);

      // Has to count up to make sure the focus gets applied to the top error
      for (var i = 0; i < validation_count; i++) {
        if (!validations[i] && (submit_event || is_ajax)) {
          if (this.settings.focus_on_invalid) {
            els[i].focus();
          }
          form.trigger('invalid.fndtn.abide');
          this.S(els[i]).closest('form').attr(this.invalid_attr, '');
          return false;
        }
      }

      if (submit_event || is_ajax) {
        form.trigger('valid.fndtn.abide');
      }

      form.removeAttr(this.invalid_attr);

      if (is_ajax) {
        return false;
      }

      return true;
    },

    parse_patterns : function (els) {
      var i = els.length,
          el_patterns = [];

      while (i--) {
        el_patterns.push(this.pattern(els[i]));
      }

      return this.check_validation_and_apply_styles(el_patterns);
    },

    pattern : function (el) {
      var type = el.getAttribute('type'),
          required = typeof el.getAttribute('required') === 'string';

      var pattern = el.getAttribute('pattern') || '';

      if (this.settings.patterns.hasOwnProperty(pattern) && pattern.length > 0) {
        return [el, this.settings.patterns[pattern], required];
      } else if (pattern.length > 0) {
        return [el, new RegExp(pattern), required];
      }

      if (this.settings.patterns.hasOwnProperty(type)) {
        return [el, this.settings.patterns[type], required];
      }

      pattern = /.*/;

      return [el, pattern, required];
    },

    // TODO: Break this up into smaller methods, getting hard to read.
    check_validation_and_apply_styles : function (el_patterns) {
      var i = el_patterns.length,
          validations = [];
      if (i == 0) {
        return validations;
      }
      var form = this.S(el_patterns[0][0]).closest('[data-' + this.attr_name(true) + ']'),
          settings = form.data(this.attr_name(true) + '-init') || {};
      while (i--) {
        var el = el_patterns[i][0],
            required = el_patterns[i][2],
            value = el.value.trim(),
            direct_parent = this.S(el).parent(),
            validator = el.getAttribute(this.add_namespace('data-abide-validator')),
            is_radio = el.type === 'radio',
            is_checkbox = el.type === 'checkbox',
            label = this.S('label[for="' + el.getAttribute('id') + '"]'),
            valid_length = (required) ? (el.value.length > 0) : true,
            el_validations = [];

        var parent, valid;

        // support old way to do equalTo validations
        if (el.getAttribute(this.add_namespace('data-equalto'))) { validator = 'equalTo' }

        if (!direct_parent.is('label')) {
          parent = direct_parent;
        } else {
          parent = direct_parent.parent();
        }

        if (is_radio && required) {
          el_validations.push(this.valid_radio(el, required));
        } else if (is_checkbox && required) {
          el_validations.push(this.valid_checkbox(el, required));

        } else if (validator) {
          // Validate using each of the specified (space-delimited) validators.
          var validators = validator.split(' ');
          var last_valid = true, all_valid = true;
          for (var iv = 0; iv < validators.length; iv++) {
              valid = this.settings.validators[validators[iv]].apply(this, [el, required, parent])
              el_validations.push(valid);
              all_valid = valid && last_valid;
              last_valid = valid;
          }
          if (all_valid) {
              this.S(el).removeAttr(this.invalid_attr);
              parent.removeClass('error');
              if (label.length > 0 && this.settings.error_labels) {
                label.removeClass(this.settings.error_class).removeAttr('role');
              }
              $(el).triggerHandler('valid');
          } else {
              this.S(el).attr(this.invalid_attr, '');
              parent.addClass('error');
              if (label.length > 0 && this.settings.error_labels) {
                label.addClass(this.settings.error_class).attr('role', 'alert');
              }
              $(el).triggerHandler('invalid');
          }
        } else {

          if (el_patterns[i][1].test(value) && valid_length ||
            !required && el.value.length < 1 || $(el).attr('disabled')) {
            el_validations.push(true);
          } else {
            el_validations.push(false);
          }

          el_validations = [el_validations.every(function (valid) {return valid;})];
          if (el_validations[0]) {
            this.S(el).removeAttr(this.invalid_attr);
            el.setAttribute('aria-invalid', 'false');
            el.removeAttribute('aria-describedby');
            parent.removeClass(this.settings.error_class);
            if (label.length > 0 && this.settings.error_labels) {
              label.removeClass(this.settings.error_class).removeAttr('role');
            }
            $(el).triggerHandler('valid');
          } else {
            this.S(el).attr(this.invalid_attr, '');
            el.setAttribute('aria-invalid', 'true');

            // Try to find the error associated with the input
            var errorElem = parent.find('small.' + this.settings.error_class, 'span.' + this.settings.error_class);
            var errorID = errorElem.length > 0 ? errorElem[0].id : '';
            if (errorID.length > 0) {
              el.setAttribute('aria-describedby', errorID);
            }

            // el.setAttribute('aria-describedby', $(el).find('.error')[0].id);
            parent.addClass(this.settings.error_class);
            if (label.length > 0 && this.settings.error_labels) {
              label.addClass(this.settings.error_class).attr('role', 'alert');
            }
            $(el).triggerHandler('invalid');
          }
        }
        validations = validations.concat(el_validations);
      }

      return validations;
    },

    valid_checkbox : function (el, required) {
      var el = this.S(el),
          valid = (el.is(':checked') || !required || el.get(0).getAttribute('disabled'));

      if (valid) {
        el.removeAttr(this.invalid_attr).parent().removeClass(this.settings.error_class);
        $(el).triggerHandler('valid');
      } else {
        el.attr(this.invalid_attr, '').parent().addClass(this.settings.error_class);
        $(el).triggerHandler('invalid');
      }

      return valid;
    },

    valid_radio : function (el, required) {
      var name = el.getAttribute('name'),
          group = this.S(el).closest('[data-' + this.attr_name(true) + ']').find("[name='" + name + "']"),
          count = group.length,
          valid = false,
          disabled = false;

      // Has to count up to make sure the focus gets applied to the top error
      for (var i=0; i < count; i++) {
        if( group[i].getAttribute('disabled') ){
          disabled=true;
          valid=true;
        } else {
          if (group[i].checked){
            valid = true;
          } else {
            if( disabled ){
              valid = false;
            }
          }
        }
      }

      // Has to count up to make sure the focus gets applied to the top error
      for (var i = 0; i < count; i++) {
        if (valid) {
          this.S(group[i]).removeAttr(this.invalid_attr).parent().removeClass(this.settings.error_class);
          $(group[i]).triggerHandler('valid');
        } else {
          this.S(group[i]).attr(this.invalid_attr, '').parent().addClass(this.settings.error_class);
          $(group[i]).triggerHandler('invalid');
        }
      }

      return valid;
    },

    valid_equal : function (el, required, parent) {
      var from  = document.getElementById(el.getAttribute(this.add_namespace('data-equalto'))).value,
          to    = el.value,
          valid = (from === to);

      if (valid) {
        this.S(el).removeAttr(this.invalid_attr);
        parent.removeClass(this.settings.error_class);
        if (label.length > 0 && settings.error_labels) {
          label.removeClass(this.settings.error_class);
        }
      } else {
        this.S(el).attr(this.invalid_attr, '');
        parent.addClass(this.settings.error_class);
        if (label.length > 0 && settings.error_labels) {
          label.addClass(this.settings.error_class);
        }
      }

      return valid;
    },

    valid_oneof : function (el, required, parent, doNotValidateOthers) {
      var el = this.S(el),
        others = this.S('[' + this.add_namespace('data-oneof') + ']'),
        valid = others.filter(':checked').length > 0;

      if (valid) {
        el.removeAttr(this.invalid_attr).parent().removeClass(this.settings.error_class);
      } else {
        el.attr(this.invalid_attr, '').parent().addClass(this.settings.error_class);
      }

      if (!doNotValidateOthers) {
        var _this = this;
        others.each(function () {
          _this.valid_oneof.call(_this, this, null, null, true);
        });
      }

      return valid;
    },

    reflow : function(scope, options) {
      var self = this,
          form = self.S('[' + this.attr_name() + ']').attr('novalidate', 'novalidate');
          self.S(form).each(function (idx, el) {
            self.events(el);
          });
    }
  };
}(jQuery, window, window.document));

;(function ($, window, document, undefined) {
  'use strict';

  Foundation.libs.accordion = {
    name : 'accordion',

    version : '5.5.3',

    settings : {
      content_class : 'content',
      active_class : 'active',
      multi_expand : false,
      toggleable : true,
      callback : function () {}
    },

    init : function (scope, method, options) {
      this.bindings(method, options);
    },

    events : function (instance) {
      var self = this;
      var S = this.S;
      self.create(this.S(instance));

      S(this.scope)
      .off('.fndtn.accordion')
      .on('click.fndtn.accordion', '[' + this.attr_name() + '] > dd > a, [' + this.attr_name() + '] > li > a', function (e) {
        var accordion = S(this).closest('[' + self.attr_name() + ']'),
            groupSelector = self.attr_name() + '=' + accordion.attr(self.attr_name()),
            settings = accordion.data(self.attr_name(true) + '-init') || self.settings,
            target = S('#' + this.href.split('#')[1]),
            aunts = $('> dd, > li', accordion),
            siblings = aunts.children('.' + settings.content_class),
            active_content = siblings.filter('.' + settings.active_class);

        e.preventDefault();

        if (accordion.attr(self.attr_name())) {
          siblings = siblings.add('[' + groupSelector + '] dd > ' + '.' + settings.content_class + ', [' + groupSelector + '] li > ' + '.' + settings.content_class);
          aunts = aunts.add('[' + groupSelector + '] dd, [' + groupSelector + '] li');
        }

        if (settings.toggleable && target.is(active_content)) {
          target.parent('dd, li').toggleClass(settings.active_class, false);
          target.toggleClass(settings.active_class, false);
          S(this).attr('aria-expanded', function(i, attr){
              return attr === 'true' ? 'false' : 'true';
          });
          settings.callback(target);
          target.triggerHandler('toggled', [accordion]);
          accordion.triggerHandler('toggled', [target]);
          return;
        }

        if (!settings.multi_expand) {
          siblings.removeClass(settings.active_class);
          aunts.removeClass(settings.active_class);
          aunts.children('a').attr('aria-expanded','false');
        }

        target.addClass(settings.active_class).parent().addClass(settings.active_class);
        settings.callback(target);
        target.triggerHandler('toggled', [accordion]);
        accordion.triggerHandler('toggled', [target]);
        S(this).attr('aria-expanded','true');
      });
    },

    create: function($instance) {
      var self = this,
          accordion = $instance,
          aunts = $('> .accordion-navigation', accordion),
          settings = accordion.data(self.attr_name(true) + '-init') || self.settings;

      aunts.children('a').attr('aria-expanded','false');
      aunts.has('.' + settings.content_class + '.' + settings.active_class).addClass(settings.active_class).children('a').attr('aria-expanded','true');

      if (settings.multi_expand) {
        $instance.attr('aria-multiselectable','true');
      }
    },
	
  	toggle : function(options) {
  		var options = typeof options !== 'undefined' ? options : {};
  		var selector = typeof options.selector !== 'undefined' ? options.selector : '';
  		var toggle_state = typeof options.toggle_state !== 'undefined' ? options.toggle_state : '';
  		var $accordion = typeof options.$accordion !== 'undefined' ? options.$accordion : this.S(this.scope).closest('[' + this.attr_name() + ']');
  
  		var $items = $accordion.find('> dd' + selector + ', > li' + selector);
  		if ( $items.length < 1 ) {
  			if ( window.console ) {
  				console.error('Selection not found.', selector);
  			}
  			return false;
  		}
  
  		var S = this.S;
  		var active_class = this.settings.active_class;
  		$items.each(function() {
  			var $item = S(this);
  			var is_active = $item.hasClass(active_class);
  			if ( ( is_active && toggle_state === 'close' ) || ( !is_active && toggle_state === 'open' ) || toggle_state === '' ) {
  				$item.find('> a').trigger('click.fndtn.accordion');
  			}
  		});
  	},
  
  	open : function(options) {
  		var options = typeof options !== 'undefined' ? options : {};
  		options.toggle_state = 'open';
  		this.toggle(options);
  	},
  
  	close : function(options) {
  		var options = typeof options !== 'undefined' ? options : {};
  		options.toggle_state = 'close';
  		this.toggle(options);
  	},	

    off : function () {},

    reflow : function () {}
  };
}(jQuery, window, window.document));

;(function ($, window, document, undefined) {
  'use strict';

  Foundation.libs.alert = {
    name : 'alert',

    version : '5.5.3',

    settings : {
      callback : function () {}
    },

    init : function (scope, method, options) {
      this.bindings(method, options);
    },

    events : function () {
      var self = this,
          S = this.S;

      $(this.scope).off('.alert').on('click.fndtn.alert', '[' + this.attr_name() + '] .close', function (e) {
        var alertBox = S(this).closest('[' + self.attr_name() + ']'),
            settings = alertBox.data(self.attr_name(true) + '-init') || self.settings;

        e.preventDefault();
        if (Modernizr.csstransitions) {
          alertBox.addClass('alert-close');
          alertBox.on('transitionend webkitTransitionEnd oTransitionEnd', function (e) {
            S(this).trigger('close.fndtn.alert').remove();
            settings.callback();
          });
        } else {
          alertBox.fadeOut(300, function () {
            S(this).trigger('close.fndtn.alert').remove();
            settings.callback();
          });
        }
      });
    },

    reflow : function () {}
  };
}(jQuery, window, window.document));

;(function ($, window, document, undefined) {
  'use strict';

  Foundation.libs.clearing = {
    name : 'clearing',

    version : '5.5.3',

    settings : {
      templates : {
        viewing : '<a href="#" class="clearing-close">&times;</a>' +
          '<div class="visible-img" style="display: none"><div class="clearing-touch-label"></div><img src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D" alt="" />' +
          '<p class="clearing-caption"></p><a href="#" class="clearing-main-prev"><span></span></a>' +
          '<a href="#" class="clearing-main-next"><span></span></a></div>' +
          '<img class="clearing-preload-next" style="display: none" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D" alt="" />' +
          '<img class="clearing-preload-prev" style="display: none" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D" alt="" />'
      },

      // comma delimited list of selectors that, on click, will close clearing,
      // add 'div.clearing-blackout, div.visible-img' to close on background click
      close_selectors : '.clearing-close, div.clearing-blackout',

      // Default to the entire li element.
      open_selectors : '',

      // Image will be skipped in carousel.
      skip_selector : '',

      touch_label : '',

      // event initializer and locks
      init : false,
      locked : false
    },

    init : function (scope, method, options) {
      var self = this;
      Foundation.inherit(this, 'throttle image_loaded');

      this.bindings(method, options);

      if (self.S(this.scope).is('[' + this.attr_name() + ']')) {
        this.assemble(self.S('li', this.scope));
      } else {
        self.S('[' + this.attr_name() + ']', this.scope).each(function () {
          self.assemble(self.S('li', this));
        });
      }
    },

    events : function (scope) {
      var self = this,
          S = self.S,
          $scroll_container = $('.scroll-container');

      if ($scroll_container.length > 0) {
        this.scope = $scroll_container;
      }

      S(this.scope)
        .off('.clearing')
        .on('click.fndtn.clearing', 'ul[' + this.attr_name() + '] li ' + this.settings.open_selectors,
          function (e, current, target) {
            var current = current || S(this),
                target = target || current,
                next = current.next('li'),
                settings = current.closest('[' + self.attr_name() + ']').data(self.attr_name(true) + '-init'),
                image = S(e.target);

            e.preventDefault();

            if (!settings) {
              self.init();
              settings = current.closest('[' + self.attr_name() + ']').data(self.attr_name(true) + '-init');
            }

            // if clearing is open and the current image is
            // clicked, go to the next image in sequence
            if (target.hasClass('visible') &&
              current[0] === target[0] &&
              next.length > 0 && self.is_open(current)) {
              target = next;
              image = S('img', target);
            }

            // set current and target to the clicked li if not otherwise defined.
            self.open(image, current, target);
            self.update_paddles(target);
          })

        .on('click.fndtn.clearing', '.clearing-main-next',
          function (e) { self.nav(e, 'next') })
        .on('click.fndtn.clearing', '.clearing-main-prev',
          function (e) { self.nav(e, 'prev') })
        .on('click.fndtn.clearing', this.settings.close_selectors,
          function (e) { Foundation.libs.clearing.close(e, this) });

      $(document).on('keydown.fndtn.clearing',
          function (e) { self.keydown(e) });

      S(window).off('.clearing').on('resize.fndtn.clearing',
        function () { self.resize() });

      this.swipe_events(scope);
    },

    swipe_events : function (scope) {
      var self = this,
      S = self.S;

      S(this.scope)
        .on('touchstart.fndtn.clearing', '.visible-img', function (e) {
          if (!e.touches) { e = e.originalEvent; }
          var data = {
                start_page_x : e.touches[0].pageX,
                start_page_y : e.touches[0].pageY,
                start_time : (new Date()).getTime(),
                delta_x : 0,
                is_scrolling : undefined
              };

          S(this).data('swipe-transition', data);
          e.stopPropagation();
        })
        .on('touchmove.fndtn.clearing', '.visible-img', function (e) {
          if (!e.touches) {
            e = e.originalEvent;
          }
          // Ignore pinch/zoom events
          if (e.touches.length > 1 || e.scale && e.scale !== 1) {
            return;
          }

          var data = S(this).data('swipe-transition');

          if (typeof data === 'undefined') {
            data = {};
          }

          data.delta_x = e.touches[0].pageX - data.start_page_x;

          if (Foundation.rtl) {
            data.delta_x = -data.delta_x;
          }

          if (typeof data.is_scrolling === 'undefined') {
            data.is_scrolling = !!( data.is_scrolling || Math.abs(data.delta_x) < Math.abs(e.touches[0].pageY - data.start_page_y) );
          }

          if (!data.is_scrolling && !data.active) {
            e.preventDefault();
            var direction = (data.delta_x < 0) ? 'next' : 'prev';
            data.active = true;
            self.nav(e, direction);
          }
        })
        .on('touchend.fndtn.clearing', '.visible-img', function (e) {
          S(this).data('swipe-transition', {});
          e.stopPropagation();
        });
    },

    assemble : function ($li) {
      var $el = $li.parent();

      if ($el.parent().hasClass('carousel')) {
        return;
      }

      $el.after('<div id="foundationClearingHolder"></div>');

      var grid = $el.detach(),
          grid_outerHTML = '';

      if (grid[0] == null) {
        return;
      } else {
        grid_outerHTML = grid[0].outerHTML;
      }

      var holder = this.S('#foundationClearingHolder'),
          settings = $el.data(this.attr_name(true) + '-init'),
          data = {
            grid : '<div class="carousel">' + grid_outerHTML + '</div>',
            viewing : settings.templates.viewing
          },
          wrapper = '<div class="clearing-assembled"><div>' + data.viewing +
            data.grid + '</div></div>',
          touch_label = this.settings.touch_label;

      if (Modernizr.touch) {
        wrapper = $(wrapper).find('.clearing-touch-label').html(touch_label).end();
      }

      holder.after(wrapper).remove();
    },

    open : function ($image, current, target) {
      var self = this,
          body = $(document.body),
          root = target.closest('.clearing-assembled'),
          container = self.S('div', root).first(),
          visible_image = self.S('.visible-img', container),
          image = self.S('img', visible_image).not($image),
          label = self.S('.clearing-touch-label', container),
          error = false,
          loaded = {};

      // Event to disable scrolling on touch devices when Clearing is activated
      $('body').on('touchmove', function (e) {
        e.preventDefault();
      });

      image.error(function () {
        error = true;
      });

      function startLoad() {
        setTimeout(function () {
          this.image_loaded(image, function () {
            if (image.outerWidth() === 1 && !error) {
              startLoad.call(this);
            } else {
              cb.call(this, image);
            }
          }.bind(this));
        }.bind(this), 100);
      }

      function cb (image) {
        var $image = $(image);
        $image.css('visibility', 'visible');
        $image.trigger('imageVisible');
        // toggle the gallery
        body.css('overflow', 'hidden');
        root.addClass('clearing-blackout');
        container.addClass('clearing-container');
        visible_image.show();
        this.fix_height(target)
          .caption(self.S('.clearing-caption', visible_image), self.S('img', target))
          .center_and_label(image, label)
          .shift(current, target, function () {
            target.closest('li').siblings().removeClass('visible');
            target.closest('li').addClass('visible');
          });
        visible_image.trigger('opened.fndtn.clearing')
      }

      if (!this.locked()) {
        visible_image.trigger('open.fndtn.clearing');
        // set the image to the selected thumbnail
        loaded = this.load($image);
        if (loaded.interchange) {
          image
            .attr('data-interchange', loaded.interchange)
            .foundation('interchange', 'reflow');
        } else {
          image
            .attr('src', loaded.src)
            .attr('data-interchange', '');
        }
        image.css('visibility', 'hidden');

        startLoad.call(this);
      }
    },

    close : function (e, el) {
      e.preventDefault();

      var root = (function (target) {
            if (/blackout/.test(target.selector)) {
              return target;
            } else {
              return target.closest('.clearing-blackout');
            }
          }($(el))),
          body = $(document.body), container, visible_image;

      if (el === e.target && root) {
        body.css('overflow', '');
        container = $('div', root).first();
        visible_image = $('.visible-img', container);
        visible_image.trigger('close.fndtn.clearing');
        this.settings.prev_index = 0;
        $('ul[' + this.attr_name() + ']', root)
          .attr('style', '').closest('.clearing-blackout')
          .removeClass('clearing-blackout');
        container.removeClass('clearing-container');
        visible_image.hide();
        visible_image.trigger('closed.fndtn.clearing');
      }

      // Event to re-enable scrolling on touch devices
      $('body').off('touchmove');

      return false;
    },

    is_open : function (current) {
      return current.parent().prop('style').length > 0;
    },

    keydown : function (e) {
      var clearing = $('.clearing-blackout ul[' + this.attr_name() + ']'),
          NEXT_KEY = this.rtl ? 37 : 39,
          PREV_KEY = this.rtl ? 39 : 37,
          ESC_KEY = 27;

      if (e.which === NEXT_KEY) {
        this.go(clearing, 'next');
      }
      if (e.which === PREV_KEY) {
        this.go(clearing, 'prev');
      }
      if (e.which === ESC_KEY) {
        this.S('a.clearing-close').trigger('click.fndtn.clearing');
      }
    },

    nav : function (e, direction) {
      var clearing = $('ul[' + this.attr_name() + ']', '.clearing-blackout');

      e.preventDefault();
      this.go(clearing, direction);
    },

    resize : function () {
      var image = $('img', '.clearing-blackout .visible-img'),
          label = $('.clearing-touch-label', '.clearing-blackout');

      if (image.length) {
        this.center_and_label(image, label);
        image.trigger('resized.fndtn.clearing')
      }
    },

    // visual adjustments
    fix_height : function (target) {
      var lis = target.parent().children(),
          self = this;

      lis.each(function () {
        var li = self.S(this),
            image = li.find('img');

        if (li.height() > image.outerHeight()) {
          li.addClass('fix-height');
        }
      })
      .closest('ul')
      .width(lis.length * 100 + '%');

      return this;
    },

    update_paddles : function (target) {
      target = target.closest('li');
      var visible_image = target
        .closest('.carousel')
        .siblings('.visible-img');

      if (target.next().length > 0) {
        this.S('.clearing-main-next', visible_image).removeClass('disabled');
      } else {
        this.S('.clearing-main-next', visible_image).addClass('disabled');
      }

      if (target.prev().length > 0) {
        this.S('.clearing-main-prev', visible_image).removeClass('disabled');
      } else {
        this.S('.clearing-main-prev', visible_image).addClass('disabled');
      }
    },

    center_and_label : function (target, label) {
      if (!this.rtl && label.length > 0) {
        label.css({
          marginLeft : -(label.outerWidth() / 2),
          marginTop : -(target.outerHeight() / 2)-label.outerHeight()-10
        });
      } else {
        label.css({
          marginRight : -(label.outerWidth() / 2),
          marginTop : -(target.outerHeight() / 2)-label.outerHeight()-10,
          left: 'auto',
          right: '50%'
        });
      }
      return this;
    },

    // image loading and preloading

    load : function ($image) {
      var href,
          interchange,
          closest_a;

      if ($image[0].nodeName === 'A') {
        href = $image.attr('href');
        interchange = $image.data('clearing-interchange');
      } else {
        closest_a = $image.closest('a');
        href = closest_a.attr('href');
        interchange = closest_a.data('clearing-interchange');
      }

      this.preload($image);

      return {
        'src': href ? href : $image.attr('src'),
        'interchange': href ? interchange : $image.data('clearing-interchange')
      }
    },

    preload : function ($image) {
      this
        .img($image.closest('li').next(), 'next')
        .img($image.closest('li').prev(), 'prev');
    },

    img : function (img, sibling_type) {
      if (img.length) {
        var preload_img = $('.clearing-preload-' + sibling_type),
            new_a = this.S('a', img),
            src,
            interchange,
            image;

        if (new_a.length) {
          src = new_a.attr('href');
          interchange = new_a.data('clearing-interchange');
        } else {
          image = this.S('img', img);
          src = image.attr('src');
          interchange = image.data('clearing-interchange');
        }

        if (interchange) {
          preload_img.attr('data-interchange', interchange);
        } else {
          preload_img.attr('src', src);
          preload_img.attr('data-interchange', '');
        }
      }
      return this;
    },

    // image caption

    caption : function (container, $image) {
      var caption = $image.attr('data-caption');

      if (caption) {
      	var containerPlain = container.get(0);
      	containerPlain.innerHTML = caption;
        container.show();
      } else {
        container
          .text('')
          .hide();
      }
      return this;
    },

    // directional methods

    go : function ($ul, direction) {
      var current = this.S('.visible', $ul),
          target = current[direction]();

      // Check for skip selector.
      if (this.settings.skip_selector && target.find(this.settings.skip_selector).length != 0) {
        target = target[direction]();
      }

      if (target.length) {
        this.S('img', target)
          .trigger('click.fndtn.clearing', [current, target])
          .trigger('change.fndtn.clearing');
      }
    },

    shift : function (current, target, callback) {
      var clearing = target.parent(),
          old_index = this.settings.prev_index || target.index(),
          direction = this.direction(clearing, current, target),
          dir = this.rtl ? 'right' : 'left',
          left = parseInt(clearing.css('left'), 10),
          width = target.outerWidth(),
          skip_shift;

      var dir_obj = {};

      // we use jQuery animate instead of CSS transitions because we
      // need a callback to unlock the next animation
      // needs support for RTL **
      if (target.index() !== old_index && !/skip/.test(direction)) {
        if (/left/.test(direction)) {
          this.lock();
          dir_obj[dir] = left + width;
          clearing.animate(dir_obj, 300, this.unlock());
        } else if (/right/.test(direction)) {
          this.lock();
          dir_obj[dir] = left - width;
          clearing.animate(dir_obj, 300, this.unlock());
        }
      } else if (/skip/.test(direction)) {
        // the target image is not adjacent to the current image, so
        // do we scroll right or not
        skip_shift = target.index() - this.settings.up_count;
        this.lock();

        if (skip_shift > 0) {
          dir_obj[dir] = -(skip_shift * width);
          clearing.animate(dir_obj, 300, this.unlock());
        } else {
          dir_obj[dir] = 0;
          clearing.animate(dir_obj, 300, this.unlock());
        }
      }

      callback();
    },

    direction : function ($el, current, target) {
      var lis = this.S('li', $el),
          li_width = lis.outerWidth() + (lis.outerWidth() / 4),
          up_count = Math.floor(this.S('.clearing-container').outerWidth() / li_width) - 1,
          target_index = lis.index(target),
          response;

      this.settings.up_count = up_count;

      if (this.adjacent(this.settings.prev_index, target_index)) {
        if ((target_index > up_count) && target_index > this.settings.prev_index) {
          response = 'right';
        } else if ((target_index > up_count - 1) && target_index <= this.settings.prev_index) {
          response = 'left';
        } else {
          response = false;
        }
      } else {
        response = 'skip';
      }

      this.settings.prev_index = target_index;

      return response;
    },

    adjacent : function (current_index, target_index) {
      for (var i = target_index + 1; i >= target_index - 1; i--) {
        if (i === current_index) {
          return true;
        }
      }
      return false;
    },

    // lock management

    lock : function () {
      this.settings.locked = true;
    },

    unlock : function () {
      this.settings.locked = false;
    },

    locked : function () {
      return this.settings.locked;
    },

    off : function () {
      this.S(this.scope).off('.fndtn.clearing');
      this.S(window).off('.fndtn.clearing');
    },

    reflow : function () {
      this.init();
    }
  };

}(jQuery, window, window.document));

;(function ($, window, document, undefined) {
  'use strict';

  Foundation.libs.dropdown = {
    name : 'dropdown',

    version : '5.5.3',

    settings : {
      active_class : 'open',
      disabled_class : 'disabled',
      mega_class : 'mega',
      align : 'bottom',
      is_hover : false,
      hover_timeout : 150,
      opened : function () {},
      closed : function () {}
    },

    init : function (scope, method, options) {
      Foundation.inherit(this, 'throttle');

      $.extend(true, this.settings, method, options);
      this.bindings(method, options);
    },

    events : function (scope) {
      var self = this,
          S = self.S;

      S(this.scope)
        .off('.dropdown')
        .on('click.fndtn.dropdown', '[' + this.attr_name() + ']', function (e) {
          var settings = S(this).data(self.attr_name(true) + '-init') || self.settings;
          if (!settings.is_hover || Modernizr.touch) {
            e.preventDefault();
            if (S(this).parent('[data-reveal-id]').length) {
              e.stopPropagation();
            }
            self.toggle($(this));
          }
        })
        .on('mouseenter.fndtn.dropdown', '[' + this.attr_name() + '], [' + this.attr_name() + '-content]', function (e) {
          var $this = S(this),
              dropdown,
              target;

          clearTimeout(self.timeout);

          if ($this.data(self.data_attr())) {
            dropdown = S('#' + $this.data(self.data_attr()));
            target = $this;
          } else {
            dropdown = $this;
            target = S('[' + self.attr_name() + '="' + dropdown.attr('id') + '"]');
          }

          var settings = target.data(self.attr_name(true) + '-init') || self.settings;

          if (S(e.currentTarget).data(self.data_attr()) && settings.is_hover) {
            self.closeall.call(self);
          }

          if (settings.is_hover) {
            self.open.apply(self, [dropdown, target]);
          }
        })
        .on('mouseleave.fndtn.dropdown', '[' + this.attr_name() + '], [' + this.attr_name() + '-content]', function (e) {
          var $this = S(this);
          var settings;

          if ($this.data(self.data_attr())) {
              settings = $this.data(self.data_attr(true) + '-init') || self.settings;
          } else {
              var target   = S('[' + self.attr_name() + '="' + S(this).attr('id') + '"]'),
                  settings = target.data(self.attr_name(true) + '-init') || self.settings;
          }

          self.timeout = setTimeout(function () {
            if ($this.data(self.data_attr())) {
              if (settings.is_hover) {
                self.close.call(self, S('#' + $this.data(self.data_attr())));
              }
            } else {
              if (settings.is_hover) {
                self.close.call(self, $this);
              }
            }
          }.bind(this), settings.hover_timeout);
        })
        .on('click.fndtn.dropdown', function (e) {
          var parent = S(e.target).closest('[' + self.attr_name() + '-content]');
          var links  = parent.find('a');

          if (links.length > 0 && parent.attr('aria-autoclose') !== 'false') {
              self.close.call(self, S('[' + self.attr_name() + '-content]'));
          }

          if (e.target !== document && !$.contains(document.documentElement, e.target)) {
            return;
          }

          if (S(e.target).closest('[' + self.attr_name() + ']').length > 0) {
            return;
          }

          if (!(S(e.target).data('revealId')) &&
            (parent.length > 0 && (S(e.target).is('[' + self.attr_name() + '-content]') ||
              $.contains(parent.first()[0], e.target)))) {
            e.stopPropagation();
            return;
          }

          self.close.call(self, S('[' + self.attr_name() + '-content]'));
        })
        .on('opened.fndtn.dropdown', '[' + self.attr_name() + '-content]', function () {
          self.settings.opened.call(this);
        })
        .on('closed.fndtn.dropdown', '[' + self.attr_name() + '-content]', function () {
          self.settings.closed.call(this);
        });

      S(window)
        .off('.dropdown')
        .on('resize.fndtn.dropdown', self.throttle(function () {
          self.resize.call(self);
        }, 50));

      this.resize();
    },

    close : function (dropdown) {
      var self = this;
      dropdown.each(function (idx) {
        var original_target = $('[' + self.attr_name() + '=' + dropdown[idx].id + ']') || $('aria-controls=' + dropdown[idx].id + ']');
        original_target.attr('aria-expanded', 'false');
        if (self.S(this).hasClass(self.settings.active_class)) {
          self.S(this)
            .css(Foundation.rtl ? 'right' : 'left', '-99999px')
            .attr('aria-hidden', 'true')
            .removeClass(self.settings.active_class)
            .prev('[' + self.attr_name() + ']')
            .removeClass(self.settings.active_class)
            .removeData('target');

          self.S(this).trigger('closed.fndtn.dropdown', [dropdown]);
        }
      });
      dropdown.removeClass('f-open-' + this.attr_name(true));
    },

    closeall : function () {
      var self = this;
      $.each(self.S('.f-open-' + this.attr_name(true)), function () {
        self.close.call(self, self.S(this));
      });
    },

    open : function (dropdown, target) {
      this
        .css(dropdown
        .addClass(this.settings.active_class), target);
      dropdown.prev('[' + this.attr_name() + ']').addClass(this.settings.active_class);
      dropdown.data('target', target.get(0)).trigger('opened.fndtn.dropdown', [dropdown, target]);
      dropdown.attr('aria-hidden', 'false');
      target.attr('aria-expanded', 'true');
      dropdown.focus();
      dropdown.addClass('f-open-' + this.attr_name(true));
    },

    data_attr : function () {
      if (this.namespace.length > 0) {
        return this.namespace + '-' + this.name;
      }

      return this.name;
    },

    toggle : function (target) {
      if (target.hasClass(this.settings.disabled_class)) {
        return;
      }
      var dropdown = this.S('#' + target.data(this.data_attr()));
      if (dropdown.length === 0) {
        // No dropdown found, not continuing
        return;
      }

      this.close.call(this, this.S('[' + this.attr_name() + '-content]').not(dropdown));

      if (dropdown.hasClass(this.settings.active_class)) {
        this.close.call(this, dropdown);
        if (dropdown.data('target') !== target.get(0)) {
          this.open.call(this, dropdown, target);
        }
      } else {
        this.open.call(this, dropdown, target);
      }
    },

    resize : function () {
      var dropdown = this.S('[' + this.attr_name() + '-content].open');
      var target = $(dropdown.data("target"));

      if (dropdown.length && target.length) {
        this.css(dropdown, target);
      }
    },

    css : function (dropdown, target) {
      var left_offset = Math.max((target.width() - dropdown.width()) / 2, 8),
          settings = target.data(this.attr_name(true) + '-init') || this.settings,
          parentOverflow = dropdown.parent().css('overflow-y') || dropdown.parent().css('overflow');

      this.clear_idx();



      if (this.small()) {
        var p = this.dirs.bottom.call(dropdown, target, settings);

        dropdown.attr('style', '').removeClass('drop-left drop-right drop-top').css({
          position : 'absolute',
          width : '95%',
          'max-width' : 'none',
          top : p.top
        });

        dropdown.css(Foundation.rtl ? 'right' : 'left', left_offset);
      }
      // detect if dropdown is in an overflow container
      else if (parentOverflow !== 'visible') {
        var offset = target[0].offsetTop + target[0].offsetHeight;

        dropdown.attr('style', '').css({
          position : 'absolute',
          top : offset
        });

        dropdown.css(Foundation.rtl ? 'right' : 'left', left_offset);
      }
      else {

        this.style(dropdown, target, settings);
      }

      return dropdown;
    },

    style : function (dropdown, target, settings) {
      var css = $.extend({position : 'absolute'},
        this.dirs[settings.align].call(dropdown, target, settings));

      dropdown.attr('style', '').css(css);
    },

    // return CSS property object
    // `this` is the dropdown
    dirs : {
      // Calculate target offset
      _base : function (t, s) {
        var o_p = this.offsetParent(),
            o = o_p.offset(),
            p = t.offset();

        p.top -= o.top;
        p.left -= o.left;

        //set some flags on the p object to pass along
        p.missRight = false;
        p.missTop = false;
        p.missLeft = false;
        p.leftRightFlag = false;

        //lets see if the panel will be off the screen
        //get the actual width of the page and store it
        var actualBodyWidth;
        var windowWidth = window.innerWidth;
        
        if (document.getElementsByClassName('row')[0]) {
          actualBodyWidth = document.getElementsByClassName('row')[0].clientWidth;
        } else {
          actualBodyWidth = windowWidth;
        }

        var actualMarginWidth = (windowWidth - actualBodyWidth) / 2;
        var actualBoundary = actualBodyWidth;

        if (!this.hasClass('mega') && !s.ignore_repositioning) {
          var outerWidth = this.outerWidth();
          var o_left = t.offset().left;
		  
          //miss top
          if (t.offset().top <= this.outerHeight()) {
            p.missTop = true;
            actualBoundary = windowWidth - actualMarginWidth;
            p.leftRightFlag = true;
          }

          //miss right
          if (o_left + outerWidth > o_left + actualMarginWidth && o_left - actualMarginWidth > outerWidth) {
            p.missRight = true;
            p.missLeft = false;
          }

          //miss left
          if (o_left - outerWidth <= 0) {
            p.missLeft = true;
            p.missRight = false;
          }
        }

        return p;
      },

      top : function (t, s) {
        var self = Foundation.libs.dropdown,
            p = self.dirs._base.call(this, t, s);

        this.addClass('drop-top');

        if (p.missTop == true) {
          p.top = p.top + t.outerHeight() + this.outerHeight();
          this.removeClass('drop-top');
        }

        if (p.missRight == true) {
          p.left = p.left - this.outerWidth() + t.outerWidth();
        }

        if (t.outerWidth() < this.outerWidth() || self.small() || this.hasClass(s.mega_menu)) {
          self.adjust_pip(this, t, s, p);
        }

        if (Foundation.rtl) {
          return {left : p.left - this.outerWidth() + t.outerWidth(),
            top : p.top - this.outerHeight()};
        }

        return {left : p.left, top : p.top - this.outerHeight()};
      },

      bottom : function (t, s) {
        var self = Foundation.libs.dropdown,
            p = self.dirs._base.call(this, t, s);

        if (p.missRight == true) {
          p.left = p.left - this.outerWidth() + t.outerWidth();
        }

        if (t.outerWidth() < this.outerWidth() || self.small() || this.hasClass(s.mega_menu)) {
          self.adjust_pip(this, t, s, p);
        }

        if (self.rtl) {
          return {left : p.left - this.outerWidth() + t.outerWidth(), top : p.top + t.outerHeight()};
        }

        return {left : p.left, top : p.top + t.outerHeight()};
      },

      left : function (t, s) {
        var p = Foundation.libs.dropdown.dirs._base.call(this, t, s);

        this.addClass('drop-left');

        if (p.missLeft == true) {
          p.left =  p.left + this.outerWidth();
          p.top = p.top + t.outerHeight();
          this.removeClass('drop-left');
        }

        return {left : p.left - this.outerWidth(), top : p.top};
      },

      right : function (t, s) {
        var p = Foundation.libs.dropdown.dirs._base.call(this, t, s);

        this.addClass('drop-right');

        if (p.missRight == true) {
          p.left = p.left - this.outerWidth();
          p.top = p.top + t.outerHeight();
          this.removeClass('drop-right');
        } else {
          p.triggeredRight = true;
        }

        var self = Foundation.libs.dropdown;

        if (t.outerWidth() < this.outerWidth() || self.small() || this.hasClass(s.mega_menu)) {
          self.adjust_pip(this, t, s, p);
        }

        return {left : p.left + t.outerWidth(), top : p.top};
      }
    },

    // Insert rule to style psuedo elements
    adjust_pip : function (dropdown, target, settings, position) {
      var sheet = Foundation.stylesheet,
          pip_offset_base = 8;

      if (dropdown.hasClass(settings.mega_class)) {
        pip_offset_base = position.left + (target.outerWidth() / 2) - 8;
      } else if (this.small()) {
        pip_offset_base += position.left - 8;
      }

      this.rule_idx = sheet.cssRules.length;

      //default
      var sel_before = '.f-dropdown.open:before',
          sel_after  = '.f-dropdown.open:after',
          css_before = 'left: ' + pip_offset_base + 'px;',
          css_after  = 'left: ' + (pip_offset_base - 1) + 'px;';

      if (position.missRight == true) {
        pip_offset_base = dropdown.outerWidth() - 23;
        sel_before = '.f-dropdown.open:before',
        sel_after  = '.f-dropdown.open:after',
        css_before = 'left: ' + pip_offset_base + 'px;',
        css_after  = 'left: ' + (pip_offset_base - 1) + 'px;';
      }

      //just a case where right is fired, but its not missing right
      if (position.triggeredRight == true) {
        sel_before = '.f-dropdown.open:before',
        sel_after  = '.f-dropdown.open:after',
        css_before = 'left:-12px;',
        css_after  = 'left:-14px;';
      }

      if (sheet.insertRule) {
        sheet.insertRule([sel_before, '{', css_before, '}'].join(' '), this.rule_idx);
        sheet.insertRule([sel_after, '{', css_after, '}'].join(' '), this.rule_idx + 1);
      } else {
        sheet.addRule(sel_before, css_before, this.rule_idx);
        sheet.addRule(sel_after, css_after, this.rule_idx + 1);
      }
    },

    // Remove old dropdown rule index
    clear_idx : function () {
      var sheet = Foundation.stylesheet;

      if (typeof this.rule_idx !== 'undefined') {
        sheet.deleteRule(this.rule_idx);
        sheet.deleteRule(this.rule_idx);
        delete this.rule_idx;
      }
    },

    small : function () {
      return matchMedia(Foundation.media_queries.small).matches &&
        !matchMedia(Foundation.media_queries.medium).matches;
    },

    off : function () {
      this.S(this.scope).off('.fndtn.dropdown');
      this.S('html, body').off('.fndtn.dropdown');
      this.S(window).off('.fndtn.dropdown');
      this.S('[data-dropdown-content]').off('.fndtn.dropdown');
    },

    reflow : function () {}
  };
}(jQuery, window, window.document));

;(function ($, window, document, undefined) {
  'use strict';

  Foundation.libs.equalizer = {
    name : 'equalizer',

    version : '5.5.3',

    settings : {
      use_tallest : true,
      before_height_change : $.noop,
      after_height_change : $.noop,
      equalize_on_stack : false,
      act_on_hidden_el: false
    },

    init : function (scope, method, options) {
      Foundation.inherit(this, 'image_loaded');
      this.bindings(method, options);
      this.reflow();
    },

    events : function () {
      this.S(window).off('.equalizer').on('resize.fndtn.equalizer', function (e) {
        this.reflow();
      }.bind(this));
    },

    equalize : function (equalizer) {
      var isStacked = false,
          group = equalizer.data('equalizer'),
          settings = equalizer.data(this.attr_name(true)+'-init') || this.settings,
          vals,
          firstTopOffset;

      if (settings.act_on_hidden_el) {
        vals = group ? equalizer.find('['+this.attr_name()+'-watch="'+group+'"]') : equalizer.find('['+this.attr_name()+'-watch]');
      }
      else {
        vals = group ? equalizer.find('['+this.attr_name()+'-watch="'+group+'"]:visible') : equalizer.find('['+this.attr_name()+'-watch]:visible');
      }
      
      if (vals.length === 0) {
        return;
      }

      settings.before_height_change();
      equalizer.trigger('before-height-change.fndth.equalizer');
      vals.height('inherit');

      if (settings.equalize_on_stack === false) {
        firstTopOffset = vals.first().offset().top;
        vals.each(function () {
          if ($(this).offset().top !== firstTopOffset) {
            isStacked = true;
            return false;
          }
        });
        if (isStacked) {
          return;
        }
      }

      var heights = vals.map(function () { return $(this).outerHeight(false) }).get();

      if (settings.use_tallest) {
        var max = Math.max.apply(null, heights);
        vals.css('height', max);
      } else {
        var min = Math.min.apply(null, heights);
        vals.css('height', min);
      }

      settings.after_height_change();
      equalizer.trigger('after-height-change.fndtn.equalizer');
    },

    reflow : function () {
      var self = this;

      this.S('[' + this.attr_name() + ']', this.scope).each(function () {
        var $eq_target = $(this),
            media_query = $eq_target.data('equalizer-mq'),
            ignore_media_query = true;

        if (media_query) {
          media_query = 'is_' + media_query.replace(/-/g, '_');
          if (Foundation.utils.hasOwnProperty(media_query)) {
            ignore_media_query = false;
          }
        }

        self.image_loaded(self.S('img', this), function () {
          if (ignore_media_query || Foundation.utils[media_query]()) {
            self.equalize($eq_target)
          } else {
            var vals = $eq_target.find('[' + self.attr_name() + '-watch]:visible');
            vals.css('height', 'auto');
          }
        });
      });
    }
  };
})(jQuery, window, window.document);

;(function ($, window, document, undefined) {
  'use strict';

  Foundation.libs.interchange = {
    name : 'interchange',

    version : '5.5.3',

    cache : {},

    images_loaded : false,
    nodes_loaded : false,

    settings : {
      load_attr : 'interchange',

      named_queries : {
        'default'     : 'only screen',
        'small'       : Foundation.media_queries['small'],
        'small-only'  : Foundation.media_queries['small-only'],
        'medium'      : Foundation.media_queries['medium'],
        'medium-only' : Foundation.media_queries['medium-only'],
        'large'       : Foundation.media_queries['large'],
        'large-only'  : Foundation.media_queries['large-only'],
        'xlarge'      : Foundation.media_queries['xlarge'],
        'xlarge-only' : Foundation.media_queries['xlarge-only'],
        'xxlarge'     : Foundation.media_queries['xxlarge'],
        'landscape'   : 'only screen and (orientation: landscape)',
        'portrait'    : 'only screen and (orientation: portrait)',
        'retina'      : 'only screen and (-webkit-min-device-pixel-ratio: 2),' +
          'only screen and (min--moz-device-pixel-ratio: 2),' +
          'only screen and (-o-min-device-pixel-ratio: 2/1),' +
          'only screen and (min-device-pixel-ratio: 2),' +
          'only screen and (min-resolution: 192dpi),' +
          'only screen and (min-resolution: 2dppx)'
      },

      directives : {
        replace : function (el, path, trigger) {
          // The trigger argument, if called within the directive, fires
          // an event named after the directive on the element, passing
          // any parameters along to the event that you pass to trigger.
          //
          // ex. trigger(), trigger([a, b, c]), or trigger(a, b, c)
          //
          // This allows you to bind a callback like so:
          // $('#interchangeContainer').on('replace', function (e, a, b, c) {
          //   console.log($(this).html(), a, b, c);
          // });

          if (el !== null && /IMG/.test(el[0].nodeName)) {
            var orig_path = $.each(el, function(){this.src = path;});
            // var orig_path = el[0].src;

            if (new RegExp(path, 'i').test(orig_path)) {
              return;
            }

            el.attr("src", path);

            return trigger(el[0].src);
          }
          var last_path = el.data(this.data_attr + '-last-path'),
              self = this;

          if (last_path == path) {
            return;
          }

          if (/\.(gif|jpg|jpeg|tiff|png)([?#].*)?/i.test(path)) {
            $(el).css('background-image', 'url(' + path + ')');
            el.data('interchange-last-path', path);
            return trigger(path);
          }

          return $.get(path, function (response) {
            el.html(response);
            el.data(self.data_attr + '-last-path', path);
            trigger();
          });

        }
      }
    },

    init : function (scope, method, options) {
      Foundation.inherit(this, 'throttle random_str');

      this.data_attr = this.set_data_attr();
      $.extend(true, this.settings, method, options);
      this.bindings(method, options);
      this.reflow();
    },

    get_media_hash : function () {
        var mediaHash = '';
        for (var queryName in this.settings.named_queries ) {
            mediaHash += matchMedia(this.settings.named_queries[queryName]).matches.toString();
        }
        return mediaHash;
    },

    events : function () {
      var self = this, prevMediaHash;

      $(window)
        .off('.interchange')
        .on('resize.fndtn.interchange', self.throttle(function () {
            var currMediaHash = self.get_media_hash();
            if (currMediaHash !== prevMediaHash) {
                self.resize();
            }
            prevMediaHash = currMediaHash;
        }, 50));

      return this;
    },

    resize : function () {
      var cache = this.cache;

      if (!this.images_loaded || !this.nodes_loaded) {
        setTimeout($.proxy(this.resize, this), 50);
        return;
      }

      for (var uuid in cache) {
        if (cache.hasOwnProperty(uuid)) {
          var passed = this.results(uuid, cache[uuid]);
          if (passed) {
            this.settings.directives[passed
              .scenario[1]].call(this, passed.el, passed.scenario[0], (function (passed) {
                if (arguments[0] instanceof Array) {
                  var args = arguments[0];
                } else {
                  var args = Array.prototype.slice.call(arguments, 0);
                }

                return function() {
                  passed.el.trigger(passed.scenario[1], args);
                }
              }(passed)));
          }
        }
      }

    },

    results : function (uuid, scenarios) {
      var count = scenarios.length;

      if (count > 0) {
        var el = this.S('[' + this.add_namespace('data-uuid') + '="' + uuid + '"]');

        while (count--) {
          var mq, rule = scenarios[count][2];
          if (this.settings.named_queries.hasOwnProperty(rule)) {
            mq = matchMedia(this.settings.named_queries[rule]);
          } else {
            mq = matchMedia(rule);
          }
          if (mq.matches) {
            return {el : el, scenario : scenarios[count]};
          }
        }
      }

      return false;
    },

    load : function (type, force_update) {
      if (typeof this['cached_' + type] === 'undefined' || force_update) {
        this['update_' + type]();
      }

      return this['cached_' + type];
    },

    update_images : function () {
      var images = this.S('img[' + this.data_attr + ']'),
          count = images.length,
          i = count,
          loaded_count = 0,
          data_attr = this.data_attr;

      this.cache = {};
      this.cached_images = [];
      this.images_loaded = (count === 0);

      while (i--) {
        loaded_count++;
        if (images[i]) {
          var str = images[i].getAttribute(data_attr) || '';

          if (str.length > 0) {
            this.cached_images.push(images[i]);
          }
        }

        if (loaded_count === count) {
          this.images_loaded = true;
          this.enhance('images');
        }
      }

      return this;
    },

    update_nodes : function () {
      var nodes = this.S('[' + this.data_attr + ']').not('img'),
          count = nodes.length,
          i = count,
          loaded_count = 0,
          data_attr = this.data_attr;

      this.cached_nodes = [];
      this.nodes_loaded = (count === 0);

      while (i--) {
        loaded_count++;
        var str = nodes[i].getAttribute(data_attr) || '';

        if (str.length > 0) {
          this.cached_nodes.push(nodes[i]);
        }

        if (loaded_count === count) {
          this.nodes_loaded = true;
          this.enhance('nodes');
        }
      }

      return this;
    },

    enhance : function (type) {
      var i = this['cached_' + type].length;

      while (i--) {
        this.object($(this['cached_' + type][i]));
      }

      return $(window).trigger('resize.fndtn.interchange');
    },

    convert_directive : function (directive) {

      var trimmed = this.trim(directive);

      if (trimmed.length > 0) {
        return trimmed;
      }

      return 'replace';
    },

    parse_scenario : function (scenario) {
      // This logic had to be made more complex since some users were using commas in the url path
      // So we cannot simply just split on a comma

      var directive_match = scenario[0].match(/(.+),\s*(\w+)\s*$/),
      // getting the mq has gotten a bit complicated since we started accounting for several use cases
      // of URLs. For now we'll continue to match these scenarios, but we may consider having these scenarios
      // as nested objects or arrays in F6.
      // regex: match everything before close parenthesis for mq
      media_query         = scenario[1].match(/(.*)\)/);

      if (directive_match) {
        var path  = directive_match[1],
        directive = directive_match[2];

      } else {
        var cached_split = scenario[0].split(/,\s*$/),
        path             = cached_split[0],
        directive        = '';
      }

      return [this.trim(path), this.convert_directive(directive), this.trim(media_query[1])];
    },

    object : function (el) {
      var raw_arr = this.parse_data_attr(el),
          scenarios = [],
          i = raw_arr.length;

      if (i > 0) {
        while (i--) {
          // split array between comma delimited content and mq
          // regex: comma, optional space, open parenthesis
          var scenario = raw_arr[i].split(/,\s?\(/);

          if (scenario.length > 1) {
            var params = this.parse_scenario(scenario);
            scenarios.push(params);
          }
        }
      }

      return this.store(el, scenarios);
    },

    store : function (el, scenarios) {
      var uuid = this.random_str(),
          current_uuid = el.data(this.add_namespace('uuid', true));

      if (this.cache[current_uuid]) {
        return this.cache[current_uuid];
      }

      el.attr(this.add_namespace('data-uuid'), uuid);
      return this.cache[uuid] = scenarios;
    },

    trim : function (str) {

      if (typeof str === 'string') {
        return $.trim(str);
      }

      return str;
    },

    set_data_attr : function (init) {
      if (init) {
        if (this.namespace.length > 0) {
          return this.namespace + '-' + this.settings.load_attr;
        }

        return this.settings.load_attr;
      }

      if (this.namespace.length > 0) {
        return 'data-' + this.namespace + '-' + this.settings.load_attr;
      }

      return 'data-' + this.settings.load_attr;
    },

    parse_data_attr : function (el) {
      var raw = el.attr(this.attr_name()).split(/\[(.*?)\]/),
          i = raw.length,
          output = [];

      while (i--) {
        if (raw[i].replace(/[\W\d]+/, '').length > 4) {
          output.push(raw[i]);
        }
      }

      return output;
    },

    reflow : function () {
      this.load('images', true);
      this.load('nodes', true);
    }

  };

}(jQuery, window, window.document));

;(function ($, window, document, undefined) {
  'use strict';

  var Modernizr = Modernizr || false;

  Foundation.libs.joyride = {
    name : 'joyride',

    version : '5.5.3',

    defaults : {
      expose                   : false,     // turn on or off the expose feature
      modal                    : true,      // Whether to cover page with modal during the tour
      keyboard                 : true,      // enable left, right and esc keystrokes
      tip_location             : 'bottom',  // 'top', 'bottom', 'left' or 'right' in relation to parent
      nub_position             : 'auto',    // override on a per tooltip bases
      scroll_speed             : 1500,      // Page scrolling speed in milliseconds, 0 = no scroll animation
      scroll_animation         : 'linear',  // supports 'swing' and 'linear', extend with jQuery UI.
      timer                    : 0,         // 0 = no timer , all other numbers = timer in milliseconds
      start_timer_on_click     : true,      // true or false - true requires clicking the first button start the timer
      start_offset             : 0,         // the index of the tooltip you want to start on (index of the li)
      next_button              : true,      // true or false to control whether a next button is used
      prev_button              : true,      // true or false to control whether a prev button is used
      tip_animation            : 'fade',    // 'pop' or 'fade' in each tip
      pause_after              : [],        // array of indexes where to pause the tour after
      exposed                  : [],        // array of expose elements
      tip_animation_fade_speed : 300,       // when tipAnimation = 'fade' this is speed in milliseconds for the transition
      cookie_monster           : false,     // true or false to control whether cookies are used
      cookie_name              : 'joyride', // Name the cookie you'll use
      cookie_domain            : false,     // Will this cookie be attached to a domain, ie. '.notableapp.com'
      cookie_expires           : 365,       // set when you would like the cookie to expire.
      tip_container            : 'body',    // Where will the tip be attached
      abort_on_close           : true,      // When true, the close event will not fire any callback
      tip_location_patterns    : {
        top : ['bottom'],
        bottom : [], // bottom should not need to be repositioned
        left : ['right', 'top', 'bottom'],
        right : ['left', 'top', 'bottom']
      },
      post_ride_callback     : function () {},    // A method to call once the tour closes (canceled or complete)
      post_step_callback     : function () {},    // A method to call after each step
      pre_step_callback      : function () {},    // A method to call before each step
      pre_ride_callback      : function () {},    // A method to call before the tour starts (passed index, tip, and cloned exposed element)
      post_expose_callback   : function () {},    // A method to call after an element has been exposed
      template : { // HTML segments for tip layout
        link          : '<a href="#close" class="joyride-close-tip">&times;</a>',
        timer         : '<div class="joyride-timer-indicator-wrap"><span class="joyride-timer-indicator"></span></div>',
        tip           : '<div class="joyride-tip-guide"><span class="joyride-nub"></span></div>',
        wrapper       : '<div class="joyride-content-wrapper"></div>',
        button        : '<a href="#" class="small button joyride-next-tip"></a>',
        prev_button   : '<a href="#" class="small button joyride-prev-tip"></a>',
        modal         : '<div class="joyride-modal-bg"></div>',
        expose        : '<div class="joyride-expose-wrapper"></div>',
        expose_cover  : '<div class="joyride-expose-cover"></div>'
      },
      expose_add_class : '' // One or more space-separated class names to be added to exposed element
    },

    init : function (scope, method, options) {
      Foundation.inherit(this, 'throttle random_str');

      this.settings = this.settings || $.extend({}, this.defaults, (options || method));

      this.bindings(method, options)
    },

    go_next : function () {
      if (this.settings.$li.next().length < 1) {
        this.end();
      } else if (this.settings.timer > 0) {
        clearTimeout(this.settings.automate);
        this.hide();
        this.show();
        this.startTimer();
      } else {
        this.hide();
        this.show();
      }
    },

    go_prev : function () {
      if (this.settings.$li.prev().length < 1) {
        // Do nothing if there are no prev element
      } else if (this.settings.timer > 0) {
        clearTimeout(this.settings.automate);
        this.hide();
        this.show(null, true);
        this.startTimer();
      } else {
        this.hide();
        this.show(null, true);
      }
    },

    events : function () {
      var self = this;

      $(this.scope)
        .off('.joyride')
        .on('click.fndtn.joyride', '.joyride-next-tip, .joyride-modal-bg', function (e) {
          e.preventDefault();
          this.go_next()
        }.bind(this))
        .on('click.fndtn.joyride', '.joyride-prev-tip', function (e) {
          e.preventDefault();
          this.go_prev();
        }.bind(this))

        .on('click.fndtn.joyride', '.joyride-close-tip', function (e) {
          e.preventDefault();
          this.end(this.settings.abort_on_close);
        }.bind(this))

        .on('keyup.fndtn.joyride', function (e) {
          // Don't do anything if keystrokes are disabled
          // or if the joyride is not being shown
          if (!this.settings.keyboard || !this.settings.riding) {
            return;
          }

          switch (e.which) {
            case 39: // right arrow
              e.preventDefault();
              this.go_next();
              break;
            case 37: // left arrow
              e.preventDefault();
              this.go_prev();
              break;
            case 27: // escape
              e.preventDefault();
              this.end(this.settings.abort_on_close);
          }
        }.bind(this));

      $(window)
        .off('.joyride')
        .on('resize.fndtn.joyride', self.throttle(function () {
          if ($('[' + self.attr_name() + ']').length > 0 && self.settings.$next_tip && self.settings.riding) {
            if (self.settings.exposed.length > 0) {
              var $els = $(self.settings.exposed);

              $els.each(function () {
                var $this = $(this);
                self.un_expose($this);
                self.expose($this);
              });
            }

            if (self.is_phone()) {
              self.pos_phone();
            } else {
              self.pos_default(false);
            }
          }
        }, 100));
    },

    start : function () {
      var self = this,
          $this = $('[' + this.attr_name() + ']', this.scope),
          integer_settings = ['timer', 'scrollSpeed', 'startOffset', 'tipAnimationFadeSpeed', 'cookieExpires'],
          int_settings_count = integer_settings.length;

      if (!$this.length > 0) {
        return;
      }

      if (!this.settings.init) {
        this.events();
      }

      this.settings = $this.data(this.attr_name(true) + '-init');

      // non configureable settings
      this.settings.$content_el = $this;
      this.settings.$body = $(this.settings.tip_container);
      this.settings.body_offset = $(this.settings.tip_container).position();
      this.settings.$tip_content = this.settings.$content_el.find('> li');
      this.settings.paused = false;
      this.settings.attempts = 0;
      this.settings.riding = true;

      // can we create cookies?
      if (typeof $.cookie !== 'function') {
        this.settings.cookie_monster = false;
      }

      // generate the tips and insert into dom.
      if (!this.settings.cookie_monster || this.settings.cookie_monster && !$.cookie(this.settings.cookie_name)) {
        this.settings.$tip_content.each(function (index) {
          var $this = $(this);
          this.settings = $.extend({}, self.defaults, self.data_options($this));

          // Make sure that settings parsed from data_options are integers where necessary
          var i = int_settings_count;
          while (i--) {
            self.settings[integer_settings[i]] = parseInt(self.settings[integer_settings[i]], 10);
          }
          self.create({$li : $this, index : index});
        });

        // show first tip
        if (!this.settings.start_timer_on_click && this.settings.timer > 0) {
          this.show('init');
          this.startTimer();
        } else {
          this.show('init');
        }

      }
    },

    resume : function () {
      this.set_li();
      this.show();
    },

    tip_template : function (opts) {
      var $blank, content;

      opts.tip_class = opts.tip_class || '';

      $blank = $(this.settings.template.tip).addClass(opts.tip_class);
      content = $.trim($(opts.li).html()) +
        this.prev_button_text(opts.prev_button_text, opts.index) +
        this.button_text(opts.button_text) +
        this.settings.template.link +
        this.timer_instance(opts.index);

      $blank.append($(this.settings.template.wrapper));
      $blank.first().attr(this.add_namespace('data-index'), opts.index);
      $('.joyride-content-wrapper', $blank).append(content);

      return $blank[0];
    },

    timer_instance : function (index) {
      var txt;

      if ((index === 0 && this.settings.start_timer_on_click && this.settings.timer > 0) || this.settings.timer === 0) {
        txt = '';
      } else {
        txt = $(this.settings.template.timer)[0].outerHTML;
      }
      return txt;
    },

    button_text : function (txt) {
      if (this.settings.tip_settings.next_button) {
        txt = $.trim(txt) || 'Next';
        txt = $(this.settings.template.button).append(txt)[0].outerHTML;
      } else {
        txt = '';
      }
      return txt;
    },

    prev_button_text : function (txt, idx) {
      if (this.settings.tip_settings.prev_button) {
        txt = $.trim(txt) || 'Previous';

        // Add the disabled class to the button if it's the first element
        if (idx == 0) {
          txt = $(this.settings.template.prev_button).append(txt).addClass('disabled')[0].outerHTML;
        } else {
          txt = $(this.settings.template.prev_button).append(txt)[0].outerHTML;
        }
      } else {
        txt = '';
      }
      return txt;
    },

    create : function (opts) {
      this.settings.tip_settings = $.extend({}, this.settings, this.data_options(opts.$li));
      var buttonText = opts.$li.attr(this.add_namespace('data-button')) || opts.$li.attr(this.add_namespace('data-text')),
          prevButtonText = opts.$li.attr(this.add_namespace('data-button-prev')) || opts.$li.attr(this.add_namespace('data-prev-text')),
        tipClass = opts.$li.attr('class'),
        $tip_content = $(this.tip_template({
          tip_class : tipClass,
          index : opts.index,
          button_text : buttonText,
          prev_button_text : prevButtonText,
          li : opts.$li
        }));

      $(this.settings.tip_container).append($tip_content);
    },

    show : function (init, is_prev) {
      var $timer = null;

      // are we paused?
      if (this.settings.$li === undefined || ($.inArray(this.settings.$li.index(), this.settings.pause_after) === -1)) {

        // don't go to the next li if the tour was paused
        if (this.settings.paused) {
          this.settings.paused = false;
        } else {
          this.set_li(init, is_prev);
        }

        this.settings.attempts = 0;

        if (this.settings.$li.length && this.settings.$target.length > 0) {
          if (init) { //run when we first start
            this.settings.pre_ride_callback(this.settings.$li.index(), this.settings.$next_tip);
            if (this.settings.modal) {
              this.show_modal();
            }
          }

          this.settings.pre_step_callback(this.settings.$li.index(), this.settings.$next_tip);

          if (this.settings.modal && this.settings.expose) {
            this.expose();
          }

          this.settings.tip_settings = $.extend({}, this.settings, this.data_options(this.settings.$li));

          this.settings.timer = parseInt(this.settings.timer, 10);

          this.settings.tip_settings.tip_location_pattern = this.settings.tip_location_patterns[this.settings.tip_settings.tip_location];

          // scroll and hide bg if not modal and not expose
          if (!/body/i.test(this.settings.$target.selector) && !this.settings.expose) {
            var joyridemodalbg = $('.joyride-modal-bg');
            if (/pop/i.test(this.settings.tipAnimation)) {
                joyridemodalbg.hide();
            } else {
                joyridemodalbg.fadeOut(this.settings.tipAnimationFadeSpeed);
            }
            this.scroll_to();
          }

          if (this.is_phone()) {
            this.pos_phone(true);
          } else {
            this.pos_default(true);
          }

          $timer = this.settings.$next_tip.find('.joyride-timer-indicator');

          if (/pop/i.test(this.settings.tip_animation)) {

            $timer.width(0);

            if (this.settings.timer > 0) {

              this.settings.$next_tip.show();

              setTimeout(function () {
                $timer.animate({
                  width : $timer.parent().width()
                }, this.settings.timer, 'linear');
              }.bind(this), this.settings.tip_animation_fade_speed);

            } else {
              this.settings.$next_tip.show();

            }

          } else if (/fade/i.test(this.settings.tip_animation)) {

            $timer.width(0);

            if (this.settings.timer > 0) {

              this.settings.$next_tip
                .fadeIn(this.settings.tip_animation_fade_speed)
                .show();

              setTimeout(function () {
                $timer.animate({
                  width : $timer.parent().width()
                }, this.settings.timer, 'linear');
              }.bind(this), this.settings.tip_animation_fade_speed);

            } else {
              this.settings.$next_tip.fadeIn(this.settings.tip_animation_fade_speed);
            }
          }

          this.settings.$current_tip = this.settings.$next_tip;

        // skip non-existant targets
        } else if (this.settings.$li && this.settings.$target.length < 1) {

          this.show(init, is_prev);

        } else {

          this.end();

        }
      } else {

        this.settings.paused = true;

      }

    },

    is_phone : function () {
      return matchMedia(Foundation.media_queries.small).matches &&
        !matchMedia(Foundation.media_queries.medium).matches;
    },

    hide : function () {
      if (this.settings.modal && this.settings.expose) {
        this.un_expose();
      }

      if (!this.settings.modal) {
        $('.joyride-modal-bg').hide();
      }

      // Prevent scroll bouncing...wait to remove from layout
      this.settings.$current_tip.css('visibility', 'hidden');
      setTimeout($.proxy(function () {
        this.hide();
        this.css('visibility', 'visible');
      }, this.settings.$current_tip), 0);
      this.settings.post_step_callback(this.settings.$li.index(),
        this.settings.$current_tip);
    },

    set_li : function (init, is_prev) {
      if (init) {
        this.settings.$li = this.settings.$tip_content.eq(this.settings.start_offset);
        this.set_next_tip();
        this.settings.$current_tip = this.settings.$next_tip;
      } else {
        if (is_prev) {
          this.settings.$li = this.settings.$li.prev();
        } else {
          this.settings.$li = this.settings.$li.next();
        }
        this.set_next_tip();
      }

      this.set_target();
    },

    set_next_tip : function () {
      this.settings.$next_tip = $('.joyride-tip-guide').eq(this.settings.$li.index());
      this.settings.$next_tip.data('closed', '');
    },

    set_target : function () {
      var cl = this.settings.$li.attr(this.add_namespace('data-class')),
          id = this.settings.$li.attr(this.add_namespace('data-id')),
          $sel = function () {
            if (id) {
              return $(document.getElementById(id));
            } else if (cl) {
              return $('.' + cl).first();
            } else {
              return $('body');
            }
          };

      this.settings.$target = $sel();
    },

    scroll_to : function () {
      var window_half, tipOffset;

      window_half = $(window).height() / 2;
      tipOffset = Math.ceil(this.settings.$target.offset().top - window_half + this.settings.$next_tip.outerHeight());

      if (tipOffset != 0) {
        $('html, body').stop().animate({
          scrollTop : tipOffset
        }, this.settings.scroll_speed, 'swing');
      }
    },

    paused : function () {
      return ($.inArray((this.settings.$li.index() + 1), this.settings.pause_after) === -1);
    },

    restart : function () {
      this.hide();
      this.settings.$li = undefined;
      this.show('init');
    },

    pos_default : function (init) {
      var $nub = this.settings.$next_tip.find('.joyride-nub'),
          nub_width = Math.ceil($nub.outerWidth() / 2),
          nub_height = Math.ceil($nub.outerHeight() / 2),
          toggle = init || false;

      // tip must not be "display: none" to calculate position
      if (toggle) {
        this.settings.$next_tip.css('visibility', 'hidden');
        this.settings.$next_tip.show();
      }

      if (!/body/i.test(this.settings.$target.selector)) {
        var topAdjustment = this.settings.tip_settings.tipAdjustmentY ? parseInt(this.settings.tip_settings.tipAdjustmentY) : 0,
            leftAdjustment = this.settings.tip_settings.tipAdjustmentX ? parseInt(this.settings.tip_settings.tipAdjustmentX) : 0;

        if (this.bottom()) {
          if (this.rtl) {
            this.settings.$next_tip.css({
              top : (this.settings.$target.offset().top + nub_height + this.settings.$target.outerHeight() + topAdjustment),
              left : this.settings.$target.offset().left + this.settings.$target.outerWidth() - this.settings.$next_tip.outerWidth() + leftAdjustment});
          } else {
            this.settings.$next_tip.css({
              top : (this.settings.$target.offset().top + nub_height + this.settings.$target.outerHeight() + topAdjustment),
              left : this.settings.$target.offset().left + leftAdjustment});
          }

          this.nub_position($nub, this.settings.tip_settings.nub_position, 'top');

        } else if (this.top()) {
          if (this.rtl) {
            this.settings.$next_tip.css({
              top : (this.settings.$target.offset().top - this.settings.$next_tip.outerHeight() - nub_height + topAdjustment),
              left : this.settings.$target.offset().left + this.settings.$target.outerWidth() - this.settings.$next_tip.outerWidth()});
          } else {
            this.settings.$next_tip.css({
              top : (this.settings.$target.offset().top - this.settings.$next_tip.outerHeight() - nub_height + topAdjustment),
              left : this.settings.$target.offset().left + leftAdjustment});
          }

          this.nub_position($nub, this.settings.tip_settings.nub_position, 'bottom');

        } else if (this.right()) {

          this.settings.$next_tip.css({
            top : this.settings.$target.offset().top + topAdjustment,
            left : (this.settings.$target.outerWidth() + this.settings.$target.offset().left + nub_width + leftAdjustment)});

          this.nub_position($nub, this.settings.tip_settings.nub_position, 'left');

        } else if (this.left()) {

          this.settings.$next_tip.css({
            top : this.settings.$target.offset().top + topAdjustment,
            left : (this.settings.$target.offset().left - this.settings.$next_tip.outerWidth() - nub_width + leftAdjustment)});

          this.nub_position($nub, this.settings.tip_settings.nub_position, 'right');

        }

        if (!this.visible(this.corners(this.settings.$next_tip)) && this.settings.attempts < this.settings.tip_settings.tip_location_pattern.length) {

          $nub.removeClass('bottom')
            .removeClass('top')
            .removeClass('right')
            .removeClass('left');

          this.settings.tip_settings.tip_location = this.settings.tip_settings.tip_location_pattern[this.settings.attempts];

          this.settings.attempts++;

          this.pos_default();

        }

      } else if (this.settings.$li.length) {

        this.pos_modal($nub);

      }

      if (toggle) {
        this.settings.$next_tip.hide();
        this.settings.$next_tip.css('visibility', 'visible');
      }

    },

    pos_phone : function (init) {
      var tip_height = this.settings.$next_tip.outerHeight(),
          tip_offset = this.settings.$next_tip.offset(),
          target_height = this.settings.$target.outerHeight(),
          $nub = $('.joyride-nub', this.settings.$next_tip),
          nub_height = Math.ceil($nub.outerHeight() / 2),
          toggle = init || false;

      $nub.removeClass('bottom')
        .removeClass('top')
        .removeClass('right')
        .removeClass('left');

      if (toggle) {
        this.settings.$next_tip.css('visibility', 'hidden');
        this.settings.$next_tip.show();
      }

      if (!/body/i.test(this.settings.$target.selector)) {

        if (this.top()) {

            this.settings.$next_tip.offset({top : this.settings.$target.offset().top - tip_height - nub_height});
            $nub.addClass('bottom');

        } else {

          this.settings.$next_tip.offset({top : this.settings.$target.offset().top + target_height + nub_height});
          $nub.addClass('top');

        }

      } else if (this.settings.$li.length) {
        this.pos_modal($nub);
      }

      if (toggle) {
        this.settings.$next_tip.hide();
        this.settings.$next_tip.css('visibility', 'visible');
      }
    },

    pos_modal : function ($nub) {
      this.center();
      $nub.hide();

      this.show_modal();
    },

    show_modal : function () {
      if (!this.settings.$next_tip.data('closed')) {
        var joyridemodalbg =  $('.joyride-modal-bg');
        if (joyridemodalbg.length < 1) {
          var joyridemodalbg = $(this.settings.template.modal);
          joyridemodalbg.appendTo('body');
        }

        if (/pop/i.test(this.settings.tip_animation)) {
            joyridemodalbg.show();
        } else {
            joyridemodalbg.fadeIn(this.settings.tip_animation_fade_speed);
        }
      }
    },

    expose : function () {
      var expose,
          exposeCover,
          el,
          origCSS,
          origClasses,
          randId = 'expose-' + this.random_str(6);

      if (arguments.length > 0 && arguments[0] instanceof $) {
        el = arguments[0];
      } else if (this.settings.$target && !/body/i.test(this.settings.$target.selector)) {
        el = this.settings.$target;
      } else {
        return false;
      }

      if (el.length < 1) {
        if (window.console) {
          console.error('element not valid', el);
        }
        return false;
      }

      expose = $(this.settings.template.expose);
      this.settings.$body.append(expose);
      expose.css({
        top : el.offset().top,
        left : el.offset().left,
        width : el.outerWidth(true),
        height : el.outerHeight(true)
      });

      exposeCover = $(this.settings.template.expose_cover);

      origCSS = {
        zIndex : el.css('z-index'),
        position : el.css('position')
      };

      origClasses = el.attr('class') == null ? '' : el.attr('class');

      el.css('z-index', parseInt(expose.css('z-index')) + 1);

      if (origCSS.position == 'static') {
        el.css('position', 'relative');
      }

      el.data('expose-css', origCSS);
      el.data('orig-class', origClasses);
      el.attr('class', origClasses + ' ' + this.settings.expose_add_class);

      exposeCover.css({
        top : el.offset().top,
        left : el.offset().left,
        width : el.outerWidth(true),
        height : el.outerHeight(true)
      });

      if (this.settings.modal) {
        this.show_modal();
      }

      this.settings.$body.append(exposeCover);
      expose.addClass(randId);
      exposeCover.addClass(randId);
      el.data('expose', randId);
      this.settings.post_expose_callback(this.settings.$li.index(), this.settings.$next_tip, el);
      this.add_exposed(el);
    },

    un_expose : function () {
      var exposeId,
          el,
          expose,
          origCSS,
          origClasses,
          clearAll = false;

      if (arguments.length > 0 && arguments[0] instanceof $) {
        el = arguments[0];
      } else if (this.settings.$target && !/body/i.test(this.settings.$target.selector)) {
        el = this.settings.$target;
      } else {
        return false;
      }

      if (el.length < 1) {
        if (window.console) {
          console.error('element not valid', el);
        }
        return false;
      }

      exposeId = el.data('expose');
      expose = $('.' + exposeId);

      if (arguments.length > 1) {
        clearAll = arguments[1];
      }

      if (clearAll === true) {
        $('.joyride-expose-wrapper,.joyride-expose-cover').remove();
      } else {
        expose.remove();
      }

      origCSS = el.data('expose-css');

      if (origCSS.zIndex == 'auto') {
        el.css('z-index', '');
      } else {
        el.css('z-index', origCSS.zIndex);
      }

      if (origCSS.position != el.css('position')) {
        if (origCSS.position == 'static') {// this is default, no need to set it.
          el.css('position', '');
        } else {
          el.css('position', origCSS.position);
        }
      }

      origClasses = el.data('orig-class');
      el.attr('class', origClasses);
      el.removeData('orig-classes');

      el.removeData('expose');
      el.removeData('expose-z-index');
      this.remove_exposed(el);
    },

    add_exposed : function (el) {
      this.settings.exposed = this.settings.exposed || [];
      if (el instanceof $ || typeof el === 'object') {
        this.settings.exposed.push(el[0]);
      } else if (typeof el == 'string') {
        this.settings.exposed.push(el);
      }
    },

    remove_exposed : function (el) {
      var search, i;
      if (el instanceof $) {
        search = el[0]
      } else if (typeof el == 'string') {
        search = el;
      }

      this.settings.exposed = this.settings.exposed || [];
      i = this.settings.exposed.length;

      while (i--) {
        if (this.settings.exposed[i] == search) {
          this.settings.exposed.splice(i, 1);
          return;
        }
      }
    },

    center : function () {
      var $w = $(window);

      this.settings.$next_tip.css({
        top : ((($w.height() - this.settings.$next_tip.outerHeight()) / 2) + $w.scrollTop()),
        left : ((($w.width() - this.settings.$next_tip.outerWidth()) / 2) + $w.scrollLeft())
      });

      return true;
    },

    bottom : function () {
      return /bottom/i.test(this.settings.tip_settings.tip_location);
    },

    top : function () {
      return /top/i.test(this.settings.tip_settings.tip_location);
    },

    right : function () {
      return /right/i.test(this.settings.tip_settings.tip_location);
    },

    left : function () {
      return /left/i.test(this.settings.tip_settings.tip_location);
    },

    corners : function (el) {
      if (el.length === 0) {
         return [false, false, false, false];   
      }
      
      var w = $(window),
          window_half = w.height() / 2,
          //using this to calculate since scroll may not have finished yet.
          tipOffset = Math.ceil(this.settings.$target.offset().top - window_half + this.settings.$next_tip.outerHeight()),
          right = w.width() + w.scrollLeft(),
          offsetBottom =  w.height() + tipOffset,
          bottom = w.height() + w.scrollTop(),
          top = w.scrollTop();

      if (tipOffset < top) {
        if (tipOffset < 0) {
          top = 0;
        } else {
          top = tipOffset;
        }
      }

      if (offsetBottom > bottom) {
        bottom = offsetBottom;
      }

      return [
        el.offset().top < top,
        right < el.offset().left + el.outerWidth(),
        bottom < el.offset().top + el.outerHeight(),
        w.scrollLeft() > el.offset().left
      ];
    },

    visible : function (hidden_corners) {
      var i = hidden_corners.length;

      while (i--) {
        if (hidden_corners[i]) {
          return false;
        }
      }

      return true;
    },

    nub_position : function (nub, pos, def) {
      if (pos === 'auto') {
        nub.addClass(def);
      } else {
        nub.addClass(pos);
      }
    },

    startTimer : function () {
      if (this.settings.$li.length) {
        this.settings.automate = setTimeout(function () {
          this.hide();
          this.show();
          this.startTimer();
        }.bind(this), this.settings.timer);
      } else {
        clearTimeout(this.settings.automate);
      }
    },

    end : function (abort) {
      if (this.settings.cookie_monster) {
        $.cookie(this.settings.cookie_name, 'ridden', {expires : this.settings.cookie_expires, domain : this.settings.cookie_domain});
      }

      if (this.settings.timer > 0) {
        clearTimeout(this.settings.automate);
      }

      if (this.settings.modal && this.settings.expose) {
        this.un_expose();
      }

      // Unplug keystrokes listener
      $(this.scope).off('keyup.joyride')

      this.settings.$next_tip.data('closed', true);
      this.settings.riding = false;

      $('.joyride-modal-bg').hide();
      this.settings.$current_tip.hide();

      if (typeof abort === 'undefined' || abort === false) {
        this.settings.post_step_callback(this.settings.$li.index(), this.settings.$current_tip);
        this.settings.post_ride_callback(this.settings.$li.index(), this.settings.$current_tip);
      }

      $('.joyride-tip-guide').remove();
    },

    off : function () {
      $(this.scope).off('.joyride');
      $(window).off('.joyride');
      $('.joyride-close-tip, .joyride-next-tip, .joyride-modal-bg').off('.joyride');
      $('.joyride-tip-guide, .joyride-modal-bg').remove();
      clearTimeout(this.settings.automate);
    },

    reflow : function () {}
  };
}(jQuery, window, window.document));

;(function ($, window, document, undefined) {
  'use strict';

  Foundation.libs['magellan-expedition'] = {
    name : 'magellan-expedition',

    version : '5.5.3',

    settings : {
      active_class : 'active',
      threshold : 0, // pixels from the top of the expedition for it to become fixes
      destination_threshold : 20, // pixels from the top of destination for it to be considered active
      throttle_delay : 30, // calculation throttling to increase framerate
      fixed_top : 0, // top distance in pixels assigend to the fixed element on scroll
      offset_by_height : true,  // whether to offset the destination by the expedition height. Usually you want this to be true, unless your expedition is on the side.
      duration : 700, // animation duration time
      easing : 'swing' // animation easing
    },

    init : function (scope, method, options) {
      Foundation.inherit(this, 'throttle');
      this.bindings(method, options);
    },

    events : function () {
      var self = this,
          S = self.S,
          settings = self.settings;

      // initialize expedition offset
      self.set_expedition_position();

      S(self.scope)
        .off('.magellan')
        .on('click.fndtn.magellan', '[' + self.add_namespace('data-magellan-arrival') + '] a[href*=#]', function (e) {
          var sameHost = ((this.hostname === location.hostname) || !this.hostname),
              samePath = self.filterPathname(location.pathname) === self.filterPathname(this.pathname),
              testHash = this.hash.replace(/(:|\.|\/)/g, '\\$1'),
              anchor = this;

          if (sameHost && samePath && testHash) {
            e.preventDefault();
            var expedition = $(this).closest('[' + self.attr_name() + ']'),
                settings = expedition.data('magellan-expedition-init'),
                hash = this.hash.split('#').join(''),
                target = $('a[name="' + hash + '"]');

            if (target.length === 0) {
              target = $('#' + hash);

            }

            // Account for expedition height if fixed position
            var scroll_top = target.offset().top - settings.destination_threshold + 1;
            if (settings.offset_by_height) {
              scroll_top = scroll_top - expedition.outerHeight();
            }
            $('html, body').stop().animate({
              'scrollTop' : scroll_top
            }, settings.duration, settings.easing, function () {
              if (history.pushState) {
                history.pushState(null, null, anchor.pathname + anchor.search + '#' + hash);
              } else {
                location.hash = anchor.pathname + anchor.search + '#' + hash;
              }
            });
          }
        })
        .on('scroll.fndtn.magellan', self.throttle(this.check_for_arrivals.bind(this), settings.throttle_delay));
    },

    check_for_arrivals : function () {
      var self = this;
      self.update_arrivals();
      self.update_expedition_positions();
    },

    set_expedition_position : function () {
      var self = this;
      $('[' + this.attr_name() + '=fixed]', self.scope).each(function (idx, el) {
        var expedition = $(this),
            settings = expedition.data('magellan-expedition-init'),
            styles = expedition.attr('styles'), // save styles
            top_offset, fixed_top;

        expedition.attr('style', '');
        top_offset = expedition.offset().top + settings.threshold;

        //set fixed-top by attribute
        fixed_top = parseInt(expedition.data('magellan-fixed-top'));
        if (!isNaN(fixed_top)) {
          self.settings.fixed_top = fixed_top;
        }

        expedition.data(self.data_attr('magellan-top-offset'), top_offset);
        expedition.attr('style', styles);
      });
    },

    update_expedition_positions : function () {
      var self = this,
          window_top_offset = $(window).scrollTop();

      $('[' + this.attr_name() + '=fixed]', self.scope).each(function () {
        var expedition = $(this),
            settings = expedition.data('magellan-expedition-init'),
            styles = expedition.attr('style'), // save styles
            top_offset = expedition.data('magellan-top-offset');

        //scroll to the top distance
        if (window_top_offset + self.settings.fixed_top >= top_offset) {
          // Placeholder allows height calculations to be consistent even when
          // appearing to switch between fixed/non-fixed placement
          var placeholder = expedition.prev('[' + self.add_namespace('data-magellan-expedition-clone') + ']');
          if (placeholder.length === 0) {
            placeholder = expedition.clone();
            placeholder.removeAttr(self.attr_name());
            placeholder.attr(self.add_namespace('data-magellan-expedition-clone'), '');
            expedition.before(placeholder);
          }
          expedition.css({position :'fixed', top : settings.fixed_top}).addClass('fixed');
        } else {
          expedition.prev('[' + self.add_namespace('data-magellan-expedition-clone') + ']').remove();
          expedition.attr('style', styles).css('position', '').css('top', '').removeClass('fixed');
        }
      });
    },

    update_arrivals : function () {
      var self = this,
          window_top_offset = $(window).scrollTop();

      $('[' + this.attr_name() + ']', self.scope).each(function () {
        var expedition = $(this),
            settings = expedition.data(self.attr_name(true) + '-init'),
            offsets = self.offsets(expedition, window_top_offset),
            arrivals = expedition.find('[' + self.add_namespace('data-magellan-arrival') + ']'),
            active_item = false;
        offsets.each(function (idx, item) {
          if (item.viewport_offset >= item.top_offset) {
            var arrivals = expedition.find('[' + self.add_namespace('data-magellan-arrival') + ']');
            arrivals.not(item.arrival).removeClass(settings.active_class);
            item.arrival.addClass(settings.active_class);
            active_item = true;
            return true;
          }
        });

        if (!active_item) {
          arrivals.removeClass(settings.active_class);
        }
      });
    },

    offsets : function (expedition, window_offset) {
      var self = this,
          settings = expedition.data(self.attr_name(true) + '-init'),
          viewport_offset = window_offset;

      return expedition.find('[' + self.add_namespace('data-magellan-arrival') + ']').map(function (idx, el) {
        var name = $(this).data(self.data_attr('magellan-arrival')),
            dest = $('[' + self.add_namespace('data-magellan-destination') + '=' + name + ']');
        if (dest.length > 0) {
          var top_offset = dest.offset().top - settings.destination_threshold;
          if (settings.offset_by_height) {
            top_offset = top_offset - expedition.outerHeight();
          }
          top_offset = Math.floor(top_offset);
          return {
            destination : dest,
            arrival : $(this),
            top_offset : top_offset,
            viewport_offset : viewport_offset
          }
        }
      }).sort(function (a, b) {
        if (a.top_offset < b.top_offset) {
          return -1;
        }
        if (a.top_offset > b.top_offset) {
          return 1;
        }
        return 0;
      });
    },

    data_attr : function (str) {
      if (this.namespace.length > 0) {
        return this.namespace + '-' + str;
      }

      return str;
    },

    off : function () {
      this.S(this.scope).off('.magellan');
      this.S(window).off('.magellan');
    },

    filterPathname : function (pathname) {
      pathname = pathname || '';
      return pathname
          .replace(/^\//,'')
          .replace(/(?:index|default).[a-zA-Z]{3,4}$/,'')
          .replace(/\/$/,'');
    },

    reflow : function () {
      var self = this;
      // remove placeholder expeditions used for height calculation purposes
      $('[' + self.add_namespace('data-magellan-expedition-clone') + ']', self.scope).remove();
    }
  };
}(jQuery, window, window.document));

;(function ($, window, document, undefined) {
  'use strict';

  Foundation.libs.offcanvas = {
    name : 'offcanvas',

    version : '5.5.3',

    settings : {
      open_method : 'move',
      close_on_click : false
    },

    init : function (scope, method, options) {
      this.bindings(method, options);
    },

    events : function () {
      var self = this,
          S = self.S,
          move_class = '',
          right_postfix = '',
          left_postfix = '',
          top_postfix = '',
          bottom_postfix = '';

      if (this.settings.open_method === 'move') {
        move_class = 'move-';
        right_postfix = 'right';
        left_postfix = 'left';
        top_postfix = 'top';
        bottom_postfix = 'bottom';
      } else if (this.settings.open_method === 'overlap_single') {
        move_class = 'offcanvas-overlap-';
        right_postfix = 'right';
        left_postfix = 'left';
        top_postfix = 'top';
        bottom_postfix = 'bottom';
      } else if (this.settings.open_method === 'overlap') {
        move_class = 'offcanvas-overlap';
      }

      S(this.scope).off('.offcanvas')
        .on('click.fndtn.offcanvas', '.left-off-canvas-toggle', function (e) {
          self.click_toggle_class(e, move_class + right_postfix);
          if (self.settings.open_method !== 'overlap') {
            S('.left-submenu').removeClass(move_class + right_postfix);
          }
          $('.left-off-canvas-toggle').attr('aria-expanded', 'true');
        })
        .on('click.fndtn.offcanvas', '.left-off-canvas-menu a', function (e) {
          var settings = self.get_settings(e);
          var parent = S(this).parent();

          if (settings.close_on_click && !parent.hasClass('has-submenu') && !parent.hasClass('back')) {
            self.hide.call(self, move_class + right_postfix, self.get_wrapper(e));
            parent.parent().removeClass(move_class + right_postfix);
          } else if (S(this).parent().hasClass('has-submenu')) {
            e.preventDefault();
            S(this).siblings('.left-submenu').toggleClass(move_class + right_postfix);
          } else if (parent.hasClass('back')) {
            e.preventDefault();
            parent.parent().removeClass(move_class + right_postfix);
          }
          $('.left-off-canvas-toggle').attr('aria-expanded', 'true');
        })
        //end of left canvas
        .on('click.fndtn.offcanvas', '.right-off-canvas-toggle', function (e) {
          self.click_toggle_class(e, move_class + left_postfix);
          if (self.settings.open_method !== 'overlap') {
            S('.right-submenu').removeClass(move_class + left_postfix);
          }
          $('.right-off-canvas-toggle').attr('aria-expanded', 'true');
        })
        .on('click.fndtn.offcanvas', '.right-off-canvas-menu a', function (e) {
          var settings = self.get_settings(e);
          var parent = S(this).parent();

          if (settings.close_on_click && !parent.hasClass('has-submenu') && !parent.hasClass('back')) {
            self.hide.call(self, move_class + left_postfix, self.get_wrapper(e));
            parent.parent().removeClass(move_class + left_postfix);
          } else if (S(this).parent().hasClass('has-submenu')) {
            e.preventDefault();
            S(this).siblings('.right-submenu').toggleClass(move_class + left_postfix);
          } else if (parent.hasClass('back')) {
            e.preventDefault();
            parent.parent().removeClass(move_class + left_postfix);
          }
          $('.right-off-canvas-toggle').attr('aria-expanded', 'true');
        })
        //end of right canvas
        .on('click.fndtn.offcanvas', '.top-off-canvas-toggle', function (e) {
          self.click_toggle_class(e, move_class + bottom_postfix);
          if (self.settings.open_method !== 'overlap') {
            S('.top-submenu').removeClass(move_class + bottom_postfix);
          }
          $('.top-off-canvas-toggle').attr('aria-expanded', 'true');
        })
        .on('click.fndtn.offcanvas', '.top-off-canvas-menu a', function (e) {
          var settings = self.get_settings(e);
          var parent = S(this).parent();

          if (settings.close_on_click && !parent.hasClass('has-submenu') && !parent.hasClass('back')) {
            self.hide.call(self, move_class + bottom_postfix, self.get_wrapper(e));
            parent.parent().removeClass(move_class + bottom_postfix);
          } else if (S(this).parent().hasClass('has-submenu')) {
            e.preventDefault();
            S(this).siblings('.top-submenu').toggleClass(move_class + bottom_postfix);
          } else if (parent.hasClass('back')) {
            e.preventDefault();
            parent.parent().removeClass(move_class + bottom_postfix);
          }
          $('.top-off-canvas-toggle').attr('aria-expanded', 'true');
        })
        //end of top canvas
        .on('click.fndtn.offcanvas', '.bottom-off-canvas-toggle', function (e) {
          self.click_toggle_class(e, move_class + top_postfix);
          if (self.settings.open_method !== 'overlap') {
            S('.bottom-submenu').removeClass(move_class + top_postfix);
          }
          $('.bottom-off-canvas-toggle').attr('aria-expanded', 'true');
        })
        .on('click.fndtn.offcanvas', '.bottom-off-canvas-menu a', function (e) {
          var settings = self.get_settings(e);
          var parent = S(this).parent();

          if (settings.close_on_click && !parent.hasClass('has-submenu') && !parent.hasClass('back')) {
            self.hide.call(self, move_class + top_postfix, self.get_wrapper(e));
            parent.parent().removeClass(move_class + top_postfix);
          } else if (S(this).parent().hasClass('has-submenu')) {
            e.preventDefault();
            S(this).siblings('.bottom-submenu').toggleClass(move_class + top_postfix);
          } else if (parent.hasClass('back')) {
            e.preventDefault();
            parent.parent().removeClass(move_class + top_postfix);
          }
          $('.bottom-off-canvas-toggle').attr('aria-expanded', 'true');
        })
        //end of bottom
        .on('click.fndtn.offcanvas', '.exit-off-canvas', function (e) {
          self.click_remove_class(e, move_class + left_postfix);
          S('.right-submenu').removeClass(move_class + left_postfix);
          if (right_postfix) {
            self.click_remove_class(e, move_class + right_postfix);
            S('.left-submenu').removeClass(move_class + left_postfix);
          }
          $('.right-off-canvas-toggle').attr('aria-expanded', 'true');
        })
        .on('click.fndtn.offcanvas', '.exit-off-canvas', function (e) {
          self.click_remove_class(e, move_class + left_postfix);
          $('.left-off-canvas-toggle').attr('aria-expanded', 'false');
          if (right_postfix) {
            self.click_remove_class(e, move_class + right_postfix);
            $('.right-off-canvas-toggle').attr('aria-expanded', 'false');
          }
        })
        .on('click.fndtn.offcanvas', '.exit-off-canvas', function (e) {
          self.click_remove_class(e, move_class + top_postfix);
          S('.bottom-submenu').removeClass(move_class + top_postfix);
          if (bottom_postfix) {
            self.click_remove_class(e, move_class + bottom_postfix);
            S('.top-submenu').removeClass(move_class + top_postfix);
          }
          $('.bottom-off-canvas-toggle').attr('aria-expanded', 'true');
        })
        .on('click.fndtn.offcanvas', '.exit-off-canvas', function (e) {
          self.click_remove_class(e, move_class + top_postfix);
          $('.top-off-canvas-toggle').attr('aria-expanded', 'false');
          if (bottom_postfix) {
            self.click_remove_class(e, move_class + bottom_postfix);
            $('.bottom-off-canvas-toggle').attr('aria-expanded', 'false');
          }
        });
    },

    toggle : function (class_name, $off_canvas) {
      $off_canvas = $off_canvas || this.get_wrapper();
      if ($off_canvas.is('.' + class_name)) {
        this.hide(class_name, $off_canvas);
      } else {
        this.show(class_name, $off_canvas);
      }
    },

    show : function (class_name, $off_canvas) {
      $off_canvas = $off_canvas || this.get_wrapper();
      $off_canvas.trigger('open.fndtn.offcanvas');
      $off_canvas.addClass(class_name);
    },

    hide : function (class_name, $off_canvas) {
      $off_canvas = $off_canvas || this.get_wrapper();
      $off_canvas.trigger('close.fndtn.offcanvas');
      $off_canvas.removeClass(class_name);
    },

    click_toggle_class : function (e, class_name) {
      e.preventDefault();
      var $off_canvas = this.get_wrapper(e);
      this.toggle(class_name, $off_canvas);
    },

    click_remove_class : function (e, class_name) {
      e.preventDefault();
      var $off_canvas = this.get_wrapper(e);
      this.hide(class_name, $off_canvas);
    },

    get_settings : function (e) {
      var offcanvas  = this.S(e.target).closest('[' + this.attr_name() + ']');
      return offcanvas.data(this.attr_name(true) + '-init') || this.settings;
    },

    get_wrapper : function (e) {
      var $off_canvas = this.S(e ? e.target : this.scope).closest('.off-canvas-wrap');

      if ($off_canvas.length === 0) {
        $off_canvas = this.S('.off-canvas-wrap');
      }
      return $off_canvas;
    },

    reflow : function () {}
  };
}(jQuery, window, window.document));

;(function ($, window, document, undefined) {
  'use strict';

  var noop = function () {};

  var Orbit = function (el, settings) {
    // Don't reinitialize plugin
    if (el.hasClass(settings.slides_container_class)) {
      return this;
    }

    var self = this,
        container,
        slides_container = el,
        number_container,
        bullets_container,
        timer_container,
        idx = 0,
        animate,
        timer,
        locked = false,
        adjust_height_after = false;

    self.slides = function () {
      return slides_container.children(settings.slide_selector);
    };

    self.slides().first().addClass(settings.active_slide_class);

    self.update_slide_number = function (index) {
      if (settings.slide_number) {
        number_container.find('span:first').text(parseInt(index) + 1);
        number_container.find('span:last').text(self.slides().length);
      }
      if (settings.bullets) {
        bullets_container.children().removeClass(settings.bullets_active_class);
        $(bullets_container.children().get(index)).addClass(settings.bullets_active_class);
      }
    };

    self.update_active_link = function (index) {
      var link = $('[data-orbit-link="' + self.slides().eq(index).attr('data-orbit-slide') + '"]');
      link.siblings().removeClass(settings.bullets_active_class);
      link.addClass(settings.bullets_active_class);
    };

    self.build_markup = function () {
      slides_container.wrap('<div class="' + settings.container_class + '"></div>');
      container = slides_container.parent();
      slides_container.addClass(settings.slides_container_class);

      if (settings.stack_on_small) {
        container.addClass(settings.stack_on_small_class);
      }

      if (settings.navigation_arrows) {
        container.append($('<a href="#"><span></span></a>').addClass(settings.prev_class));
        container.append($('<a href="#"><span></span></a>').addClass(settings.next_class));
      }

      if (settings.timer) {
        timer_container = $('<div>').addClass(settings.timer_container_class);
        timer_container.append('<span>');
        timer_container.append($('<div>').addClass(settings.timer_progress_class));
        timer_container.addClass(settings.timer_paused_class);
        container.append(timer_container);
      }

      if (settings.slide_number) {
        number_container = $('<div>').addClass(settings.slide_number_class);
        number_container.append('<span></span> ' + settings.slide_number_text + ' <span></span>');
        container.append(number_container);
      }

      if (settings.bullets) {
        bullets_container = $('<ol>').addClass(settings.bullets_container_class);
        container.append(bullets_container);
        bullets_container.wrap('<div class="orbit-bullets-container"></div>');
        self.slides().each(function (idx, el) {
          var bullet = $('<li>').attr('data-orbit-slide', idx).on('click', self.link_bullet);;
          bullets_container.append(bullet);
        });
      }

    };

    self._goto = function (next_idx, start_timer) {
      // if (locked) {return false;}
      if (next_idx === idx) {return false;}
      if (typeof timer === 'object') {timer.restart();}
      var slides = self.slides();

      var dir = 'next';
      locked = true;
      if (next_idx < idx) {dir = 'prev';}
      if (next_idx >= slides.length) {
        if (!settings.circular) {
          return false;
        }
        next_idx = 0;
      } else if (next_idx < 0) {
        if (!settings.circular) {
          return false;
        }
        next_idx = slides.length - 1;
      }

      var current = $(slides.get(idx));
      var next = $(slides.get(next_idx));

      current.css('zIndex', 2);
      current.removeClass(settings.active_slide_class);
      next.css('zIndex', 4).addClass(settings.active_slide_class);

      slides_container.trigger('before-slide-change.fndtn.orbit');
      settings.before_slide_change();
      self.update_active_link(next_idx);

      var callback = function () {
        var unlock = function () {
          idx = next_idx;
          locked = false;
          if (start_timer === true) {timer = self.create_timer(); timer.start();}
          self.update_slide_number(idx);
          slides_container.trigger('after-slide-change.fndtn.orbit', [{slide_number : idx, total_slides : slides.length}]);
          settings.after_slide_change(idx, slides.length);
        };
        if (slides_container.outerHeight() != next.outerHeight() && settings.variable_height) {
          slides_container.animate({'height': next.outerHeight()}, 250, 'linear', unlock);
        } else {
          unlock();
        }
      };

      if (slides.length === 1) {callback(); return false;}

      var start_animation = function () {
        if (dir === 'next') {animate.next(current, next, callback);}
        if (dir === 'prev') {animate.prev(current, next, callback);}
      };

      if (next.outerHeight() > slides_container.outerHeight() && settings.variable_height) {
        slides_container.animate({'height': next.outerHeight()}, 250, 'linear', start_animation);
      } else {
        start_animation();
      }
    };

    self.next = function (e) {
      e.stopImmediatePropagation();
      e.preventDefault();
      self._goto(idx + 1);
    };

    self.prev = function (e) {
      e.stopImmediatePropagation();
      e.preventDefault();
      self._goto(idx - 1);
    };

    self.link_custom = function (e) {
      e.preventDefault();
      var link = $(this).attr('data-orbit-link');
      if ((typeof link === 'string') && (link = $.trim(link)) != '') {
        var slide = container.find('[data-orbit-slide=' + link + ']');
        if (slide.index() != -1) {self._goto(slide.index());}
      }
    };

    self.link_bullet = function (e) {
      var index = $(this).attr('data-orbit-slide');
      if ((typeof index === 'string') && (index = $.trim(index)) != '') {
        if (isNaN(parseInt(index))) {
          var slide = container.find('[data-orbit-slide=' + index + ']');
          if (slide.index() != -1) {self._goto(slide.index() + 1);}
        } else {
          self._goto(parseInt(index));
        }
      }

    }

    self.timer_callback = function () {
      self._goto(idx + 1, true);
    }

    self.compute_dimensions = function () {
      var current = $(self.slides().get(idx));
      var h = current.outerHeight();
      if (!settings.variable_height) {
        self.slides().each(function(){
          if ($(this).outerHeight() > h) { h = $(this).outerHeight(); }
        });
      }
      slides_container.height(h);
    };

    self.create_timer = function () {
      var t = new Timer(
        container.find('.' + settings.timer_container_class),
        settings,
        self.timer_callback
      );
      return t;
    };

    self.stop_timer = function () {
      if (typeof timer === 'object') {
        timer.stop();
      }
    };

    self.toggle_timer = function () {
      var t = container.find('.' + settings.timer_container_class);
      if (t.hasClass(settings.timer_paused_class)) {
        if (typeof timer === 'undefined') {timer = self.create_timer();}
        timer.start();
      } else {
        if (typeof timer === 'object') {timer.stop();}
      }
    };

    self.init = function () {
      self.build_markup();
      if (settings.timer) {
        timer = self.create_timer();
        Foundation.utils.image_loaded(this.slides().children('img'), timer.start);
      }
      animate = new FadeAnimation(settings, slides_container);
      if (settings.animation === 'slide') {
        animate = new SlideAnimation(settings, slides_container);
      }

      container.on('click', '.' + settings.next_class, self.next);
      container.on('click', '.' + settings.prev_class, self.prev);

      if (settings.next_on_click) {
        container.on('click', '.' + settings.slides_container_class + ' [data-orbit-slide]', self.link_bullet);
      }

      container.on('click', self.toggle_timer);
      if (settings.swipe) {
        container.on('touchstart.fndtn.orbit', function (e) {
          if (!e.touches) {e = e.originalEvent;}
          var data = {
            start_page_x : e.touches[0].pageX,
            start_page_y : e.touches[0].pageY,
            start_time : (new Date()).getTime(),
            delta_x : 0,
            is_scrolling : undefined
          };
          container.data('swipe-transition', data);
          e.stopPropagation();
        })
        .on('touchmove.fndtn.orbit', function (e) {
          if (!e.touches) {
            e = e.originalEvent;
          }
          // Ignore pinch/zoom events
          if (e.touches.length > 1 || e.scale && e.scale !== 1) {
            return;
          }

          var data = container.data('swipe-transition');
          if (typeof data === 'undefined') {data = {};}

          data.delta_x = e.touches[0].pageX - data.start_page_x;

          if ( typeof data.is_scrolling === 'undefined') {
            data.is_scrolling = !!( data.is_scrolling || Math.abs(data.delta_x) < Math.abs(e.touches[0].pageY - data.start_page_y) );
          }

          if (!data.is_scrolling && !data.active) {
            e.preventDefault();
            var direction = (data.delta_x < 0) ? (idx + 1) : (idx - 1);
            data.active = true;
            self._goto(direction);
          }
        })
        .on('touchend.fndtn.orbit', function (e) {
          container.data('swipe-transition', {});
          e.stopPropagation();
        })
      }
      container.on('mouseenter.fndtn.orbit', function (e) {
        if (settings.timer && settings.pause_on_hover) {
          self.stop_timer();
        }
      })
      .on('mouseleave.fndtn.orbit', function (e) {
        if (settings.timer && settings.resume_on_mouseout) {
          timer.start();
        }
      });

      $(document).on('click', '[data-orbit-link]', self.link_custom);
      $(window).on('load resize', self.compute_dimensions);
      Foundation.utils.image_loaded(this.slides().children('img'), self.compute_dimensions);
      Foundation.utils.image_loaded(this.slides().children('img'), function () {
        container.prev('.' + settings.preloader_class).css('display', 'none');
        self.update_slide_number(0);
        self.update_active_link(0);
        slides_container.trigger('ready.fndtn.orbit');
      });
    };

    self.init();
  };

  var Timer = function (el, settings, callback) {
    var self = this,
        duration = settings.timer_speed,
        progress = el.find('.' + settings.timer_progress_class),
        start,
        timeout,
        left = -1;

    this.update_progress = function (w) {
      var new_progress = progress.clone();
      new_progress.attr('style', '');
      new_progress.css('width', w + '%');
      progress.replaceWith(new_progress);
      progress = new_progress;
    };

    this.restart = function () {
      clearTimeout(timeout);
      el.addClass(settings.timer_paused_class);
      left = -1;
      self.update_progress(0);
    };

    this.start = function () {
      if (!el.hasClass(settings.timer_paused_class)) {return true;}
      left = (left === -1) ? duration : left;
      el.removeClass(settings.timer_paused_class);
      start = new Date().getTime();
      progress.animate({'width' : '100%'}, left, 'linear');
      timeout = setTimeout(function () {
        self.restart();
        callback();
      }, left);
      el.trigger('timer-started.fndtn.orbit')
    };

    this.stop = function () {
      if (el.hasClass(settings.timer_paused_class)) {return true;}
      clearTimeout(timeout);
      el.addClass(settings.timer_paused_class);
      var end = new Date().getTime();
      left = left - (end - start);
      var w = 100 - ((left / duration) * 100);
      self.update_progress(w);
      el.trigger('timer-stopped.fndtn.orbit');
    };
  };

  var SlideAnimation = function (settings, container) {
    var duration = settings.animation_speed;
    var is_rtl = ($('html[dir=rtl]').length === 1);
    var margin = is_rtl ? 'marginRight' : 'marginLeft';
    var animMargin = {};
    animMargin[margin] = '0%';

    this.next = function (current, next, callback) {
      current.animate({marginLeft : '-100%'}, duration);
      next.animate(animMargin, duration, function () {
        current.css(margin, '100%');
        callback();
      });
    };

    this.prev = function (current, prev, callback) {
      current.animate({marginLeft : '100%'}, duration);
      prev.css(margin, '-100%');
      prev.animate(animMargin, duration, function () {
        current.css(margin, '100%');
        callback();
      });
    };
  };

  var FadeAnimation = function (settings, container) {
    var duration = settings.animation_speed;
    var is_rtl = ($('html[dir=rtl]').length === 1);
    var margin = is_rtl ? 'marginRight' : 'marginLeft';

    this.next = function (current, next, callback) {
      next.css({'margin' : '0%', 'opacity' : '0.01'});
      next.animate({'opacity' :'1'}, duration, 'linear', function () {
        current.css('margin', '100%');
        callback();
      });
    };

    this.prev = function (current, prev, callback) {
      prev.css({'margin' : '0%', 'opacity' : '0.01'});
      prev.animate({'opacity' : '1'}, duration, 'linear', function () {
        current.css('margin', '100%');
        callback();
      });
    };
  };

  Foundation.libs = Foundation.libs || {};

  Foundation.libs.orbit = {
    name : 'orbit',

    version : '5.5.3',

    settings : {
      animation : 'slide',
      timer_speed : 10000,
      pause_on_hover : true,
      resume_on_mouseout : false,
      next_on_click : true,
      animation_speed : 500,
      stack_on_small : false,
      navigation_arrows : true,
      slide_number : true,
      slide_number_text : 'of',
      container_class : 'orbit-container',
      stack_on_small_class : 'orbit-stack-on-small',
      next_class : 'orbit-next',
      prev_class : 'orbit-prev',
      timer_container_class : 'orbit-timer',
      timer_paused_class : 'paused',
      timer_progress_class : 'orbit-progress',
      slides_container_class : 'orbit-slides-container',
      preloader_class : 'preloader',
      slide_selector : '*',
      bullets_container_class : 'orbit-bullets',
      bullets_active_class : 'active',
      slide_number_class : 'orbit-slide-number',
      caption_class : 'orbit-caption',
      active_slide_class : 'active',
      orbit_transition_class : 'orbit-transitioning',
      bullets : true,
      circular : true,
      timer : true,
      variable_height : false,
      swipe : true,
      before_slide_change : noop,
      after_slide_change : noop
    },

    init : function (scope, method, options) {
      var self = this;
      this.bindings(method, options);
    },

    events : function (instance) {
      var orbit_instance = new Orbit(this.S(instance), this.S(instance).data('orbit-init'));
      this.S(instance).data(this.name + '-instance', orbit_instance);
    },

    reflow : function () {
      var self = this;

      if (self.S(self.scope).is('[data-orbit]')) {
        var $el = self.S(self.scope);
        var instance = $el.data(self.name + '-instance');
        instance.compute_dimensions();
      } else {
        self.S('[data-orbit]', self.scope).each(function (idx, el) {
          var $el = self.S(el);
          var opts = self.data_options($el);
          var instance = $el.data(self.name + '-instance');
          instance.compute_dimensions();
        });
      }
    }
  };

}(jQuery, window, window.document));

;(function ($, window, document, undefined) {
  'use strict';

  var openModals = [];

  Foundation.libs.reveal = {
    name : 'reveal',

    version : '5.5.3',

    locked : false,

    settings : {
      animation : 'fadeAndPop',
      animation_speed : 250,
      close_on_background_click : true,
      close_on_esc : true,
      dismiss_modal_class : 'close-reveal-modal',
      multiple_opened : false,
      bg_class : 'reveal-modal-bg',
      root_element : 'body',
      open : function(){},
      opened : function(){},
      close : function(){},
      closed : function(){},
      on_ajax_error: $.noop,
      bg : $('.reveal-modal-bg'),
      css : {
        open : {
          'opacity' : 0,
          'visibility' : 'visible',
          'display' : 'block'
        },
        close : {
          'opacity' : 1,
          'visibility' : 'hidden',
          'display' : 'none'
        }
      }
    },

    init : function (scope, method, options) {
      $.extend(true, this.settings, method, options);
      this.bindings(method, options);
    },

    events : function (scope) {
      var self = this,
          S = self.S;

      S(this.scope)
        .off('.reveal')
        .on('click.fndtn.reveal', '[' + this.add_namespace('data-reveal-id') + ']:not([disabled])', function (e) {
          e.preventDefault();

          if (!self.locked) {
            var element = S(this),
                ajax = element.data(self.data_attr('reveal-ajax')),
                replaceContentSel = element.data(self.data_attr('reveal-replace-content'));

            self.locked = true;

            if (typeof ajax === 'undefined') {
              self.open.call(self, element);
            } else {
              var url = ajax === true ? element.attr('href') : ajax;
              self.open.call(self, element, {url : url}, { replaceContentSel : replaceContentSel });
            }
          }
        });

      S(document)
        .on('click.fndtn.reveal', this.close_targets(), function (e) {
          e.preventDefault();
          if (!self.locked) {
            var settings = S('[' + self.attr_name() + '].open').data(self.attr_name(true) + '-init') || self.settings,
                bg_clicked = S(e.target)[0] === S('.' + settings.bg_class)[0];

            if (bg_clicked) {
              if (settings.close_on_background_click) {
                e.stopPropagation();
              } else {
                return;
              }
            }

            self.locked = true;
            self.close.call(self, bg_clicked ? S('[' + self.attr_name() + '].open:not(.toback)') : S(this).closest('[' + self.attr_name() + ']'));
          }
        });

      if (S('[' + self.attr_name() + ']', this.scope).length > 0) {
        S(this.scope)
          // .off('.reveal')
          .on('open.fndtn.reveal', this.settings.open)
          .on('opened.fndtn.reveal', this.settings.opened)
          .on('opened.fndtn.reveal', this.open_video)
          .on('close.fndtn.reveal', this.settings.close)
          .on('closed.fndtn.reveal', this.settings.closed)
          .on('closed.fndtn.reveal', this.close_video);
      } else {
        S(this.scope)
          // .off('.reveal')
          .on('open.fndtn.reveal', '[' + self.attr_name() + ']', this.settings.open)
          .on('opened.fndtn.reveal', '[' + self.attr_name() + ']', this.settings.opened)
          .on('opened.fndtn.reveal', '[' + self.attr_name() + ']', this.open_video)
          .on('close.fndtn.reveal', '[' + self.attr_name() + ']', this.settings.close)
          .on('closed.fndtn.reveal', '[' + self.attr_name() + ']', this.settings.closed)
          .on('closed.fndtn.reveal', '[' + self.attr_name() + ']', this.close_video);
      }

      return true;
    },

    // PATCH #3: turning on key up capture only when a reveal window is open
    key_up_on : function (scope) {
      var self = this;

      // PATCH #1: fixing multiple keyup event trigger from single key press
      self.S('body').off('keyup.fndtn.reveal').on('keyup.fndtn.reveal', function ( event ) {
        var open_modal = self.S('[' + self.attr_name() + '].open'),
            settings = open_modal.data(self.attr_name(true) + '-init') || self.settings ;
        // PATCH #2: making sure that the close event can be called only while unlocked,
        //           so that multiple keyup.fndtn.reveal events don't prevent clean closing of the reveal window.
        if ( settings && event.which === 27  && settings.close_on_esc && !self.locked) { // 27 is the keycode for the Escape key
          self.close.call(self, open_modal);
        }
      });

      return true;
    },

    // PATCH #3: turning on key up capture only when a reveal window is open
    key_up_off : function (scope) {
      this.S('body').off('keyup.fndtn.reveal');
      return true;
    },

    open : function (target, ajax_settings) {
      var self = this,
          modal;

      if (target) {
        if (typeof target.selector !== 'undefined') {
          // Find the named node; only use the first one found, since the rest of the code assumes there's only one node
          modal = self.S('#' + target.data(self.data_attr('reveal-id'))).first();
        } else {
          modal = self.S(this.scope);

          ajax_settings = target;
        }
      } else {
        modal = self.S(this.scope);
      }

      var settings = modal.data(self.attr_name(true) + '-init');
      settings = settings || this.settings;


      if (modal.hasClass('open') && target !== undefined && target.attr('data-reveal-id') == modal.attr('id')) {
        return self.close(modal);
      }

      if (!modal.hasClass('open')) {
        var open_modal = self.S('[' + self.attr_name() + '].open');

        if (typeof modal.data('css-top') === 'undefined') {
          modal.data('css-top', parseInt(modal.css('top'), 10))
            .data('offset', this.cache_offset(modal));
        }

        modal.attr('tabindex','0').attr('aria-hidden','false');

        this.key_up_on(modal);    // PATCH #3: turning on key up capture only when a reveal window is open

        // Prevent namespace event from triggering twice
        modal.on('open.fndtn.reveal', function(e) {
          if (e.namespace !== 'fndtn.reveal') return;
        });

        modal.on('open.fndtn.reveal').trigger('open.fndtn.reveal');

        if (open_modal.length < 1) {
          this.toggle_bg(modal, true);
        }

        if (typeof ajax_settings === 'string') {
          ajax_settings = {
            url : ajax_settings
          };
        }

        var openModal = function() {
          if(open_modal.length > 0) {
            if(settings.multiple_opened) {
              self.to_back(open_modal);
            } else {
              self.hide(open_modal, settings.css.close);
            }
          }

          // bl: add the open_modal that isn't already in the background to the openModals array
          if(settings.multiple_opened) {
            openModals.push(modal);
          }

          self.show(modal, settings.css.open);
        };

        if (typeof ajax_settings === 'undefined' || !ajax_settings.url) {
          openModal();
        } else {
          var old_success = typeof ajax_settings.success !== 'undefined' ? ajax_settings.success : null;
          $.extend(ajax_settings, {
            success : function (data, textStatus, jqXHR) {
              if ( $.isFunction(old_success) ) {
                var result = old_success(data, textStatus, jqXHR);
                if (typeof result == 'string') {
                  data = result;
                }
              }

              if (typeof options !== 'undefined' && typeof options.replaceContentSel !== 'undefined') {
                modal.find(options.replaceContentSel).html(data);
              } else {
                modal.html(data);
              }

              self.S(modal).foundation('section', 'reflow');
              self.S(modal).children().foundation();

              openModal();
            }
          });

          // check for if user initalized with error callback
          if (settings.on_ajax_error !== $.noop) {
            $.extend(ajax_settings, {
              error : settings.on_ajax_error
            });
          }

          $.ajax(ajax_settings);
        }
      }
      self.S(window).trigger('resize');
    },

    close : function (modal) {
      var modal = modal && modal.length ? modal : this.S(this.scope),
          open_modals = this.S('[' + this.attr_name() + '].open'),
          settings = modal.data(this.attr_name(true) + '-init') || this.settings,
          self = this;

      if (open_modals.length > 0) {

        modal.removeAttr('tabindex','0').attr('aria-hidden','true');

        this.locked = true;
        this.key_up_off(modal);   // PATCH #3: turning on key up capture only when a reveal window is open

        modal.trigger('close.fndtn.reveal');

        if ((settings.multiple_opened && open_modals.length === 1) || !settings.multiple_opened || modal.length > 1) {
          self.toggle_bg(modal, false);
          self.to_front(modal);
        }

        if (settings.multiple_opened) {
          var isCurrent = modal.is(':not(.toback)');
          self.hide(modal, settings.css.close, settings);
          if(isCurrent) {
            // remove the last modal since it is now closed
            openModals.pop();
          } else {
            // if this isn't the current modal, then find it in the array and remove it
            openModals = $.grep(openModals, function(elt) {
              var isThis = elt[0]===modal[0];
              if(isThis) {
                // since it's not currently in the front, put it in the front now that it is hidden
                // so that if it's re-opened, it won't be .toback
                self.to_front(modal);
              }
              return !isThis;
            });
          }
          // finally, show the next modal in the stack, if there is one
          if(openModals.length>0) {
            self.to_front(openModals[openModals.length - 1]);
          }
        } else {
          self.hide(open_modals, settings.css.close, settings);
        }
      }
    },

    close_targets : function () {
      var base = '.' + this.settings.dismiss_modal_class;

      if (this.settings.close_on_background_click) {
        return base + ', .' + this.settings.bg_class;
      }

      return base;
    },

    toggle_bg : function (modal, state) {
      if (this.S('.' + this.settings.bg_class).length === 0) {
        this.settings.bg = $('<div />', {'class': this.settings.bg_class})
          .appendTo('body').hide();
      }

      var visible = this.settings.bg.filter(':visible').length > 0;
      if ( state != visible ) {
        if ( state == undefined ? visible : !state ) {
          this.hide(this.settings.bg);
        } else {
          this.show(this.settings.bg);
        }
      }
    },

    show : function (el, css) {
      // is modal
      if (css) {
        var settings = el.data(this.attr_name(true) + '-init') || this.settings,
            root_element = settings.root_element,
            context = this;

        if (el.parent(root_element).length === 0) {
          var placeholder = el.wrap('<div style="display: none;" />').parent();

          el.on('closed.fndtn.reveal.wrapped', function () {
            el.detach().appendTo(placeholder);
            el.unwrap().unbind('closed.fndtn.reveal.wrapped');
          });

          el.detach().appendTo(root_element);
        }

        var animData = getAnimationData(settings.animation);
        if (!animData.animate) {
          this.locked = false;
        }
        if (animData.pop) {
          css.top = $(window).scrollTop() - el.data('offset') + 'px';
          var end_css = {
            top: $(window).scrollTop() + el.data('css-top') + 'px',
            opacity: 1
          };

          return setTimeout(function () {
            return el
              .css(css)
              .animate(end_css, settings.animation_speed, 'linear', function () {
                context.locked = false;
                el.trigger('opened.fndtn.reveal');
              })
              .addClass('open');
          }, settings.animation_speed / 2);
        }

        css.top = $(window).scrollTop() + el.data('css-top') + 'px';

        if (animData.fade) {
          var end_css = {opacity: 1};

          return setTimeout(function () {
            return el
              .css(css)
              .animate(end_css, settings.animation_speed, 'linear', function () {
                context.locked = false;
                el.trigger('opened.fndtn.reveal');
              })
              .addClass('open');
          }, settings.animation_speed / 2);
        }

        return el.css(css).show().css({opacity : 1}).addClass('open').trigger('opened.fndtn.reveal');
      }

      var settings = this.settings;

      // should we animate the background?
      if (getAnimationData(settings.animation).fade) {
        return el.fadeIn(settings.animation_speed / 2);
      }

      this.locked = false;

      return el.show();
    },

    to_back : function(el) {
      el.addClass('toback');
    },

    to_front : function(el) {
      el.removeClass('toback');
    },

    hide : function (el, css) {
      // is modal
      if (css) {
        var settings = el.data(this.attr_name(true) + '-init'),
            context = this;
        settings = settings || this.settings;

        var animData = getAnimationData(settings.animation);
        if (!animData.animate) {
          this.locked = false;
        }
        if (animData.pop) {
          var end_css = {
            top: - $(window).scrollTop() - el.data('offset') + 'px',
            opacity: 0
          };

          return setTimeout(function () {
            return el
              .animate(end_css, settings.animation_speed, 'linear', function () {
                context.locked = false;
                el.css(css).trigger('closed.fndtn.reveal');
              })
              .removeClass('open');
          }, settings.animation_speed / 2);
        }

        if (animData.fade) {
          var end_css = {opacity : 0};

          return setTimeout(function () {
            return el
              .animate(end_css, settings.animation_speed, 'linear', function () {
                context.locked = false;
                el.css(css).trigger('closed.fndtn.reveal');
              })
              .removeClass('open');
          }, settings.animation_speed / 2);
        }

        return el.hide().css(css).removeClass('open').trigger('closed.fndtn.reveal');
      }

      var settings = this.settings;

      // should we animate the background?
      if (getAnimationData(settings.animation).fade) {
        return el.fadeOut(settings.animation_speed / 2);
      }

      return el.hide();
    },

    close_video : function (e) {
      var video = $('.flex-video', e.target),
          iframe = $('iframe', video);

      if (iframe.length > 0) {
        iframe.attr('data-src', iframe[0].src);
        iframe.attr('src', iframe.attr('src'));
        video.hide();
      }
    },

    open_video : function (e) {
      var video = $('.flex-video', e.target),
          iframe = video.find('iframe');

      if (iframe.length > 0) {
        var data_src = iframe.attr('data-src');
        if (typeof data_src === 'string') {
          iframe[0].src = iframe.attr('data-src');
        } else {
          var src = iframe[0].src;
          iframe[0].src = undefined;
          iframe[0].src = src;
        }
        video.show();
      }
    },

    data_attr : function (str) {
      if (this.namespace.length > 0) {
        return this.namespace + '-' + str;
      }

      return str;
    },

    cache_offset : function (modal) {
      var offset = modal.show().height() + parseInt(modal.css('top'), 10) + modal.scrollY;

      modal.hide();

      return offset;
    },

    off : function () {
      $(this.scope).off('.fndtn.reveal');
    },

    reflow : function () {}
  };

  /*
   * getAnimationData('popAndFade') // {animate: true,  pop: true,  fade: true}
   * getAnimationData('fade')       // {animate: true,  pop: false, fade: true}
   * getAnimationData('pop')        // {animate: true,  pop: true,  fade: false}
   * getAnimationData('foo')        // {animate: false, pop: false, fade: false}
   * getAnimationData(null)         // {animate: false, pop: false, fade: false}
   */
  function getAnimationData(str) {
    var fade = /fade/i.test(str);
    var pop = /pop/i.test(str);
    return {
      animate : fade || pop,
      pop : pop,
      fade : fade
    };
  }
}(jQuery, window, window.document));

;(function ($, window, document, undefined) {
  'use strict';

  Foundation.libs.slider = {
    name : 'slider',

    version : '5.5.3',

    settings : {
      start : 0,
      end : 100,
      step : 1,
      precision : 2,
      initial : null,
      display_selector : '',
      vertical : false,
      trigger_input_change : false,
      on_change : function () {}
    },

    cache : {},

    init : function (scope, method, options) {
      Foundation.inherit(this, 'throttle');
      this.bindings(method, options);
      this.reflow();
    },

    events : function () {
      var self = this;
      $(this.scope)
        .off('.slider')
        .on('mousedown.fndtn.slider touchstart.fndtn.slider pointerdown.fndtn.slider',
        '[' + self.attr_name() + ']:not(.disabled, [disabled]) .range-slider-handle', function (e) {
          if (!self.cache.active) {
            e.preventDefault();
            self.set_active_slider($(e.target));
          }
        })
        .on('mousemove.fndtn.slider touchmove.fndtn.slider pointermove.fndtn.slider', function (e) {
          if (!!self.cache.active) {
            e.preventDefault();
            if ($.data(self.cache.active[0], 'settings').vertical) {
              var scroll_offset = 0;
              if (!e.pageY) {
                scroll_offset = window.scrollY;
              }
              self.calculate_position(self.cache.active, self.get_cursor_position(e, 'y') + scroll_offset);
            } else {
              self.calculate_position(self.cache.active, self.get_cursor_position(e, 'x'));
            }
          }
        })
        .on('mouseup.fndtn.slider touchend.fndtn.slider pointerup.fndtn.slider', function (e) {
          if(!self.cache.active) {
            // if the user has just clicked into the slider without starting to drag the handle
            var slider = $(e.target).attr('role') === 'slider' ? $(e.target) : $(e.target).closest('.range-slider').find("[role='slider']");

            if (slider.length && (!slider.parent().hasClass('disabled') && !slider.parent().attr('disabled'))) {
              self.set_active_slider(slider);
              if ($.data(self.cache.active[0], 'settings').vertical) {
                var scroll_offset = 0;
                if (!e.pageY) {
                  scroll_offset = window.scrollY;
                }
                self.calculate_position(self.cache.active, self.get_cursor_position(e, 'y') + scroll_offset);
              } else {
                self.calculate_position(self.cache.active, self.get_cursor_position(e, 'x'));
              }
            }
          }
          self.remove_active_slider();
        })
        .on('change.fndtn.slider', function (e) {
          self.settings.on_change();
        });

      self.S(window)
        .on('resize.fndtn.slider', self.throttle(function (e) {
          self.reflow();
        }, 300));

      // update slider value as users change input value
      this.S('[' + this.attr_name() + ']').each(function () {
        var slider = $(this),
            handle = slider.children('.range-slider-handle')[0],
            settings = self.initialize_settings(handle);

        if (settings.display_selector != '') {
          $(settings.display_selector).each(function(){
            if ($(this).attr('value')) {
              $(this).off('change').on('change', function () {
                slider.foundation("slider", "set_value", $(this).val());
              });
            }
          });
        }
      });
    },

    get_cursor_position : function (e, xy) {
      var pageXY = 'page' + xy.toUpperCase(),
          clientXY = 'client' + xy.toUpperCase(),
          position;

      if (typeof e[pageXY] !== 'undefined') {
        position = e[pageXY];
      } else if (typeof e.originalEvent[clientXY] !== 'undefined') {
        position = e.originalEvent[clientXY];
      } else if (e.originalEvent.touches && e.originalEvent.touches[0] && typeof e.originalEvent.touches[0][clientXY] !== 'undefined') {
        position = e.originalEvent.touches[0][clientXY];
      } else if (e.currentPoint && typeof e.currentPoint[xy] !== 'undefined') {
        position = e.currentPoint[xy];
      }

      return position;
    },

    set_active_slider : function ($handle) {
      this.cache.active = $handle;
    },

    remove_active_slider : function () {
      this.cache.active = null;
    },

    calculate_position : function ($handle, cursor_x) {
      var self = this,
          settings = $.data($handle[0], 'settings'),
          handle_l = $.data($handle[0], 'handle_l'),
          handle_o = $.data($handle[0], 'handle_o'),
          bar_l = $.data($handle[0], 'bar_l'),
          bar_o = $.data($handle[0], 'bar_o');

      requestAnimationFrame(function () {
        var pct;

        if (Foundation.rtl && !settings.vertical) {
          pct = self.limit_to(((bar_o + bar_l - cursor_x) / bar_l), 0, 1);
        } else {
          pct = self.limit_to(((cursor_x - bar_o) / bar_l), 0, 1);
        }

        pct = settings.vertical ? 1 - pct : pct;

        var norm = self.normalized_value(pct, settings.start, settings.end, settings.step, settings.precision);

        self.set_ui($handle, norm);
      });
    },

    set_ui : function ($handle, value) {
      var settings = $.data($handle[0], 'settings'),
          handle_l = $.data($handle[0], 'handle_l'),
          bar_l = $.data($handle[0], 'bar_l'),
          norm_pct = this.normalized_percentage(value, settings.start, settings.end),
          handle_offset = norm_pct * (bar_l - handle_l) - 1,
          progress_bar_length = norm_pct * 100,
          $handle_parent = $handle.parent(),
          $hidden_inputs = $handle.parent().children('input[type=hidden]');

      if (Foundation.rtl && !settings.vertical) {
        handle_offset = -handle_offset;
      }

      handle_offset = settings.vertical ? -handle_offset + bar_l - handle_l + 1 : handle_offset;
      this.set_translate($handle, handle_offset, settings.vertical);

      if (settings.vertical) {
        $handle.siblings('.range-slider-active-segment').css('height', progress_bar_length + '%');
      } else {
        $handle.siblings('.range-slider-active-segment').css('width', progress_bar_length + '%');
      }

      $handle_parent.attr(this.attr_name(), value).trigger('change.fndtn.slider');

      $hidden_inputs.val(value);
      if (settings.trigger_input_change) {
          $hidden_inputs.trigger('change.fndtn.slider');
      }

      if (!$handle[0].hasAttribute('aria-valuemin')) {
        $handle.attr({
          'aria-valuemin' : settings.start,
          'aria-valuemax' : settings.end
        });
      }
      $handle.attr('aria-valuenow', value);

      if (settings.display_selector != '') {
        $(settings.display_selector).each(function () {
          if (this.hasAttribute('value')) {
            $(this).val(value);
          } else {
            $(this).text(value);
          }
        });
      }

    },

    normalized_percentage : function (val, start, end) {
      return Math.min(1, (val - start) / (end - start));
    },

    normalized_value : function (val, start, end, step, precision) {
      var range = end - start,
          point = val * range,
          mod = (point - (point % step)) / step,
          rem = point % step,
          round = ( rem >= step * 0.5 ? step : 0);
      return ((mod * step + round) + start).toFixed(precision);
    },

    set_translate : function (ele, offset, vertical) {
      if (vertical) {
        $(ele)
          .css('-webkit-transform', 'translateY(' + offset + 'px)')
          .css('-moz-transform', 'translateY(' + offset + 'px)')
          .css('-ms-transform', 'translateY(' + offset + 'px)')
          .css('-o-transform', 'translateY(' + offset + 'px)')
          .css('transform', 'translateY(' + offset + 'px)');
      } else {
        $(ele)
          .css('-webkit-transform', 'translateX(' + offset + 'px)')
          .css('-moz-transform', 'translateX(' + offset + 'px)')
          .css('-ms-transform', 'translateX(' + offset + 'px)')
          .css('-o-transform', 'translateX(' + offset + 'px)')
          .css('transform', 'translateX(' + offset + 'px)');
      }
    },

    limit_to : function (val, min, max) {
      return Math.min(Math.max(val, min), max);
    },

    initialize_settings : function (handle) {
      var settings = $.extend({}, this.settings, this.data_options($(handle).parent())),
          decimal_places_match_result;

      if (settings.precision === null) {
        decimal_places_match_result = ('' + settings.step).match(/\.([\d]*)/);
        settings.precision = decimal_places_match_result && decimal_places_match_result[1] ? decimal_places_match_result[1].length : 0;
      }

      if (settings.vertical) {
        $.data(handle, 'bar_o', $(handle).parent().offset().top);
        $.data(handle, 'bar_l', $(handle).parent().outerHeight());
        $.data(handle, 'handle_o', $(handle).offset().top);
        $.data(handle, 'handle_l', $(handle).outerHeight());
      } else {
        $.data(handle, 'bar_o', $(handle).parent().offset().left);
        $.data(handle, 'bar_l', $(handle).parent().outerWidth());
        $.data(handle, 'handle_o', $(handle).offset().left);
        $.data(handle, 'handle_l', $(handle).outerWidth());
      }

      $.data(handle, 'bar', $(handle).parent());
      return $.data(handle, 'settings', settings);
    },

    set_initial_position : function ($ele) {
      var settings = $.data($ele.children('.range-slider-handle')[0], 'settings'),
          initial = ((typeof settings.initial == 'number' && !isNaN(settings.initial)) ? settings.initial : Math.floor((settings.end - settings.start) * 0.5 / settings.step) * settings.step + settings.start),
          $handle = $ele.children('.range-slider-handle');
      this.set_ui($handle, initial);
    },

    set_value : function (value) {
      var self = this;
      $('[' + self.attr_name() + ']', this.scope).each(function () {
        $(this).attr(self.attr_name(), value);
      });
      if (!!$(this.scope).attr(self.attr_name())) {
        $(this.scope).attr(self.attr_name(), value);
      }
      self.reflow();
    },

    reflow : function () {
      var self = this;
      self.S('[' + this.attr_name() + ']').each(function () {
        var handle = $(this).children('.range-slider-handle')[0],
            val = $(this).attr(self.attr_name());
        self.initialize_settings(handle);

        if (val) {
          self.set_ui($(handle), parseFloat(val));
        } else {
          self.set_initial_position($(this));
        }
      });
    }
  };

}(jQuery, window, window.document));

;(function ($, window, document, undefined) {
  'use strict';

  Foundation.libs.tab = {
    name : 'tab',

    version : '5.5.3',

    settings : {
      active_class : 'active',
      callback : function () {},
      deep_linking : false,
      scroll_to_content : true,
      is_hover : false
    },

    default_tab_hashes : [],

    init : function (scope, method, options) {
      var self = this,
          S = this.S;

  	  // Store the default active tabs which will be referenced when the
  	  // location hash is absent, as in the case of navigating the tabs and
  	  // returning to the first viewing via the browser Back button.
  	  S('[' + this.attr_name() + '] > .active > a', this.scope).each(function () {
  	    self.default_tab_hashes.push(this.hash);
  	  });

      this.bindings(method, options);
      this.handle_location_hash_change();
    },

    events : function () {
      var self = this,
          S = this.S;

      var usual_tab_behavior =  function (e, target) {
        var settings = S(target).closest('[' + self.attr_name() + ']').data(self.attr_name(true) + '-init');
        if (!settings.is_hover || Modernizr.touch) {
          // if user did not pressed tab key, prevent default action
          var keyCode = e.keyCode || e.which;
          if (keyCode !== 9) { 
            e.preventDefault();
            e.stopPropagation();
          }
          self.toggle_active_tab(S(target).parent());
          
        }
      };

      S(this.scope)
        .off('.tab')
        // Key event: focus/tab key
        .on('keydown.fndtn.tab', '[' + this.attr_name() + '] > * > a', function(e) {
          var keyCode = e.keyCode || e.which;
          // if user pressed tab key
          if (keyCode === 13 || keyCode === 32) { // enter or space
            var el = this;
            usual_tab_behavior(e, el);
          } 
        })
        // Click event: tab title
        .on('click.fndtn.tab', '[' + this.attr_name() + '] > * > a', function(e) {
          var el = this;
          usual_tab_behavior(e, el);
        })
        // Hover event: tab title
        .on('mouseenter.fndtn.tab', '[' + this.attr_name() + '] > * > a', function (e) {
          var settings = S(this).closest('[' + self.attr_name() + ']').data(self.attr_name(true) + '-init');
          if (settings.is_hover) {
            self.toggle_active_tab(S(this).parent());
          }
        });

      // Location hash change event
      S(window).on('hashchange.fndtn.tab', function (e) {
        e.preventDefault();
        self.handle_location_hash_change();
      });
    },

    handle_location_hash_change : function () {

      var self = this,
          S = this.S;

      S('[' + this.attr_name() + ']', this.scope).each(function () {
        var settings = S(this).data(self.attr_name(true) + '-init');
        if (settings.deep_linking) {
          // Match the location hash to a label
          var hash;
          if (settings.scroll_to_content) {
            hash = self.scope.location.hash;
          } else {
            // prefix the hash to prevent anchor scrolling
            hash = self.scope.location.hash.replace('fndtn-', '');
          }
          if (hash != '') {
            // Check whether the location hash references a tab content div or
            // another element on the page (inside or outside the tab content div)
            var hash_element = S(hash);
            if (hash_element.hasClass('content') && hash_element.parent().hasClass('tabs-content')) {
              // Tab content div
              self.toggle_active_tab($('[' + self.attr_name() + '] > * > a[href=' + hash + ']').parent());
            } else {
              // Not the tab content div. If inside the tab content, find the
              // containing tab and toggle it as active.
              var hash_tab_container_id = hash_element.closest('.content').attr('id');
              if (hash_tab_container_id != undefined) {
                self.toggle_active_tab($('[' + self.attr_name() + '] > * > a[href=#' + hash_tab_container_id + ']').parent(), hash);
              }
            }
          } else {
            // Reference the default tab hashes which were initialized in the init function
            for (var ind = 0; ind < self.default_tab_hashes.length; ind++) {
              self.toggle_active_tab($('[' + self.attr_name() + '] > * > a[href=' + self.default_tab_hashes[ind] + ']').parent());
            }
          }
        }
       });
     },

    toggle_active_tab : function (tab, location_hash) {
      var self = this,
          S = self.S,
          tabs = tab.closest('[' + this.attr_name() + ']'),
          tab_link = tab.find('a'),
          anchor = tab.children('a').first(),
          target_hash = '#' + anchor.attr('href').split('#')[1],
          target = S(target_hash),
          siblings = tab.siblings(),
          settings = tabs.data(this.attr_name(true) + '-init'),
          interpret_keyup_action = function (e) {
            // Light modification of Heydon Pickering's Practical ARIA Examples: http://heydonworks.com/practical_aria_examples/js/a11y.js

            // define current, previous and next (possible) tabs

            var $original = $(this);
            var $prev = $(this).parents('li').prev().children('[role="tab"]');
            var $next = $(this).parents('li').next().children('[role="tab"]');
            var $target;

            // find the direction (prev or next)

            switch (e.keyCode) {
              case 37:
                $target = $prev;
                break;
              case 39:
                $target = $next;
                break;
              default:
                $target = false
                  break;
            }

            if ($target.length) {
              $original.attr({
                'tabindex' : '-1',
                'aria-selected' : null
              });
              $target.attr({
                'tabindex' : '0',
                'aria-selected' : true
              }).focus();
            }

            // Hide panels

            $('[role="tabpanel"]')
              .attr('aria-hidden', 'true');

            // Show panel which corresponds to target

            $('#' + $(document.activeElement).attr('href').substring(1))
              .attr('aria-hidden', null);

          },
          go_to_hash = function(hash) {
            // This function allows correct behaviour of the browser's back button when deep linking is enabled. Without it
            // the user would get continually redirected to the default hash.
            var default_hash = settings.scroll_to_content ? self.default_tab_hashes[0] : 'fndtn-' + self.default_tab_hashes[0].replace('#', '');

            if (hash !== default_hash || window.location.hash) {
              window.location.hash = hash;
            }
          };

      // allow usage of data-tab-content attribute instead of href
      if (anchor.data('tab-content')) {
        target_hash = '#' + anchor.data('tab-content').split('#')[1];
        target = S(target_hash);
      }

      if (settings.deep_linking) {

        if (settings.scroll_to_content) {

          // retain current hash to scroll to content
          go_to_hash(location_hash || target_hash);

          if (location_hash == undefined || location_hash == target_hash) {
            tab.parent()[0].scrollIntoView();
          } else {
            S(target_hash)[0].scrollIntoView();
          }
        } else {
          // prefix the hashes so that the browser doesn't scroll down
          if (location_hash != undefined) {
            go_to_hash('fndtn-' + location_hash.replace('#', ''));
          } else {
            go_to_hash('fndtn-' + target_hash.replace('#', ''));
          }
        }
      }

      // WARNING: The activation and deactivation of the tab content must
      // occur after the deep linking in order to properly refresh the browser
      // window (notably in Chrome).
      // Clean up multiple attr instances to done once
      tab.addClass(settings.active_class).triggerHandler('opened');
      tab_link.attr({'aria-selected' : 'true',  tabindex : 0});
      siblings.removeClass(settings.active_class)
      siblings.find('a').attr({'aria-selected' : 'false'/*,  tabindex : -1*/});
      target.siblings().removeClass(settings.active_class).attr({'aria-hidden' : 'true'/*,  tabindex : -1*/});
      target.addClass(settings.active_class).attr('aria-hidden', 'false').removeAttr('tabindex');
      settings.callback(tab);
      target.triggerHandler('toggled', [target]);
      tabs.triggerHandler('toggled', [tab]);

      tab_link.off('keydown').on('keydown', interpret_keyup_action );
    },

    data_attr : function (str) {
      if (this.namespace.length > 0) {
        return this.namespace + '-' + str;
      }

      return str;
    },

    off : function () {},

    reflow : function () {}
  };
}(jQuery, window, window.document));

;(function ($, window, document, undefined) {
  'use strict';

  Foundation.libs.tooltip = {
    name : 'tooltip',

    version : '5.5.3',

    settings : {
      additional_inheritable_classes : [],
      tooltip_class : '.tooltip',
      append_to : 'body',
      touch_close_text : 'Tap To Close',
      disable_for_touch : false,
      hover_delay : 200,
      fade_in_duration : 150,
      fade_out_duration : 150,
      show_on : 'all',
      tip_template : function (selector, content) {
        return '<span data-selector="' + selector + '" id="' + selector + '" class="'
          + Foundation.libs.tooltip.settings.tooltip_class.substring(1)
          + '" role="tooltip">' + content + '<span class="nub"></span></span>';
      }
    },

    cache : {},

    init : function (scope, method, options) {
      Foundation.inherit(this, 'random_str');
      this.bindings(method, options);
    },

    should_show : function (target, tip) {
      var settings = $.extend({}, this.settings, this.data_options(target));

      if (settings.show_on === 'all') {
        return true;
      } else if (this.small() && settings.show_on === 'small') {
        return true;
      } else if (this.medium() && settings.show_on === 'medium') {
        return true;
      } else if (this.large() && settings.show_on === 'large') {
        return true;
      }
      return false;
    },

    medium : function () {
      return matchMedia(Foundation.media_queries['medium']).matches;
    },

    large : function () {
      return matchMedia(Foundation.media_queries['large']).matches;
    },

    events : function (instance) {
      var self = this,
          S = self.S;

      self.create(this.S(instance));

      function _startShow(elt, $this, immediate) {
        if (elt.timer) {
          return;
        }

        if (immediate) {
          elt.timer = null;
          self.showTip($this);
        } else {
          elt.timer = setTimeout(function () {
            elt.timer = null;
            self.showTip($this);
          }.bind(elt), self.settings.hover_delay);
        }
      }

      function _startHide(elt, $this) {
        if (elt.timer) {
          clearTimeout(elt.timer);
          elt.timer = null;
        }

        self.hide($this);
      }

      $(this.scope)
        .off('.tooltip')
        .on('mouseenter.fndtn.tooltip mouseleave.fndtn.tooltip touchstart.fndtn.tooltip MSPointerDown.fndtn.tooltip',
          '[' + this.attr_name() + ']', function (e) {
          var $this = S(this),
              settings = $.extend({}, self.settings, self.data_options($this)),
              is_touch = false;

          if (Modernizr.touch && /touchstart|MSPointerDown/i.test(e.type) && S(e.target).is('a')) {
            return false;
          }

          if (/mouse/i.test(e.type) && self.ie_touch(e)) {
            return false;
          }
          
          if ($this.hasClass('open')) {
            if (Modernizr.touch && /touchstart|MSPointerDown/i.test(e.type)) {
              e.preventDefault();
            }
            self.hide($this);
          } else {
            if (settings.disable_for_touch && Modernizr.touch && /touchstart|MSPointerDown/i.test(e.type)) {
              return;
            } else if (!settings.disable_for_touch && Modernizr.touch && /touchstart|MSPointerDown/i.test(e.type)) {
              e.preventDefault();
              S(settings.tooltip_class + '.open').hide();
              is_touch = true;
              // close other open tooltips on touch
              if ($('.open[' + self.attr_name() + ']').length > 0) {
               var prevOpen = S($('.open[' + self.attr_name() + ']')[0]);
               self.hide(prevOpen);
              }
            }

            if (/enter|over/i.test(e.type)) {
              _startShow(this, $this);

            } else if (e.type === 'mouseout' || e.type === 'mouseleave') {
              _startHide(this, $this);
            } else {
              _startShow(this, $this, true);
            }
          }
        })
        .on('mouseleave.fndtn.tooltip touchstart.fndtn.tooltip MSPointerDown.fndtn.tooltip', '[' + this.attr_name() + '].open', function (e) {
          if (/mouse/i.test(e.type) && self.ie_touch(e)) {
            return false;
          }

          if ($(this).data('tooltip-open-event-type') == 'touch' && e.type == 'mouseleave') {
            return;
          } else if ($(this).data('tooltip-open-event-type') == 'mouse' && /MSPointerDown|touchstart/i.test(e.type)) {
            self.convert_to_touch($(this));
          } else {
            _startHide(this, $(this));
          }
        })
        .on('DOMNodeRemoved DOMAttrModified', '[' + this.attr_name() + ']:not(a)', function (e) {
          _startHide(this, S(this));
        });
    },

    ie_touch : function (e) {
      // How do I distinguish between IE11 and Windows Phone 8?????
      return false;
    },

    showTip : function ($target) {
      var $tip = this.getTip($target);
      if (this.should_show($target, $tip)) {
        return this.show($target);
      }
      return;
    },

    getTip : function ($target) {
      var selector = this.selector($target),
          settings = $.extend({}, this.settings, this.data_options($target)),
          tip = null;

      if (selector) {
        tip = this.S('span[data-selector="' + selector + '"]' + settings.tooltip_class);
      }

      return (typeof tip === 'object') ? tip : false;
    },

    selector : function ($target) {
      var dataSelector = $target.attr(this.attr_name()) || $target.attr('data-selector');

      if (typeof dataSelector != 'string') {
        dataSelector = this.random_str(6);
        $target
          .attr('data-selector', dataSelector)
          .attr('aria-describedby', dataSelector);
      }

      return dataSelector;
    },

    create : function ($target) {
      var self = this,
          settings = $.extend({}, this.settings, this.data_options($target)),
          tip_template = this.settings.tip_template;

      if (typeof settings.tip_template === 'string' && window.hasOwnProperty(settings.tip_template)) {
        tip_template = window[settings.tip_template];
      }

      var $tip = $(tip_template(this.selector($target), $('<div></div>').html($target.attr('title')).html())),
          classes = this.inheritable_classes($target);

      $tip.addClass(classes).appendTo(settings.append_to);

      if (Modernizr.touch) {
        $tip.append('<span class="tap-to-close">' + settings.touch_close_text + '</span>');
        $tip.on('touchstart.fndtn.tooltip MSPointerDown.fndtn.tooltip', function (e) {
          self.hide($target);
        });
      }

      $target.removeAttr('title').attr('title', '');
    },

    reposition : function (target, tip, classes) {
      var width, nub, nubHeight, nubWidth, objPos;

      tip.css('visibility', 'hidden').show();

      width = target.data('width');
      nub = tip.children('.nub');
      nubHeight = nub.outerHeight();
      nubWidth = nub.outerWidth();

      if (this.small()) {
        tip.css({'width' : '100%'});
      } else {
        tip.css({'width' : (width) ? width : 'auto'});
      }

      objPos = function (obj, top, right, bottom, left, width) {
        return obj.css({
          'top' : (top) ? top : 'auto',
          'bottom' : (bottom) ? bottom : 'auto',
          'left' : (left) ? left : 'auto',
          'right' : (right) ? right : 'auto'
        }).end();
      };
      
      var o_top = target.offset().top;
      var o_left = target.offset().left;
      var outerHeight = target.outerHeight();

      objPos(tip, (o_top + outerHeight + 10), 'auto', 'auto', o_left);

      if (this.small()) {
        objPos(tip, (o_top + outerHeight + 10), 'auto', 'auto', 12.5, $(this.scope).width());
        tip.addClass('tip-override');
        objPos(nub, -nubHeight, 'auto', 'auto', o_left);
      } else {
        
        if (Foundation.rtl) {
          nub.addClass('rtl');
          o_left = o_left + target.outerWidth() - tip.outerWidth();
        }

        objPos(tip, (o_top + outerHeight + 10), 'auto', 'auto', o_left);
        // reset nub from small styles, if they've been applied
        if (nub.attr('style')) {
          nub.removeAttr('style');
        }
        
        tip.removeClass('tip-override');
        
        var tip_outerHeight = tip.outerHeight();
        
        if (classes && classes.indexOf('tip-top') > -1) {
          if (Foundation.rtl) {
            nub.addClass('rtl');
          }
          objPos(tip, (o_top - tip_outerHeight), 'auto', 'auto', o_left)
            .removeClass('tip-override');
        } else if (classes && classes.indexOf('tip-left') > -1) {
          objPos(tip, (o_top + (outerHeight / 2) - (tip_outerHeight / 2)), 'auto', 'auto', (o_left - tip.outerWidth() - nubHeight))
            .removeClass('tip-override');
          nub.removeClass('rtl');
        } else if (classes && classes.indexOf('tip-right') > -1) {
          objPos(tip, (o_top + (outerHeight / 2) - (tip_outerHeight / 2)), 'auto', 'auto', (o_left + target.outerWidth() + nubHeight))
            .removeClass('tip-override');
          nub.removeClass('rtl');
        }
      }

      tip.css('visibility', 'visible').hide();
    },

    small : function () {
      return matchMedia(Foundation.media_queries.small).matches &&
        !matchMedia(Foundation.media_queries.medium).matches;
    },

    inheritable_classes : function ($target) {
      var settings = $.extend({}, this.settings, this.data_options($target)),
          inheritables = ['tip-top', 'tip-left', 'tip-bottom', 'tip-right', 'radius', 'round'].concat(settings.additional_inheritable_classes),
          classes = $target.attr('class'),
          filtered = classes ? $.map(classes.split(' '), function (el, i) {
            if ($.inArray(el, inheritables) !== -1) {
              return el;
            }
          }).join(' ') : '';

      return $.trim(filtered);
    },

    convert_to_touch : function ($target) {
      var self = this,
          $tip = self.getTip($target),
          settings = $.extend({}, self.settings, self.data_options($target));

      if ($tip.find('.tap-to-close').length === 0) {
        $tip.append('<span class="tap-to-close">' + settings.touch_close_text + '</span>');
        $tip.on('click.fndtn.tooltip.tapclose touchstart.fndtn.tooltip.tapclose MSPointerDown.fndtn.tooltip.tapclose', function (e) {
          self.hide($target);
        });
      }

      $target.data('tooltip-open-event-type', 'touch');
    },

    show : function ($target) {
      var $tip = this.getTip($target);
      if ($target.data('tooltip-open-event-type') == 'touch') {
        this.convert_to_touch($target);
      }

      this.reposition($target, $tip, $target.attr('class'));
      $target.addClass('open');
      $tip.fadeIn(this.settings.fade_in_duration);
    },

    hide : function ($target) {
      var $tip = this.getTip($target);

      $tip.fadeOut(this.settings.fade_out_duration, function () {
        $tip.find('.tap-to-close').remove();
        $tip.off('click.fndtn.tooltip.tapclose MSPointerDown.fndtn.tapclose');
        $target.removeClass('open');
      });
    },

    off : function () {
      var self = this;
      this.S(this.scope).off('.fndtn.tooltip');
      this.S(this.settings.tooltip_class).each(function (i) {
        $('[' + self.attr_name() + ']').eq(i).attr('title', $(this).text());
      }).remove();
    },

    reflow : function () {}
  };
}(jQuery, window, window.document));

;(function ($, window, document, undefined) {
  'use strict';

  Foundation.libs.topbar = {
    name : 'topbar',

    version : '5.5.3',

    settings : {
      index : 0,
      start_offset : 0,
      sticky_class : 'sticky',
      custom_back_text : true,
      back_text : 'Back',
      mobile_show_parent_link : true,
      is_hover : true,
      scrolltop : true, // jump to top when sticky nav menu toggle is clicked
      sticky_on : 'all',
      dropdown_autoclose: true
    },

    init : function (section, method, options) {
      Foundation.inherit(this, 'add_custom_rule register_media throttle');
      var self = this;

      self.register_media('topbar', 'foundation-mq-topbar');

      this.bindings(method, options);

      self.S('[' + this.attr_name() + ']', this.scope).each(function () {
        var topbar = $(this),
            settings = topbar.data(self.attr_name(true) + '-init'),
            section = self.S('section, .top-bar-section', this);
        topbar.data('index', 0);
        var topbarContainer = topbar.parent();
        if (topbarContainer.hasClass('fixed') || self.is_sticky(topbar, topbarContainer, settings) ) {
          self.settings.sticky_class = settings.sticky_class;
          self.settings.sticky_topbar = topbar;
          topbar.data('height', topbarContainer.outerHeight());
          topbar.data('stickyoffset', topbarContainer.offset().top);
        } else {
          topbar.data('height', topbar.outerHeight());
        }

        if (!settings.assembled) {
          self.assemble(topbar);
        }

        if (settings.is_hover) {
          self.S('.has-dropdown', topbar).addClass('not-click');
        } else {
          self.S('.has-dropdown', topbar).removeClass('not-click');
        }

        // Pad body when sticky (scrolled) or fixed.
        self.add_custom_rule('.f-topbar-fixed { padding-top: ' + topbar.data('height') + 'px }');

        if (topbarContainer.hasClass('fixed')) {
          self.S('body').addClass('f-topbar-fixed');
        }
      });

    },

    is_sticky : function (topbar, topbarContainer, settings) {
      var sticky     = topbarContainer.hasClass(settings.sticky_class);
      var smallMatch = matchMedia(Foundation.media_queries.small).matches;
      var medMatch   = matchMedia(Foundation.media_queries.medium).matches;
      var lrgMatch   = matchMedia(Foundation.media_queries.large).matches;

      if (sticky && settings.sticky_on === 'all') {
        return true;
      }
      if (sticky && this.small() && settings.sticky_on.indexOf('small') !== -1) {
        if (smallMatch && !medMatch && !lrgMatch) { return true; }
      }
      if (sticky && this.medium() && settings.sticky_on.indexOf('medium') !== -1) {
        if (smallMatch && medMatch && !lrgMatch) { return true; }
      }
      if (sticky && this.large() && settings.sticky_on.indexOf('large') !== -1) {
        if (smallMatch && medMatch && lrgMatch) { return true; }
      }

       return false;
    },

    toggle : function (toggleEl) {
      var self = this,
          topbar;

      if (toggleEl) {
        topbar = self.S(toggleEl).closest('[' + this.attr_name() + ']');
      } else {
        topbar = self.S('[' + this.attr_name() + ']');
      }

      var settings = topbar.data(this.attr_name(true) + '-init');

      var section = self.S('section, .top-bar-section', topbar);

      if (self.breakpoint()) {
        if (!self.rtl) {
          section.css({left : '0%'});
          $('>.name', section).css({left : '100%'});
        } else {
          section.css({right : '0%'});
          $('>.name', section).css({right : '100%'});
        }

        self.S('li.moved', section).removeClass('moved');
        topbar.data('index', 0);

        topbar
          .toggleClass('expanded')
          .css('height', '');
      }

      if (settings.scrolltop) {
        if (!topbar.hasClass('expanded')) {
          if (topbar.hasClass('fixed')) {
            topbar.parent().addClass('fixed');
            topbar.removeClass('fixed');
            self.S('body').addClass('f-topbar-fixed');
          }
        } else if (topbar.parent().hasClass('fixed')) {
          if (settings.scrolltop) {
            topbar.parent().removeClass('fixed');
            topbar.addClass('fixed');
            self.S('body').removeClass('f-topbar-fixed');

            window.scrollTo(0, 0);
          } else {
            topbar.parent().removeClass('expanded');
          }
        }
      } else {
        if (self.is_sticky(topbar, topbar.parent(), settings)) {
          topbar.parent().addClass('fixed');
        }

        if (topbar.parent().hasClass('fixed')) {
          if (!topbar.hasClass('expanded')) {
            topbar.removeClass('fixed');
            topbar.parent().removeClass('expanded');
            self.update_sticky_positioning();
          } else {
            topbar.addClass('fixed');
            topbar.parent().addClass('expanded');
            self.S('body').addClass('f-topbar-fixed');
          }
        }
      }
    },

    timer : null,

    events : function (bar) {
      var self = this,
          S = this.S;

      S(this.scope)
        .off('.topbar')
        .on('click.fndtn.topbar', '[' + this.attr_name() + '] .toggle-topbar', function (e) {
          e.preventDefault();
          self.toggle(this);
        })
        .on('click.fndtn.topbar contextmenu.fndtn.topbar', '.top-bar .top-bar-section li a[href^="#"],[' + this.attr_name() + '] .top-bar-section li a[href^="#"]', function (e) {
          var li = $(this).closest('li'),
              topbar = li.closest('[' + self.attr_name() + ']'),
              settings = topbar.data(self.attr_name(true) + '-init');

          if (settings.dropdown_autoclose && settings.is_hover) {
            var hoverLi = $(this).closest('.hover');
            hoverLi.removeClass('hover');
          }
          if (self.breakpoint() && !li.hasClass('back') && !li.hasClass('has-dropdown')) {
            self.toggle();
          }

        })
        .on('click.fndtn.topbar', '[' + this.attr_name() + '] li.has-dropdown', function (e) {
          var li = S(this),
              target = S(e.target),
              topbar = li.closest('[' + self.attr_name() + ']'),
              settings = topbar.data(self.attr_name(true) + '-init');

          if (target.data('revealId')) {
            self.toggle();
            return;
          }

          if (self.breakpoint()) {
            return;
          }

          if (settings.is_hover && !Modernizr.touch) {
            return;
          }

          e.stopImmediatePropagation();

          if (li.hasClass('hover')) {
            li
              .removeClass('hover')
              .find('li')
              .removeClass('hover');

            li.parents('li.hover')
              .removeClass('hover');
          } else {
            li.addClass('hover');

            $(li).siblings().removeClass('hover');

            if (target[0].nodeName === 'A' && target.parent().hasClass('has-dropdown')) {
              e.preventDefault();
            }
          }
        })
        .on('click.fndtn.topbar', '[' + this.attr_name() + '] .has-dropdown>a', function (e) {
          if (self.breakpoint()) {

            e.preventDefault();

            var $this = S(this),
                topbar = $this.closest('[' + self.attr_name() + ']'),
                section = topbar.find('section, .top-bar-section'),
                dropdownHeight = $this.next('.dropdown').outerHeight(),
                $selectedLi = $this.closest('li');

            topbar.data('index', topbar.data('index') + 1);
            $selectedLi.addClass('moved');

            if (!self.rtl) {
              section.css({left : -(100 * topbar.data('index')) + '%'});
              section.find('>.name').css({left : 100 * topbar.data('index') + '%'});
            } else {
              section.css({right : -(100 * topbar.data('index')) + '%'});
              section.find('>.name').css({right : 100 * topbar.data('index') + '%'});
            }

            topbar.css('height', $this.siblings('ul').outerHeight(true) + topbar.data('height'));
          }
        });

      S(window).off('.topbar').on('resize.fndtn.topbar', self.throttle(function () {
          self.resize.call(self);
      }, 50)).trigger('resize.fndtn.topbar').load(function () {
          // Ensure that the offset is calculated after all of the pages resources have loaded
          S(this).trigger('resize.fndtn.topbar');
      });

      S('body').off('.topbar').on('click.fndtn.topbar', function (e) {
        var parent = S(e.target).closest('li').closest('li.hover');

        if (parent.length > 0) {
          return;
        }

        S('[' + self.attr_name() + '] li.hover').removeClass('hover');
      });

      // Go up a level on Click
      S(this.scope).on('click.fndtn.topbar', '[' + this.attr_name() + '] .has-dropdown .back', function (e) {
        e.preventDefault();

        var $this = S(this),
            topbar = $this.closest('[' + self.attr_name() + ']'),
            section = topbar.find('section, .top-bar-section'),
            settings = topbar.data(self.attr_name(true) + '-init'),
            $movedLi = $this.closest('li.moved'),
            $previousLevelUl = $movedLi.parent();

        topbar.data('index', topbar.data('index') - 1);

        if (!self.rtl) {
          section.css({left : -(100 * topbar.data('index')) + '%'});
          section.find('>.name').css({left : 100 * topbar.data('index') + '%'});
        } else {
          section.css({right : -(100 * topbar.data('index')) + '%'});
          section.find('>.name').css({right : 100 * topbar.data('index') + '%'});
        }

        if (topbar.data('index') === 0) {
          topbar.css('height', '');
        } else {
          topbar.css('height', $previousLevelUl.outerHeight(true) + topbar.data('height'));
        }

        setTimeout(function () {
          $movedLi.removeClass('moved');
        }, 300);
      });

      // Show dropdown menus when their items are focused
      S(this.scope).find('.dropdown a')
        .focus(function () {
          $(this).parents('.has-dropdown').addClass('hover');
        })
        .blur(function () {
          $(this).parents('.has-dropdown').removeClass('hover');
        });
    },

    resize : function () {
      var self = this;
      self.S('[' + this.attr_name() + ']').each(function () {
        var topbar = self.S(this),
            settings = topbar.data(self.attr_name(true) + '-init');

        var stickyContainer = topbar.parent('.' + self.settings.sticky_class);
        var stickyOffset;

        if (!self.breakpoint()) {
          var doToggle = topbar.hasClass('expanded');
          topbar
            .css('height', '')
            .removeClass('expanded')
            .find('li')
            .removeClass('hover');

            if (doToggle) {
              self.toggle(topbar);
            }
        }

        if (self.is_sticky(topbar, stickyContainer, settings)) {
          if (stickyContainer.hasClass('fixed')) {
            // Remove the fixed to allow for correct calculation of the offset.
            stickyContainer.removeClass('fixed');

            stickyOffset = stickyContainer.offset().top;
            if (self.S(document.body).hasClass('f-topbar-fixed')) {
              stickyOffset -= topbar.data('height');
            }

            topbar.data('stickyoffset', stickyOffset);
            stickyContainer.addClass('fixed');
          } else {
            stickyOffset = stickyContainer.offset().top;
            topbar.data('stickyoffset', stickyOffset);
          }
        }

      });
    },

    breakpoint : function () {
      return !matchMedia(Foundation.media_queries['topbar']).matches;
    },

    small : function () {
      return matchMedia(Foundation.media_queries['small']).matches;
    },

    medium : function () {
      return matchMedia(Foundation.media_queries['medium']).matches;
    },

    large : function () {
      return matchMedia(Foundation.media_queries['large']).matches;
    },

    assemble : function (topbar) {
      var self = this,
          settings = topbar.data(this.attr_name(true) + '-init'),
          section = self.S('section, .top-bar-section', topbar);

      // Pull element out of the DOM for manipulation
      section.detach();

      self.S('.has-dropdown>a', section).each(function () {
        var $link = self.S(this),
            $dropdown = $link.siblings('.dropdown'),
            url = $link.attr('href'),
            $titleLi;

        if (!$dropdown.find('.title.back').length) {

          if (settings.mobile_show_parent_link == true && url) {
            $titleLi = $('<li class="title back js-generated"><h5><a href="javascript:void(0)"></a></h5></li><li class="parent-link hide-for-medium-up"><a class="parent-link js-generated" href="' + url + '">' + $link.html() +'</a></li>');
          } else {
            $titleLi = $('<li class="title back js-generated"><h5><a href="javascript:void(0)"></a></h5>');
          }

          // Copy link to subnav
          if (settings.custom_back_text == true) {
            $('h5>a', $titleLi).html(settings.back_text);
          } else {
            $('h5>a', $titleLi).html('&laquo; ' + $link.html());
          }
          $dropdown.prepend($titleLi);
        }
      });

      // Put element back in the DOM
      section.appendTo(topbar);

      // check for sticky
      this.sticky();

      this.assembled(topbar);
    },

    assembled : function (topbar) {
      topbar.data(this.attr_name(true), $.extend({}, topbar.data(this.attr_name(true)), {assembled : true}));
    },

    height : function (ul) {
      var total = 0,
          self = this;

      $('> li', ul).each(function () {
        total += self.S(this).outerHeight(true);
      });

      return total;
    },

    sticky : function () {
      var self = this;

      this.S(window).on('scroll', function () {
        self.update_sticky_positioning();
      });
    },

    update_sticky_positioning : function () {
      var klass = '.' + this.settings.sticky_class,
          $window = this.S(window),
          self = this;

      if (self.settings.sticky_topbar && self.is_sticky(this.settings.sticky_topbar,this.settings.sticky_topbar.parent(), this.settings)) {
        var distance = this.settings.sticky_topbar.data('stickyoffset') + this.settings.start_offset;
        if (!self.S(klass).hasClass('expanded')) {
          if ($window.scrollTop() > (distance)) {
            if (!self.S(klass).hasClass('fixed')) {
              self.S(klass).addClass('fixed');
              self.S('body').addClass('f-topbar-fixed');
            }
          } else if ($window.scrollTop() <= distance) {
            if (self.S(klass).hasClass('fixed')) {
              self.S(klass).removeClass('fixed');
              self.S('body').removeClass('f-topbar-fixed');
            }
          }
        }
      }
    },

    off : function () {
      this.S(this.scope).off('.fndtn.topbar');
      this.S(window).off('.fndtn.topbar');
    },

    reflow : function () {}
  };
}(jQuery, window, window.document));

define("foundation", function(){});

define('breakpoints',{
    'small' : ['0em','40em'],
    'medium': ['40.063em', '59.875em'],
    'large': ['59.938em', '90em']
});

// jshint maxstatements: false, newcap: false
define('checkViewport',[],
    function() {

    "use strict";

    var isSmallViewport = function(){
        return window.matchMedia(Foundation.media_queries['small-only']).matches;
    };

    var isMediumViewport = function(){
        return window.matchMedia(Foundation.media_queries['medium-only']).matches;
    };

    var isLargeViewport = function(){
        return window.matchMedia(Foundation.media_queries['large-only']).matches;
    };

    return {
        isSmallViewport: isSmallViewport,
        isMediumViewport: isMediumViewport,
        isLargeViewport: isLargeViewport
    };
} );

// jshint maxstatements: false, newcap: false
define('modal',['jquery'],
    function ($) {

        "use strict";

        var modalLaunchSelector = '.nt-js-launch-modal',
            modalCloseSelector = '.nt-js-close-modal',
            modalSelector = '.nt-js-modal',
            $modal = $(modalSelector),
            $page = $('.nt-main-wrapper:first()'),
            activeClassName = 'active',
            activeModalSelector = modalSelector + '.' + activeClassName,
            isFixedSupported = $('html').hasClass('fixed-supported'),
            scrollTop,
            $focusOnClose;

        var returnFocusToPage = function (id) {
            if ($focusOnClose && $focusOnClose.is(':visible')) {
                $focusOnClose.focus();
                $focusOnClose = null;
                return;
            }
            $('[href="#' + id + '"]').first().focus();
        };

        var closeAll = function (id) {
            var idFound = false;
            modalOpen = false;

            var modalFadeOut = function ($modal) {

                var positionTop = parseInt($('body').css('top'));

                if (isFixedSupported) {
                    //isFixedSupport solution designed for Safari Mac OS and OSX
                    //All other browsers support non-fixed version
                    $modal.css({
                        'position': 'fixed'
                    });
                    setTimeout(function () {
                        $modal.fadeOut(function () {
                            $(this).removeAttr('style');
                            returnFocusToPage(id);
                        });
                        $modal.removeClass(activeClassName);
                        //make modal elements able to focus
                        $modal.removeAttr("tabindex", "-1");

                        $modal.trigger('modalclose');
                        jQuery('html, body').css({"overflow-y": ""});
                        $(window).scrollTop(scrollTop);
                    }, 50);

                } else {
                    $modal.css('top', scrollTop).fadeOut(function () {
                        $(this).css('top', 0);
                        returnFocusToPage(id);
                    });
                    $modal.removeClass(activeClassName);
                    $modal.trigger('modalClose');
                    jQuery('html, body').css({"overflow-y": ""});
                    $(window).scrollTop(scrollTop);
                }

                $(window).scrollTop(positionTop);

            };

            setPageHeight(false);
            if (id) {
                $modal.each(function () {
                    if ($(this).attr('id') === id) {
                        idFound = true;
                        modalFadeOut($(this));
                    } else {
                        $(this).hide();
                    }
                });
                if (!idFound) {
                    $('a:first').focus();
                }
            } else {
                $modal.hide();
            }

            //TODO reconsider hide/show of non modal elements
            //not to be confused with setPageHeight - this hides distracting sticky element
            $('.nt-main-back-to-top').show();

        };

        var close = function (id) {
            modalOpen = false;
            closeAll(id);
        };

        var setPageHeight = function (set) {

            //IE11 (only) links stop working when overflow hidden then visible applied. (IE 11.0.06 to 11.0.14 tested)
            // if(set){
            //     $page.height( $(window).height() ).css('overflow', 'hidden');
            // } else {
            //     $page.height('auto').css('overflow', 'visible');;
            // }

            //IE11 Workaround (applied to all browsers)
            if (set) {
                $('#main-content, .nt-main-wrapper > aside, .nt-footer-wrapper').hide();
            } else {
                $('#main-content, .nt-main-wrapper > aside, .nt-footer-wrapper').show();
            }
        };

        var setModalHeight = function (id) {
            // $('#'+id).css('min-height', $(window).height() +'px');
        };

        var modalOpen = false;
        var open = function (id, $newFocusOnClose) {
            closeAll();
            modalOpen = true;

            scrollTop = $(window).scrollTop();
            setModalHeight(id);

            $focusOnClose = $newFocusOnClose || null;

            //TODO reconsider hide/show of non modal elements
            //not to be confused with setPageHeight - this hides distracting sticky element
            $('.nt-main-back-to-top').hide();

            if (isFixedSupported) {
                //isFixedSupported solution designed for Safari Mac OS and OSX
                //All other browsers support non-fixed version
                $('#' + id).css({'position': 'fixed', '-webkit-backface-visibility': 'hidden', 'z-index': '9999'}).addClass(activeClassName).fadeIn(function () {
                    $modal = $(this);

                    //make modal elements able to focus
                    $modal.attr("tabindex", "-1");

                    $modal.focus();
                    $modal.css('top', 0).focus();

                    $(window).scrollTop(scrollTop);
                    jQuery('html, body').css({"overflow-y": "hidden"});

                    if (navigator.userAgent.match(/(iPhone|iPod|iPad)/i)) {
                        var $propertyModal = $('.property-modal');
                        var $ntInlineHeading = $('.nt-inline-heading');
                        var $ntModalPanel = $('.nt-modal-panel');

                        $propertyModal.css({'height': '100%', 'padding-top': '2em', 'padding-bottom': '2em'});
                        $ntInlineHeading.css({'position': 'relative'});
                        $ntModalPanel.css({'overflow': 'auto'});
                    }

                });
            } else {
                $('#' + id).css('top', scrollTop).addClass(activeClassName).fadeIn(function () {
                    $modal = $(this);
                    $(window).scrollTop(scrollTop)

                });
            }
        };

        var primeAllModals = function () {
            //move modals outside of main content and aside (AKA m57 membership table hack)
            $page.append($('#main-content, .nt-main-wrapper > aside, .nt-footer-wrapper').find(modalSelector));
        };

        var maintainActiveFocus = function ($parent, activeClassName) {
            var checkFocus = true;
            $(document).on("focusin", function (e) {
                if (checkFocus) {
                    $parent.each(function () {
                        //parent IS active
                        //parent NOT focused
                        //next focus element NOT within parent
                        if ($(this).hasClass(activeClassName) && !$(this).is(':focus') && !$(this).find(e.target).length) {
                            e.preventDefault();
                            checkFocus = false;
                            $(this).focus();
                            checkFocus = true;
                        }
                    });
                }

            });
        };

        var lastModal = null;
        var bindEvents = function () {
            primeAllModals();


            if (history.pushState) {
                window.addEventListener("popstate", function (e) {
                    if (e.state && e.state.modalId && modalOpen == false) {
                        lastModal = $("#" + e.state.modalId);
                        open(e.state.modalId, null);

                    } else if (lastModal !== null) {
                        var id = $(lastModal).closest('.nt-js-modal').attr('id');
                        close(id);
                        lastModal = null;
                    }
                    e.preventDefault();
                });
            }

            //open
            $('body').off('click.modal', modalLaunchSelector).on('click.modal', modalLaunchSelector, function (e) {
                if (modalOpen === true) {
                    e.preventDefault();
                    return;
                }
                modalOpen = true;

                var id = $(this).attr('href').replace('#', '');
                if (history.pushState && (history.state == null || typeof(history.state.modalId) == "undefined")) {
                    var name = "";
                    if ($(this).find(".nt-visitor-information-text")) {
                        name = $(this).find(".nt-visitor-information-text").text();
                    }

                    var state = {modalId: id};

                    var noHashUri = window.location.href.substr(0, window.location.href.indexOf('#'));
                    name = noHashUri + "#" + name;

                    history.pushState(state, "modal", name);
                }

                lastModal = $("#" + id);
                open(id, $(this));

                e.preventDefault();
            });

            //close
            $('body').off('click.modal', modalCloseSelector).on('click.modal', modalCloseSelector, function (e) {

                if (modalOpen === false) {
                    e.preventDefault();
                    return;
                }
                modalOpen = false;

                if (history.pushState) {
                    history.back();
                } else {
                    var id = $(lastModal).closest('.nt-js-modal').attr('id');
                    close(id);
                    lastModal = null;
                }

                e.preventDefault();
            });

            $(window).on('resize.modal', function () {
                var $activeModal = null;
                $($modal).each(function (index, elem) {
                    if ($(elem).hasClass(activeClassName)) {
                        $activeModal = $(elem);
                    }
                });
                if ($activeModal !== null) {
                    setModalHeight($activeModal.attr('id'));
                }
            });

            //Maintain focus within open modal
            maintainActiveFocus($modal, activeClassName);

            //Esc key closes modal
            $(window).on("keydown", function (e) {
                if (modalOpen === false) {
                    return;
                }
                if ($(activeModalSelector).length && e.keyCode == 27) {
                    if (history.pushState) {
                        history.back();
                    } else {
                        close($(activeModalSelector).attr('id'));
                    }
                }
            });


        };

        return {
            init: function ($isModuleOnPage) {
                if ($isModuleOnPage.length !== 0) {
                    bindEvents();

                }
            },
            open: open
        };
    });

// jshint maxstatements: false, newcap: false
define( 'iframeEvents',[ 'jquery' ],
    function( $ ) {

    "use strict";

    var iframeOpenSelector = '.nt-js-launch-iframe',
        iframeCloseSelector = '.nt-js-close-iframe';

    // Get function from string, with or without scopes (by Nicolas Gauthier)
    var getFunctionFromString = function(string)
    {
        var scope = window,
            scopeSplit = string.split('.'),
            i;
            
        for (i = 0; i < scopeSplit.length - 1; i++){
            scope = scope[scopeSplit[i]];

            if(scope == undefined) {
                return;
            }
        }

        return scope[scopeSplit[scopeSplit.length - 1]];
    };

    var callCustomMethodOnOpen = function(idSelector){
        var $script = $(idSelector).find('script:first'),
            methodName;

        if($script.length && $script.data('onload')){
            methodName = getFunctionFromString( $script.data('onload') );
            if($.isFunction(methodName)){
                methodName();
            }
        }
    };

    var callCustomMethodOnClose = function(idSelector){
        var $script = $(idSelector).find('script:first'),
            methodName;

        if($script.length && $script.data('onclose')){
            methodName = getFunctionFromString( $script.data('onclose') );
            if($.isFunction(methodName)){
                methodName();
            }
        }
    };

    var bindEvents = function() {
        //open
        $('body').on('click.iframeOpen', iframeOpenSelector, function(e){
            e.preventDefault;
            callCustomMethodOnOpen($(this).attr('href'));
        });

        //close
        $('body').on('click.iframeClose', iframeCloseSelector, function(e){
            var modalId = $(this).closest('.nt-modal').attr('id');
            e.preventDefault;
            callCustomMethodOnClose('#'+modalId);
        });
    };

    return {
        init: function( $isModuleOnPage ) {
            if ( $isModuleOnPage.length !== 0 ) {
                bindEvents();
            }
        },
        onOpen: callCustomMethodOnOpen
    };
});
// jshint maxstatements: false, newcap: false
define( 'jsLink',[ 'jquery', 'modal', 'iframeEvents' ],
    function( $, modal, iframeEvents ) {

    "use strict";

    var bindEvents = function() {
        //Create element event
        $('body').on('click.jsLink', '.nt-js-link', function(e){
            var $a = $(this).find('a:first'),
                href = $a.attr('href'),
                isLaunchModal,
                isLaunchIFrame;

            if($(e.target).is('a')){
                return;
            }

            e.preventDefault();

            isLaunchModal = $a.hasClass('nt-js-launch-modal');
            isLaunchIFrame = $a.hasClass('nt-js-launch-iframe');

            if(href && isLaunchModal){
                modal.open(href.substring(1), $a);
                if(isLaunchIFrame){
                    iframeEvents.onOpen(href);
                }
            } else if (href) {
                window.location.href = href;
            }
        });

    };

    return {
        init: function( $isModuleOnPage ) {
            if ( $isModuleOnPage.length !== 0 ) {
                bindEvents();
            }
        }
    };
} );

// jshint maxstatements: false, newcap: false

define( 'jsSocialLink',[ 'jquery' ],
    function( $ ) {

    "use strict";
    /**
     * If its a trusted event (actually clicked by user) browser / adblocker will not prevent it.
     * Just in case the pop up is prevented, it will work as regular link
     */

    var loadPopupWindow = function(url, w , h){

        var wLeft = window.screenLeft ? window.screenLeft : window.screenX,
            wTop = window.screenTop ? window.screenTop : window.screenY,
            left = wLeft + (window.innerWidth / 2) - (w / 2),
            top = wTop + (window.innerHeight / 2) - (h / 2),
            strWindowFeatures = "width=" + w + ", height=" + h + ", top=" + top + ", left=" + left;
        
        return window.open(url, "_blank", strWindowFeatures);
    };

    var bindEvents = function() {

        //Create element event
        $('.nt-js-social-link').on('click', function(e){
            var href = $(this).attr('href'),
                popupObj

            if(!href || href.indexOf('http') !== 0){
                return;
            }

            if(href){
                popupObj = loadPopupWindow(href, 600,400);

                 //if successfully open the popup window - disable link behaviour
                if(popupObj !== null){
                    e.preventDefault();
                }
            }
        });

        $('.nt-js-social-calendar-link').on('click', function(e){
            var selector = $(this).attr('href');
            e.preventDefault();
            $(this).toggleClass('active', !$(selector).hasClass('social-calendar-active'));
            $(selector).toggleClass('social-calendar-active', !$(selector).hasClass('social-calendar-active'));
        });
    };

    return {
        init: function( $isModuleOnPage ) {
            if ( $isModuleOnPage.length !== 0 ) {
                bindEvents();
            }
        }
    };
} );

// jshint maxstatements: false, newcap: false
define( 'setParentHeight',[ 'jquery'],
function( $ ) {

	"use strict";

	var parentHeightArray = [];

	function SetParentHeight($el, $parent){

		var lteClassName = 'lte-vh', //less than or equal to viewport height
			gtClassName = 'gt-vh'; //less than viewport height

		var setClassName = function(gt){
			if(gt){
				$parent.removeClass(lteClassName).addClass(gtClassName);
			} else {
				$parent.removeClass(gtClassName).addClass(lteClassName);
			}
		};

		var setHeight = function(height){
			$parent.css('height',height+'px');
		};

		this.update = function(){
			var viewportHeight = $(window).height(),
				height = $el.height();
			
			setHeight( Math.floor(height) );

			setClassName(height > viewportHeight);
		};

		this.init = function(){
			$(window).on('load resize', this.update);
			this.update();
		};
	}

	var bindEvents = function($el){
		$el.each( function(){
			parentHeightArray.unshift( new SetParentHeight($(this), $(this).parent()) );
			parentHeightArray[0].init();
		});
	};

	return {
		init: function( $isModuleOnPage ) {
            if ( $isModuleOnPage.length !== 0 ) {
                bindEvents($isModuleOnPage);
            }
        },
        updateAll: function() {
        	$.each( parentHeightArray, function(i, setParentHeightInstance){
        		setParentHeightInstance.update();
        	});
        }
	};
});

// jshint maxstatements: false, newcap: false
define( 'gotoSmooth',[ 'jquery' ],
    function( $ ) {

    "use strict";

    var inProgress = false;

    var gotoElement = function($el, gapTop, animationTime){
        var gapTop = gapTop || 0,
            animationTime = animationTime || 2000;

        if(inProgress){
            return;
        }

        if($el.length){
            //IE Mobile scrolls to content by default and cannot be overridden.
            if(navigator.userAgent.match(/iemobile/i)){
                $(document).scrollTop($el.offset().top - gapTop);
            } else {
                inProgress = true;
                $("html, body").animate({ scrollTop: $el.offset().top - gapTop }, animationTime);
                setTimeout( function(){
                    inProgress = false;
                    $el.attr("tabindex", 0);
                    $el.focus();
                }, animationTime);
            }
            
        }

    };

    var bindEvents = function() {

        $('.nt-js-goto-smooth').off('click.goto').on('click.goto', function(e){
            var targetElem = e.currentTarget.hash;
            e.preventDefault();
            gotoElement( $(targetElem) );
        });
    };

    return {
        init: function( $isModuleOnPage ) {
            if ( $isModuleOnPage.length !== 0 ) {
                bindEvents();
            }
        },
        gotoElement : gotoElement
    };
} );

// jshint maxstatements: false, newcap: false
define( 'secondaryMenu',[ 'jquery', 'checkViewport', 'setParentHeight', 'gotoSmooth' ],
	function( $, checkViewport, setParentHeight, gotoSmooth ) {

		"use strict";

/*
	Nested elements and classNames

	<? class="" OR class="active">
	  <? class="nt-js-secondary-menu"
        <a></a> $clickElem
	    ...
	  </?>
	</?>

*/
		var bindEvents = function() {
			var $clickElem = $('.nt-js-secondary-menu a');

			//Footer secondary menu resizes parent element on Small and Medium
			//viewports. Parent element relies on set parent height for
			//peek-a-boo menu.
			var checkParentHeight = function($el){
				if($el.closest('.nt-js-set-parent-height').length){
					setParentHeight.updateAll();
				}
			};

			var clearAllActiveSecondaryMenus = function(){
				$clickElem.parent().parent().removeClass('active');
			};

			var toggleActive = function( $menu, $parent, callback){
				var callback = callback || function(){},
					isActive = $parent.hasClass('active');

				clearAllActiveSecondaryMenus();
				if(!isActive){
					$parent.addClass('active');
					if($menu && $menu.length){
						setTimeout( function(){
							gotoSmooth.gotoElement($menu, 70, 1000);
						}, 100);
					}
				}

				callback($parent);
			};

			$clickElem.on('click.secondary-menu',function(e){
				e.preventDefault();
				var $el = $(this).parent(),
					$menu = $($(this).attr('href'));

				if($el.hasClass('nt-js-medium-vp-enable') && checkViewport.isMediumViewport()){
					toggleActive( $menu, $el.parent(), checkParentHeight);
				}else if(checkViewport.isSmallViewport()){
					toggleActive( $menu, $el.parent(), checkParentHeight);
				}

			});
		};

		return {
			init: function( $isModuleOnPage ) {
				if ( $isModuleOnPage.length !== 0 ) {
					bindEvents();
				}
			}
		};
	} );

// jshint maxstatements: false, newcap: false
define( 'stick',[ 'jquery' ],
    function( $ ) {

    "use strict";

    var selector = ".nt-js-stick",
        scrollTop = 0,
        vh,
        lastElInView;

    var isFixedSupported = (function(){
        var tests = 3, //adjust for optimum performance
            pass = true;

        //I currently don't know how soon filament-fixed removes the className
        //but I don't want to run hasClass every time there is a scroll event
        return function(){
            if(tests > 0){
                tests = tests -1;
                pass = $('html').hasClass('fixed-supported');
            }
            return pass;
        };
    })();

    var greatestStickChildHeight = function($el){
        var height = -1,
            thisHeight;

        $el.find('.nt-js-stick-child').each( function(){
            //height
            thisHeight = $(this).height();
            //plus distance from top of stick parent
            thisHeight = thisHeight + $(this).offset().top - $(this).closest(selector).offset().top;
            //minus any "top" set by stick function (required for scrolling from bottom to top)
            thisHeight = thisHeight - parseInt($(this).css('top'),10);

            height = thisHeight > height ? thisHeight : height;
        });
        
        return height;
    };

    // returns:
    // -1 stick status disabled
    // 0 before scrollTop
    // 1 stick status active
    // 2 after scrollTop
    var getStickStatus = function($el){
        var elTop = $el.offset().top,
            elBottom = elTop + $el.innerHeight(),
            output = 0;

        if($el.innerHeight() <= vh){
            return -1;
        }

        if(scrollTop > elTop){
            output++;
        }

        if(output === 1 && !$el.hasClass('stick-active') && greatestStickChildHeight($el) >= vh){
            return -1;
        }

        if(($el.hasClass('nt-js-stick-end') && scrollTop + vh >= elBottom) || scrollTop > elBottom){
            output++;
        }

        return output;
    };

    var checkElTop = function(){
        var status = getStickStatus($(this)),
            childTop = 0;

        $(this).toggleClass('stick-active', status === 1);

        if(status !== -1){
            if(status === 2){
                childTop = $(this).innerHeight() - vh;
            }
            if($(this).find('.nt-js-stick-child').length){
                $(this).find('.nt-js-stick-child').css( 'top', childTop + 'px');
            }
        }        
        
    };

    var onScrollEvent = function(){
        scrollTop = $(window).scrollTop();
        $(selector).each(checkElTop);

        if( !isFixedSupported ){
            unbindEvents(); //in theory improves performance
        }
    };

    var onResizeEvent = function(){
        vh = $(window).height();
        onScrollEvent();
    };

    var unbindEvents = function() {
        $(window).off('resize.stick').off('scroll.stick');
        $('.stick-active').removeClass('stick-active');
    };

    var bindEvents = function() {
        onResizeEvent();//run on document ready
        $(window).on('resize.stick', onResizeEvent).on('scroll.stick', onScrollEvent);
    };

    return {
        init: function( $isModuleOnPage ) {
            if ( $isModuleOnPage.length !== 0 ) {
                bindEvents();
            }
        }
    };
} );

// jshint maxstatements: false, newcap: false
define( 'stickItem',[ 'jquery' ],
    function( $ ) {

    "use strict";

    var selector = ".nt-js-stick-item, .nt-js-stick-item-end",
        activeClassName = 'stick-item-active',
        scrollTop = 0,
        vh,
        lastElInView,
        stickItemArray = [],
        imageInterval;

    function StickItem($el, height, scrollTop){
        this.$el =  $el;
        this.height =  height;
        this.scrollTop =  scrollTop;
    }

    var isFixedSupported = (function(){
        var tests = 3, //adjust for optimum performance
            pass = true;

        //I currently don't know how soon filament-fixed removes the className
        //but I don't want to run hasClass every time there is a scroll event
        return function(){
            if(tests > 0){
                tests = tests -1;
                pass = $('html').hasClass('fixed-supported');
            }
            return pass;
        };
    })();

    var getActiveStickItem = function(){
        var $activeItem;
        $.each(stickItemArray, function(index,item){
            if(item.scrollTop < scrollTop){
                $activeItem = item.$el;
            }
        });
        return $activeItem || false;
    };

    var setActiveStickItem = function(){
        var $activeItem = getActiveStickItem();

        if(!$activeItem || $activeItem.hasClass('nt-js-stick-item-end')){
            $(selector).removeClass( activeClassName );
        } else if(!$activeItem.hasClass( activeClassName )){
            $(selector).removeClass( activeClassName );
            $activeItem.addClass( activeClassName );
        }
    };

    var onScrollEvent = function(){
        scrollTop = $(window).scrollTop();
        setActiveStickItem();

        if( !isFixedSupported ){
            unbindEvents(); //in theory improves performance
        }
    };

    var populateStickItemArray = function(){
        var item;

        stickItemArray = [];

        $(selector).each(function(){
            item = new StickItem( $(this), $(this).height(), $(this).offset().top );
            stickItemArray.push( item );
        });

        stickItemArray.sort(function(a,b){ 
            return parseFloat(a.scrollTop) - parseFloat(b.scrollTop); 
        });
    };

    var onResizeEvent = function(){
        vh = $(window).height();
        populateStickItemArray();
        onScrollEvent();
    };

    var onImageLoadEvent = function(){
        clearInterval(imageInterval);
        imageInterval = setInterval(onResizeEvent, 500);
    };

    var unbindEvents = function() {
        $('img').off('load.stickItem');
        $(window).off('resize.stickItem').off('scroll.stickItem');
        $('.stick-item-active').removeClass('stick-item-active');
    };

    var bindEvents = function() {
        onResizeEvent();//run on document ready
        $('img').on('load.stickItem', onImageLoadEvent);
        $(window).on('resize.stickItem', onResizeEvent).on('scroll.stickItem', onScrollEvent);
    };

    return {
        init: function( $isModuleOnPage ) {
            if ( $isModuleOnPage.length !== 0 ) {
                bindEvents();
            }
        }
    };
} );

// jshint maxstatements: false, newcap: false
define( 'vhOpacity',[ 'jquery' ],
    function( $ ) {

    "use strict";

    var selector = ".nt-js-vh-opacity",
        childSelector = ".nt-js-vh-opacity-item",
        scrollTop = 0,
        tollerance = 0.35,
        vh,
        vhTollerance,
        vTop,
        vBottom;

    var checkElTop = function(){

        var elTop = $(this).offset().top,
            opacity = 0;

        if(elTop < vTop){
            opacity = 1;
        } else if(elTop < vBottom){
            opacity = 1 - ((elTop - vTop) / (vBottom - vTop));
        }
        $(this).find(childSelector).css('opacity', opacity);

    };

    var onScrollEvent = function(){
        scrollTop = $(window).scrollTop();
        vTop = scrollTop + vhTollerance;
        vBottom = scrollTop + vh - vhTollerance;

        $(selector).each(checkElTop);

    };

    var onResizeEvent = function(){
        vh = $(window).height();
        vhTollerance = vh * tollerance;

        onScrollEvent();
    };

    var bindEvents = function() {
        onResizeEvent();//run on document ready
        $(window).resize(onResizeEvent).scroll(onScrollEvent);
    };

    return {
        init: function( $isModuleOnPage ) {
            if ( $isModuleOnPage.length !== 0 ) {
                bindEvents();
            }
        }
    };
} );

// jshint maxstatements: false, newcap: false
define( 'print',[ 'jquery' ],
    function( $ ) {

    "use strict";

    var launchPrint = function(e){
        e.preventDefault();
        if(window.print){
            window.print();
        } else {
            window.alert("Cannot print from this device");
        }
    };

    var bindEvents = function() {
        $('.nt-js-print').off('click.print').on('click.print', launchPrint);
    };

    return {
        init: function( $isModuleOnPage ) {
            if ( $isModuleOnPage.length !== 0 ) {
                bindEvents();
            }
        }
    };
} );

// jshint maxstatements: false, newcap: false
define( 'toggle',[ 'jquery' ],
    function( $ ) {

    "use strict";

    var activeClassName = 'toggle-active',
        ms1000ClassName = 'toggle-active-1000',
        timeout;

    var toggle = function($el){
        var $ariaExpanded;
        /* 
         * Checks whether the toggled element has aria-expanded attribute.
         * If not, it checks to see whether it's decendent has got one.
         */
        if ($el.attr('aria-expanded')) {
            // for image figcaption accessibility
            $ariaExpanded = $el;
        } else {
            // for image figcaption accessibility
            $ariaExpanded = $el.children('[aria-expanded]');
        }
        console.debug("Expanded element is: ", $ariaExpanded);

        // For some browsers, `attr` is undefined; for others,
        // `attr` is false.  Check for both.
        if (typeof $ariaExpanded !== typeof undefined && $ariaExpanded !== false) {
            $ariaExpanded.attr('aria-expanded', function(i, attr) {
               return attr == 'true' ? 'false' : 'true';
            });

            // Also, flips the aria-hidden attribute of figcaption credit span
            var $imgCreditSpan = $ariaExpanded.children('span[aria-hidden][class=nt-image-information-credit]');
            if (typeof $imgCreditSpan !== typeof undefined && $imgCreditSpan !== false) {
                $imgCreditSpan.attr('aria-hidden', function(i, attr) {
                    return attr == 'true' ? 'false' : 'true';
                });
            }
        }

        if($el.hasClass('nt-js-disable-link')){
            e.preventDefault();
        }

        clearTimeout(timeout);
        if(!$el.hasClass(activeClassName)){
            $el.addClass(activeClassName);
            timeout = setTimeout( function(){
                $el.addClass(ms1000ClassName);
            }, 1000);
        } else {
            $el.removeClass(ms1000ClassName);
            timeout = setTimeout( function(){
                $el.removeClass(activeClassName);
            }, 50);
        }

    };

    var addToggleToTabIndex = function(){
        $('.nt-js-toggle').attr('tabindex','0');
    };

    var bindEvents = function() {

        addToggleToTabIndex();        

        $('body').off('click.toggle', '.nt-js-toggle').on('click.toggle', '.nt-js-toggle', function(e){
            $(this).blur();
            toggle($(this));
        });

        $('body').off('keyup.toggle', '.nt-js-toggle').on('keyup.toggle', '.nt-js-toggle', function(e){
             // 13 is enter key
            if ( e.keyCode === 13 ) {
                toggle($(this));
            }
        });

    };

    return {
        init: function( $isModuleOnPage ) {
            if ( $isModuleOnPage.length !== 0 ) {
                bindEvents();
            }
        }
    };
} );

// jshint maxstatements: false, newcap: false
define( 'imageLoadListener',[ 'jquery' ],
    function( $ ) {

    "use strict";

    /**
     * @method imageLoadListener
     * @description Checks for images and calls a callback when they've loaded
     * @param {object} array of one or more jQuery elements
     * @param {function} Callback to be called when one ore more images are ready
     */
    var imageLoadListener = function($parent, callback, finalCallback){

        var $images = $parent.find('img'),
            imageLoadInterval,
            eventIntervalTime = 200,
            imageLoadCount = 0;

        callback = callback || function(){};
        finalCallback = finalCallback || callback;

        var onLoadOrError = function(){
            //Throttled
            imageLoadCount++;
            clearInterval(imageLoadInterval);
            if(imageLoadCount === $images.length){
                finalCallback();
            } else {
                imageLoadInterval = setInterval(function(){
                    clearInterval(imageLoadInterval);
                    callback();
                }, eventIntervalTime);
            }
        };

        $images.one("load", onLoadOrError).one("error", onLoadOrError).each(function() {
            if(this.complete){
                $(this).load();
            }
        });

    };

    return {
        imageLoadListener: imageLoadListener
    };
});

/**
* Collapses element to a height smaller than its original size, hiding its overflowing content. If a read more button
* is inside the element, a click event listener will be attached to it that expands the element back to original height.
*
* This module is not responsible for creating the 'read more' button, instead any nested node with css class 
* 'nt-js-read-more__expand-button' will be treated as one.
*
* See method makeExpandable() for more info.
*/
// jshint maxstatements: false, newcap: false
define( 'readMore',[ 'jquery' ],
    function( $ ) {
    "use strict";


    /**
    * @property cssClasses
    * @description css classes used by this module
    */
    var cssClasses = {
        collapsed : 'nt-js-read-more--collapsed',
        expanded : 'nt-js-read-more--expanded',
        expandButton : 'nt-js-read-more__expand-button'
    }


    /**
     * @method getHeight
     * @description gets height of a given node
     * @param {object} single jQuery element
     * @return {number} element's height in pixels
     */
    var getHeight = function($el){
        //Use correct method depending on box-sizing
        var jQueryHeightType = ($el.css('box-sizing') === 'border-box') ? 'innerHeight' : 'height';

        //borderbox check
        return $el[jQueryHeightType]();
    }


    /**
     * Sets a given element's height to a given value.
     *
     * @method collapseItem
     * @description sets the height of a given box to the slice point
     * @param {object} single jQuery element
     * @param {number} height in pixels to slice the element
     */
    var collapseItem = function($item, height){
        if (isCollapsedItem($item)){ return; }
        // set height of box to slice point
        $item.css({
            'height' : height + 'px',
            'overflow-y' : 'hidden'
        });
        $item.addClass(cssClasses.collapsed);
        $item.removeClass(cssClasses.expanded);
    };


    /**
     * Clears the given element's set height.
     *
     * @method expandItem
     * @description expands a given box to the default height
     * @param {object} single jQuery element
     * @param {function} optional callback fired after expand if finished, passing item as argument
     */
    var expandItem = function($item,callback){
        if (isExpandedItem($item)){ return; }
        $item.removeClass(cssClasses.collapsed);
        $item.animate({height: $item.get(0).scrollHeight}, 200, null, function(){
            $item.css({
                'overflow-y' : ''
            });
            $item.addClass(cssClasses.expanded);
            if (callback){callback($item);}
        });
    };


    /**
     * @method isCollapsedItem
     * @description Returns whether the given element has been collapsed
     * @param {object} single jQuery element
     * @param {string} style declaration property to be removed
     * @param {boolean} true if element is collapsed, otherwise false
     */
    var isCollapsedItem = function($el){
        return ($el.hasClass(cssClasses.collapsed) // check element
        ||      $el.find('.'+cssClasses.collapsed).length > 0 ); // check children
    }


    /**
     * @method isExpandedItem
     * @description Returns whether the given element has been expanded
     * @param {object} single jQuery element
     * @param {string} style declaration property to be removed
     * @param {boolean} true if element is expanded, otherwise false
     */
    var isExpandedItem = function($el){
        return ($el.hasClass(cssClasses.expanded) // check element
        ||      $el.find('.'+cssClasses.expanded).length > 0 ); // check children
    }


    /**
    * @method makeExpandable
    * @description Adds 'read more' functionality to element(s), collapsing their height if they are too tall. Each element
    * should also contain a nested button so that will expand the element back to original height on click.
    *
    * When multiple elements are passed in the first argument, each element will collapse to the given slicePoint height
    * if the following conditions are met:
    *    - the element's height exceeds the given slicePoint by an amount larger than the given threshold
    *    - the element's height exceeds all other given elements' height by an amount larger than the given threshold
    *
    * The above two conditions prevent boxes from collapsing/expanding a small amount.
    *
    * @param {object or string} itemsOrSelector - jQuery object containing elements to hide/collapse, or a selector string.
    * @param {number} slicePoint - the value in pixels that at which the element will be sliced in half.
    * @param {number} threshold - value in pixels on top of the slicePoint. If after slicing the contents in two if
    * there are fewer pixels ins the second half than the value set by threshold, we won't bother hiding/collapsing it.
    * Likewise, if an element is exceeding the height of another provided element by an amount smaller than the threshold
    * we wont bother hiding/collapsing either of them.
    * @param {object or string} expandButton - optional node/selector for 'read more' button. If not provided,
    * 'nt-js-read-more__expand-button' will be used
    * @param {function} optional callback fired after expand passing element as argument
    */
    var makeExpandable = function(itemsOrSelector, slicePoint, threshold, expandButtonOrSelector, expandCallback){
        var items = $(itemsOrSelector),
            itemsToCollapse = [];

        if (!items.length){ return; } 

        // sort items by height, from smallest to largest
        items.sort( function(a,b) {
            return getHeight($(a)) > getHeight($(b)) ? 1 : -1;
        });

        // for each item
        for (var i = 0; i < items.length; i++){
            var $item = $(items[i]),
                height = getHeight($item);

            if (height < slicePoint + threshold){
                slicePoint = Math.max(slicePoint,height); // increase slicepoint
            }else{
                itemsToCollapse.push($item);
            }
        }

        // for each item to collapse
        for (var i = 0; i < itemsToCollapse.length; i++){
            collapseItem(itemsToCollapse[i],slicePoint);
            registerExpandHandler(itemsToCollapse[i], expandButtonOrSelector, expandCallback);
        }
    }


    /**
    * @method registerExpandHandler
    * @description Checks if a button with the CSS class for expand button exists inside a given item. If so,
    * attaches a event listener to it so it will expand on click.
    * @param {object or string} itemsOrSelector - jQuery object containing single collapsed node
    * @param {object or string} expandButton - optional node/selector for 'read more' button. If not provided,
    * 'nt-js-read-more__expand-button' will be used
    * @param {function} optional callback fired after expand if finished, passing item as argument
    */
    var registerExpandHandler = function($item, expandButtonOrSelector, expandCallback){
        var expandButton = $item.find(expandButtonOrSelector || '.'+cssClasses.expandButton);

        expandButton.click(function(e){
            expandItem($item,expandCallback);
            expandButton.hide();
            e.preventDefault();
        });
    };


    return {
        makeExpandable : makeExpandable,
        isCollapsedItem : isCollapsedItem,
        isExpandedItem : isExpandedItem
    };
} );

// jshint maxstatements: false, newcap: false
define( 'equalHeight',[ 'jquery', 'checkViewport', 'imageLoadListener', 'readMore' ],
    function( $, checkViewport, imageLoadListener, readMore ) {
    "use strict";

    /**
     * @method removeStyleDeclaraton
     * @description Removes style declartion property from the DOM without overiding any external stylesheets
     * @param {object} single jQuery element
     * @param {string} style declaration property to be removed
     */
    var removeStyleDeclaraton = function($el, removeProperty){

        var declaration = $el.attr("style") || "",
            declarationArray,
            i,
            propertyValueArray,
            property,
            returnDeclaration = [];
        //replace with regex
        declarationArray = declaration.split(";");

        for(i=0; i < declarationArray.length; i++){
            propertyValueArray = declarationArray[i].split(":");
            property = propertyValueArray[0].replace(" ","");

            if(property != removeProperty){
                returnDeclaration.push(declarationArray[i]);
            }
        }

        $el.attr('style', returnDeclaration.join(";"));

    };

    /**
     * @method getGreatestHeight
     * @description Returns the greatest height or innerHeight based on box-model from an array of $elements
     * @param {object} array of jQuery elements
     * @param {string} Optional jQuery element selector of child element to be measured
     */
    var getGreatestHeight = function($items, innerSelector) {
        var greatestHeight = 0;

        $items.each( function(){
            var $el = $(this),
                jQueryHeightType;

            //If inner then ignore parent
            if($el.find(innerSelector).length){
                $el = $el.find(innerSelector).first();
            }

            //Use correct method depending on box-sizing
            jQueryHeightType = ($el.css('box-sizing') === 'border-box') ? 'innerHeight' : 'height';

            //borderbox check
            if($el[jQueryHeightType]() > greatestHeight){
                greatestHeight = $el[jQueryHeightType]();
            }

        });

        return greatestHeight;

    };

    /**
     * @method clearSetHeight
     * @description Remove style height from an array of elements
     * @param {object} array of one or more jQuery elements
     * @param {string} Optional jQuery element selector of child element to be updated
     */
    var clearSetHeight = function($items, innerSelector){
        $items.each( function(){
            var $el = $(this);

            //If inner then ignore parent
            if($el.find(innerSelector).length){
                $el = $el.find(innerSelector).first();
            }

            // If an item has been collapsed by the read-more module then it's height has already been explicitly set, 
            // so ignore it
            if (!readMore.isCollapsedItem($el)){
                //clear any previously set height
                removeStyleDeclaraton($el,'height');
            }

        });
    };


    /**
     * @method setHeight
     * @description Add style height to an array of elements
     * @param {object} array of one or more jQuery elements
     * @param {string} Optional jQuery element selector of child element to be updated
     */
    var setHeight = function($items, height, innerSelector){
        $items.each( function(){
            var $el = $(this);
            //If inner then ignore parent
            if($el.find(innerSelector).length){
                $el = $el.find(innerSelector).first();
            }
            //Note: no border sizing check required here
            //used CSS rather than height to prevent jQuery border-box correction
            $el.css('height', height+'px');
        });
    };


    /**
     * @method setToGreatestHeight
     * @description Sets an array of $elements to the greatest height
     * @param {object} array of jQuery elements
     * @param {string} Optional jQuery element selector of child element to be measured
     */
    var setToGreatestHeight = function($items, innerSelector){

        // Ignore any items that have been expanded, or have a child that's been expanded.
        $items = $items.filter(function(){
            return (!readMore.isExpandedItem($(this)));
        });

        //if there is one item or less, no need to do anything
        if ($items.length < 2) { return; }

        var greatestHeight;

        //clear set heights
        clearSetHeight($items, innerSelector);

        //Not Small viewport check
        if( !checkViewport.isSmallViewport() ){

            //get greatest height
            greatestHeight = getGreatestHeight($items, innerSelector);

            if (greatestHeight){
                //set heights
                setHeight($items, greatestHeight, innerSelector);
            }
        }

    };


    var windowResizeListener = function($items, callback){
        var eventIntervalTime = 100,
            windowResizeInterval;

        //Throttled
        $(window).resize( function(){
            clearInterval(windowResizeInterval);
            windowResizeInterval = setInterval(function(){
                clearInterval(windowResizeInterval);
                callback();
            }, eventIntervalTime);
        });
    };


    var bindEvents = function() {
        //DOM selectors
        var itemSelector = '.nt-js-equal-height',
            innerSelector = '.nt-js-equal-height-inner',

        //private vars
            parentDataFlag = 'has-equal-height',
            $allItems = $(itemSelector),
            $allParents;

        //find shared parents
        $allItems.each( function(){
            if($(this).hasClass('nt-js-apply-to-parent-height')){
                $(this).parent().parent().attr('data-'+parentDataFlag,true);
            }else{
                $(this).parent().attr('data-'+parentDataFlag,true);
            }
        });

        $allParents = $('[data-'+parentDataFlag+'="true"]');

        $allParents.each( function(){

            var $items = $(this).find(itemSelector);

            //Check for image load event
            imageLoadListener.imageLoadListener($items, function(){
                setToGreatestHeight($items, innerSelector);
            });

            //Resize event listener
            windowResizeListener($items, function(){
                setToGreatestHeight($items, innerSelector);
            });

            //set greatest height
            setToGreatestHeight($items, innerSelector);

        });
    };

    return {
        init: function( $isModuleOnPage ) {
            if ( $isModuleOnPage.length !== 0 ) {
                bindEvents();
            }
        },
        setToGreatestHeight: setToGreatestHeight
    };
} );

// jshint maxstatements: false, newcap: false
define( 'tabs',[ 'jquery', 'equalHeight', 'gotoSmooth' ],
    function( $, equalHeight, gotoSmooth ) {

    "use strict";

    var aria = {
        prime : function($tabsParent){
            $tabsParent.find('a').each(function(){
                var $tab = $(this);
                
                if ($tab.hasClass('tabs-exclude')){
                    return true;
                }
                
                var $tabPanel = $( $tab.attr('href') );

                //Add aria attributes to tab
                $tab.attr('role','tab');
                $tab.attr('aria-controls', $tab.attr('href').replace('#',''));
                $tab.attr('aria-selected', $tab.hasClass('active'));
                if(!$tab.attr('id')){
                    $tab.attr('id', $tab.attr('href').replace('#','tab'));
                }

                //Add aria attributes to tab panel
                $tabPanel.attr('role','tabpanel');
                $tabPanel.attr('aria-labelledby', $tab.attr('id'));
                $tabPanel.attr('aria-hidden', !$tabPanel.is(':visible'));
            });
        },
        resizeListener : function($tabsParent){
            var resizeInterval,
                throttle = 200;//ms

            var resizeAction = function(){
                $tabsParent.find('a').each(function(){
                    var $tab = $(this);
                    
                    if ($tab.hasClass('tabs-exclude')){
                        return true;
                    }
                    
                    var $tabPanel = $( $tab.attr('href') );
                        
                    $tabPanel.attr('aria-hidden', !$tabPanel.is(':visible'));
                });
            };

            $(window).on('resize', function(){
                clearInterval(resizeInterval);
                resizeInterval = setInterval(resizeAction, throttle);
            });

        },
        updateTab : function($tab){
            $tab.closest('.nt-tabs, .nt-tabs-inline').find('a').each( function(){
                var $t = $(this);
                $t.attr('aria-selected', $t.hasClass('active'));
            });

        },
        updateTabPanel : function($tabPanel){
            $tabPanel.closest('.nt-tab-content-group').find('.nt-tab-content').each( function(){
                var $tP = $(this);
                $tP.attr('aria-hidden', !$tP.is(':visible'));
            });
        }
    }

    var openTabContent = function($id){
        var $group = $id.closest('.nt-tab-content-group'),
            $content = $group.find('.nt-tab-content'),
            classNameActive = 'active',
            resetDelay = 100;


        $group.css('min-height', $group.innerHeight()+'px'); //prevent page jump
        $content.removeClass(classNameActive);
        $id.addClass(classNameActive);
        setTimeout( function(){
            $group.css('min-height', 0);
        }, resetDelay);
    };

    var setTab = function($id){
        var classNameActive = 'active';

        $id.closest('.nt-tabs, .nt-tabs-inline').find('a').removeClass(classNameActive);
        $id.addClass(classNameActive);
    };

    var tabContentUpdate = function($id){
        var $content = $id.closest('.nt-tab-content-group').find('.nt-tab-content'),

        //Clumsy equal height check
        //Consider events, promises or callbacks
            parentSelector = '[data-has-equal-height="true"]',
            itemSelector = '.nt-js-equal-height',
            innerSelector = '.nt-js-equal-height-inner';

        if($content.find('.nt-js-equal-height').length){
            equalHeight.setToGreatestHeight( $content.find(itemSelector), innerSelector );
        }
    };

    var scrollToTabs = function($el){
        gotoSmooth.gotoElement( $el, 120, 800 );
    };

    var bindEvents = function() {
        var $tabsParent = $('.nt-tabs, .nt-tabs-inline');

        aria.prime($tabsParent);
        aria.resizeListener($tabsParent);

        $tabsParent.find('a').on('click', function(e){
            
            var $tab = $(this);
            
            if ($tab.hasClass('tabs-exclude')){
                return true;
            }
            
            var idSelector = $(this).attr('href');
            e.preventDefault();

            setTab( $(this) );
            openTabContent( $(idSelector) );

            aria.updateTab( $(this) );
            aria.updateTabPanel( $(idSelector) );

            tabContentUpdate( $(idSelector) );
            if (!$(this).hasClass('nt-disable-scroll')) {
                scrollToTabs( $(idSelector) );
            }
        });

    };

    return {
        init: function( $isModuleOnPage ) {
            if ( $isModuleOnPage.length !== 0 ) {
                bindEvents();
            }
        }
    };
} );

// jshint maxstatements: false, newcap: false
define( 'volunteerStoryItem',[ 'jquery' ],
    function( $ ) {

    "use strict";

    var $volunteerItem = $('.nt-volunteers .nt-volunteer'),
        $selectedQuote = $('.nt-volunteer-stories .nt-js-selected-quote');

    var displaySelectedQuote = function() {
        $selectedQuote.html($('.nt-volunteers .nt-volunteer.volunteer-module-selected .nt-volunteer-story').html());
    };

    var bindEvents = function() {
        displaySelectedQuote();

        $volunteerItem.on('click',function(e){
            var $selectedItem = $(e.currentTarget);
            if(!$selectedItem.hasClass('volunteer-module-selected')){
                $volunteerItem.removeClass('volunteer-module-selected');
                $selectedItem.addClass('volunteer-module-selected');
                displaySelectedQuote();
            }
        });
    };

    return {
        init: function( $isModuleOnPage ) {
            if ( $isModuleOnPage.length !== 0 ) {
                bindEvents();
            }
        }
    };
} );

// jshint maxstatements: false, newcap: false
define( 'articleGalleryLayout',[ 'jquery' ],
    function( $ ) {
    "use strict";

    function ArticleGalleryLayout($carousel){

        var $panel = $carousel.find('.nt-panel-outer'),
            containerSelector = '.nt-panel',
            contentSelector = '.nt-panel-inner',
            fadeSpeed = 1500,
            position = 'append',
            $jsPanel;

        var init = function(){
            $jsPanel = $panel.first().clone();
            $carousel[position]($jsPanel);
        };

        this.updatePanel = function(n){
            //Set height of jsPanel container (before hiding content)
            $jsPanel.find(containerSelector).height( $jsPanel.find(containerSelector).height() );
            //Hide jsPanel content
            $jsPanel.find(contentSelector).hide();
            //Update jsPanel content
            $jsPanel.find(contentSelector).html( $panel.eq(n).find(contentSelector).html() );
            //Show jsPanel content and clear container height
            $jsPanel.find(contentSelector).fadeIn(fadeSpeed, function(){
                $jsPanel.find(containerSelector).height('auto');
            });
        };

        init();

    }

    return {
        ArticleGalleryLayout: ArticleGalleryLayout
    };
} );
// jshint maxstatements: false, newcap: false
define( 'carousel',[ 'jquery', 'articleGalleryLayout' ],
    function( $, articleGalleryLayout ) {
    "use strict";

    var Carousel = function(settings){

        var $carouselWrapper = settings.$carouselWrapper,
            $carouselRibbon = settings.$carouselRibbon,
            $carouselItem = settings.$carouselItem,
            carouselPaginationIndexSelector = settings.carouselPaginationIndexSelector,
            carouselPaginationWrapSelector = settings.carouselPaginationWrapSelector,
            carouselPaginationNextSelector = settings.carouselPaginationNextSelector,
            carouselPaginationPreviousSelector = settings.carouselPaginationPreviousSelector,
            onChange = settings.onChange || function(){},
            n = 0;

        this.scrollTo = function(x, time, callback){

            if(Modernizr.cssanimations){

                $carouselRibbon.css({
                    //safari and ios only need -webkit-
                    transform: 'translate3d(-'+ x +'px,0,0)',
                    '-webkit-transform': 'translate3d(-'+ x +'px,0,0)',
                    'transition-property': '-webkit-transform,transform',
                    'transition-duration': time+'ms',
                    'transition-delay': 0+'s'
                });

            } else {

                //IE8 and IE9
                $carouselRibbon.animate({
                    marginLeft: '-'+x+'px'
                }, time);

            }

            if($.isFunction(callback)){
                setTimeout( callback, time );
            }

        };

        this.setRibbonWidth = function(){
            var itemWidth;

            $carouselRibbon.width('100%');
            itemWidth = $carouselItem.first().width('100%').width();
            $carouselItem.width(itemWidth);
            $carouselRibbon.width(itemWidth * $carouselItem.length);
            this.scrollTo(itemWidth * n, 0);

        };

        this.setActiveIndex = function(n){
            $carouselWrapper.find(carouselPaginationIndexSelector).find('li').removeClass('active');
            $carouselWrapper.find(carouselPaginationIndexSelector).find('li:eq('+n+')').addClass('active');
        };

        //iOS and other touch devices have sticky hover states
        //This can be resolved by removing the element and then adding it back into the DOM.
        this.removeTouchHover = function(){
            var $el = $carouselWrapper.find(carouselPaginationWrapSelector).clone();
            $carouselWrapper.find(carouselPaginationWrapSelector).remove();
            setTimeout( function(){ 
                $carouselWrapper.find(carouselPaginationIndexSelector).after($el);
            }, 600);
        };

        this.pagination = function(){
            var carousel = this;

            $carouselWrapper.off('click.carouselindex', carouselPaginationIndexSelector+' a').on('click.carouselindex', carouselPaginationIndexSelector+' a', function(e){
                var itemWidth = $carouselItem.first().width();
                n = $(this).parent().prevAll().length;
                e.preventDefault();
                carousel.scrollTo(itemWidth * n, 1000);
                carousel.setActiveIndex(n);
                onChange(n);
            });

            $carouselWrapper.off('click.carouselnext', carouselPaginationNextSelector).on('click.carouselnext', carouselPaginationNextSelector, function(e){
                var itemWidth = $carouselItem.first().width(),
                    scrollToN;
                e.preventDefault();
                carousel.removeTouchHover();
                if(n === $carouselItem.length -2){
                    scrollToN = n +1;
                    n = 0;
                    carousel.scrollTo(itemWidth * scrollToN, 1000, function(){
                        //n can be updated by user before this callback
                        carousel.scrollTo(itemWidth * n, 0);
                    });
                    carousel.setActiveIndex(0);
                    onChange(0);
                } else {
                    n++;
                    carousel.scrollTo(itemWidth * n, 1000);
                    carousel.setActiveIndex(n);
                    onChange(n);
                }
            });

            $carouselWrapper.off('click.carouselprevious', carouselPaginationPreviousSelector).on('click.carouselprevious', carouselPaginationPreviousSelector, function(e){
                var itemWidth = $carouselItem.first().width();
                e.preventDefault();
                carousel.removeTouchHover();
                if(n === 0){
                    n = $carouselItem.length -1;
                    carousel.scrollTo(itemWidth * n, 0, function(){
                        n--;
                        carousel.scrollTo(itemWidth * n, 1000);
                        carousel.setActiveIndex(n);
                        onChange(n);
                    });
                } else {
                    n--;
                    carousel.scrollTo(itemWidth * n, 1000);
                    carousel.setActiveIndex(n);
                    onChange(n);
                }
            });

        };

        this.resizeListener = function(){
            var carousel = this,
                isResized = false;

            $(window).on('resize', function(){
                if(!$carouselRibbon.is(':visible')){
                    isResized = true;
                } else {
                    carousel.setRibbonWidth();
                }
            });

            $('.nt-modal').on('modalclose.carousel', function(){
                if(isResized){
                    isResized = false;
                    carousel.setRibbonWidth();
                }
            });
        };

        this.init = function(){
            //repeat first item
            $carouselRibbon.append( $carouselItem.first().clone() );
            $carouselItem = $carouselRibbon.find(settings.$carouselItem.selector);

            this.setRibbonWidth();
            this.pagination();
            this.setActiveIndex(n);
            this.resizeListener();

            //prevent Flash Of Unstyled Content reset
            $carouselItem.show();
        };

    };

    var bindEvents = function() {

        $('.nt-carousel').each( function(){

            var galleryLayout,
                onChange = null;

            //If article gallery
            if($(this).closest('.nt-article-gallery').length){
                galleryLayout = new articleGalleryLayout.ArticleGalleryLayout($(this));
                onChange = galleryLayout.updatePanel;
            }

            var carousel = new Carousel({
                $carouselWrapper : $(this),
                $carouselRibbon : $(this).find('.nt-carousel-item-list ul'),
                $carouselItem : $(this).find('.nt-carousel-item'),
                carouselPaginationIndexSelector : '.nt-carousel-index',
                carouselPaginationWrapSelector : '.nt-carousel-button-group',
                carouselPaginationNextSelector : '.nt-carousel-next',
                carouselPaginationPreviousSelector : '.nt-carousel-previous',
                onChange : onChange
            });

            carousel.init();
        });

    };

    return {
        init: function( $isModuleOnPage ) {
            if ( $isModuleOnPage.length !== 0 ) {
                bindEvents();
            }
        }
    };
} );

// jshint maxstatements: false, newcap: false
define( 'ntAnimation',[ 'jquery'],
function( $ ) {
	"use strict";

	var ticking = false,
		lastKnownScrollY = 0,
		viewportHeight = 0,
		documentHeight = 0,

		options = {
			animationStartDelay : 500, //avoid chrome jerkiness - http://www.html5rocks.com/en/tutorials/speed/html5/
			startScrollBottom : 10,
			cssTranslateYValue : 100,
			animationStartClass : 'init',
			scrollAnimatableClass : '.nt-animatable-scroll',
            scrollFadeInClass : '.nt-anim-scroll-fadein',
			trailReadMoreSlideInClass : '.nt-anim-trail-readmore-slidein'
		};
	

	var initializeAnimation = function(){
		//Initialization
		setTimeout(function(){
            manageScrollAnimation();
		},options.animationStartDelay);

		setupScrollAnimation();
	};

	var startAnimation = function($elem){
		//$elem.addClass(options.animationStartClass);
	};

	var defaultState = function($elem){
		$elem.removeClass(options.animationStartClass);
	};

	var setupScrollAnimation = function(){
		$(window).on('load resize',(function f(){
			calculateOffsetTop();
			return f;
		})());
	};

	var calculateOffsetTop = function(){
		var $elem;
		viewportHeight = $(window).height();
		documentHeight = $(document).height();
		
		$(options.scrollAnimatableClass).each(function(i, elem){
			$elem = $(elem);
			$elem.data("posY",parseInt($elem.offset().top,10));
			if(typeof $elem.data('isAnimFinished') === 'undefined'){ //if not set
				$elem.data('isAnimFinished',false);
			}
		});
	};
	var manageScrollAnimation = function(){
		$(window).on('scroll',(function f(){
			lastKnownScrollY = $(window).scrollTop();
			requestForAnimationFrame();
			return f;
		})());
	};
	var requestForAnimationFrame = function(){
		if(!ticking) {
			//scroll debouncing. Let the browser handle update best way
			//http://www.html5rocks.com/en/tutorials/speed/animations/
			window.requestAnimFrame(scrollUpdate);
		}
		ticking = true;
	};
	var scrollUpdate = function(){
		var $elem, $animItems;
		
		ticking = false;
		$(options.scrollAnimatableClass).each(function(i, elem){
			$elem = $(elem);
			if(!isAnimationFinished($elem) && isElemPassedViewport($elem) ){
				$animItems = findAnimationClassElements($elem);
				startAnimation($animItems);
				$elem.data('isAnimFinished',true);
			}
		});
	};
	var isElemPassedViewport = function($elem){
		var triggerPosition = lastKnownScrollY + viewportHeight - options.startScrollBottom;
		if(isContainerAnimatable($elem)){
			triggerPosition += options.cssTranslateYValue;
		}
		return $elem.data('posY') < triggerPosition;
	};
	var isAnimationFinished = function($elem){
		return $elem.data('isAnimFinished');
	};
	var findAnimationClassElements = function($elem){
		if(isContainerAnimatable($elem)){
			return $elem;
		}

		if($elem.find(options.trailReadMoreSlideInClass).length > 0){
			return $elem.find(options.trailReadMoreSlideInClass);
		}
		if($elem.find(options.scrollFadeInClass).length > 0){
			return $elem.find(options.scrollFadeInClass);
		}
		console.error('animatable items not found');
		
	};
	var isContainerAnimatable = function($elem){
		return $elem.hasClass(options.scrollFadeInClass.replace(".","")) ||
				$elem.hasClass(options.trailReadMoreSlideInClass.replace(".",""));
	};

	return {
		init: function( $isModuleOnPage ) {
            if ( $isModuleOnPage.length !== 0 ) {
                initializeAnimation();
            }
        }
	};
});

// jshint maxstatements: false, newcap: false
define('ntVideo',['jquery'],
    function ($) {

        "use strict";

        var video;

        // IMPORTANT: Needs to be attached to the window object (global) to work
        var existingCall = window.onYouTubeIframeAPIReady;
        window.onYouTubeIframeAPIReady = function () {
            if (video) video.create();
            if (existingCall) existingCall();
        };

        function NTVideo() {
            var elementId,
                videoId,
                videoTitle,
                player,
                videoTrackTimer,
                previousPlayerState,
                reportedAAAPIEventStack,
                videoPlayEventTimeStack,
                aaAPIEventStack = ['videoPlayEvent',
                    'videoQuarterPlayEvent',
                    'videoHalfPlayEvent',
                    'videoThreeQuarterPlayEvent',
                    'videoNintyPercentPlayEvent',
                    'videoFullPlayEvent'];

            var init = function (eId, vId) {
                elementId = eId;
                videoId = vId;
                resetVideoEventData();

                //If YT IFrame Player API not loaded create it.
                if (!window.YT) {
                    loadIframePlayerAPI();
                } else if (!player) {
                    createIframePlayer();
                } else {
                    player.loadVideoById(videoId);
                }
            };

            // Resets video event data
            var resetVideoEventData = function () {
                reportedAAAPIEventStack = [];
                videoPlayEventTimeStack = [];
                // Video Unstarted
                previousPlayerState = -1
            };

            var loadIframePlayerAPI = function () {
                var tag = document.createElement('script'),
                    firstScriptTag = document.getElementsByTagName('script')[0];

                //Load the IFrame Player API code asynchronously.
                tag.src = "https://www.youtube.com/iframe_api";
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

                //calls onYouTubeIframeAPIReady after API loads
            };

            // Create an <iframe> (and YouTube player)
            var createIframePlayer = function () {
            	if (elementId) {
					player = new YT.Player(elementId, {
						videoId: videoId,
						playerVars: {
							autoplay: 1
						},
						events: {
							'onReady': onPlayerReady,
							'onStateChange': onPlayerStateChange
						}
					});
                }
            };

            // On video player ready.
            var onPlayerReady = function (event) {
                event.target.setVolume(100);
            };

            var pauseIframePlayer = function () {
                // pauseVideo prevents subsequent videos working correctly on iPad3 iOS7
                // the audio would play but no video-image (just black)
                //player.pauseVideo();

                // destroy player so new player object is used for each video
                player.stopVideo();
                player.destroy();
                player = null;

                // Ensures to clear ${videoTrackTimer} when the video is closed
                clearInterval(videoTrackTimer);
            };

            // On video player ready.
            var onPlayerStateChange = function (event) {
                if (player.getPlayerState() == YT.PlayerState.PLAYING) {
                    // Reset video event tracking data if the video is restarted/replayed
                    if (previousPlayerState == YT.PlayerState.ENDED && player.getCurrentTime() == 0) {
                        resetVideoEventData();
                    }

                    // Tracks passed events
                    if (previousPlayerState == YT.PlayerState.BUFFERING || previousPlayerState == YT.PlayerState.PAUSED) {
                        trackPassedAAEvents();
                    }

                    // Reports videoPlayEvent
                    if ($.inArray('videoPlayEvent', reportedAAAPIEventStack) == -1) {
                        videoPlayEventTimeStack.push(
                            0,
                            Math.round(player.getDuration() * 25 / 100),
                            Math.round(player.getDuration() * 50 / 100),
                            Math.round(player.getDuration() * 75 / 100),
                            Math.round(player.getDuration() * 90 / 100),
                            Math.round(player.getDuration()));
                        trackVideoPlay();
                    }
                    // Tracks the video play progress for every second
                    videoTrackTimer = setInterval(trackVideoPlay, 1000);
                } else {
                    clearInterval(videoTrackTimer);
                    // Ensures to track the 'videoFullPlayEvent' event in case
                    // if the player state chages to YT.PlayerState.ENDED before
                    // it is being tracked as part of trackVideoPlay method.
                    if (player.getPlayerState() == YT.PlayerState.ENDED) {
                        trackAAEvent('videoFullPlayEvent');
                    }
                }
                previousPlayerState = player.getPlayerState();
            };

            // Tracks the progress of the play and reports AA accordingly
            var trackVideoPlay = function () {
                if (player.getPlayerState() == YT.PlayerState.PLAYING) {
                    // Checks that the current video time is one of the event time
                    var eventIndex = $.inArray(Math.round(player.getCurrentTime()), videoPlayEventTimeStack);
                    if (eventIndex != -1) {
                        trackAAEvent(aaAPIEventStack[eventIndex]);
                    }
                }
            };

            // Tracks the passed AA events (missed during Fast Forward)
            var trackPassedAAEvents = function () {
                var videoProgressed = Math.round(player.getCurrentTime());
                for (var i = 0; i < videoPlayEventTimeStack.length; i++) {
                    if (videoPlayEventTimeStack[i] <= videoProgressed) {
                        var aaAPIEvent = aaAPIEventStack[i];
                        trackAAEvent(aaAPIEvent);
                    } else {
                        break;
                    }
                }
            };

            // Tracks AA events
            var trackAAEvent = function (aaAPIEvent) {
                if ($.inArray(aaAPIEvent, reportedAAAPIEventStack) == -1) {
                    if(!videoTitle) {
                        $.getJSON("https://www.googleapis.com/youtube/v3/videos",{part: "snippet", id: videoId, key: "AIzaSyDKIzOcpR-5CcJAWJjkEJWNz-_8W7lKsmY"}).done( function(json) {
                            videoTitle = json.items[0].snippet.title;
                            reportAAEvent(aaAPIEvent, videoTitle);
                        });
                    } else {
                        reportAAEvent(aaAPIEvent, videoTitle);
                    }
                    reportedAAAPIEventStack.push(aaAPIEvent);
                    if ('videoFullPlayEvent' == aaAPIEvent) {
                        clearInterval(videoTrackTimer);
                    }
                }
            };

            // Reports AA on Video Events
            var reportAAEvent = (function () {
                var aaAPIEvents = {
                    'videoPlayEvent': {
                        'name': 'event10',
                        'message': 'YT Video Playing'
                    },
                    'videoQuarterPlayEvent': {
                        'name': 'event12',
                        'message': 'YT Video Played 25%'
                    },
                    'videoHalfPlayEvent': {
                        'name': 'event13',
                        'message': 'YT Video Played 50%'
                    },
                    'videoThreeQuarterPlayEvent': {
                        'name': 'event14',
                        'message': 'YT Video Played 75%'
                    },
                    'videoNintyPercentPlayEvent': {
                        'name': 'event15',
                        'message': 'YT Video Played 90%'
                    },
                    'videoFullPlayEvent': {
                        'name': 'event11',
                        'message': 'YT Video Played 100%'
                    }
                };
                return function (aaAPIEvent, videoTitle) {
                    if (typeof s_gi === 'function') {
                        var s = s_gi(s_account);
                        var linkTrackVars = s.linkTrackVars + ',eVar27,events';

                        // Add an additional tracking property 'prop27' only for videoPlayEvent
                        if ('videoPlayEvent' == aaAPIEvent) {
                            linkTrackVars += linkTrackVars + ',prop27';
                            s.prop27 = videoTitle;
                        }
                        s.linkTrackVars = linkTrackVars;
                        s.eVar27 = videoTitle;
                        s.linkTrackEvents = aaAPIEvents[aaAPIEvent].name;
                        s.events = aaAPIEvents[aaAPIEvent].name;
                        s.tl(this, 'o', aaAPIEvents[aaAPIEvent].message);
                    }
                };
            })();

            return {
                init: init,
                create: createIframePlayer,
                pause: pauseIframePlayer
            };
        }

        var bindEvents = function () {
            video = new NTVideo();

            $('body').off('click.video', '.nt-js-launch-video').on('click.video', '.nt-js-launch-video', function (e) {
                var videoId = $(this).data('video-id');
                e.preventDefault();
                video.init('player', videoId);
            });

            $('body').off('click.videoClose', '.nt-js-close-video').on('click.videoClose', '.nt-js-close-video', function (e) {
                e.preventDefault();
                video.pause();
            });

            //Esc key pauses (& ultimately stops & destroys) the player
            $(window).on("keydown", function (e) {
                if ($('.nt-modal-video.active').length && e.keyCode == 27) {
                    video.pause();
                }
            });
        };

        return {
            init: function ($isModuleOnPage) {
                if ($isModuleOnPage.length !== 0) {
                    bindEvents();
                }
            }
        };
    });

// jshint maxstatements: false, newcap: false
define( 'splash',[ 'jquery' ],
    function( $ ) {

    "use strict";

    var timeout,
        callback = function(){};

    var end = function(){
        $(window).scrollTop(0);
        $('.nt-splash').fadeOut( function(){
            callback();
        });
    };

    var play = function( cb, duration) {
     
        duration = duration || 10000;
        callback = cb || function(){};
        
        timeout = setTimeout( function(){
            end();
        }, duration);

    };

    var skip = function() {
        clearTimeout(timeout);
        end();
    };

    return {
        play: play,
        skip : skip
    };
} );

// jshint maxstatements: false, newcap: false
define( 'ntResrc',[ 'jquery' ],
    function( $ ) {
        "use strict";

        // Still referencing nt-resrc so we can run both responsive image severs simultaneously until transition complete
		var responsiveImageServerConfigElement = $('script[data-nt-resrc-server]');
        var settings = {
            server : responsiveImageServerConfigElement.data('nt-resrc-server'),
            resrcOnResizeDown : false,
            resrcOnPinch : false,
            imageQuality : 85,
            pixelRounding : 20,
            ssl: (responsiveImageServerConfigElement.data('protocol') !== 'http')
        };

        var update = function($parent){
            var imageArray = [];

            //If no parent then update all
            if(!$parent || !$parent.length){
                resrc.run();
                return;
            }

            //If parent IS actually the image
            if($parent.is('img') && $parent.hasClass('resrc')){
                resrc.run($parent);
                return;
            }

            //Else update all images within parent
            if($parent.find('.resrc').length){
                resrc.run( $parent.find('.resrc') );
            }

        };

        var onResrcReady = function(){
            resrc.configure(settings).run();
        };

        var resrcAsyncronousEmbed = function () {
            var d = false;
            var r = document.createElement("script");
            // secure domain requests for resource it should be directed to the main site
            if (window.location.hostname.indexOf('secure') === 0) {
            	r.src = window.location.protocol + '//' + window.location.hostname.replace('secure', 'www') + "/assets/js/lib/resrc.min.js";
			} else {
				r.src = "/assets/js/lib/resrc.min.js";
			}
            r.type = "text/javascript";
            r.async = "true";
            r.onload = r.onreadystatechange = function () {
                var rs = this.readyState;
                if (d || rs && rs != "complete" && rs != "loaded") return;
                d = true;
                resrc.ready(onResrcReady);
            };
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(r, s);
        };



        return {
            init: function() {
                resrcAsyncronousEmbed();
            },
            update: update
        };
    } );

// jshint maxstatements: false, newcap: false
define( 'moreInsert',[ 'jquery', 'checkViewport', 'ntResrc' ], function( $, checkViewport, ntResrc ) {

    "use strict";

    var config = {
        "timeline-posts" : {
            "parentId" : "timeline-posts",
            "contentId" : "timeline-posts",
            "multiplyContentsSelector" : ".nt-posts-line",
            "insertSelector" : ".nt-see-more",
            "insertMethod" : "replaceWith"
        }
    };

    var settings,
        inProgress = false;

    var insertContent = function(data) {
        //Find content in data and add to DOM
        var content = settings.contentId ? $(data).find('#'+settings.contentId).html() : $(data);
        $('#'+settings.parentId).find(settings.insertSelector)[settings.insertMethod](content);

        //Multply content of an element (e.g. timeline dotted line)
        if(settings.multiplyContent){
            $('#'+settings.parentId).find(settings.multiplyContentsSelector).append(settings.multiplyContent);
        }

        //Update timeline images
        ntResrc.update( $('#'+settings.parentId) );
    };

    var getContent = function(e){
        e.preventDefault();

        var jqxhr,
            $button = $(this);

        settings = config[$button.data('more-id')];

        //Prevent mulitple requests
        //Must have settings
        if(inProgress || !settings.parentId){
            return false;
        }

        inProgress = true;

        jqxhr = $.get( $(this).attr('href'), insertContent)
                .fail(function() {
                    //fail
                })
                .always(function() {
                    inProgress = false;
                });

    };


    var setupMultiplyContent = function(){
        var $button = $(this),
            settings = config[$button.data('more-id')];

        //Save HTML to config object (via settings reference)
        if(settings.multiplyContentsSelector){
            settings.multiplyContent = $('#'+settings.parentId).find(settings.multiplyContentsSelector).html();
        }
    };


    var bindEvents = function() {
        
        $('body').off('click.more', '.nt-js-more-button').on('click.more', '.nt-js-more-button', getContent);

        $('.nt-js-more-button').each( setupMultiplyContent );

    };

    return {
        init: function( $isModuleOnPage ) {
            if ( $isModuleOnPage.length !== 0 ) {
                bindEvents();
            }
        },
    };
} );

// jshint maxstatements: false, newcap: false
define('animFrame',[],
    function() {
        "use strict";
        // rAF shim layer with setTimeout fallback - by Paul Irish
        window.requestAnimFrame = (function(){
            return  window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame   ||
            window.mozRequestAnimationFrame      ||
            function( callback ){
                window.setTimeout(callback, 1000 / 60);
            };
        })();
    } 
);

// jshint maxstatements: false, newcap: false
define( 'menuNav',[ 'jquery' ],
    function( $ ) {

    "use strict";

    var moreLinkWidth = $('#more-link').width(),
        $moreLink = $('#more-link').detach(),
        $headerNavUL = $('#nt-masthead-nav ul'),
        $mainNavigationContainer = $('.nt-main-navigation-container'),
        $mainSearchContainer = $('.nt-main-search-container'),
  	$primarySearchInput = $('.nt-primary-search-input'),
        headerNavLiWidth = [],
        currentMoreLinkIndex = $headerNavUL.find('li').length,
        $mobileNavUL = $('#main-navigation-list'),
        $launchMainNavigationElement;

    var hideMoreLink = function(){
        $moreLink.css('display','none');
    };

    var showMoreLink = function(){
        $moreLink.css('display','inline-block');
    };

    var calculateIndividualLiWidth = function(){
        headerNavLiWidth = [];
        $headerNavUL.find('li').each(function(){
            if($(this).attr('id') !== 'more-link'){
                headerNavLiWidth.push($(this).width());
            }
        });
    };

    var calculateLastVisibleLiIndex = function(headerWidth){
        var i, len = headerNavLiWidth.length;

        var sum = 0;
        for(i = 0; i < len; i++){
            sum += headerNavLiWidth[i];
            if(headerWidth < sum){
                break;
            }
        }
        return i;
    };

    var hideAllLinkAfterMore = function(position){
        var i = position;
        for(i; i <= headerNavLiWidth.length; i++){
            $headerNavUL.find('li:eq('+i+')').hide();
        }
    };

    var showAllLinkBeforeMore = function(position){
        var i = 0;
        for(i; i < position; i++){
            $headerNavUL.find('li:eq('+i+')').show();
        }
    };

    var injectMoreLink = function(){
        var headNavWidth = $headerNavUL.width(),
            visibleLiIndex = calculateLastVisibleLiIndex(headNavWidth);

        if(visibleLiIndex === headerNavLiWidth.length){
            $headerNavUL.find('li').show();
            hideMoreLink();
        }else if(visibleLiIndex <= currentMoreLinkIndex){
            showMoreLink();
            $headerNavUL.find('li:eq('+(visibleLiIndex-2)+')').after($moreLink);
            hideAllLinkAfterMore(visibleLiIndex);
        }else if(visibleLiIndex > currentMoreLinkIndex){
            showMoreLink();
            $headerNavUL.find('li:eq('+(visibleLiIndex-1)+')').after($moreLink);
            showAllLinkBeforeMore(visibleLiIndex);
        }

        currentMoreLinkIndex = visibleLiIndex;
    };

    var showHideMoreLink = function(){
        $(window).on('resize',(function fn(){
            calculateIndividualLiWidth();
            injectMoreLink();
            return fn;
        })());
    };

    var maintainActiveFocus = function($parent){
        var checkFocus = true;
        $(document).on("focusin", function(e) {
            if(checkFocus){
                //parent IS active
                //parent NOT focused
                //next focus element NOT within parent
                if( $parent.hasClass('active') && !$parent.is(':focus') && !$parent.find(e.target).length ){
                    e.preventDefault();
                    checkFocus = false;
                    $parent.focus();
                    checkFocus = true;
                }
            }

        });
    };

    var primeMainElements = function(){
        $mainNavigationContainer.attr("tabindex","-1");
        $mainSearchContainer.attr("tabindex","-1");
    };
    
    function showMainMenu(e){
        e.preventDefault();
        $launchMainNavigationElement = $(e.target);
        $mainNavigationContainer.fadeIn(function(){
            $mainNavigationContainer.addClass("active");
            $mainNavigationContainer.focus();
        });
    }

    function showMainSearch(e){
        e.preventDefault();
        $mainSearchContainer.fadeIn(function(){
            $mainSearchContainer.addClass("active");
            $primarySearchInput.focus();
            $('.nt-skip-to-search a').attr('aria-expanded', true);
            $mainSearchContainer.attr('aria-hidden', false);
        });
    }

    function hideMainMenu(e){
        e.preventDefault();
        $mainNavigationContainer.removeClass("active");
        $mainNavigationContainer.fadeOut(function(){
            if($launchMainNavigationElement){
                $launchMainNavigationElement.focus();
            } else {
                $('.nt-skip-to-nav a').focus();
            }
        });
    }

    function hideMainSearch(e){
        e.preventDefault();
        $mainSearchContainer.removeClass("active");
        $mainSearchContainer.fadeOut(function(){
            $('.nt-skip-to-search a').focus();
            $('.nt-skip-to-search a').attr('aria-expanded', false);
            $mainSearchContainer.attr('aria-hidden', true);
        });
    }

    function showHideChildren(e){
        var activeClass = 'expanded';
        var $parents = $mobileNavUL.find('li.has-children');

        // when click on a parent
        $parents.click(function(e){
            var $parent = $(this);
            var isExpanded = $parent.hasClass(activeClass);
            $parents.removeClass(activeClass); // collapse all parents
            if (!isExpanded){
                $parent.addClass(activeClass);
            }

            e.preventDefault();
        });
        
        // Prevents the parent link triggering event of collapsing
        $parents.find("a.nt-link-dim").click(function(e) {
            e.stopPropagation();
        });
        // Prevents the childs triggering parent event of collapsing
        $parents.find("ul.nt-mob-child-nav").click(function(e) {
            e.stopPropagation();
        });
    }

    var bindEvents = function() {
        showHideMoreLink();
        primeMainElements();
        maintainActiveFocus($mainNavigationContainer);
        maintainActiveFocus($mainSearchContainer);
        showHideChildren();

        $('.nt-skip-to-nav a').on('click', showMainMenu);
        $('.nt-skip-to-search a').on('click', showMainSearch);
        $moreLink.on('click', showMainMenu);
        $mainNavigationContainer.find(' .nt-back-to-top a').on('click', function(e){
            hideMainMenu(e);
        });
        $mainSearchContainer.find(' .nt-back-to-top a').on('click', function(e){
            hideMainSearch(e);
        });

        //Esc key closes nav or search
        $(window).on("keydown", function(e) {
            if ($mainNavigationContainer.hasClass('active') && e.keyCode == 27) {
                hideMainMenu(e);
            }
            if ($mainSearchContainer.hasClass('active') && e.keyCode == 27) {
                hideMainSearch(e);
            }
        });

    };

    return {
        init: function() {
            bindEvents();
        }
    };
} );

// jshint maxstatements: false, newcap: false
define( 'fixWebLinkHeight',[ 'jquery' ],
    function( $ ) {

    "use strict";

    var $sourceItem = $('.nt-related-items-container .regular-related-item'),
        $externalLinkItem = $('.nt-related-items-container .external-link-related-item');

    var bindEvents = function() {
        setTimeout( function(){
            $(window).on('resize',(function fn(){
                $externalLinkItem.height($sourceItem.height());
                return fn;
            })());
        }, 50);
    };

    return {
        init: function() {
            bindEvents();
        }
    };
} );

// jshint maxstatements: false, newcap: false
define( 'backButton',[ 'jquery' ],
function( $ ) {

    "use strict";

    var mainPageTemplates = '.nt-home-template, .nt-days-out-template, .nt-join-template, .nt-search-template',
        searchResultTemplates = '.nt-masonry-results, .nt-search-campaign-item',
        backToString = 'Back to',
        maxCharacter = 40, //TODO This limit should've been handled by CSS on template by template basis
        visitHistory = [],
        currentPage = {},
        $backButtonContainer = $('.nt-js-backlink'),
        backButton,
        cookieName = 'pageStack',
        cookieRegEx = new RegExp(cookieName + '=([^;]+)');

    function updateCookie(){
        //Without expiration date this will work as session cookie
        document.cookie = cookieName+'='+JSON.stringify(visitHistory)+';path=/';
    }

    function getCurrentPageName(){
        var headingText = $('div:not(.nt-splash-content) > h1:first').text();
        if(headingText){
            //remove new lines and normalise space characters.
            headingText = headingText.replace(/(\r\n|\n|\r)/gm,'').replace(/\s+/gm,' ');
            headingText = $.trim(headingText);
            return headingText;
        } else if(document.title.indexOf("|")) {
            return $.trim(document.title.split("|")[0]);
        }
        return "Not available";
    }

    function getCurrentPageURL(){
        return window.location.href;
    }

    function getValueOfHistoryCookie(){
        var value = cookieRegEx.exec(document.cookie);
        return (value != null) ? JSON.parse(value[1]) : [];
    }

    function injectBackButton(name, url){
        backButton = $('<a href="'+url+'" ><span class="icon" data-icon="&#xe61b;"></span><span>'+name+'</span></a>');
        
        $backButtonContainer.html(backButton);
        $backButtonContainer.addClass('active');
    }

    function getPreviousPage(index){
        var previousPage = {};

        index = index || 1;

        if(visitHistory.length - index >= 0){ //history not avaialble
            
            visitHistory = getValueOfHistoryCookie();
            previousPage = $.extend(true,{},visitHistory[visitHistory.length - index]); //clone 
            previousPage.name = backToString+" "+previousPage.name;

            if(previousPage.name.length > maxCharacter){
                previousPage.name = previousPage.name.slice(0,maxCharacter-3)+'&hellip;';
            }
        }
        return previousPage;
    }

    function addCurrentPageToCookie(){
        //prevent duplicate page entry 
        if((visitHistory.length > 0) && (currentPage.url === visitHistory[visitHistory.length - 1].url)){
            return;
        }

        //Keep last 10 histories to reduce cookie size
        if(visitHistory.length > 10){
            //console.warn('deleting first one');
            visitHistory.shift();
        }

        visitHistory.push(currentPage);
        updateCookie();
    }

    function updateCurrentPageUrlInCookie(pageUrl) {
        visitHistory = getValueOfHistoryCookie();
        var updatedCurrentPage;
    	if(visitHistory.length > 0) {
    		updatedCurrentPage = visitHistory[visitHistory.length - 1];
    		updatedCurrentPage.url = pageUrl;
		} else {
    		updatedCurrentPage = { "name": getCurrentPageName(), "url": pageUrl };
         	visitHistory.push(updatedCurrentPage);
		}
		updateCookie();
    }

    function showBackButtonIfAvailable(){
        var previousPage;
        if(visitHistory.length > 0){
            //current page is the Last Back button
            if(currentPage.url === visitHistory[visitHistory.length - 1].url){
                previousPage = getPreviousPage(2);
            }else{
                previousPage = getPreviousPage();
            }

            if(!$.isEmptyObject(previousPage)){
                injectBackButton(previousPage.name, previousPage.url);
            }
        }
    }

    function removePreviousPage(){
        visitHistory.pop();
        updateCookie();
    }
    
    function clearHistoryFromCookie(){
        visitHistory = [];
        updateCookie();
    }

    function isMainPage(){
        if($('.nt-main-wrapper').is(mainPageTemplates)){
            //if its search page and there is no search result - then main search page.
            //if it has search result then back button needs to be shown
            //There should be better option available from backend integration.
            if($('.nt-main-wrapper').hasClass('nt-search-template') && ($('.nt-main-wrapper').find(searchResultTemplates).length > 0) ){
                //its result result page - not the main search page.
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }
    }

    var bindEvents = function() {
        if(isMainPage()){
            clearHistoryFromCookie();
        }
        //update history var from Cookie; 
        visitHistory = getValueOfHistoryCookie();
        currentPage = {"name" : getCurrentPageName(), "url" : getCurrentPageURL()};

        showBackButtonIfAvailable();

        addCurrentPageToCookie();

        $(backButton).on('click',function(){
            removePreviousPage();
        });
    };

    return {
        init: function() {
            bindEvents();
        },
        replaceCurrentPageUrlInCookie: function(pageUrl) {
        	updateCurrentPageUrlInCookie(pageUrl);
        }
    };
} );

// jshint maxstatements: false, newcap: false
define( 'search',[ 'jquery' ],
function( $ ) {

    "use strict";

    var searchInputSelector = '.nt-js-search',
        searchInputPredictiveSelector = '.nt-js-search-predictive',
        searchContainerSelector = '.nt-search-container',
        disableOverflowSelector = '.nt-js-disable-overflow-on-search',

        searchSubmitDisabledClass = 'nt-js-search-submit-disabled',
        showGeolocationClass = 'nt-show-geolocation',
        showSuggestionsClass = 'nt-show-suggestions',
        searchPlaceButtonSelector = '#search-place',

        miniumumStringLength = 3;

    var getPrimarySearchContainer = function($el){
        return $el.closest(searchContainerSelector);
    };

    var getOverflowedContainer = function($el){
        return $el.closest(disableOverflowSelector);
    };

    var allowSearchResultOutsideContainer = function(e, $overflowedContainer){
        if(e.type === "focusin"){
            $overflowedContainer.css('overflow','visible');
        }else if(e.type === "focusout"){
            $overflowedContainer.css('overflow','hidden');
        }
    };

    var primarySearchFocus = function(e){

        var $input = $(this),
            $primarySearchContainer = getPrimarySearchContainer($input),
            $overflowedContainer = getOverflowedContainer($input);

        if($overflowedContainer.length > 0){
            allowSearchResultOutsideContainer(e, $overflowedContainer);
        }
        
        $primarySearchContainer.toggleClass(showGeolocationClass, !$primarySearchContainer.hasClass(showGeolocationClass));
        $primarySearchContainer.removeClass(showSuggestionsClass);
    };

    var showSuggestions = function(e){
        var $input = $(e.target),
            length = $input.val().length,
            $primarySearchContainer = getPrimarySearchContainer($input);
        
        $primarySearchContainer.toggleClass(showSuggestionsClass, length >= miniumumStringLength);
    };

    var enableSubmit = function(e) {
        var $input = $(this),
            $container = getPrimarySearchContainer( $input ),
            $button = $container.find('button:first');

        if($input.val().length >= miniumumStringLength || $(searchPlaceButtonSelector).hasClass('active')) {
            $button.removeAttr('disabled');
            $container.removeClass(searchSubmitDisabledClass);
        } else {
            $button.attr('disabled', 'disabled');
            $container.addClass(searchSubmitDisabledClass);
        }
    };
    
    var bindEvents = function() {
        //On page load
        $(searchInputSelector).each(enableSubmit);

        //Enable/disable submit
        $(searchInputSelector).on('focusin keyup', enableSubmit);

        //Predictive and location search
        $(searchInputPredictiveSelector).on('focusin focusout', primarySearchFocus);
        $(searchInputPredictiveSelector).on('focusin keyup', showSuggestions);
        
    };

    $.fn.putCursorAtEnd = function() {

   	  return this.each(function() {

   	    $(this).focus()

   	    // If this function exists...
   	    if (this.setSelectionRange) {
   	      // ... then use it (Doesn't work in IE)

   	      // Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
   	      var len = $(this).val().length * 2;

   	      this.setSelectionRange(len, len);

   	    } else {
   	    // ... otherwise replace the contents with itself
   	    // (Doesn't work in Google Chrome)

   	      $(this).val($(this).val());

   	    }

   	    // Scroll to the bottom, in case we're in a tall textarea
   	    // (Necessary for Firefox and Google Chrome)
   	    this.scrollTop = 999999;

   	  });

   	};

    return {
        init: function( $isModuleOnPage ) {
            if ( $isModuleOnPage.length !== 0 ) {
                bindEvents();
            }
        }
    };
} );

// jshint maxstatements: false, newcap: false
define( 'site-search',['jquery'],
    function($) {

    "use strict";
    var query, $searchBox;

    function putQueryInSearchBox() {
      $searchBox = $('#cse-search-input-box-id');
      query = $.getParameterByName('q');
        if(query) {
            $searchBox.val(query);
        }
    }

    return {
        init: function( $isModuleOnPage ) {
            if ( $isModuleOnPage.length !== 0 ) {
                putQueryInSearchBox();
            }
        }
    };

  });

// jshint maxstatements: false, newcap: false
define( 'footer',[ 'jquery' ],
    function( $ ) {

    "use strict";
    var $cookieStatement = $('.nt-footer-cookie-statement');
    var $closeBtn = $('<a class="cls-btn" href="javascript:void(0);"><span data-icon="&#xe602;"></span></a>');


    var moveCookiePolicy = function() {

      $cookieStatement.find('.columns p').first().append($closeBtn);
      $('body').append($cookieStatement);
    }

    var bindEvents = function() {

        $closeBtn.click(function (e) {
          e.preventDefault();
          $cookieStatement.hide();
        });

    };

    return {
        init: function( $isModuleOnPage ) {
            if ( $isModuleOnPage.length !== 0 ) {
                bindEvents();
                moveCookiePolicy();
            }
        }
    };
} );

// jshint maxstatements: false, newcap: false
define( 'searchRefine',[ 'jquery' ],
    function( $ ) {

    "use strict";

    var refineModalId = '#refine-by-category-modal',
        applyFilterButtonClass = '.nt-apply-filter',
        clearFilterButtonClass = '.nt-clear-filter',
		filterGroupsDataAttribute = 'nt-filter-groups',
        groupCheckboxClass = '.nt-filter-group',
        toggleAllEvent = 'toggle-all',
        undoFilterChangesEvent = 'undo-filter-changes',
        appliedFilterTagClass = '.nt-search-filters-tag',
        undoFilterChangesClass = '.nt-undo-filter-changes';

    // target: the dom element that the event will be raised from
    // type: the event name
    // data: any additional data to attach to the event
    var raiseEvent = function (target, type, data) {
        $(target).trigger($.extend({type: type}, data));
    };

    var raiseUndoFiltersEvent = function () {
        raiseEvent($(this), undoFilterChangesEvent, {
            filterGroups: $(this).data(filterGroupsDataAttribute).split(',')
        });
    };

    var getModalCheckboxesSelectorForFilterGroup = function (group) {
        return 'input' + groupCheckboxClass + '[name="' + group + '"]';
    };

	var getSearchFormInputSelectorFromModalInputId = function (modalInputId) {
        return '#' + modalInputId + '-actual';
    };

    var updateGroupCheckedStatus = function (e) {
		$.each(e.filterGroups, function (index, filterGroup) {
			$(getModalCheckboxesSelectorForFilterGroup(filterGroup), refineModalId)
				.prop('checked', e.isSelected)
				.trigger('change.refine');
		});
    };

    var copyStatusToActualSearchInput = function () {
        var modalInputId = $(this).prop('id');
        $(getSearchFormInputSelectorFromModalInputId(modalInputId)).prop('checked', $(this).prop('checked'));
    };

    var updateWithCheckedStatusActiveSearch = function () {
        var modalInputId = $(this).prop('id'),
            activeSearchCheckedStatus = $(getSearchFormInputSelectorFromModalInputId(modalInputId)).prop('checked');
        $(this).prop('checked', activeSearchCheckedStatus).trigger('change.refine');
    };

    var performSearch = function () {
        $('#search-submit')[0].click();
    };

    var applyFilter = function (event) {
        var filterGroups = $(this).data(filterGroupsDataAttribute).split(',');
		$.each(filterGroups, function (index, filterGroup) {
			$(getModalCheckboxesSelectorForFilterGroup(filterGroup), refineModalId).each(copyStatusToActualSearchInput);
		});
        performSearch();
        event.preventDefault();
    };

    var resetFilterStatus = function (e) {
		$.each(e.filterGroups, function (index, filterGroup) {
			$(getModalCheckboxesSelectorForFilterGroup(filterGroup), refineModalId).each(updateWithCheckedStatusActiveSearch);
		});
    };

    var clearFilter = function (event) {
        // trigger an apply-all event containing checked status of false and the group of filters
        // to check/unckeck
        raiseEvent($(this), toggleAllEvent, {
            isSelected: false,
            filterGroups: $(this).data(filterGroupsDataAttribute).split(',')
        });
        event.preventDefault();
    };

    var ignoreDefaultBehaviour = function (event) {
        event.preventDefault();
    };

    var bindEvents = function () {
		var body = $('body');
		body.off('click.undo-filters').on('click.undo-filters', undoFilterChangesClass, raiseUndoFiltersEvent);
		body.off(toggleAllEvent).on(toggleAllEvent, updateGroupCheckedStatus);
		body.off(undoFilterChangesEvent).on(undoFilterChangesEvent, resetFilterStatus);
		body.off('click.apply-search').on('click.apply-search', applyFilterButtonClass, applyFilter);
		body.off('click.clear-search').on('click.clear-search', clearFilterButtonClass, clearFilter);
		body.off('click.tag').on('click.tag', appliedFilterTagClass, ignoreDefaultBehaviour);
    };

    return {
        init: function ( $isModuleOnPage ) {
            if ($isModuleOnPage.length !== 0) {
                bindEvents($isModuleOnPage);
            }
        }
    };
});
// jshint maxstatements: false, newcap: false
define( 'backToTop',[ 'jquery', 'gotoSmooth' ],
    function( $, gotoSmooth ) {

    "use strict";

    var bindEvents = function($backToTop) {
        
        //On click smoothly goto top
        $backToTop.find('a').on('click', function(e){
            console.log('top');
            var distance = $(document).scrollTop(),
                speed = 2,
                time = distance / speed;
            e.preventDefault();
            gotoSmooth.gotoElement($('#top'), time);
        });

        //On scroll show/hide back to top button
        $(window).on('scroll',(function fn(){
            var vh = $(window).height(),
                graceDistance = vh * 2.25,
                currentTop = $(document).scrollTop() + vh;
            if(currentTop > graceDistance){
                $backToTop.addClass('back-to-top-active');
            } else {
                $backToTop.removeClass('back-to-top-active');
            }
            return fn;
        })());

    };

    return {
        init: function( $isModuleOnPage ) {
            if ( $isModuleOnPage.length !== 0 ) {
                bindEvents($isModuleOnPage);
            }
        }
    };
} );

define('walkingTrailsMap',['jquery'],
    function ($) {
        "use strict";


        /**
         *
         * BrowserDetect
         *
         * Taken from a stackoverflow using Moderizr features
         *
         * @author  http://stackoverflow.com/questions/13478303/correct-way-to-use-modernizr-to-detect-ie
         *
         */
        var BrowserDetect = {
            init: function () {
                this.browser = this.searchString(this.dataBrowser) || "Other";
                this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
            },
            searchString: function (data) {
                for (var i = 0; i < data.length; i++) {
                    var dataString = data[i].string;
                    this.versionSearchString = data[i].subString;

                    if (dataString.indexOf(data[i].subString) !== -1) {
                        return data[i].identity;
                    }
                }
            },
            searchVersion: function (dataString) {
                var index = dataString.indexOf(this.versionSearchString);
                if (index === -1) {
                    return;
                }

                var rv = dataString.indexOf("rv:");
                if (this.versionSearchString === "Trident" && rv !== -1) {
                    return parseFloat(dataString.substring(rv + 3));
                } else {
                    return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
                }
            },

            dataBrowser: [{
                string: navigator.userAgent,
                subString: "Edge",
                identity: "MS Edge"
            }, {
                string: navigator.userAgent,
                subString: "Chrome",
                identity: "Chrome"
            }, {
                string: navigator.userAgent,
                subString: "MSIE",
                identity: "Explorer"
            }, {
                string: navigator.userAgent,
                subString: "Trident",
                identity: "Explorer"
            }, {
                string: navigator.userAgent,
                subString: "Firefox",
                identity: "Firefox"
            }, {
                string: navigator.userAgent,
                subString: "Safari",
                identity: "Safari"
            }, {
                string: navigator.userAgent,
                subString: "Opera",
                identity: "Opera"
            }]

        };


        /**
         *
         * The National Trust Walking Trails map and search functionality
         *
         * This module provide the walking trails map/search functionality. It
         * loads geoJson from the CMS which we pull in creating individual markers
         *
         *
         * @file
         * @author  Dryden Williams - Manifesto Digital
         * @version 1.0
         *
         */
        var WalkingTrailsMap = {
            // Constances
            TRAILS_MAP: "",
            MARKER_CLUSTERER: "",
            CENTRAL_ENGLAND: {
                lat: 52.438713,
                lng: -1.647794
            },
            MOBILE_BREAKPOINT: 550,
            // Module Variables
            allMarkers: [],
            allInfowindows: [],
            searchMarkers: [],
            activeMarker: "",
            activeInfowindow: "",
            infowindowVisible: null,

            init: function () {
                if (typeof google !== 'undefined') {
                    google.maps.event.addDomListener(window, 'load', $.proxy(function () {
                        this.initializeMap();
                        this.initSearch();
                    }, this));
                }
            },


            /**
             *
             * A function to initilise the map
             *
             * This function is called once the google maps script
             * has been loaded into the DOM
             *
             */
            initializeMap: function () {
                this.TRAILS_MAP = new google.maps.Map(document.getElementById('mapCanvas'), {
                    zoom: 6,
                    center: this.CENTRAL_ENGLAND,
                    panControl: false,
                    streetViewControl: false,
                    zoomControl: true,
                    zoomControlOptions: {
                        position: google.maps.ControlPosition.RIGHT_CENTER
                    }
                });
                this.initMarkers();
                this.initEvents();
            },

            /*
             * A method for adding all markers to the google map
             *
             */
            initMarkers: function () {
                var _this = this;
                $.getJSON("/search/data/walking-trails.geojson", function (data) {
                    for (var i = 0; i < data.features.length; i++) {

                        (function (i, _this) {

                            var _this = _this;

                            var feature = data.features[i];
                            var featureLat = feature.geometry.coordinates[0];
                            var featureLng = feature.geometry.coordinates[1];
                            var featurelatLng = new google.maps.LatLng(featureLng, featureLat);
                            var marker = _this.addMarker(feature, featurelatLng);
                            marker.setMap(null);
                            google.maps.event.addListener(marker, 'click', function () {
                                _this._closeInfowindows();
                                var myOptions = {
                                    position: featurelatLng,
                                    pixelOffset: new google.maps.Size(-200, -60),
                                    infoBoxClearance: new google.maps.Size(1, 1),
                                    maxWidth: 400,
                                    isHidden: false,
                                    pane: "floatPane",
                                    alignBottom: true,
                                    disableAutoPan: false,
                                    enableEventPropagation: false,
                                    closeBoxMargin: "2px 2px 2px 2px",
                                    closeBoxURL: "/assets/img/nt-close-card-bk.png",
                                    content: _this.addInfowindowContent(feature),
                                    boxStyle: {
                                        width: "400px",
                                        backgroundColor: "blue"
                                    }
                                };
                                var infowindow = new InfoBox(myOptions);
                                google.maps.event.addListener(infowindow, 'closeclick', function () {
                                    _this.infowindowVisible = null;
                                });

                                _this.activeMarker = marker;
                                _this.activeInfowindow = infowindow;
                                _this.allInfowindows.push(infowindow);
                                _this.infowindowVisible = true;

                                if (_this._checkWindowWidth() <= _this.MOBILE_BREAKPOINT) {
                                    _this.openMobileCard(infowindow);
                                } else {
                                    infowindow.open(_this.TRAILS_MAP, marker);
                                    _this.TRAILS_MAP.setCenter(marker.getPosition());
                                }

                            });
                            _this.allMarkers.push(marker);

                        })(i, _this);

                    }

                    var markerIcon = "../../assets/img/svg-icons/44-location-searched-purple.svg";
                    if (BrowserDetect.browser === "Explorer") {
                        markerIcon = "../../assets/img/44-location-searched-purple.png";
                    }

                    var MarkerClustererOptions = {
                        gridSize: 50,
                        maxZoom: 21,
                        styles: [{
                            url: markerIcon,
                            height: 32,
                            width: 32,
                            textColor: "white"
                        }]
                    };
                    _this.MARKER_CLUSTERER = new MarkerClusterer(_this.TRAILS_MAP, _this.allMarkers, MarkerClustererOptions);

                });


            },


            /*
             * A method to register the event listeners required by this module
             *
             */
            initEvents: function () {


                $(window).on("resize", $.proxy(this._windowResize, this));
                $(document).on("change", ".nt-trails-map-filters .dropdown", $.proxy(this.filterTrailsMap, this));
                $(document).on("touchstart click", "#ntJsLaunchModal", $.proxy(this._refreshMap, this));
                $(document).on("touchstart click", "#closeCardBtn", $.proxy(function () {
                    this.closeMobileCard();
                    setTimeout(function () {
                        $(".walking-trails-card").empty();
                    }, 600);
                    this.infowindowVisible = null;
                }, this));
                $(document).on("touchstart click", "#clearFilters", $.proxy(function () {
                    this._closeInfowindows();
                    this._clearFilters();
                }, this));

            },

            /*
             * A method that uses google maps places to set a marker
             * in a specific loaction
             *
             */
            initSearch: function () {
                var _this = this;
                // Create the search box and link it to the UI element.
                var input = document.getElementById('pac-input');
                var searchBox = new google.maps.places.SearchBox(input);
                // Bias the SearchBox results towards current map's viewport.
                _this.TRAILS_MAP.addListener('BOUNDS_changed', function () {
                    searchBox.setBounds(_this.TRAILS_MAP.getBounds());
                });
                searchBox.addListener('places_changed', function () {
                    var places = searchBox.getPlaces();
                    if (places.length === 0) {
                        return;
                    }
                    // Clear out the old allMarkers.
                    _this._deleteSearchMarkers();
                    $.each(places, function (index, place) {
                        var searchMarker = new google.maps.Marker({
                            map: _this.TRAILS_MAP,
                            position: place.geometry.location
                        });
                        _this.searchMarkers.push(searchMarker);
                        _this.TRAILS_MAP.setCenter(place.geometry.location);
                        _this.TRAILS_MAP.setZoom(12);
                    });
                });
            },


            addMarker: function (feature, featurelatLng) {
                var latLng = featurelatLng;
                return new google.maps.Marker({
                    map: this.TRAILS_MAP,
                    icon: {
                        url: "../../assets/img/43-location-pin-pink.png",
                        size: new google.maps.Size(25, 34)
                    },
                    position: latLng,
                    title: feature.properties.name,
                    grade: feature.properties.grade.toLowerCase(),
                    distance: feature.properties.distance,
                    hours: feature.properties.timeHours,
                    mins: feature.properties.timeMins
                });
            },


            /*
             *  Template structure for all infowindows
             *
             */
            addInfowindowContent: function (feature) {
                var title = feature.properties.title;
                var description = feature.properties.abstract;
                var imageUrl = feature.properties.imageUrl;
                var imageAlt = feature.properties.imageDescription;
                var grade = feature.properties.grade;
                var distance = feature.properties.distance + " miles";
                var duration = feature.properties.timeHours + "h " + feature.properties.timeMins + "mins";
                var articleUrl = feature.properties.articleUrl;

                var htmlString = '<div class="card nt-masonry-single-result" id="walking-card">';
                htmlString += '<div class="card-inner nt-masonry-single-result-inner">';
                htmlString += '<div class="icon close-card-btn" id="closeCardBtn" data-icon="&#xe61b;"></div>';
                htmlString += '<div class="image nt-masonry-single-result-image nt-image-wrap nt-image-wrap-16x9">';
                htmlString += '<img src="' + imageUrl + '" alt="' + imageAlt + '">';
                htmlString += '</div>';
                htmlString += '<div class="content nt-masonry-single-result-inner">';
                htmlString += '<div class="nt-masonry-single-result-category">Walking Trail</div>';
                htmlString += '<h3 class="card-title nt-link-chevron">';
                htmlString += '<a href="';
                htmlString += articleUrl;
                htmlString += '">';
                htmlString += title;
                htmlString += '</a>';
                htmlString += '</h3>';
                htmlString += '<p>';
                htmlString += description;
                htmlString += '</p>';
                htmlString += '<ul class="nt-masonry-single-result-list">';
                htmlString += '<li>';
                htmlString += '<span class="icon icon-grade" data-icon="&#xe61f;"></span>';
                htmlString += grade;
                htmlString += '</li>';
                htmlString += '<li>';
                htmlString += '<span class="icon icon-distance" data-icon="&#xe621;"></span>';
                htmlString += distance;
                htmlString += '</li>';
                htmlString += '<li>';
                htmlString += '<span class="icon icon-duration" data-icon="&#xe61e;"></span>';
                htmlString += duration;
                htmlString += '</li>';
                htmlString += '</ul>';
                htmlString += '</div>';
                htmlString += '</div>';
                htmlString += '</div>';
                return htmlString;
            },

            /*
             *  openMobileCard
             *  ====================================
             *
             */
            openMobileCard: function (infowindow) {
                $(".walking-trails-card").html(infowindow.content_);
                $("#mapWrapper").addClass("active");
            },

            /*
             *  closeMobileCard
             *  ====================================
             *
             */
            closeMobileCard: function () {
                $("#mapWrapper").removeClass("active");
            },

            /*
             *  filterTrailsMap
             *  ====================================
             *  A method for filtering all of the markers on the map
             *
             *
             */
            filterTrailsMap: function () {
                var _this = this;
                _this._closeInfowindows();
                // Dropdown values
                var $difficultyValue = $("#grade option:selected").val().toLowerCase();
                var $distanceValue = $("#distance option:selected").attr("value");
                var $durationValue = $("#duration option:selected").attr("value");

                _this.MARKER_CLUSTERER.clearMarkers();

                $.each(this.allMarkers, function (i, marker) {
                    if ($difficultyValue != "all" && !_this.filterDifficulty($difficultyValue, marker)) {
                        return true;
                    }
                    if ($distanceValue != "all" && !_this.filterDistance($distanceValue, marker)) {
                        return true;
                    }
                    if ($durationValue != "all" && !_this.filterDuration($durationValue, marker)) {
                        return true;
                    }
                    _this.MARKER_CLUSTERER.addMarker(marker);
                });
            },
            filterDifficulty: function ($difficultyValue, marker) {
                if ($difficultyValue === marker.grade) {
                    return true;
                } else {
                    return false;
                }
            },
            filterDistance: function ($distanceValue, marker) {
                // Filter distance option value
                var minDistance = parseInt($distanceValue.substring(0, $distanceValue.indexOf("-")));
                var maxDistance = parseInt($distanceValue.substr($distanceValue.indexOf("-") + 1));
                if (this._between(marker.distance, minDistance, maxDistance)) {
                    return true;
                } else {
                    return false;
                }
            },
            filterDuration: function ($durationValue, marker) {
                var minTime = this._hoursToMinutes(parseFloat($durationValue.substr(0, $durationValue.indexOf("-"))));
                var maxTime = this._hoursToMinutes(parseFloat($durationValue.substr($durationValue.indexOf("-") + 1)));
                var trailHoursInMinutes = parseInt(this._hoursToMinutes(marker.hours));
                var trailMinutes = parseInt(marker.mins);
                var trailTimeInMinutes = trailHoursInMinutes + trailMinutes;
                if (this._between(trailTimeInMinutes, minTime, maxTime)) {
                    return true;
                } else {
                    return false;
                }
            },


            /*
             * The underscore donates utility methods
             *
             *
             */

            // Base methods
            _checkWindowWidth: function () {
                return $(window).width();
            },
            _between: function (x, min, max) {
                return x >= min && x <= max;
            },
            _hoursToMinutes: function (hours) {
                return hours * 60;
            },

            // Map related methods
            _windowResize: function () {
                // if (!this.infowindowVisible) {
                //   this.TRAILS_MAP.setCenter(this.CENTRAL_ENGLAND);
                // }
                // If window is smaller than 550px
                if (this._checkWindowWidth() <= this.MOBILE_BREAKPOINT && this.infowindowVisible) {
                    // Small Screens
                    this.activeInfowindow.close();
                    this.openMobileCard(this.activeInfowindow);
                }

                // If window is bigger than 550px
                if (this._checkWindowWidth() >= this.MOBILE_BREAKPOINT && this.infowindowVisible) {
                    // Large Screens
                    this.closeMobileCard();
                    this.activeInfowindow.open(this.TRAILS_MAP, this.activeMarker);
                }
            },
            _clearFilters: function () {
                this.infowindowVisible = null;
                this._centerMap();
                this._deleteSearchMarkers();
                this.MARKER_CLUSTERER.addMarkers(this.allMarkers);
                $(".dropdown").val("all");
            },
            _deleteSearchMarkers: function () {
                for (var i = 0; i < this.searchMarkers.length; i += 1) {
                    this.searchMarkers[i].setMap(null);
                }
            },
            _centerMap: function () {
                this.TRAILS_MAP.setCenter(this.CENTRAL_ENGLAND);
                this.TRAILS_MAP.setZoom(6);
            },

            _refreshMap: function () {
                var _this = this;
                window.setTimeout(function () {
                    google.maps.event.trigger(_this.TRAILS_MAP, "resize");
                    _this.TRAILS_MAP.setCenter(_this.CENTRAL_ENGLAND);
                }, 400);
            },
            _closeInfowindows: function () {
                for (var i = 0; i < this.allInfowindows.length; i += 1) {
                    this.allInfowindows[i].close();
                }
            }

        };


        /*
         * This return function checks if the module is on the page
         * and inits the walking trails map
         */
        return {
            init: function ($isModuleOnPage) {
                if ($isModuleOnPage.length !== 0) {
                    BrowserDetect.init();
                    WalkingTrailsMap.init();
                }
            }
        };

    });

// jshint maxstatements: false, newcap: false
define('mediaCards',['jquery'],
    function ($) {

        "use strict";

        function mediaCenterCheckBox() {

            var $checkbox = $('.js-term-check');

            $checkbox.on('change', function (e) {

                if ($(this).is(':checked') === true) {
                    console.log($(this));
                    $(this).parent().next().removeClass('is-disabled');
                } else {
                    $(this).parent().next().addClass('is-disabled');
                }

            });

        }

        function pagination() {
            var linkHolder = $('.js-pagination'),
                contentHolder = $('.js-pagination-content'),
                items = contentHolder.children(),
                count = items.length,
                amount = 18,
                pages = count / amount;

            for (var i = 0; i < pages; i++) {
                var number = i + 1;
                $('<a href="" class="js-pagination-link">' + number + '</a>').appendTo(linkHolder);
            }

            for (var i = 0; i < items.length; i += amount) {
                items.slice(i, i + amount).wrapAll("<div class='js-pagination-page  pagination-page'></div>");
            }

            $('.js-pagination-link').first().addClass('is-active');

            $('.js-pagination-link').on('click', function (e) {
                e.preventDefault();
                var index = $(this).index();
                $(this).siblings().removeClass('is-active');
                $(this).addClass('is-active');
                $('.js-pagination-page').hide();
                $('.js-pagination-page').eq(index).show();
                $("html, body").animate({scrollTop: 0}, "fast");
            });

        }

        return {
            init: function ($isModuleOnPage) {
                if ($isModuleOnPage.length !== 0) {
                    mediaCenterCheckBox();

                    pagination();
                }
            }
        };

    });

// jshint maxstatements: false, newcap: false
define("NtPromoCodes", ['jquery'], function ($) {

    "use strict";

    var queryParams,
        legacyPromoParam = "sc",
        newPromoParam = "promoCode";

    var cookieValue;

    function grabParams() {
        queryParams = parse(location.search);
    }

    function duplicatePromoParams() {
        queryParams[legacyPromoParam] = cookieValue;
        queryParams[newPromoParam] = cookieValue;
    };

    function passParams() {
        $('.nt-modal-membership-table-cta a').on('click', function (e) {
            var linkLocation = $(this).attr('href');
            e.preventDefault();

            if (linkLocation.indexOf('?') !== -1) {
                location = linkLocation + "&" + serialize(queryParams);
            } else {
                location = linkLocation + "?" + serialize(queryParams);
            }

        });
    };

    function extractQuery(string) {
        if (string.indexOf('?') >= 0) {
            return string.split('?')[1];
        } else if (string.indexOf('=') >= 0) {
            return string;
        } else {
            return '';
        }
    };

    function parseValue(value) {
        value = decodeURIComponent(value);
        try {
            return JSON.parse(value);
        } catch (e) {
            return value;
        }
    };

    function parse(url) {
        var params = {},
            query = extractQuery(url);

        if (!query) {
            return params;
        }

        $.each(query.split('&'), function (idx, pair) {
            var key, value, oldValue;
            pair = pair.split('=');
            key = pair[0].replace('[]', ''); // FIXME
            value = parseValue(pair[1] || '');
            if (params.hasOwnProperty(key)) {
                if (!params[key].push) {
                    oldValue = params[key];
                    params[key] = [oldValue];
                }
                params[key].push(value);
            } else {
                params[key] = value;
            }
        });

        return params;
    };

    function getCookie(name) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length == 2) return parts.pop().split(";").shift();
    };

    function setCookie() {
        var legacyPromoVal = queryParams[legacyPromoParam];

        if (legacyPromoVal) {
            var expiryDate = new Date().addDays(7);
            document.cookie = 'sc=' + legacyPromoVal + '; Path=/; Expires=' + expiryDate.toUTCString() + ';';
            cookieValue = legacyPromoVal;

        } else {
            cookieValue = getCookie('sc');

        }
    };

    function serialize(params) {
        var pairs = [], currentKey, currentValue;

        for (var key in params) {
            if (params.hasOwnProperty(key)) {
                currentKey = key;
                currentValue = params[key];

                if (typeof currentValue === 'object') {
                    for (subKey in currentValue) {
                        if (currentValue.hasOwnProperty(subKey)) {
                            pairs.push(currentKey + '[' + (isNaN(subKey, 10) ? subKey : '') + ']=' + encodeURIComponent(currentValue[subKey]));
                        }
                    }
                } else {
                    pairs.push(currentKey + '=' + encodeURIComponent(currentValue));
                }
            }
        }
        return pairs.join("&");
    };

    return {
        init: function () {
            grabParams();
            setCookie();

            if (cookieValue) {
                duplicatePromoParams();
                passParams();

            }
        }
    };

});

/*
     _ _      _       _
 ___| (_) ___| | __  (_)___
/ __| | |/ __| |/ /  | / __|
\__ \ | | (__|   < _ | \__ \
|___/_|_|\___|_|\_(_)/ |___/
                   |__/

 Version: 1.7.1
  Author: Ken Wheeler
 Website: http://kenwheeler.github.io
    Docs: http://kenwheeler.github.io/slick
    Repo: http://github.com/kenwheeler/slick
  Issues: http://github.com/kenwheeler/slick/issues

 */
/* global window, document, define, jQuery, setInterval, clearInterval */
;(function(factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define('slick',['jquery'], factory);
    } else if (typeof exports !== 'undefined') {
        module.exports = factory(require('jquery'));
    } else {
        factory(jQuery);
    }

}(function($) {
    'use strict';
    var Slick = window.Slick || {};

    Slick = (function() {

        var instanceUid = 0;

        function Slick(element, settings) {

            var _ = this, dataSettings;

            _.defaults = {
                accessibility: true,
                adaptiveHeight: false,
                appendArrows: $(element),
                appendDots: $(element),
                arrows: true,
                asNavFor: null,
                prevArrow: '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
                nextArrow: '<button class="slick-next" aria-label="Next" type="button">Next</button>',
                autoplay: false,
                autoplaySpeed: 3000,
                centerMode: false,
                centerPadding: '50px',
                cssEase: 'ease',
                customPaging: function(slider, i) {
                    return $('<button type="button" />').text(i + 1);
                },
                dots: false,
                dotsClass: 'slick-dots',
                draggable: true,
                easing: 'linear',
                edgeFriction: 0.35,
                fade: false,
                focusOnSelect: false,
                infinite: true,
                initialSlide: 0,
                lazyLoad: 'ondemand',
                mobileFirst: false,
                pauseOnHover: true,
                pauseOnFocus: true,
                pauseOnDotsHover: false,
                respondTo: 'window',
                responsive: null,
                rows: 1,
                rtl: false,
                slide: '',
                slidesPerRow: 1,
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 500,
                swipe: true,
                swipeToSlide: false,
                touchMove: true,
                touchThreshold: 5,
                useCSS: true,
                useTransform: true,
                variableWidth: false,
                vertical: false,
                verticalSwiping: false,
                waitForAnimate: true,
                zIndex: 1000
            };

            _.initials = {
                animating: false,
                dragging: false,
                autoPlayTimer: null,
                currentDirection: 0,
                currentLeft: null,
                currentSlide: 0,
                direction: 1,
                $dots: null,
                listWidth: null,
                listHeight: null,
                loadIndex: 0,
                $nextArrow: null,
                $prevArrow: null,
                scrolling: false,
                slideCount: null,
                slideWidth: null,
                $slideTrack: null,
                $slides: null,
                sliding: false,
                slideOffset: 0,
                swipeLeft: null,
                swiping: false,
                $list: null,
                touchObject: {},
                transformsEnabled: false,
                unslicked: false
            };

            $.extend(_, _.initials);

            _.activeBreakpoint = null;
            _.animType = null;
            _.animProp = null;
            _.breakpoints = [];
            _.breakpointSettings = [];
            _.cssTransitions = false;
            _.focussed = false;
            _.interrupted = false;
            _.hidden = 'hidden';
            _.paused = true;
            _.positionProp = null;
            _.respondTo = null;
            _.rowCount = 1;
            _.shouldClick = true;
            _.$slider = $(element);
            _.$slidesCache = null;
            _.transformType = null;
            _.transitionType = null;
            _.visibilityChange = 'visibilitychange';
            _.windowWidth = 0;
            _.windowTimer = null;

            dataSettings = $(element).data('slick') || {};

            _.options = $.extend({}, _.defaults, settings, dataSettings);

            _.currentSlide = _.options.initialSlide;

            _.originalSettings = _.options;

            if (typeof document.mozHidden !== 'undefined') {
                _.hidden = 'mozHidden';
                _.visibilityChange = 'mozvisibilitychange';
            } else if (typeof document.webkitHidden !== 'undefined') {
                _.hidden = 'webkitHidden';
                _.visibilityChange = 'webkitvisibilitychange';
            }

            _.autoPlay = $.proxy(_.autoPlay, _);
            _.autoPlayClear = $.proxy(_.autoPlayClear, _);
            _.autoPlayIterator = $.proxy(_.autoPlayIterator, _);
            _.changeSlide = $.proxy(_.changeSlide, _);
            _.clickHandler = $.proxy(_.clickHandler, _);
            _.selectHandler = $.proxy(_.selectHandler, _);
            _.setPosition = $.proxy(_.setPosition, _);
            _.swipeHandler = $.proxy(_.swipeHandler, _);
            _.dragHandler = $.proxy(_.dragHandler, _);
            _.keyHandler = $.proxy(_.keyHandler, _);

            _.instanceUid = instanceUid++;

            // A simple way to check for HTML strings
            // Strict HTML recognition (must start with <)
            // Extracted from jQuery v1.11 source
            _.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/;


            _.registerBreakpoints();
            _.init(true);

        }

        return Slick;

    }());

    Slick.prototype.activateADA = function() {
        var _ = this;

        _.$slideTrack.find('.slick-active').attr({
            'aria-hidden': 'false'
        }).find('a, input, button, select').attr({
            'tabindex': '0'
        });

    };

    Slick.prototype.addSlide = Slick.prototype.slickAdd = function(markup, index, addBefore) {

        var _ = this;

        if (typeof(index) === 'boolean') {
            addBefore = index;
            index = null;
        } else if (index < 0 || (index >= _.slideCount)) {
            return false;
        }

        _.unload();

        if (typeof(index) === 'number') {
            if (index === 0 && _.$slides.length === 0) {
                $(markup).appendTo(_.$slideTrack);
            } else if (addBefore) {
                $(markup).insertBefore(_.$slides.eq(index));
            } else {
                $(markup).insertAfter(_.$slides.eq(index));
            }
        } else {
            if (addBefore === true) {
                $(markup).prependTo(_.$slideTrack);
            } else {
                $(markup).appendTo(_.$slideTrack);
            }
        }

        _.$slides = _.$slideTrack.children(this.options.slide);

        _.$slideTrack.children(this.options.slide).detach();

        _.$slideTrack.append(_.$slides);

        _.$slides.each(function(index, element) {
            $(element).attr('data-slick-index', index);
        });

        _.$slidesCache = _.$slides;

        _.reinit();

    };

    Slick.prototype.animateHeight = function() {
        var _ = this;
        if (_.options.slidesToShow === 1 && _.options.adaptiveHeight === true && _.options.vertical === false) {
            var targetHeight = _.$slides.eq(_.currentSlide).outerHeight(true);
            _.$list.animate({
                height: targetHeight
            }, _.options.speed);
        }
    };

    Slick.prototype.animateSlide = function(targetLeft, callback) {

        var animProps = {},
            _ = this;

        _.animateHeight();

        if (_.options.rtl === true && _.options.vertical === false) {
            targetLeft = -targetLeft;
        }
        if (_.transformsEnabled === false) {
            if (_.options.vertical === false) {
                _.$slideTrack.animate({
                    left: targetLeft
                }, _.options.speed, _.options.easing, callback);
            } else {
                _.$slideTrack.animate({
                    top: targetLeft
                }, _.options.speed, _.options.easing, callback);
            }

        } else {

            if (_.cssTransitions === false) {
                if (_.options.rtl === true) {
                    _.currentLeft = -(_.currentLeft);
                }
                $({
                    animStart: _.currentLeft
                }).animate({
                    animStart: targetLeft
                }, {
                    duration: _.options.speed,
                    easing: _.options.easing,
                    step: function(now) {
                        now = Math.ceil(now);
                        if (_.options.vertical === false) {
                            animProps[_.animType] = 'translate(' +
                                now + 'px, 0px)';
                            _.$slideTrack.css(animProps);
                        } else {
                            animProps[_.animType] = 'translate(0px,' +
                                now + 'px)';
                            _.$slideTrack.css(animProps);
                        }
                    },
                    complete: function() {
                        if (callback) {
                            callback.call();
                        }
                    }
                });

            } else {

                _.applyTransition();
                targetLeft = Math.ceil(targetLeft);

                if (_.options.vertical === false) {
                    animProps[_.animType] = 'translate3d(' + targetLeft + 'px, 0px, 0px)';
                } else {
                    animProps[_.animType] = 'translate3d(0px,' + targetLeft + 'px, 0px)';
                }
                _.$slideTrack.css(animProps);

                if (callback) {
                    setTimeout(function() {

                        _.disableTransition();

                        callback.call();
                    }, _.options.speed);
                }

            }

        }

    };

    Slick.prototype.getNavTarget = function() {

        var _ = this,
            asNavFor = _.options.asNavFor;

        if ( asNavFor && asNavFor !== null ) {
            asNavFor = $(asNavFor).not(_.$slider);
        }

        return asNavFor;

    };

    Slick.prototype.asNavFor = function(index) {

        var _ = this,
            asNavFor = _.getNavTarget();

        if ( asNavFor !== null && typeof asNavFor === 'object' ) {
            asNavFor.each(function() {
                var target = $(this).slick('getSlick');
                if(!target.unslicked) {
                    target.slideHandler(index, true);
                }
            });
        }

    };

    Slick.prototype.applyTransition = function(slide) {

        var _ = this,
            transition = {};

        if (_.options.fade === false) {
            transition[_.transitionType] = _.transformType + ' ' + _.options.speed + 'ms ' + _.options.cssEase;
        } else {
            transition[_.transitionType] = 'opacity ' + _.options.speed + 'ms ' + _.options.cssEase;
        }

        if (_.options.fade === false) {
            _.$slideTrack.css(transition);
        } else {
            _.$slides.eq(slide).css(transition);
        }

    };

    Slick.prototype.autoPlay = function() {

        var _ = this;

        _.autoPlayClear();

        if ( _.slideCount > _.options.slidesToShow ) {
            _.autoPlayTimer = setInterval( _.autoPlayIterator, _.options.autoplaySpeed );
        }

    };

    Slick.prototype.autoPlayClear = function() {

        var _ = this;

        if (_.autoPlayTimer) {
            clearInterval(_.autoPlayTimer);
        }

    };

    Slick.prototype.autoPlayIterator = function() {

        var _ = this,
            slideTo = _.currentSlide + _.options.slidesToScroll;

        if ( !_.paused && !_.interrupted && !_.focussed ) {

            if ( _.options.infinite === false ) {

                if ( _.direction === 1 && ( _.currentSlide + 1 ) === ( _.slideCount - 1 )) {
                    _.direction = 0;
                }

                else if ( _.direction === 0 ) {

                    slideTo = _.currentSlide - _.options.slidesToScroll;

                    if ( _.currentSlide - 1 === 0 ) {
                        _.direction = 1;
                    }

                }

            }

            _.slideHandler( slideTo );

        }

    };

    Slick.prototype.buildArrows = function() {

        var _ = this;

        if (_.options.arrows === true ) {

            _.$prevArrow = $(_.options.prevArrow).addClass('slick-arrow');
            _.$nextArrow = $(_.options.nextArrow).addClass('slick-arrow');

            if( _.slideCount > _.options.slidesToShow ) {

                _.$prevArrow.removeClass('slick-hidden').removeAttr('aria-hidden tabindex');
                _.$nextArrow.removeClass('slick-hidden').removeAttr('aria-hidden tabindex');

                if (_.htmlExpr.test(_.options.prevArrow)) {
                    _.$prevArrow.prependTo(_.options.appendArrows);
                }

                if (_.htmlExpr.test(_.options.nextArrow)) {
                    _.$nextArrow.appendTo(_.options.appendArrows);
                }

                if (_.options.infinite !== true) {
                    _.$prevArrow
                        .addClass('slick-disabled')
                        .attr('aria-disabled', 'true');
                }

            } else {

                _.$prevArrow.add( _.$nextArrow )

                    .addClass('slick-hidden')
                    .attr({
                        'aria-disabled': 'true',
                        'tabindex': '-1'
                    });

            }

        }

    };

    Slick.prototype.buildDots = function() {

        var _ = this,
            i, dot;

        if (_.options.dots === true) {

            _.$slider.addClass('slick-dotted');

            dot = $('<ul />').addClass(_.options.dotsClass);

            for (i = 0; i <= _.getDotCount(); i += 1) {
                dot.append($('<li />').append(_.options.customPaging.call(this, _, i)));
            }

            _.$dots = dot.appendTo(_.options.appendDots);

            _.$dots.find('li').first().addClass('slick-active');

        }

    };

    Slick.prototype.buildOut = function() {

        var _ = this;

        _.$slides =
            _.$slider
                .children( _.options.slide + ':not(.slick-cloned)')
                .addClass('slick-slide');

        _.slideCount = _.$slides.length;

        _.$slides.each(function(index, element) {
            $(element)
                .attr('data-slick-index', index)
                .data('originalStyling', $(element).attr('style') || '');
        });

        _.$slider.addClass('slick-slider');

        _.$slideTrack = (_.slideCount === 0) ?
            $('<div class="slick-track"/>').appendTo(_.$slider) :
            _.$slides.wrapAll('<div class="slick-track"/>').parent();

        _.$list = _.$slideTrack.wrap(
            '<div class="slick-list"/>').parent();
        _.$slideTrack.css('opacity', 0);

        if (_.options.centerMode === true || _.options.swipeToSlide === true) {
            _.options.slidesToScroll = 1;
        }

        $('img[data-lazy]', _.$slider).not('[src]').addClass('slick-loading');

        _.setupInfinite();

        _.buildArrows();

        _.buildDots();

        _.updateDots();


        _.setSlideClasses(typeof _.currentSlide === 'number' ? _.currentSlide : 0);

        if (_.options.draggable === true) {
            _.$list.addClass('draggable');
        }

    };

    Slick.prototype.buildRows = function() {

        var _ = this, a, b, c, newSlides, numOfSlides, originalSlides,slidesPerSection;

        newSlides = document.createDocumentFragment();
        originalSlides = _.$slider.children();

        if(_.options.rows > 1) {

            slidesPerSection = _.options.slidesPerRow * _.options.rows;
            numOfSlides = Math.ceil(
                originalSlides.length / slidesPerSection
            );

            for(a = 0; a < numOfSlides; a++){
                var slide = document.createElement('div');
                for(b = 0; b < _.options.rows; b++) {
                    var row = document.createElement('div');
                    for(c = 0; c < _.options.slidesPerRow; c++) {
                        var target = (a * slidesPerSection + ((b * _.options.slidesPerRow) + c));
                        if (originalSlides.get(target)) {
                            row.appendChild(originalSlides.get(target));
                        }
                    }
                    slide.appendChild(row);
                }
                newSlides.appendChild(slide);
            }

            _.$slider.empty().append(newSlides);
            _.$slider.children().children().children()
                .css({
                    'width':(100 / _.options.slidesPerRow) + '%',
                    'display': 'inline-block'
                });

        }

    };

    Slick.prototype.checkResponsive = function(initial, forceUpdate) {

        var _ = this,
            breakpoint, targetBreakpoint, respondToWidth, triggerBreakpoint = false;
        var sliderWidth = _.$slider.width();
        var windowWidth = window.innerWidth || $(window).width();

        if (_.respondTo === 'window') {
            respondToWidth = windowWidth;
        } else if (_.respondTo === 'slider') {
            respondToWidth = sliderWidth;
        } else if (_.respondTo === 'min') {
            respondToWidth = Math.min(windowWidth, sliderWidth);
        }

        if ( _.options.responsive &&
            _.options.responsive.length &&
            _.options.responsive !== null) {

            targetBreakpoint = null;

            for (breakpoint in _.breakpoints) {
                if (_.breakpoints.hasOwnProperty(breakpoint)) {
                    if (_.originalSettings.mobileFirst === false) {
                        if (respondToWidth < _.breakpoints[breakpoint]) {
                            targetBreakpoint = _.breakpoints[breakpoint];
                        }
                    } else {
                        if (respondToWidth > _.breakpoints[breakpoint]) {
                            targetBreakpoint = _.breakpoints[breakpoint];
                        }
                    }
                }
            }

            if (targetBreakpoint !== null) {
                if (_.activeBreakpoint !== null) {
                    if (targetBreakpoint !== _.activeBreakpoint || forceUpdate) {
                        _.activeBreakpoint =
                            targetBreakpoint;
                        if (_.breakpointSettings[targetBreakpoint] === 'unslick') {
                            _.unslick(targetBreakpoint);
                        } else {
                            _.options = $.extend({}, _.originalSettings,
                                _.breakpointSettings[
                                    targetBreakpoint]);
                            if (initial === true) {
                                _.currentSlide = _.options.initialSlide;
                            }
                            _.refresh(initial);
                        }
                        triggerBreakpoint = targetBreakpoint;
                    }
                } else {
                    _.activeBreakpoint = targetBreakpoint;
                    if (_.breakpointSettings[targetBreakpoint] === 'unslick') {
                        _.unslick(targetBreakpoint);
                    } else {
                        _.options = $.extend({}, _.originalSettings,
                            _.breakpointSettings[
                                targetBreakpoint]);
                        if (initial === true) {
                            _.currentSlide = _.options.initialSlide;
                        }
                        _.refresh(initial);
                    }
                    triggerBreakpoint = targetBreakpoint;
                }
            } else {
                if (_.activeBreakpoint !== null) {
                    _.activeBreakpoint = null;
                    _.options = _.originalSettings;
                    if (initial === true) {
                        _.currentSlide = _.options.initialSlide;
                    }
                    _.refresh(initial);
                    triggerBreakpoint = targetBreakpoint;
                }
            }

            // only trigger breakpoints during an actual break. not on initialize.
            if( !initial && triggerBreakpoint !== false ) {
                _.$slider.trigger('breakpoint', [_, triggerBreakpoint]);
            }
        }

    };

    Slick.prototype.changeSlide = function(event, dontAnimate) {

        var _ = this,
            $target = $(event.currentTarget),
            indexOffset, slideOffset, unevenOffset;

        // If target is a link, prevent default action.
        if($target.is('a')) {
            event.preventDefault();
        }

        // If target is not the <li> element (ie: a child), find the <li>.
        if(!$target.is('li')) {
            $target = $target.closest('li');
        }

        unevenOffset = (_.slideCount % _.options.slidesToScroll !== 0);
        indexOffset = unevenOffset ? 0 : (_.slideCount - _.currentSlide) % _.options.slidesToScroll;

        switch (event.data.message) {

            case 'previous':
                slideOffset = indexOffset === 0 ? _.options.slidesToScroll : _.options.slidesToShow - indexOffset;
                if (_.slideCount > _.options.slidesToShow) {
                    _.slideHandler(_.currentSlide - slideOffset, false, dontAnimate);
                }
                break;

            case 'next':
                slideOffset = indexOffset === 0 ? _.options.slidesToScroll : indexOffset;
                if (_.slideCount > _.options.slidesToShow) {
                    _.slideHandler(_.currentSlide + slideOffset, false, dontAnimate);
                }
                break;

            case 'index':
                var index = event.data.index === 0 ? 0 :
                    event.data.index || $target.index() * _.options.slidesToScroll;

                _.slideHandler(_.checkNavigable(index), false, dontAnimate);
                $target.children().trigger('focus');
                break;

            default:
                return;
        }

    };

    Slick.prototype.checkNavigable = function(index) {

        var _ = this,
            navigables, prevNavigable;

        navigables = _.getNavigableIndexes();
        prevNavigable = 0;
        if (index > navigables[navigables.length - 1]) {
            index = navigables[navigables.length - 1];
        } else {
            for (var n in navigables) {
                if (index < navigables[n]) {
                    index = prevNavigable;
                    break;
                }
                prevNavigable = navigables[n];
            }
        }

        return index;
    };

    Slick.prototype.cleanUpEvents = function() {

        var _ = this;

        if (_.options.dots && _.$dots !== null) {

            $('li', _.$dots)
                .off('click.slick', _.changeSlide)
                .off('mouseenter.slick', $.proxy(_.interrupt, _, true))
                .off('mouseleave.slick', $.proxy(_.interrupt, _, false));

            if (_.options.accessibility === true) {
                _.$dots.off('keydown.slick', _.keyHandler);
            }
        }

        _.$slider.off('focus.slick blur.slick');

        if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
            _.$prevArrow && _.$prevArrow.off('click.slick', _.changeSlide);
            _.$nextArrow && _.$nextArrow.off('click.slick', _.changeSlide);

            if (_.options.accessibility === true) {
                _.$prevArrow.off('keydown.slick', _.keyHandler);
                _.$nextArrow.off('keydown.slick', _.keyHandler);
            }
        }

        _.$list.off('touchstart.slick mousedown.slick', _.swipeHandler);
        _.$list.off('touchmove.slick mousemove.slick', _.swipeHandler);
        _.$list.off('touchend.slick mouseup.slick', _.swipeHandler);
        _.$list.off('touchcancel.slick mouseleave.slick', _.swipeHandler);

        _.$list.off('click.slick', _.clickHandler);

        $(document).off(_.visibilityChange, _.visibility);

        _.cleanUpSlideEvents();

        if (_.options.accessibility === true) {
            _.$list.off('keydown.slick', _.keyHandler);
        }

        if (_.options.focusOnSelect === true) {
            $(_.$slideTrack).children().off('click.slick', _.selectHandler);
        }

        $(window).off('orientationchange.slick.slick-' + _.instanceUid, _.orientationChange);

        $(window).off('resize.slick.slick-' + _.instanceUid, _.resize);

        $('[draggable!=true]', _.$slideTrack).off('dragstart', _.preventDefault);

        $(window).off('load.slick.slick-' + _.instanceUid, _.setPosition);

    };

    Slick.prototype.cleanUpSlideEvents = function() {

        var _ = this;

        _.$list.off('mouseenter.slick', $.proxy(_.interrupt, _, true));
        _.$list.off('mouseleave.slick', $.proxy(_.interrupt, _, false));

    };

    Slick.prototype.cleanUpRows = function() {

        var _ = this, originalSlides;

        if(_.options.rows > 1) {
            originalSlides = _.$slides.children().children();
            originalSlides.removeAttr('style');
            _.$slider.empty().append(originalSlides);
        }

    };

    Slick.prototype.clickHandler = function(event) {

        var _ = this;

        if (_.shouldClick === false) {
            event.stopImmediatePropagation();
            event.stopPropagation();
            event.preventDefault();
        }

    };

    Slick.prototype.destroy = function(refresh) {

        var _ = this;

        _.autoPlayClear();

        _.touchObject = {};

        _.cleanUpEvents();

        $('.slick-cloned', _.$slider).detach();

        if (_.$dots) {
            _.$dots.remove();
        }

        if ( _.$prevArrow && _.$prevArrow.length ) {

            _.$prevArrow
                .removeClass('slick-disabled slick-arrow slick-hidden')
                .removeAttr('aria-hidden aria-disabled tabindex')
                .css('display','');

            if ( _.htmlExpr.test( _.options.prevArrow )) {
                _.$prevArrow.remove();
            }
        }

        if ( _.$nextArrow && _.$nextArrow.length ) {

            _.$nextArrow
                .removeClass('slick-disabled slick-arrow slick-hidden')
                .removeAttr('aria-hidden aria-disabled tabindex')
                .css('display','');

            if ( _.htmlExpr.test( _.options.nextArrow )) {
                _.$nextArrow.remove();
            }
        }


        if (_.$slides) {

            _.$slides
                .removeClass('slick-slide slick-active slick-center slick-visible slick-current')
                .removeAttr('aria-hidden')
                .removeAttr('data-slick-index')
                .each(function(){
                    $(this).attr('style', $(this).data('originalStyling'));
                });

            _.$slideTrack.children(this.options.slide).detach();

            _.$slideTrack.detach();

            _.$list.detach();

            _.$slider.append(_.$slides);
        }

        _.cleanUpRows();

        _.$slider.removeClass('slick-slider');
        _.$slider.removeClass('slick-initialized');
        _.$slider.removeClass('slick-dotted');

        _.unslicked = true;

        if(!refresh) {
            _.$slider.trigger('destroy', [_]);
        }

    };

    Slick.prototype.disableTransition = function(slide) {

        var _ = this,
            transition = {};

        transition[_.transitionType] = '';

        if (_.options.fade === false) {
            _.$slideTrack.css(transition);
        } else {
            _.$slides.eq(slide).css(transition);
        }

    };

    Slick.prototype.fadeSlide = function(slideIndex, callback) {

        var _ = this;

        if (_.cssTransitions === false) {

            _.$slides.eq(slideIndex).css({
                zIndex: _.options.zIndex
            });

            _.$slides.eq(slideIndex).animate({
                opacity: 1
            }, _.options.speed, _.options.easing, callback);

        } else {

            _.applyTransition(slideIndex);

            _.$slides.eq(slideIndex).css({
                opacity: 1,
                zIndex: _.options.zIndex
            });

            if (callback) {
                setTimeout(function() {

                    _.disableTransition(slideIndex);

                    callback.call();
                }, _.options.speed);
            }

        }

    };

    Slick.prototype.fadeSlideOut = function(slideIndex) {

        var _ = this;

        if (_.cssTransitions === false) {

            _.$slides.eq(slideIndex).animate({
                opacity: 0,
                zIndex: _.options.zIndex - 2
            }, _.options.speed, _.options.easing);

        } else {

            _.applyTransition(slideIndex);

            _.$slides.eq(slideIndex).css({
                opacity: 0,
                zIndex: _.options.zIndex - 2
            });

        }

    };

    Slick.prototype.filterSlides = Slick.prototype.slickFilter = function(filter) {

        var _ = this;

        if (filter !== null) {

            _.$slidesCache = _.$slides;

            _.unload();

            _.$slideTrack.children(this.options.slide).detach();

            _.$slidesCache.filter(filter).appendTo(_.$slideTrack);

            _.reinit();

        }

    };

    Slick.prototype.focusHandler = function() {

        var _ = this;

        _.$slider
            .off('focus.slick blur.slick')
            .on('focus.slick blur.slick', '*', function(event) {

            event.stopImmediatePropagation();
            var $sf = $(this);

            setTimeout(function() {

                if( _.options.pauseOnFocus ) {
                    _.focussed = $sf.is(':focus');
                    _.autoPlay();
                }

            }, 0);

        });
    };

    Slick.prototype.getCurrent = Slick.prototype.slickCurrentSlide = function() {

        var _ = this;
        return _.currentSlide;

    };

    Slick.prototype.getDotCount = function() {

        var _ = this;

        var breakPoint = 0;
        var counter = 0;
        var pagerQty = 0;

        if (_.options.infinite === true) {
            if (_.slideCount <= _.options.slidesToShow) {
                 ++pagerQty;
            } else {
                while (breakPoint < _.slideCount) {
                    ++pagerQty;
                    breakPoint = counter + _.options.slidesToScroll;
                    counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll : _.options.slidesToShow;
                }
            }
        } else if (_.options.centerMode === true) {
            pagerQty = _.slideCount;
        } else if(!_.options.asNavFor) {
            pagerQty = 1 + Math.ceil((_.slideCount - _.options.slidesToShow) / _.options.slidesToScroll);
        }else {
            while (breakPoint < _.slideCount) {
                ++pagerQty;
                breakPoint = counter + _.options.slidesToScroll;
                counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll : _.options.slidesToShow;
            }
        }

        return pagerQty - 1;

    };

    Slick.prototype.getLeft = function(slideIndex) {

        var _ = this,
            targetLeft,
            verticalHeight,
            verticalOffset = 0,
            targetSlide;

        _.slideOffset = 0;
        verticalHeight = _.$slides.first().outerHeight(true);

        if (_.options.infinite === true) {
            if (_.slideCount > _.options.slidesToShow) {
                _.slideOffset = (_.slideWidth * _.options.slidesToShow) * -1;
                verticalOffset = (verticalHeight * _.options.slidesToShow) * -1;
            }
            if (_.slideCount % _.options.slidesToScroll !== 0) {
                if (slideIndex + _.options.slidesToScroll > _.slideCount && _.slideCount > _.options.slidesToShow) {
                    if (slideIndex > _.slideCount) {
                        _.slideOffset = ((_.options.slidesToShow - (slideIndex - _.slideCount)) * _.slideWidth) * -1;
                        verticalOffset = ((_.options.slidesToShow - (slideIndex - _.slideCount)) * verticalHeight) * -1;
                    } else {
                        _.slideOffset = ((_.slideCount % _.options.slidesToScroll) * _.slideWidth) * -1;
                        verticalOffset = ((_.slideCount % _.options.slidesToScroll) * verticalHeight) * -1;
                    }
                }
            }
        } else {
            if (slideIndex + _.options.slidesToShow > _.slideCount) {
                _.slideOffset = ((slideIndex + _.options.slidesToShow) - _.slideCount) * _.slideWidth;
                verticalOffset = ((slideIndex + _.options.slidesToShow) - _.slideCount) * verticalHeight;
            }
        }

        if (_.slideCount <= _.options.slidesToShow) {
            _.slideOffset = 0;
            verticalOffset = 0;
        }

        if (_.options.centerMode === true && _.slideCount <= _.options.slidesToShow) {
            _.slideOffset = ((_.slideWidth * Math.floor(_.options.slidesToShow)) / 2) - ((_.slideWidth * _.slideCount) / 2);
        } else if (_.options.centerMode === true && _.options.infinite === true) {
            _.slideOffset += _.slideWidth * Math.floor(_.options.slidesToShow / 2) - _.slideWidth;
        } else if (_.options.centerMode === true) {
            _.slideOffset = 0;
            _.slideOffset += _.slideWidth * Math.floor(_.options.slidesToShow / 2);
        }

        if (_.options.vertical === false) {
            targetLeft = ((slideIndex * _.slideWidth) * -1) + _.slideOffset;
        } else {
            targetLeft = ((slideIndex * verticalHeight) * -1) + verticalOffset;
        }

        if (_.options.variableWidth === true) {

            if (_.slideCount <= _.options.slidesToShow || _.options.infinite === false) {
                targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex);
            } else {
                targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex + _.options.slidesToShow);
            }

            if (_.options.rtl === true) {
                if (targetSlide[0]) {
                    targetLeft = (_.$slideTrack.width() - targetSlide[0].offsetLeft - targetSlide.width()) * -1;
                } else {
                    targetLeft =  0;
                }
            } else {
                targetLeft = targetSlide[0] ? targetSlide[0].offsetLeft * -1 : 0;
            }

            if (_.options.centerMode === true) {
                if (_.slideCount <= _.options.slidesToShow || _.options.infinite === false) {
                    targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex);
                } else {
                    targetSlide = _.$slideTrack.children('.slick-slide').eq(slideIndex + _.options.slidesToShow + 1);
                }

                if (_.options.rtl === true) {
                    if (targetSlide[0]) {
                        targetLeft = (_.$slideTrack.width() - targetSlide[0].offsetLeft - targetSlide.width()) * -1;
                    } else {
                        targetLeft =  0;
                    }
                } else {
                    targetLeft = targetSlide[0] ? targetSlide[0].offsetLeft * -1 : 0;
                }

                targetLeft += (_.$list.width() - targetSlide.outerWidth()) / 2;
            }
        }

        return targetLeft;

    };

    Slick.prototype.getOption = Slick.prototype.slickGetOption = function(option) {

        var _ = this;

        return _.options[option];

    };

    Slick.prototype.getNavigableIndexes = function() {

        var _ = this,
            breakPoint = 0,
            counter = 0,
            indexes = [],
            max;

        if (_.options.infinite === false) {
            max = _.slideCount;
        } else {
            breakPoint = _.options.slidesToScroll * -1;
            counter = _.options.slidesToScroll * -1;
            max = _.slideCount * 2;
        }

        while (breakPoint < max) {
            indexes.push(breakPoint);
            breakPoint = counter + _.options.slidesToScroll;
            counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll : _.options.slidesToShow;
        }

        return indexes;

    };

    Slick.prototype.getSlick = function() {

        return this;

    };

    Slick.prototype.getSlideCount = function() {

        var _ = this,
            slidesTraversed, swipedSlide, centerOffset;

        centerOffset = _.options.centerMode === true ? _.slideWidth * Math.floor(_.options.slidesToShow / 2) : 0;

        if (_.options.swipeToSlide === true) {
            _.$slideTrack.find('.slick-slide').each(function(index, slide) {
                if (slide.offsetLeft - centerOffset + ($(slide).outerWidth() / 2) > (_.swipeLeft * -1)) {
                    swipedSlide = slide;
                    return false;
                }
            });

            slidesTraversed = Math.abs($(swipedSlide).attr('data-slick-index') - _.currentSlide) || 1;

            return slidesTraversed;

        } else {
            return _.options.slidesToScroll;
        }

    };

    Slick.prototype.goTo = Slick.prototype.slickGoTo = function(slide, dontAnimate) {

        var _ = this;

        _.changeSlide({
            data: {
                message: 'index',
                index: parseInt(slide)
            }
        }, dontAnimate);

    };

    Slick.prototype.init = function(creation) {

        var _ = this;

        if (!$(_.$slider).hasClass('slick-initialized')) {

            $(_.$slider).addClass('slick-initialized');

            _.buildRows();
            _.buildOut();
            _.setProps();
            _.startLoad();
            _.loadSlider();
            _.initializeEvents();
            _.updateArrows();
            _.updateDots();
            _.checkResponsive(true);
            _.focusHandler();

        }

        if (creation) {
            _.$slider.trigger('init', [_]);
        }

        if (_.options.accessibility === true) {
            _.initADA();
        }

        if ( _.options.autoplay ) {

            _.paused = false;
            _.autoPlay();

        }

    };

    Slick.prototype.initADA = function() {
        var _ = this,
                numDotGroups = Math.ceil(_.slideCount / _.options.slidesToShow),
                tabControlIndexes = _.getNavigableIndexes().filter(function(val) {
                    return (val >= 0) && (val < _.slideCount);
                });

        _.$slides.add(_.$slideTrack.find('.slick-cloned')).attr({
            'aria-hidden': 'true',
            'tabindex': '-1'
        }).find('a, input, button, select').attr({
            'tabindex': '-1'
        });

        if (_.$dots !== null) {
            _.$slides.not(_.$slideTrack.find('.slick-cloned')).each(function(i) {
                var slideControlIndex = tabControlIndexes.indexOf(i);

                $(this).attr({
                    'role': 'tabpanel',
                    'id': 'slick-slide' + _.instanceUid + i,
                    'tabindex': -1
                });            

                if (slideControlIndex !== -1) {
                    $(this).attr({
                        'aria-describedby': 'slick-slide-control' + _.instanceUid + slideControlIndex
                    });
                }
            });

            _.$dots.attr('role', 'tablist').find('li').each(function(i) {
                var mappedSlideIndex = tabControlIndexes[i];
        
                $(this).attr({
                    'role': 'presentation'
                });

                $(this).find('button').first().attr({
                    'role': 'tab',
                    'id': 'slick-slide-control' + _.instanceUid + i,
                    'aria-controls': 'slick-slide' + _.instanceUid + mappedSlideIndex,
                    'aria-label': (i + 1) + ' of ' + numDotGroups,
                    'aria-selected': null,
                    'tabindex': '-1'
                });

            }).eq(_.currentSlide).find('button').attr({
                'aria-selected': 'true',
                'tabindex': '0'
            }).end();
        }

        for (var i=_.currentSlide, max=i+_.options.slidesToShow; i < max; i++) {
            _.$slides.eq(i).attr('tabindex', 0);
        }

        _.activateADA();

    };

    Slick.prototype.initArrowEvents = function() {

        var _ = this;

        if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
            _.$prevArrow
               .off('click.slick')
               .on('click.slick', {
                    message: 'previous'
               }, _.changeSlide);
            _.$nextArrow
               .off('click.slick')
               .on('click.slick', {
                    message: 'next'
               }, _.changeSlide);

            if (_.options.accessibility === true) {
                _.$prevArrow.on('keydown.slick', _.keyHandler);
                _.$nextArrow.on('keydown.slick', _.keyHandler);
            }   
        }

    };

    Slick.prototype.initDotEvents = function() {

        var _ = this;

        if (_.options.dots === true) {
            $('li', _.$dots).on('click.slick', {
                message: 'index'
            }, _.changeSlide);

            if (_.options.accessibility === true) {
                _.$dots.on('keydown.slick', _.keyHandler);
            }
        }

        if ( _.options.dots === true && _.options.pauseOnDotsHover === true ) {

            $('li', _.$dots)
                .on('mouseenter.slick', $.proxy(_.interrupt, _, true))
                .on('mouseleave.slick', $.proxy(_.interrupt, _, false));

        }

    };

    Slick.prototype.initSlideEvents = function() {

        var _ = this;

        if ( _.options.pauseOnHover ) {

            _.$list.on('mouseenter.slick', $.proxy(_.interrupt, _, true));
            _.$list.on('mouseleave.slick', $.proxy(_.interrupt, _, false));

        }

    };

    Slick.prototype.initializeEvents = function() {

        var _ = this;

        _.initArrowEvents();

        _.initDotEvents();
        _.initSlideEvents();

        _.$list.on('touchstart.slick mousedown.slick', {
            action: 'start'
        }, _.swipeHandler);
        _.$list.on('touchmove.slick mousemove.slick', {
            action: 'move'
        }, _.swipeHandler);
        _.$list.on('touchend.slick mouseup.slick', {
            action: 'end'
        }, _.swipeHandler);
        _.$list.on('touchcancel.slick mouseleave.slick', {
            action: 'end'
        }, _.swipeHandler);

        _.$list.on('click.slick', _.clickHandler);

        $(document).on(_.visibilityChange, $.proxy(_.visibility, _));

        if (_.options.accessibility === true) {
            _.$list.on('keydown.slick', _.keyHandler);
        }

        if (_.options.focusOnSelect === true) {
            $(_.$slideTrack).children().on('click.slick', _.selectHandler);
        }

        $(window).on('orientationchange.slick.slick-' + _.instanceUid, $.proxy(_.orientationChange, _));

        $(window).on('resize.slick.slick-' + _.instanceUid, $.proxy(_.resize, _));

        $('[draggable!=true]', _.$slideTrack).on('dragstart', _.preventDefault);

        $(window).on('load.slick.slick-' + _.instanceUid, _.setPosition);
        $(_.setPosition);

    };

    Slick.prototype.initUI = function() {

        var _ = this;

        if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {

            _.$prevArrow.show();
            _.$nextArrow.show();

        }

        if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {

            _.$dots.show();

        }

    };

    Slick.prototype.keyHandler = function(event) {

        var _ = this;
         //Dont slide if the cursor is inside the form fields and arrow keys are pressed
        if(!event.target.tagName.match('TEXTAREA|INPUT|SELECT')) {
            if (event.keyCode === 37 && _.options.accessibility === true) {
                _.changeSlide({
                    data: {
                        message: _.options.rtl === true ? 'next' :  'previous'
                    }
                });
            } else if (event.keyCode === 39 && _.options.accessibility === true) {
                _.changeSlide({
                    data: {
                        message: _.options.rtl === true ? 'previous' : 'next'
                    }
                });
            }
        }

    };

    Slick.prototype.lazyLoad = function() {

        var _ = this,
            loadRange, cloneRange, rangeStart, rangeEnd;

        function loadImages(imagesScope) {

            $('img[data-lazy]', imagesScope).each(function() {

                var image = $(this),
                    imageSource = $(this).attr('data-lazy'),
                    imageSrcSet = $(this).attr('data-srcset'),
                    imageSizes  = $(this).attr('data-sizes') || _.$slider.attr('data-sizes'),
                    imageToLoad = document.createElement('img');

                imageToLoad.onload = function() {

                    image
                        .animate({ opacity: 0 }, 100, function() {

                            if (imageSrcSet) {
                                image
                                    .attr('srcset', imageSrcSet );

                                if (imageSizes) {
                                    image
                                        .attr('sizes', imageSizes );
                                }
                            }

                            image
                                .attr('src', imageSource)
                                .animate({ opacity: 1 }, 200, function() {
                                    image
                                        .removeAttr('data-lazy data-srcset data-sizes')
                                        .removeClass('slick-loading');
                                });
                            _.$slider.trigger('lazyLoaded', [_, image, imageSource]);
                        });

                };

                imageToLoad.onerror = function() {

                    image
                        .removeAttr( 'data-lazy' )
                        .removeClass( 'slick-loading' )
                        .addClass( 'slick-lazyload-error' );

                    _.$slider.trigger('lazyLoadError', [ _, image, imageSource ]);

                };

                imageToLoad.src = imageSource;

            });

        }

        if (_.options.centerMode === true) {
            if (_.options.infinite === true) {
                rangeStart = _.currentSlide + (_.options.slidesToShow / 2 + 1);
                rangeEnd = rangeStart + _.options.slidesToShow + 2;
            } else {
                rangeStart = Math.max(0, _.currentSlide - (_.options.slidesToShow / 2 + 1));
                rangeEnd = 2 + (_.options.slidesToShow / 2 + 1) + _.currentSlide;
            }
        } else {
            rangeStart = _.options.infinite ? _.options.slidesToShow + _.currentSlide : _.currentSlide;
            rangeEnd = Math.ceil(rangeStart + _.options.slidesToShow);
            if (_.options.fade === true) {
                if (rangeStart > 0) rangeStart--;
                if (rangeEnd <= _.slideCount) rangeEnd++;
            }
        }

        loadRange = _.$slider.find('.slick-slide').slice(rangeStart, rangeEnd);

        if (_.options.lazyLoad === 'anticipated') {
            var prevSlide = rangeStart - 1,
                nextSlide = rangeEnd,
                $slides = _.$slider.find('.slick-slide');

            for (var i = 0; i < _.options.slidesToScroll; i++) {
                if (prevSlide < 0) prevSlide = _.slideCount - 1;
                loadRange = loadRange.add($slides.eq(prevSlide));
                loadRange = loadRange.add($slides.eq(nextSlide));
                prevSlide--;
                nextSlide++;
            }
        }

        loadImages(loadRange);

        if (_.slideCount <= _.options.slidesToShow) {
            cloneRange = _.$slider.find('.slick-slide');
            loadImages(cloneRange);
        } else
        if (_.currentSlide >= _.slideCount - _.options.slidesToShow) {
            cloneRange = _.$slider.find('.slick-cloned').slice(0, _.options.slidesToShow);
            loadImages(cloneRange);
        } else if (_.currentSlide === 0) {
            cloneRange = _.$slider.find('.slick-cloned').slice(_.options.slidesToShow * -1);
            loadImages(cloneRange);
        }

    };

    Slick.prototype.loadSlider = function() {

        var _ = this;

        _.setPosition();

        _.$slideTrack.css({
            opacity: 1
        });

        _.$slider.removeClass('slick-loading');

        _.initUI();

        if (_.options.lazyLoad === 'progressive') {
            _.progressiveLazyLoad();
        }

    };

    Slick.prototype.next = Slick.prototype.slickNext = function() {

        var _ = this;

        _.changeSlide({
            data: {
                message: 'next'
            }
        });

    };

    Slick.prototype.orientationChange = function() {

        var _ = this;

        _.checkResponsive();
        _.setPosition();

    };

    Slick.prototype.pause = Slick.prototype.slickPause = function() {

        var _ = this;

        _.autoPlayClear();
        _.paused = true;

    };

    Slick.prototype.play = Slick.prototype.slickPlay = function() {

        var _ = this;

        _.autoPlay();
        _.options.autoplay = true;
        _.paused = false;
        _.focussed = false;
        _.interrupted = false;

    };

    Slick.prototype.postSlide = function(index) {

        var _ = this;

        if( !_.unslicked ) {

            _.$slider.trigger('afterChange', [_, index]);

            _.animating = false;

            if (_.slideCount > _.options.slidesToShow) {
                _.setPosition();
            }

            _.swipeLeft = null;

            if ( _.options.autoplay ) {
                _.autoPlay();
            }

            if (_.options.accessibility === true) {
                _.initADA();
                // for non-autoplay: once active slide (group) has updated, set focus on first newly showing slide 
                if (!_.options.autoplay) {
                    var $currentSlide = $(_.$slides.get(_.currentSlide));
                    $currentSlide.attr('tabindex', 0).focus();
                }
            }

        }

    };

    Slick.prototype.prev = Slick.prototype.slickPrev = function() {

        var _ = this;

        _.changeSlide({
            data: {
                message: 'previous'
            }
        });

    };

    Slick.prototype.preventDefault = function(event) {

        event.preventDefault();

    };

    Slick.prototype.progressiveLazyLoad = function( tryCount ) {

        tryCount = tryCount || 1;

        var _ = this,
            $imgsToLoad = $( 'img[data-lazy]', _.$slider ),
            image,
            imageSource,
            imageSrcSet,
            imageSizes,
            imageToLoad;

        if ( $imgsToLoad.length ) {

            image = $imgsToLoad.first();
            imageSource = image.attr('data-lazy');
            imageSrcSet = image.attr('data-srcset');
            imageSizes  = image.attr('data-sizes') || _.$slider.attr('data-sizes');
            imageToLoad = document.createElement('img');

            imageToLoad.onload = function() {

                if (imageSrcSet) {
                    image
                        .attr('srcset', imageSrcSet );

                    if (imageSizes) {
                        image
                            .attr('sizes', imageSizes );
                    }
                }

                image
                    .attr( 'src', imageSource )
                    .removeAttr('data-lazy data-srcset data-sizes')
                    .removeClass('slick-loading');

                if ( _.options.adaptiveHeight === true ) {
                    _.setPosition();
                }

                _.$slider.trigger('lazyLoaded', [ _, image, imageSource ]);
                _.progressiveLazyLoad();

            };

            imageToLoad.onerror = function() {

                if ( tryCount < 3 ) {

                    /**
                     * try to load the image 3 times,
                     * leave a slight delay so we don't get
                     * servers blocking the request.
                     */
                    setTimeout( function() {
                        _.progressiveLazyLoad( tryCount + 1 );
                    }, 500 );

                } else {

                    image
                        .removeAttr( 'data-lazy' )
                        .removeClass( 'slick-loading' )
                        .addClass( 'slick-lazyload-error' );

                    _.$slider.trigger('lazyLoadError', [ _, image, imageSource ]);

                    _.progressiveLazyLoad();

                }

            };

            imageToLoad.src = imageSource;

        } else {

            _.$slider.trigger('allImagesLoaded', [ _ ]);

        }

    };

    Slick.prototype.refresh = function( initializing ) {

        var _ = this, currentSlide, lastVisibleIndex;

        lastVisibleIndex = _.slideCount - _.options.slidesToShow;

        // in non-infinite sliders, we don't want to go past the
        // last visible index.
        if( !_.options.infinite && ( _.currentSlide > lastVisibleIndex )) {
            _.currentSlide = lastVisibleIndex;
        }

        // if less slides than to show, go to start.
        if ( _.slideCount <= _.options.slidesToShow ) {
            _.currentSlide = 0;

        }

        currentSlide = _.currentSlide;

        _.destroy(true);

        $.extend(_, _.initials, { currentSlide: currentSlide });

        _.init();

        if( !initializing ) {

            _.changeSlide({
                data: {
                    message: 'index',
                    index: currentSlide
                }
            }, false);

        }

    };

    Slick.prototype.registerBreakpoints = function() {

        var _ = this, breakpoint, currentBreakpoint, l,
            responsiveSettings = _.options.responsive || null;

        if ( $.type(responsiveSettings) === 'array' && responsiveSettings.length ) {

            _.respondTo = _.options.respondTo || 'window';

            for ( breakpoint in responsiveSettings ) {

                l = _.breakpoints.length-1;

                if (responsiveSettings.hasOwnProperty(breakpoint)) {
                    currentBreakpoint = responsiveSettings[breakpoint].breakpoint;

                    // loop through the breakpoints and cut out any existing
                    // ones with the same breakpoint number, we don't want dupes.
                    while( l >= 0 ) {
                        if( _.breakpoints[l] && _.breakpoints[l] === currentBreakpoint ) {
                            _.breakpoints.splice(l,1);
                        }
                        l--;
                    }

                    _.breakpoints.push(currentBreakpoint);
                    _.breakpointSettings[currentBreakpoint] = responsiveSettings[breakpoint].settings;

                }

            }

            _.breakpoints.sort(function(a, b) {
                return ( _.options.mobileFirst ) ? a-b : b-a;
            });

        }

    };

    Slick.prototype.reinit = function() {

        var _ = this;

        _.$slides =
            _.$slideTrack
                .children(_.options.slide)
                .addClass('slick-slide');

        _.slideCount = _.$slides.length;

        if (_.currentSlide >= _.slideCount && _.currentSlide !== 0) {
            _.currentSlide = _.currentSlide - _.options.slidesToScroll;
        }

        if (_.slideCount <= _.options.slidesToShow) {
            _.currentSlide = 0;
        }

        _.registerBreakpoints();

        _.setProps();
        _.setupInfinite();
        _.buildArrows();
        _.updateArrows();
        _.initArrowEvents();
        _.buildDots();
        _.updateDots();
        _.initDotEvents();
        _.cleanUpSlideEvents();
        _.initSlideEvents();

        _.checkResponsive(false, true);

        if (_.options.focusOnSelect === true) {
            $(_.$slideTrack).children().on('click.slick', _.selectHandler);
        }

        _.setSlideClasses(typeof _.currentSlide === 'number' ? _.currentSlide : 0);

        _.setPosition();
        _.focusHandler();

        _.paused = !_.options.autoplay;
        _.autoPlay();

        _.$slider.trigger('reInit', [_]);

    };

    Slick.prototype.resize = function() {

        var _ = this;

        if ($(window).width() !== _.windowWidth) {
            clearTimeout(_.windowDelay);
            _.windowDelay = window.setTimeout(function() {
                _.windowWidth = $(window).width();
                _.checkResponsive();
                if( !_.unslicked ) { _.setPosition(); }
            }, 50);
        }
    };

    Slick.prototype.removeSlide = Slick.prototype.slickRemove = function(index, removeBefore, removeAll) {

        var _ = this;

        if (typeof(index) === 'boolean') {
            removeBefore = index;
            index = removeBefore === true ? 0 : _.slideCount - 1;
        } else {
            index = removeBefore === true ? --index : index;
        }

        if (_.slideCount < 1 || index < 0 || index > _.slideCount - 1) {
            return false;
        }

        _.unload();

        if (removeAll === true) {
            _.$slideTrack.children().remove();
        } else {
            _.$slideTrack.children(this.options.slide).eq(index).remove();
        }

        _.$slides = _.$slideTrack.children(this.options.slide);

        _.$slideTrack.children(this.options.slide).detach();

        _.$slideTrack.append(_.$slides);

        _.$slidesCache = _.$slides;

        _.reinit();

    };

    Slick.prototype.setCSS = function(position) {

        var _ = this,
            positionProps = {},
            x, y;

        if (_.options.rtl === true) {
            position = -position;
        }
        x = _.positionProp == 'left' ? Math.ceil(position) + 'px' : '0px';
        y = _.positionProp == 'top' ? Math.ceil(position) + 'px' : '0px';

        positionProps[_.positionProp] = position;

        if (_.transformsEnabled === false) {
            _.$slideTrack.css(positionProps);
        } else {
            positionProps = {};
            if (_.cssTransitions === false) {
                positionProps[_.animType] = 'translate(' + x + ', ' + y + ')';
                _.$slideTrack.css(positionProps);
            } else {
                positionProps[_.animType] = 'translate3d(' + x + ', ' + y + ', 0px)';
                _.$slideTrack.css(positionProps);
            }
        }

    };

    Slick.prototype.setDimensions = function() {

        var _ = this;

        if (_.options.vertical === false) {
            if (_.options.centerMode === true) {
                _.$list.css({
                    padding: ('0px ' + _.options.centerPadding)
                });
            }
        } else {
            _.$list.height(_.$slides.first().outerHeight(true) * _.options.slidesToShow);
            if (_.options.centerMode === true) {
                _.$list.css({
                    padding: (_.options.centerPadding + ' 0px')
                });
            }
        }

        _.listWidth = _.$list.width();
        _.listHeight = _.$list.height();


        if (_.options.vertical === false && _.options.variableWidth === false) {
            _.slideWidth = Math.ceil(_.listWidth / _.options.slidesToShow);
            _.$slideTrack.width(Math.ceil((_.slideWidth * _.$slideTrack.children('.slick-slide').length)));

        } else if (_.options.variableWidth === true) {
            _.$slideTrack.width(5000 * _.slideCount);
        } else {
            _.slideWidth = Math.ceil(_.listWidth);
            _.$slideTrack.height(Math.ceil((_.$slides.first().outerHeight(true) * _.$slideTrack.children('.slick-slide').length)));
        }

        var offset = _.$slides.first().outerWidth(true) - _.$slides.first().width();
        if (_.options.variableWidth === false) _.$slideTrack.children('.slick-slide').width(_.slideWidth - offset);

    };

    Slick.prototype.setFade = function() {

        var _ = this,
            targetLeft;

        _.$slides.each(function(index, element) {
            targetLeft = (_.slideWidth * index) * -1;
            if (_.options.rtl === true) {
                $(element).css({
                    position: 'relative',
                    right: targetLeft,
                    top: 0,
                    zIndex: _.options.zIndex - 2,
                    opacity: 0
                });
            } else {
                $(element).css({
                    position: 'relative',
                    left: targetLeft,
                    top: 0,
                    zIndex: _.options.zIndex - 2,
                    opacity: 0
                });
            }
        });

        _.$slides.eq(_.currentSlide).css({
            zIndex: _.options.zIndex - 1,
            opacity: 1
        });

    };

    Slick.prototype.setHeight = function() {

        var _ = this;

        if (_.options.slidesToShow === 1 && _.options.adaptiveHeight === true && _.options.vertical === false) {
            var targetHeight = _.$slides.eq(_.currentSlide).outerHeight(true);
            _.$list.css('height', targetHeight);
        }

    };

    Slick.prototype.setOption =
    Slick.prototype.slickSetOption = function() {

        /**
         * accepts arguments in format of:
         *
         *  - for changing a single option's value:
         *     .slick("setOption", option, value, refresh )
         *
         *  - for changing a set of responsive options:
         *     .slick("setOption", 'responsive', [{}, ...], refresh )
         *
         *  - for updating multiple values at once (not responsive)
         *     .slick("setOption", { 'option': value, ... }, refresh )
         */

        var _ = this, l, item, option, value, refresh = false, type;

        if( $.type( arguments[0] ) === 'object' ) {

            option =  arguments[0];
            refresh = arguments[1];
            type = 'multiple';

        } else if ( $.type( arguments[0] ) === 'string' ) {

            option =  arguments[0];
            value = arguments[1];
            refresh = arguments[2];

            if ( arguments[0] === 'responsive' && $.type( arguments[1] ) === 'array' ) {

                type = 'responsive';

            } else if ( typeof arguments[1] !== 'undefined' ) {

                type = 'single';

            }

        }

        if ( type === 'single' ) {

            _.options[option] = value;


        } else if ( type === 'multiple' ) {

            $.each( option , function( opt, val ) {

                _.options[opt] = val;

            });


        } else if ( type === 'responsive' ) {

            for ( item in value ) {

                if( $.type( _.options.responsive ) !== 'array' ) {

                    _.options.responsive = [ value[item] ];

                } else {

                    l = _.options.responsive.length-1;

                    // loop through the responsive object and splice out duplicates.
                    while( l >= 0 ) {

                        if( _.options.responsive[l].breakpoint === value[item].breakpoint ) {

                            _.options.responsive.splice(l,1);

                        }

                        l--;

                    }

                    _.options.responsive.push( value[item] );

                }

            }

        }

        if ( refresh ) {

            _.unload();
            _.reinit();

        }

    };

    Slick.prototype.setPosition = function() {

        var _ = this;

        _.setDimensions();

        _.setHeight();

        if (_.options.fade === false) {
            _.setCSS(_.getLeft(_.currentSlide));
        } else {
            _.setFade();
        }

        _.$slider.trigger('setPosition', [_]);

    };

    Slick.prototype.setProps = function() {

        var _ = this,
            bodyStyle = document.body.style;

        _.positionProp = _.options.vertical === true ? 'top' : 'left';

        if (_.positionProp === 'top') {
            _.$slider.addClass('slick-vertical');
        } else {
            _.$slider.removeClass('slick-vertical');
        }

        if (bodyStyle.WebkitTransition !== undefined ||
            bodyStyle.MozTransition !== undefined ||
            bodyStyle.msTransition !== undefined) {
            if (_.options.useCSS === true) {
                _.cssTransitions = true;
            }
        }

        if ( _.options.fade ) {
            if ( typeof _.options.zIndex === 'number' ) {
                if( _.options.zIndex < 3 ) {
                    _.options.zIndex = 3;
                }
            } else {
                _.options.zIndex = _.defaults.zIndex;
            }
        }

        if (bodyStyle.OTransform !== undefined) {
            _.animType = 'OTransform';
            _.transformType = '-o-transform';
            _.transitionType = 'OTransition';
            if (bodyStyle.perspectiveProperty === undefined && bodyStyle.webkitPerspective === undefined) _.animType = false;
        }
        if (bodyStyle.MozTransform !== undefined) {
            _.animType = 'MozTransform';
            _.transformType = '-moz-transform';
            _.transitionType = 'MozTransition';
            if (bodyStyle.perspectiveProperty === undefined && bodyStyle.MozPerspective === undefined) _.animType = false;
        }
        if (bodyStyle.webkitTransform !== undefined) {
            _.animType = 'webkitTransform';
            _.transformType = '-webkit-transform';
            _.transitionType = 'webkitTransition';
            if (bodyStyle.perspectiveProperty === undefined && bodyStyle.webkitPerspective === undefined) _.animType = false;
        }
        if (bodyStyle.msTransform !== undefined) {
            _.animType = 'msTransform';
            _.transformType = '-ms-transform';
            _.transitionType = 'msTransition';
            if (bodyStyle.msTransform === undefined) _.animType = false;
        }
        if (bodyStyle.transform !== undefined && _.animType !== false) {
            _.animType = 'transform';
            _.transformType = 'transform';
            _.transitionType = 'transition';
        }
        _.transformsEnabled = _.options.useTransform && (_.animType !== null && _.animType !== false);
    };


    Slick.prototype.setSlideClasses = function(index) {

        var _ = this,
            centerOffset, allSlides, indexOffset, remainder;

        allSlides = _.$slider
            .find('.slick-slide')
            .removeClass('slick-active slick-center slick-current')
            .attr('aria-hidden', 'true');

        _.$slides
            .eq(index)
            .addClass('slick-current');

        if (_.options.centerMode === true) {

            centerOffset = Math.floor(_.options.slidesToShow / 2);

            if (_.options.infinite === true) {

                if (index >= centerOffset && index <= (_.slideCount - 1) - centerOffset) {

                    _.$slides
                        .slice(index - centerOffset, index + centerOffset + 1)
                        .addClass('slick-active')
                        .attr('aria-hidden', 'false');

                } else {

                    indexOffset = _.options.slidesToShow + index;
                    allSlides
                        .slice(indexOffset - centerOffset + 1, indexOffset + centerOffset + 2)
                        .addClass('slick-active')
                        .attr('aria-hidden', 'false');

                }

                if (index === 0) {

                    allSlides
                        .eq(allSlides.length - 1 - _.options.slidesToShow)
                        .addClass('slick-center');

                } else if (index === _.slideCount - 1) {

                    allSlides
                        .eq(_.options.slidesToShow)
                        .addClass('slick-center');

                }

            }

            _.$slides
                .eq(index)
                .addClass('slick-center');

        } else {

            if (index >= 0 && index <= (_.slideCount - _.options.slidesToShow)) {

                _.$slides
                    .slice(index, index + _.options.slidesToShow)
                    .addClass('slick-active')
                    .attr('aria-hidden', 'false');

            } else if (allSlides.length <= _.options.slidesToShow) {

                allSlides
                    .addClass('slick-active')
                    .attr('aria-hidden', 'false');

            } else {

                remainder = _.slideCount % _.options.slidesToShow;
                indexOffset = _.options.infinite === true ? _.options.slidesToShow + index : index;

                if (_.options.slidesToShow == _.options.slidesToScroll && (_.slideCount - index) < _.options.slidesToShow) {

                    allSlides
                        .slice(indexOffset - (_.options.slidesToShow - remainder), indexOffset + remainder)
                        .addClass('slick-active')
                        .attr('aria-hidden', 'false');

                } else {

                    allSlides
                        .slice(indexOffset, indexOffset + _.options.slidesToShow)
                        .addClass('slick-active')
                        .attr('aria-hidden', 'false');

                }

            }

        }

        if (_.options.lazyLoad === 'ondemand' || _.options.lazyLoad === 'anticipated') {
            _.lazyLoad();
        }
    };

    Slick.prototype.setupInfinite = function() {

        var _ = this,
            i, slideIndex, infiniteCount;

        if (_.options.fade === true) {
            _.options.centerMode = false;
        }

        if (_.options.infinite === true && _.options.fade === false) {

            slideIndex = null;

            if (_.slideCount > _.options.slidesToShow) {

                if (_.options.centerMode === true) {
                    infiniteCount = _.options.slidesToShow + 1;
                } else {
                    infiniteCount = _.options.slidesToShow;
                }

                for (i = _.slideCount; i > (_.slideCount -
                        infiniteCount); i -= 1) {
                    slideIndex = i - 1;
                    $(_.$slides[slideIndex]).clone(true).attr('id', '')
                        .attr('data-slick-index', slideIndex - _.slideCount)
                        .prependTo(_.$slideTrack).addClass('slick-cloned');
                }
                for (i = 0; i < infiniteCount  + _.slideCount; i += 1) {
                    slideIndex = i;
                    $(_.$slides[slideIndex]).clone(true).attr('id', '')
                        .attr('data-slick-index', slideIndex + _.slideCount)
                        .appendTo(_.$slideTrack).addClass('slick-cloned');
                }
                _.$slideTrack.find('.slick-cloned').find('[id]').each(function() {
                    $(this).attr('id', '');
                });

            }

        }

    };

    Slick.prototype.interrupt = function( toggle ) {

        var _ = this;

        if( !toggle ) {
            _.autoPlay();
        }
        _.interrupted = toggle;

    };

    Slick.prototype.selectHandler = function(event) {

        var _ = this;

        var targetElement =
            $(event.target).is('.slick-slide') ?
                $(event.target) :
                $(event.target).parents('.slick-slide');

        var index = parseInt(targetElement.attr('data-slick-index'));

        if (!index) index = 0;

        if (_.slideCount <= _.options.slidesToShow) {

            _.slideHandler(index, false, true);
            return;

        }

        _.slideHandler(index);

    };

    Slick.prototype.slideHandler = function(index, sync, dontAnimate) {

        var targetSlide, animSlide, oldSlide, slideLeft, targetLeft = null,
            _ = this, navTarget;

        sync = sync || false;

        if (_.animating === true && _.options.waitForAnimate === true) {
            return;
        }

        if (_.options.fade === true && _.currentSlide === index) {
            return;
        }

        if (sync === false) {
            _.asNavFor(index);
        }

        targetSlide = index;
        targetLeft = _.getLeft(targetSlide);
        slideLeft = _.getLeft(_.currentSlide);

        _.currentLeft = _.swipeLeft === null ? slideLeft : _.swipeLeft;

        if (_.options.infinite === false && _.options.centerMode === false && (index < 0 || index > _.getDotCount() * _.options.slidesToScroll)) {
            if (_.options.fade === false) {
                targetSlide = _.currentSlide;
                if (dontAnimate !== true) {
                    _.animateSlide(slideLeft, function() {
                        _.postSlide(targetSlide);
                    });
                } else {
                    _.postSlide(targetSlide);
                }
            }
            return;
        } else if (_.options.infinite === false && _.options.centerMode === true && (index < 0 || index > (_.slideCount - _.options.slidesToScroll))) {
            if (_.options.fade === false) {
                targetSlide = _.currentSlide;
                if (dontAnimate !== true) {
                    _.animateSlide(slideLeft, function() {
                        _.postSlide(targetSlide);
                    });
                } else {
                    _.postSlide(targetSlide);
                }
            }
            return;
        }

        if ( _.options.autoplay ) {
            clearInterval(_.autoPlayTimer);
        }

        if (targetSlide < 0) {
            if (_.slideCount % _.options.slidesToScroll !== 0) {
                animSlide = _.slideCount - (_.slideCount % _.options.slidesToScroll);
            } else {
                animSlide = _.slideCount + targetSlide;
            }
        } else if (targetSlide >= _.slideCount) {
            if (_.slideCount % _.options.slidesToScroll !== 0) {
                animSlide = 0;
            } else {
                animSlide = targetSlide - _.slideCount;
            }
        } else {
            animSlide = targetSlide;
        }

        _.animating = true;

        _.$slider.trigger('beforeChange', [_, _.currentSlide, animSlide]);

        oldSlide = _.currentSlide;
        _.currentSlide = animSlide;

        _.setSlideClasses(_.currentSlide);

        if ( _.options.asNavFor ) {

            navTarget = _.getNavTarget();
            navTarget = navTarget.slick('getSlick');

            if ( navTarget.slideCount <= navTarget.options.slidesToShow ) {
                navTarget.setSlideClasses(_.currentSlide);
            }

        }

        _.updateDots();
        _.updateArrows();

        if (_.options.fade === true) {
            if (dontAnimate !== true) {

                _.fadeSlideOut(oldSlide);

                _.fadeSlide(animSlide, function() {
                    _.postSlide(animSlide);
                });

            } else {
                _.postSlide(animSlide);
            }
            _.animateHeight();
            return;
        }

        if (dontAnimate !== true) {
            _.animateSlide(targetLeft, function() {
                _.postSlide(animSlide);
            });
        } else {
            _.postSlide(animSlide);
        }

    };

    Slick.prototype.startLoad = function() {

        var _ = this;

        if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {

            _.$prevArrow.hide();
            _.$nextArrow.hide();

        }

        if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {

            _.$dots.hide();

        }

        _.$slider.addClass('slick-loading');

    };

    Slick.prototype.swipeDirection = function() {

        var xDist, yDist, r, swipeAngle, _ = this;

        xDist = _.touchObject.startX - _.touchObject.curX;
        yDist = _.touchObject.startY - _.touchObject.curY;
        r = Math.atan2(yDist, xDist);

        swipeAngle = Math.round(r * 180 / Math.PI);
        if (swipeAngle < 0) {
            swipeAngle = 360 - Math.abs(swipeAngle);
        }

        if ((swipeAngle <= 45) && (swipeAngle >= 0)) {
            return (_.options.rtl === false ? 'left' : 'right');
        }
        if ((swipeAngle <= 360) && (swipeAngle >= 315)) {
            return (_.options.rtl === false ? 'left' : 'right');
        }
        if ((swipeAngle >= 135) && (swipeAngle <= 225)) {
            return (_.options.rtl === false ? 'right' : 'left');
        }
        if (_.options.verticalSwiping === true) {
            if ((swipeAngle >= 35) && (swipeAngle <= 135)) {
                return 'down';
            } else {
                return 'up';
            }
        }

        return 'vertical';

    };

    Slick.prototype.swipeEnd = function(event) {

        var _ = this,
            slideCount,
            direction;

        _.dragging = false;
        _.swiping = false;

        if (_.scrolling) {
            _.scrolling = false;
            return false;
        }

        _.interrupted = false;
        _.shouldClick = ( _.touchObject.swipeLength > 10 ) ? false : true;

        if ( _.touchObject.curX === undefined ) {
            return false;
        }

        if ( _.touchObject.edgeHit === true ) {
            _.$slider.trigger('edge', [_, _.swipeDirection() ]);
        }

        if ( _.touchObject.swipeLength >= _.touchObject.minSwipe ) {

            direction = _.swipeDirection();

            switch ( direction ) {

                case 'left':
                case 'down':

                    slideCount =
                        _.options.swipeToSlide ?
                            _.checkNavigable( _.currentSlide + _.getSlideCount() ) :
                            _.currentSlide + _.getSlideCount();

                    _.currentDirection = 0;

                    break;

                case 'right':
                case 'up':

                    slideCount =
                        _.options.swipeToSlide ?
                            _.checkNavigable( _.currentSlide - _.getSlideCount() ) :
                            _.currentSlide - _.getSlideCount();

                    _.currentDirection = 1;

                    break;

                default:


            }

            if( direction != 'vertical' ) {

                _.slideHandler( slideCount );
                _.touchObject = {};
                _.$slider.trigger('swipe', [_, direction ]);

            }

        } else {

            if ( _.touchObject.startX !== _.touchObject.curX ) {

                _.slideHandler( _.currentSlide );
                _.touchObject = {};

            }

        }

    };

    Slick.prototype.swipeHandler = function(event) {

        var _ = this;

        if ((_.options.swipe === false) || ('ontouchend' in document && _.options.swipe === false)) {
            return;
        } else if (_.options.draggable === false && event.type.indexOf('mouse') !== -1) {
            return;
        }

        _.touchObject.fingerCount = event.originalEvent && event.originalEvent.touches !== undefined ?
            event.originalEvent.touches.length : 1;

        _.touchObject.minSwipe = _.listWidth / _.options
            .touchThreshold;

        if (_.options.verticalSwiping === true) {
            _.touchObject.minSwipe = _.listHeight / _.options
                .touchThreshold;
        }

        switch (event.data.action) {

            case 'start':
                _.swipeStart(event);
                break;

            case 'move':
                _.swipeMove(event);
                break;

            case 'end':
                _.swipeEnd(event);
                break;

        }

    };

    Slick.prototype.swipeMove = function(event) {

        var _ = this,
            edgeWasHit = false,
            curLeft, swipeDirection, swipeLength, positionOffset, touches, verticalSwipeLength;

        touches = event.originalEvent !== undefined ? event.originalEvent.touches : null;

        if (!_.dragging || _.scrolling || touches && touches.length !== 1) {
            return false;
        }

        curLeft = _.getLeft(_.currentSlide);

        _.touchObject.curX = touches !== undefined ? touches[0].pageX : event.clientX;
        _.touchObject.curY = touches !== undefined ? touches[0].pageY : event.clientY;

        _.touchObject.swipeLength = Math.round(Math.sqrt(
            Math.pow(_.touchObject.curX - _.touchObject.startX, 2)));

        verticalSwipeLength = Math.round(Math.sqrt(
            Math.pow(_.touchObject.curY - _.touchObject.startY, 2)));

        if (!_.options.verticalSwiping && !_.swiping && verticalSwipeLength > 4) {
            _.scrolling = true;
            return false;
        }

        if (_.options.verticalSwiping === true) {
            _.touchObject.swipeLength = verticalSwipeLength;
        }

        swipeDirection = _.swipeDirection();

        if (event.originalEvent !== undefined && _.touchObject.swipeLength > 4) {
            _.swiping = true;
            event.preventDefault();
        }

        positionOffset = (_.options.rtl === false ? 1 : -1) * (_.touchObject.curX > _.touchObject.startX ? 1 : -1);
        if (_.options.verticalSwiping === true) {
            positionOffset = _.touchObject.curY > _.touchObject.startY ? 1 : -1;
        }


        swipeLength = _.touchObject.swipeLength;

        _.touchObject.edgeHit = false;

        if (_.options.infinite === false) {
            if ((_.currentSlide === 0 && swipeDirection === 'right') || (_.currentSlide >= _.getDotCount() && swipeDirection === 'left')) {
                swipeLength = _.touchObject.swipeLength * _.options.edgeFriction;
                _.touchObject.edgeHit = true;
            }
        }

        if (_.options.vertical === false) {
            _.swipeLeft = curLeft + swipeLength * positionOffset;
        } else {
            _.swipeLeft = curLeft + (swipeLength * (_.$list.height() / _.listWidth)) * positionOffset;
        }
        if (_.options.verticalSwiping === true) {
            _.swipeLeft = curLeft + swipeLength * positionOffset;
        }

        if (_.options.fade === true || _.options.touchMove === false) {
            return false;
        }

        if (_.animating === true) {
            _.swipeLeft = null;
            return false;
        }

        _.setCSS(_.swipeLeft);

    };

    Slick.prototype.swipeStart = function(event) {

        var _ = this,
            touches;

        _.interrupted = true;

        if (_.touchObject.fingerCount !== 1 || _.slideCount <= _.options.slidesToShow) {
            _.touchObject = {};
            return false;
        }

        if (event.originalEvent !== undefined && event.originalEvent.touches !== undefined) {
            touches = event.originalEvent.touches[0];
        }

        _.touchObject.startX = _.touchObject.curX = touches !== undefined ? touches.pageX : event.clientX;
        _.touchObject.startY = _.touchObject.curY = touches !== undefined ? touches.pageY : event.clientY;

        _.dragging = true;

    };

    Slick.prototype.unfilterSlides = Slick.prototype.slickUnfilter = function() {

        var _ = this;

        if (_.$slidesCache !== null) {

            _.unload();

            _.$slideTrack.children(this.options.slide).detach();

            _.$slidesCache.appendTo(_.$slideTrack);

            _.reinit();

        }

    };

    Slick.prototype.unload = function() {

        var _ = this;

        $('.slick-cloned', _.$slider).remove();

        if (_.$dots) {
            _.$dots.remove();
        }

        if (_.$prevArrow && _.htmlExpr.test(_.options.prevArrow)) {
            _.$prevArrow.remove();
        }

        if (_.$nextArrow && _.htmlExpr.test(_.options.nextArrow)) {
            _.$nextArrow.remove();
        }

        _.$slides
            .removeClass('slick-slide slick-active slick-visible slick-current')
            .attr('aria-hidden', 'true')
            .css('width', '');

    };

    Slick.prototype.unslick = function(fromBreakpoint) {

        var _ = this;
        _.$slider.trigger('unslick', [_, fromBreakpoint]);
        _.destroy();

    };

    Slick.prototype.updateArrows = function() {

        var _ = this,
            centerOffset;

        centerOffset = Math.floor(_.options.slidesToShow / 2);

        if ( _.options.arrows === true &&
            _.slideCount > _.options.slidesToShow &&
            !_.options.infinite ) {

            _.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');
            _.$nextArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');

            if (_.currentSlide === 0) {

                _.$prevArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
                _.$nextArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');

            } else if (_.currentSlide >= _.slideCount - _.options.slidesToShow && _.options.centerMode === false) {

                _.$nextArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
                _.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');

            } else if (_.currentSlide >= _.slideCount - 1 && _.options.centerMode === true) {

                _.$nextArrow.addClass('slick-disabled').attr('aria-disabled', 'true');
                _.$prevArrow.removeClass('slick-disabled').attr('aria-disabled', 'false');

            }

        }

    };

    Slick.prototype.updateDots = function() {

        var _ = this;

        if (_.$dots !== null) {

            _.$dots
                .find('li')
                    .removeClass('slick-active')
                    .end();

            _.$dots
                .find('li')
                .eq(Math.floor(_.currentSlide / _.options.slidesToScroll))
                .addClass('slick-active');

        }

    };

    Slick.prototype.visibility = function() {

        var _ = this;

        if ( _.options.autoplay ) {

            if ( document[_.hidden] ) {

                _.interrupted = true;

            } else {

                _.interrupted = false;

            }

        }

    };

    $.fn.slick = function() {
        var _ = this,
            opt = arguments[0],
            args = Array.prototype.slice.call(arguments, 1),
            l = _.length,
            i,
            ret;
        for (i = 0; i < l; i++) {
            if (typeof opt == 'object' || typeof opt == 'undefined')
                _[i].slick = new Slick(_[i], opt);
            else
                ret = _[i].slick[opt].apply(_[i].slick, args);
            if (typeof ret != 'undefined') return ret;
        }
        return _;
    };

}));

// jshint maxstatements: false, newcap: false
define('carouselHome',['jquery', 'splash', 'imageLoadListener', 'gotoSmooth', 'slick'],
  function($, splash, imageLoadListener, gotoSmooth) {
    "use strict";

    var CarouselHome = function(settings) {

      var carouselPauseSelector = settings.carouselPauseSelector,
          carouselWrapper = settings.carouselWrapper,
          carouselAutoPlaySpeed = settings.carouselAutoPlaySpeed,
          carouselContentDelay = settings.carouselContentDelay,
          slideContentTimer = null,
          theCarousel = {};


      this.init = function() {

        // Fires once Slick carousel has initialised
        $(carouselWrapper).on('init', function(event, slick){

          // Add the pause/play button
          if(!$('button.nt-home-carousel-pause').length){
            $( ".slick-dots" ).append( $( "<button type='button' role='button' class='nt-home-carousel-pause'></button>" ) );
          }

          // First slide's content to fade in after carouselContentDelay (2 seconds)
          setTimeout(function() {
               $('.slick-slide.slick-active').addClass('nt-home-carousel-content-active');
          }, carouselContentDelay);

          // pan class applies the CSS transform
          $('.slick-slide.slick-active').addClass('pan');

        });

        theCarousel = $(carouselWrapper).slick({
          dots: true,
          infinite: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          fade: true,
          autoplay: true,
          autoplaySpeed: carouselAutoPlaySpeed,
          cssEase: 'linear',
          pauseOnHover: false,
          pauseOnFocus: false
        });

      };

      // Pause/play functionality
      $(carouselWrapper).on('click', carouselPauseSelector, function(){

        if($(this).hasClass("nt-home-carousel-play")){

          // Switch the pause/play icon
          $(this).removeClass("nt-home-carousel-play");

          // Reset the overlay and content opacity
          $(".slick-slide.slick-active .nt-content-header-home-content").css('opacity', '');
          $(".slick-slide.slick-active .nt-content-header-home-media-overlay").css('opacity', '');

          // *playing* class overwrites any previously applied transform inline styles
          $(".slick-slide.slick-active .nt-background-image").addClass('playing');

          theCarousel.slick('slickPlay');

        } else {

          // Switch the pause/play icon
          $(this).addClass("nt-home-carousel-play");

          // Make sure the overriding *playing* class isn't there
          $(".slick-slide.slick-active .nt-background-image").removeClass('playing');

          // Stop the overlay and content from changing opacity
          var slideContentOpacity = $(".slick-slide.slick-active .nt-content-header-home-content").css('opacity');
          var slideContentOverlayOpacity = $(".slick-slide.slick-active .nt-content-header-home-media-overlay").css('opacity');
          $(".slick-slide.slick-active .nt-content-header-home-content").css('opacity', slideContentOpacity);
          $(".slick-slide.slick-active .nt-content-header-home-media-overlay").css('opacity', slideContentOverlayOpacity);

          if(Modernizr.cssanimations){
            // Get the current sate of the panning transform
            var obj = $('.slick-slide.slick-active .nt-background-image');
            var transformMatrix = obj.css("-webkit-transform") ||
             obj.css("-moz-transform")    ||
             obj.css("-ms-transform")     ||
             obj.css("-o-transform")      ||
             obj.css("transform");
            var matrix = transformMatrix.replace(/[^0-9\-.,]/g, '').split(',');
            var translate3dX = matrix[12] || matrix[4];//translate x

            // Apply the translate3dX as an inline style
            $('.slick-active .nt-background-image').css({
              '-webkit-transform' : 'translate3d(' + translate3dX + 'px, 0, 0)',
              '-moz-transform'    : 'translate3d(' + translate3dX + 'px, 0, 0)',
              '-ms-transform'     : 'translate3d(' + translate3dX + 'px, 0, 0)',
              '-o-transform'      : 'translate3d(' + translate3dX + 'px, 0, 0)',
              'transform'         : 'translate3d(' + translate3dX + 'px, 0, 0)'
            });
          }

          theCarousel.slick('slickPause');

        }

      });

      $(carouselWrapper).on('afterChange', function(event, slick, currentSlide){

        // Start Slick playing
        theCarousel.slick('slickPlay');

        // Wait for the slide fade tranistion to finish before removing the pan
        setTimeout(function() {
          $('.slick-slide').not( ".slick-slide.slick-active" ).removeClass('pan');
          // Force the transition to stop, otherwise it might still be going if user returns to the same slide quickly
          $('.slick-slide').not( ".slick-slide.slick-active" ).addClass('notransition');
        }, 300);

        // Remove the play icon
        $('.nt-home-carousel-pause').removeClass( "nt-home-carousel-play" );

        // Fade the the slide's content in afer carouselContentDelay
        function homeSlideContentFade() {
          $(slick.$slides.get(currentSlide)).addClass('nt-home-carousel-content-active');
        }

        clearTimeout(slideContentTimer); //cancel the previous timer.
        slideContentTimer = null;
        slideContentTimer = setTimeout(homeSlideContentFade, carouselContentDelay);

      });


      $(carouselWrapper).on('beforeChange', function(event, slick, currentSlide, nextSlide){

        $('.slick-slide').removeClass('nt-home-carousel-content-active');

        $(slick.$slides.get(nextSlide)).removeClass('notransition');
        // For a smoother transition start the pan transition of the next slide
        $(slick.$slides.get(nextSlide)).addClass('pan');

        // Reset paused states. Otherwise when you go back to that slide they're still set.
        $('.slick-slide.slick-active .nt-background-image').css({
          '-webkit-transform' : '',
          '-moz-transform'    : '',
          '-ms-transform'     : '',
          '-o-transform'      : '',
          'transform'         : ''
        });
          // Reset the overlay and content opacity
        $(".slick-slide .nt-content-header-home-content").css('opacity', '');
        $(".slick-slide .nt-content-header-home-media-overlay").css('opacity', '');

      });


    };




    var cookieName = 'cookieSplashShownOnce',
      cookieRegEx = new RegExp(cookieName + '=([^;]+)');

    var setCookieSplashShownOnce = function(value) {
      // IE will only set a session cookie if you remove the expires completely
      document.cookie = cookieName + '=' + value + '; path=/';
    };

    var getCookieSplashShownOnce = function() {
      var value = cookieRegEx.exec(document.cookie);
      return value;
    };
    //IF images are loaded then play intro sequence
    //ELSE show splash screen for 3s, then start into sequence regardless
    var splashInit = function(startCarousel) {
      var maxDuration = 3500,
        imageLoadCheckDuration = 1000,
        imagesLoaded = false;

      if (getCookieSplashShownOnce()) {
        maxDuration = 500;
      } else {
        setCookieSplashShownOnce(true);
      }
      splash.play(function() {
        startCarousel.init();
      }, maxDuration);

      imageLoadListener.imageLoadListener($('.nt-content-header-home'), null, function() {
        splash.skip();
      });
    };


    var bindEvents = function() {

      var startCarousel = new CarouselHome({
        carouselPauseSelector: '.nt-home-carousel-pause',
        carouselWrapper: '.nt-content-header-home-group',
        carouselAutoPlaySpeed: 5000,
        carouselContentDelay: 2000
      });

      splashInit(startCarousel);

    };


    return {
      init: function($isModuleOnPage) {
        if ($isModuleOnPage.length !== 0) {
          bindEvents();
        }
      }
    };
  });

define('carouselHolidayProperty',['jquery', 'splash', 'slick'] , function($, splash) {
    "use strict";

    var bindEvents = function () {
        var startCarousel = new PropertyCarousel({
            carouselWrapper: '.nt-hol-property-images__images',
            carouselNavigation: '.nt-hol-property-images__thumbs',
            carouselImage: '.nt-hol-property-image--image',
            carouselNext: '.nt-hol-property-images__next',
            carouselPrev: '.nt-hol-property-images__prev',
            carouselThumb: '.nt-hol-property-images__thumb',
            carouselThumbPrev: '.nt-hol-property-thumbs__prev',
            carouselThumbNext: '.nt-hol-property-thumbs__next',
            carouselParent: '.nt-hol-property-images'
        });

        if ($('.nt-hol-property-image').length > 1) {
            splashInit(startCarousel);
        }
    }

    var splashInit = function(startCarousel) {
       startCarousel.init();
    }

    // duplicates the images in a given carousel
    var duplicateCarouselImages = function($carousel){
        $carousel.children().each(function(){
            $carousel.append($(this).clone());
        });
    }

    var addSlickIndex = function($source, $target) {
        var images = $source.find('figure').not('.slick-cloned');
        var thumbs = $target.find('img');

        for (var i = 0; i < images.length; i++) {
            var index = $(images[i]).attr('data-slick-index');
            $(thumbs[i]).attr('data-slick-index', index);
        }

        $(thumbs[0]).addClass('nt-hol-property-images__thumb--active');
    }

    var PropertyCarousel = function(settings) {
        var carouselWrapper = settings.carouselWrapper,
            carouselNext = settings.carouselNext,
            carouselPrev = settings.carouselPrev,
            carouselThumb = settings.carouselThumb,
            carouselThumbPrev = settings.carouselThumbPrev,
            carouselThumbNext = settings.carouselThumbNext,
            carouselNavigation = settings.carouselNavigation,
            carouselParent = settings.carouselParent,
            carouselImage = settings.carouselImage;

        // After Main Carousel loads
        $(carouselWrapper).on('init',function(){
            $(carouselParent).removeClass('nt-hol-property-images--no-slider');
        });

        // initialize Main Carousel
        this.init = function() {

            var $carouselThumb = $(carouselThumb),
                $carouselWrapper = $(carouselWrapper),
                $carouselNavigation = $(carouselNavigation),
                noOfSlides = $carouselThumb.length;

            $(carouselWrapper).slick({
                centerMode: true,
                centerPadding: '100px',
                slidesToShow: 1,
                infinite: true,
                slidesToScroll: 1,
                variableWidth : false,
                responsive: [
                    {
                        breakpoint: 600,
                        settings: {
                            arrows: true,
                            centerMode: false,
                            centerPadding: '0',
                            slidesToShow: 1,
                            variableWidth : false
                        }
                    },
                    {
                        breakpoint: 900,
                        settings: {
                            centerMode: false,
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            variableWidth : false
                        }
                    }
                ],
                arrows : true,
                prevArrow : $(carouselPrev),
                nextArrow : $(carouselNext),
                asNavFor: noOfSlides < 7 ? false : '.nt-hol-property-images__thumbs'
            });

            // Check noOfSlides, if less than 7 do not init thumb carousel, add custom functionality
            if (noOfSlides < 7) {
                addSlickIndex($carouselWrapper, $carouselNavigation);
                
                $(carouselThumb).on('click', function() {
                    $('.nt-hol-property-images__thumb--active').removeClass('nt-hol-property-images__thumb--active');
                    $(this).addClass('nt-hol-property-images__thumb--active');
                    $(carouselWrapper).slick('slickGoTo', $(this).attr('data-slick-index'));
                });

                $(carouselWrapper).on('afterChange', function(event, slick, currentSlide, nextSlide){
                    var tar = '[data-slick-index="' + currentSlide + '"]';
                    var cssClass = 'nt-hol-property-images__thumb--active'
                    $('.' + cssClass).removeClass(cssClass);
                    $carouselNavigation.find(tar).addClass(cssClass);
                });
            } else {
                // Since setting 'slidesToShow' must be less than number of slides, use
                // the following variable as max for the value passed into it.
                var maxSlidesToShow = noOfSlides - 1;

                $(carouselNavigation).slick({
                    slidesToShow: Math.min(7, maxSlidesToShow),
                    slidesToScroll: 1,
                    variableWidth : false,
                    infinite: true,
                    centerMode: true,
                    responsive: [
                        {
                            breakpoint: 600,
                            settings: {
                                arrows: true,
                                centerMode: false,
                                centerPadding: '0',
                                slidesToShow: 1
                            }
                        },
                        {
                            breakpoint: 900,
                            settings: {
                                arrows: true,
                                centerMode: true,
                                centerPadding: '0',
                                slidesToShow: Math.min(4, maxSlidesToShow)
                            }
                        }
                    ],
                    arrows: true,
                    dots: false,
                    prevArrow : $(carouselThumbPrev),
                    nextArrow : $(carouselThumbNext),
                    asNavFor: '.nt-hol-property-images__images'
                });

                $(carouselThumb).on('click', function() {
                    $(carouselNavigation).slick('slickGoTo', $(this).attr('data-slick-index'));
                });
            }

            var delay = (function(){
                var timer = 0;
                return function(callback, ms){
                    clearTimeout (timer);
                    timer = setTimeout(callback, ms);
                };
            })();

            var videos = $(carouselWrapper).find('iframe');

            if (videos) {

                videos.height($(carouselWrapper).find(carouselImage).first().height());
    
                $(window).resize(function(){
                    
                    delay(function(){
                        videos.height($(carouselWrapper).find(carouselImage).first().height());
                    }, 100);
                    
                });
            }

        }
    }

    return {
        init: function( $isModuleOnPage ) {
            if ( $isModuleOnPage.length !== 0 ) {
                bindEvents();
            }
        }
    };
});
// jshint maxstatements: false, newcap: false

define('holidayCottagesSecondaryNav',['jquery'],
  function($) {

    "use strict";

    var moreLinkWidth = $('#more-link-hol').width(),
        $moreLink = $('#more-link-hol').detach(),
        $headerNavUL = $('#nt-holiday-secondary-nav ul'),
        $mainNavigationContainer = $('.nt-holiday-secondary-nav-container'),
        headerNavLiWidth = [],
        currentMoreLinkIndex = $headerNavUL.find('li').length,
        $launchMainNavigationElement,
        $menuWrapper = $('.nt-holiday-secondary-nav-wrapper'),
        mobileBreak = 642;

    var hideMoreLink = function(){
        $moreLink.css('display','none');
    };

    var showMoreLink = function(){
        $moreLink.css('display','inline-block');
    };

    var calculateIndividualLiWidth = function() {
        headerNavLiWidth = [];
        $headerNavUL.find('li').each(function(){
            if($(this).attr('id') !== 'more-link'){
                headerNavLiWidth.push($(this).width());
            }
        });
    };

    var calculateLastVisibleLiIndex = function(headerWidth){
        var i, len = headerNavLiWidth.length;

        var sum = 0;
        for (i = 0; i < len; i++) {
            sum += headerNavLiWidth[i];
            if (headerWidth < sum) {
                break;
            }
        }
        return i;
    };

    var hideAllLinkAfterMore = function(position) {
        var i = position;
        for (i; i <= headerNavLiWidth.length; i++) {
            $headerNavUL.find('li:eq('+i+')').hide();
        }
    };

    var showAllLinkBeforeMore = function(position) {
        var i = 0;
        for (i; i < position; i++) {
            $headerNavUL.find('li:eq('+i+')').show();
        }
    };

    var injectMoreLink = function(){
        var headNavWidth = $headerNavUL.width(),
            visibleLiIndex = calculateLastVisibleLiIndex(headNavWidth);

        if (visibleLiIndex === headerNavLiWidth.length){
            $headerNavUL.find('li').show();
            hideMoreLink();
        } else if (visibleLiIndex <= currentMoreLinkIndex){
            showMoreLink();
            $headerNavUL.find('li:eq('+(visibleLiIndex-2)+')').after($moreLink);
            hideAllLinkAfterMore(visibleLiIndex);
        } else if (visibleLiIndex > currentMoreLinkIndex){
            showMoreLink();
            $headerNavUL.find('li:eq('+(visibleLiIndex-1)+')').after($moreLink);
            showAllLinkBeforeMore(visibleLiIndex);
        }
        currentMoreLinkIndex = visibleLiIndex;
    };

    var showHideMoreLink = function() {
        $(window).on('resize',(function fn() {
            calculateIndividualLiWidth();
            injectMoreLink();
            return fn;
        })());
    };

    var maintainActiveFocus = function($parent) {
        var checkFocus = true;
        $(document).on("focusin", function(e) {
            if(checkFocus){
                //parent IS active
                //parent NOT focused
                //next focus element NOT within parent
                if ( $parent.hasClass('active') && !$parent.is(':focus') && !$parent.find(e.target).length ){
                    e.preventDefault();
                    checkFocus = false;
                    $parent.focus();
                    checkFocus = true;
                }
            }
        });
    };

    var primeMainElements = function() {
        $mainNavigationContainer.attr("tabindex","-1");
    };

    function showMainMenu(e){
        e.preventDefault();
        $launchMainNavigationElement = $(e.target);
        $mainNavigationContainer.fadeIn(function() {
            $mainNavigationContainer.addClass("active");
            $mainNavigationContainer.focus();
        });
    }

    function hideMainMenu(e){
        e.preventDefault();
        $mainNavigationContainer.removeClass("active");
        $mainNavigationContainer.fadeOut(function() {
            if ($launchMainNavigationElement){
                $launchMainNavigationElement.focus();
            } else {
                $('.nt-skip-to-nav a').focus();
            }
        });
    }

    var bindEvents = function() {
        showHideMoreLink();
        primeMainElements();
        maintainActiveFocus($mainNavigationContainer);

        $moreLink.on('click', showMainMenu);
        $mainNavigationContainer.find(' .nt-back-to-top a').on('click', function(e){
            hideMainMenu(e);
        });

        //Esc key closes nav or search
        $(window).on("keydown", function(e) {
            var ESC_KEY = 27;
            if ($mainNavigationContainer.hasClass('active') && e.keyCode == ESC_KEY) {
                hideMainMenu(e);
            }
        });
    };

    var setPrimaryNavActiveState = function() {
        setTimeout(function() {
          var $colour = $('.nt-holiday-secondary-nav-wrapper').css("background-color");
          $("#nt-masthead-nav .nt-primary-item.active").css({
              "background-color": $colour,
              "color": "#ffffff"
          });
        }, 1500); // Wait for CSS transition
    };

    return {
      init: function($isModuleOnPage) {
        if ($isModuleOnPage.length !== 0) {
          setPrimaryNavActiveState();
          bindEvents();
        }
      }
    };
  }
);

/*jshint maxparams:false, maxstatements: false */
//Define jquery despite being an external script
//Must be included on the page before main.js
define('jquery', [], function() {
    return jQuery;
});

require([
    'jquery',
    'foundation',
    'breakpoints',
    'checkViewport',
    'jsLink',
    'jsSocialLink',
    'secondaryMenu',
    'gotoSmooth',
    'stick',
    'stickItem',
    'vhOpacity',
    'print',
    'toggle',
    'modal',
    'tabs',
    'equalHeight',
    'volunteerStoryItem',
    'carousel',
    'ntAnimation',
    'ntVideo',
    'splash',
    'moreInsert',
    'animFrame',
    "setParentHeight",
    'imageLoadListener',
    'iframeEvents',
    'menuNav',
    'fixWebLinkHeight',
    'backButton',
    'search',
    'site-search',
    'ntResrc',
    'footer',
    'searchRefine',
    'backToTop',
    'walkingTrailsMap',
    'mediaCards',
    'NtPromoCodes',
    'carouselHome',
    'carouselHolidayProperty',
    'holidayCottagesSecondaryNav'
], function (
    $,
    Foundation,
    breakpoints,
    checkViewport,
    jsLink,
    jsSocialLink,
    secondaryMenu,
    gotoSmooth,
    stick,
    stickItem,
    vhOpacity,
    print,
    toggle,
    modal,
    tabs,
    equalHeight,
    volunteerStoryItem,
    carousel,
    ntAnimation,
    ntVideo,
    slash,
    moreInsert,
    animFrame,
    setParentHeight,
    imageLoadListener,
    iframeEvents,
    menuNav,
    fixWebLinkHeight,
    backButton,
    search,
    siteSearch,
    ntResrc,
    footer,
    searchRefine,
    backToTop,
    walkingTrailsMap,
    mediaCards,
    NtPromoCodes,
    carouselHome,
    carouselHolidayProperty,
    holidayCottagesSecondaryNav
) {
    // Start Foundation
    $(document).foundation();

    // JS Link
    jsLink.init( $('.nt-js-link') );

    //JS Social Link
    jsSocialLink.init( $('.nt-js-social-link') );

    // Secondary Menu
    secondaryMenu.init( $('.nt-js-secondary-menu') );

    // Goto Smooth
    gotoSmooth.init( $('.nt-js-goto-smooth') );

    // Stick
    stick.init( $('.nt-js-stick') );

    // Stick Item
    stickItem.init( $('.nt-js-stick-item') );

    // VH Opacity
    vhOpacity.init( $('.nt-js-vh-opacity') );

    // Print
    print.init( $('.nt-js-print') );

    // Toggle
    toggle.init( $('.nt-js-toggle') );

    // Modal
    modal.init( $('.nt-js-launch-modal') );

    // Tabs
    tabs.init( $('.nt-tabs, .nt-tabs-inline') );

    // Equal Height Modules
    equalHeight.init( $('.nt-js-equal-height') );

    // Volunteer Story Item
    volunteerStoryItem.init( $('.nt-volunteer') );

    // Carousel
    carousel.init( $('.nt-carousel') );

    // NT Animation
    ntAnimation.init($('[class^="nt-anim"]'));

    // NT Video
    ntVideo.init( $('.nt-js-launch-video') );

    // More Insert (timeline ajax content)
    moreInsert.init( $('.nt-js-more-button') );

    // Set parent height (for footer)
    setParentHeight.init($('.nt-js-set-parent-height'));

    // Trigger iframe custom method calls
    iframeEvents.init($('.nt-js-launch-iframe'));

    //General Search
    search.init($('.nt-search-container'));

    //Site Search
    siteSearch.init($('#cse-search-input-box-id'));

    //ReSRC image integration
    ntResrc.init();

    //Search refine checkboxes
    searchRefine.init($('.search-modal input'));

    //Back to top button
    backToTop.init($('.nt-main-back-to-top'));

    //Pass promocodes on
    NtPromoCodes.init($('.nt-membership-table'));

    // Main Navigation
    // Run globally
    menuNav.init();

    // Fix Web Link Height
    // Run globally
    fixWebLinkHeight.init();

    //Back Button Functionality
    //Run globally
    backButton.init();

    // Walking Trails JS Init
    walkingTrailsMap.init( $('.nt-walking-trails-map') );

    // Home page carousel
    carouselHome.init( $('.nt-js-sequence-item') );

    // Holiday property carousel
    carouselHolidayProperty.init( $('.nt-hol-property-images__images') );

    // Holiday Cottages Nav
    holidayCottagesSecondaryNav.init( $('.nt-holiday-secondary-nav') );


});

define("develop/js/main-ie8.js", function(){});

}());
//# sourceMappingURL=main-ie8.js.map