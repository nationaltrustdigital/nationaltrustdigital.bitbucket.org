/*
	NT Template JS is for templates only. It should not be included in any CMS integration

*/
(function($, window, undefined){
	"use strict";

	/**
	 * @method getURLParameter
	 * @description Reads passed URL Parameter
	 * @param {string} url parameter
	 * @returns {string} value of parameter
	 */
	//http://stackoverflow.com/questions/11582512/how-to-get-url-parameters-with-javascript/11582513#11582513
	function getURLParameter(name) {
		return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
	}


	/**
	 * @method updateTemplatePalette
	 * @description Set set-palette className of HTML element
	 * @param {number} paletteId
	 */
	function updateTemplatePalette(paletteId){

		var secondTierTemplateList = '.nt-feature-template, .nt-list-template, .nt-trails-template, .nt-recipe-template, .nt-project-template, .nt-profile-template, .nt-on-the-fly-template, .nt-event-detail-template .nt-longform-template',
		paletteFileName,
		isDev = $("link[href='../../assets/css/all.css']").length ? true : false;

		//remove nt-set-palette-x
		$('html[class*="nt-set-palette-"]').removeClass(function(index, css) {
			return (css.match(/\bnt-set-palette-\S+/g) || []).join(' ');
		});

		if($('.nt-main-wrapper').is(secondTierTemplateList)){
			paletteFileName = "palette-second-tier-" + paletteId;
			//$('html').addClass('nt-set-palette nt-set-palette-second-tier-'+paletteId);
		}else if($('.nt-main-wrapper').hasClass('nt-search-template')){
			paletteFileName = "palette-search-tier";
			//$('html').addClass('nt-set-palette nt-set-palette-search-tier');
		}else if($('.nt-main-wrapper').hasClass('nt-property-sub-page-opening-times')){
			paletteFileName = "palette-calendar-tier";
			//$('html').addClass('nt-set-palette nt-set-palette-calendar-tier');
		}else{
			paletteFileName = "palette-" + paletteId;
			//$('html').addClass('nt-set-palette nt-set-palette-'+paletteId);
		}
		addPaletteCSSLink(paletteFileName, (true === isDev) ? "all" : "all.min", isDev);
		addPaletteCSSLink(paletteFileName, "all-ie9", isDev);
		addPaletteCSSLink(paletteFileName + "-ie8", "all-ie8", isDev);

		if($('.nt-main-wrapper').hasClass('nt-property-sub-page-template')){
			$('html').addClass('nt-set-palette-sub');
		}
	}

	/**
	 * @method addPaletteCSSLink
	 * @description Injects palette CSS links dynamically onto the template(s)
	 * @param {string} palette file name
	 * @param {string} element after which the palette css link needs to be placed
	 * @param {boolean} true if dev environment, false otherwise
	 */
	function addPaletteCSSLink (paletteFileName, insertNextTo, isDev) {
		var css_link = $("<link>", {
		    rel: "stylesheet",
		    type: "text/css",
		    href: "../../assets/css/" + paletteFileName + ((true === isDev) ? ".css" : ".min.css")
		});
		css_link.insertAfter("link[href$='" + insertNextTo + ".css']");
	}

	/**
	 * @method devGridActive
	 * @description Toggles whether Grid guides are visable
	 * @description relates to dev01-grid
	 */
	function devGridActive(){
		var showGrid = function(){
			$('.dev-grid').addClass('active');
		};
		var hideGrid = function(){
			$('.dev-grid').removeClass('active');
		};

		$('.dev-grid-activate').on('click', showGrid);
		$('.dev-guide').on('click', hideGrid);
	}

	/**
	 * @method  selectVolunteerModuleItem
	 * @description Selects the first item of volunteer module
	 */
	function selectVolunteerModuleItem(){
		var selectedClass = "volunteer-module-selected";
		$('.nt-volunteer-stories ul.nt-volunteers li.nt-volunteer:first-child').addClass(selectedClass);
	}


	/**
	 * @method  populateJumpTo
	 * @description Updates the links in the jump to navigation to match DOM
	 */
	function populateJumpTo(){
		var $parent = $('.nt-jump-to:first'),
			$headingList = $('.nt-inline-heading'),
			$template = $parent.find('li:first').clone();

		if(!$parent.length){
			return;
		}

		$parent.find('ul').html("");

		$headingList.each( function(){
			var id = $(this).attr('id'),
				label = $(this).find('h2').text(),
				$output;

			if(typeof id !== "undefined"){
				$output = $template.clone();
				$output.find('a').attr('href', '#'+id);
				$output.find('.text').text(label);

				$parent.find('ul').append($output);
			}
		});
	}

	function init(){
		var paletteId = parseInt(getURLParameter('palette'),10);
		if(paletteId){
			updateTemplatePalette(paletteId);
		}

		devGridActive();

		selectVolunteerModuleItem();

		populateJumpTo();
	}

	init();

})(jQuery, window);
